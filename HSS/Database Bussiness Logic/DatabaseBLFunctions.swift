//
//  DatabaseBLFunctions.swift
//  Tazligen
//
//  Created by Rajni on 03/11/16.
//  Copyright © 2016 Arc Coders. All rights reserved.
//

import UIKit
import CoreData

class DatabaseBLFunctions: NSObject {
    
    //MARK:-  SAVE DATA FUNCTIONS
    
    //--Save Countries
    class func saveCountries(array: NSArray) {
        
        print(array)
        
        var arrCountries = [MyCountry]()
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("SelectedCountry",
                                                        inManagedObjectContext:managedContext)
        //3
        for item in array{
            if let country = item as? NSMutableDictionary{
                
                let entityCountry = NSManagedObject(entity: entity!,
                                                    insertIntoManagedObjectContext: managedContext) as! MyCountry
                if let countryID = country.objectForKey("CountryID"){
                    entityCountry.setValue(countryID, forKey: "countryID")
                }
                if let countryName = country.objectForKey("CountryName"){
                    entityCountry.setValue(countryName, forKey: "countryName")
                }
                if let isoAlpha2 = country.objectForKey("ISOAlpha2"){
                    entityCountry.setValue(isoAlpha2, forKey: "isoAlpha2")
                }
                if let currencyCode = country.objectForKey("CurrencyCode"){
                    entityCountry.setValue(currencyCode, forKey: "currencyCode")
                }
                if let currrencySymbol = country.objectForKey("CurrrencySymbol"){
                    entityCountry.setValue(currrencySymbol, forKey: "currencySymbol")
                }
                if let CountryFlag = country.objectForKey("CountryFlag"){
                    entityCountry.setValue(CountryFlag, forKey: "countryFlag")
                }
                if let CallingCode = country.objectForKey("CallingCode"){
                    entityCountry.setValue(CallingCode, forKey: "callingCode")
                }
                if let North = country.objectForKey("North"){
                    entityCountry.setValue(North, forKey: "north")
                }
                if let South = country.objectForKey("South"){
                    entityCountry.setValue(South, forKey: "south")
                }
                if let East = country.objectForKey("East"){
                    entityCountry.setValue(East, forKey: "east")
                }
                if let West = country.objectForKey("West"){
                    entityCountry.setValue(West, forKey: "west")
                }
                
                arrCountries.append(entityCountry)
                //4
                do {
                    try managedContext.save()
                    //5
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
            
        }
        
        
        
    }
    
    
    
    //- Save property type
    class func savePropertyType(array: NSArray) {
        
        print(array)
        
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("PropertyType",
                                                        inManagedObjectContext:managedContext)
        //3
        for item in array{
            if let property = item as? NSMutableDictionary{
                
                let entityProperty = NSManagedObject(entity: entity!,
                                                    insertIntoManagedObjectContext: managedContext) as! PropertyType
                if let id = property.objectForKey("id"){
                    entityProperty.setValue(id, forKey: "id")
                }
                if let name = property.objectForKey("name"){
                    entityProperty.setValue(name, forKey: "name")
                }
                
                //4
                do {
                    try managedContext.save()
                    //5
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
            
        }
        
    }
    
    //- Save accommodation type
    class func saveAccommodationType(array: NSArray) {
        
        print(array)
        
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as? AppDelegate
        let managedContext = appDelegate!.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("AccommodationType",
                                                        inManagedObjectContext:managedContext)
        //3
        for item in array{
            if let property = item as? NSMutableDictionary{
                
                let entityProperty = NSManagedObject(entity: entity!,
                                                     insertIntoManagedObjectContext: managedContext) as! AccommodationType
                if let id = property.objectForKey("id"){
                    entityProperty.setValue(id, forKey: "id")
                }
                if let name = property.objectForKey("name"){
                    entityProperty.setValue(name, forKey: "name")
                }
                
                //4
                do {
                    try managedContext.save()
                    //5
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
            
        }
        
    }
    
    //- Save room type
    class func saveRoomType(array: NSArray) {
        
        print(array)
        
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as? AppDelegate
        let managedContext = appDelegate!.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("RoomType",
                                                        inManagedObjectContext:managedContext)
        //3
        for item in array{
            if let property = item as? NSMutableDictionary{
                
                let entityProperty = NSManagedObject(entity: entity!,
                                                     insertIntoManagedObjectContext: managedContext) as! RoomType
                if let id = property.objectForKey("id"){
                    entityProperty.setValue(id, forKey: "id")
                }
                if let name = property.objectForKey("name"){
                    entityProperty.setValue(name, forKey: "name")
                }
                
                //4
                do {
                    try managedContext.save()
                    //5
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
            
        }
        
    }
    
    //save user registration data
    class func saveUserRegistrationDataSIGNUP(UserData: Registration) {
        
        print(UserData)
        
        //check for previous enteries
        let arr = self.fetchUserRegistrationData()
        if arr.count > 0 {
            self.deleteAllData("UserRegistration")
        }
        
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //2
        
            let entity =  NSEntityDescription.entityForName("UserRegistration",
                                                            inManagedObjectContext:managedContext)
            
            let user = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext) as! UserRegistration
            
            if let applicationId = UserData.applicationId {
                user.setValue(applicationId, forKey: "applicationId")
            }
            if let contactName = UserData.contactName {
                user.setValue(contactName, forKey: "contactName")
            }
            if let countryCode = UserData.countryCode {
                user.setValue(countryCode, forKey: "countryCode")
            }
            if let dateOfBirth = UserData.DOB {
                user.setValue(dateOfBirth, forKey: "dateOfBirth")
            }
            if let deviceType = UserData.deviceType {
                user.setValue(deviceType, forKey: "deviceType")
            }
            if let email = UserData.email {
                user.setValue(email, forKey: "email")
            }
            if let firstName = UserData.firstName {
                user.setValue(firstName, forKey: "firstName")
            }
            if let gender = UserData.gender {
                user.setValue(gender, forKey: "gender")
            }
            if let language = UserData.language {
                user.setValue(language, forKey: "language")
            }
            if let lastName = UserData.lastName {
                user.setValue(lastName, forKey: "lastName")
            }
            if let locationLatitude = UserData.locationLatitude {
                user.setValue(locationLatitude, forKey: "locationLatitude")
            }
            if let locationLongitude = UserData.locationLongitude {
                user.setValue(locationLongitude, forKey: "locationLongitude")
            }
            if let locationName = UserData.locationName {
                user.setValue(locationName, forKey: "locationName")
            }
            if let mobileNumber = UserData.mobileNumber {
                user.setValue(mobileNumber, forKey: "mobileNumber")
            }
            if let nationality = UserData.nationality {
                user.setValue(nationality, forKey: "nationality")
            }
            if let password = UserData.password {
                user.setValue(password, forKey: "password")
            }
            if let userProfileImage = UserData.userProfileImage {
                user.setValue(userProfileImage, forKey: "userProfileImage")
            }
           if let photoIdentity = UserData.photoIdentity {
                user.setValue(photoIdentity, forKey: "photoIdentity")
            }
          if let registerVia = UserData.registerVia {
                user.setValue(registerVia, forKey: "registerVia")
            }
            if let relationshipStatus = UserData.relationshipStatus {
                user.setValue(relationshipStatus, forKey: "relationshipStatus")
            }
            if let socialMediaId = UserData.socialMediaId {
                user.setValue(socialMediaId, forKey: "socialMediaId")
            }
            if let userId = UserData.userId {
                user.setValue(userId, forKey: "userId")
            }
            if let userType = UserData.userType {
                user.setValue(userType, forKey: "userType")
            }
           if let authenticateToken = UserData.authenticateToken {
                user.setValue(authenticateToken, forKey: "authenticateToken")
           }
        
            //4
            do {
                try managedContext.save()
                //5
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
    }
    
    
    //save service provider registration data
    class func saveUserRegistrationDataSIGNUPServiceProvider(UserData: ServiceProviderRegistration) {
        
        print(UserData)
        
        //check for previous enteries
        let arr = self.fetchServiceProviderUserRegistrationData()
        if arr.count > 0 {
            self.deleteAllData("ServiceProviderRegistrationDB")
        }
        
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //2
        
        let entity =  NSEntityDescription.entityForName("ServiceProviderRegistrationDB",
                                                        inManagedObjectContext:managedContext)
        
        let user = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext) as! ServiceProviderRegistrationDB
        
        if let userType = UserData.userType {
            user.setValue(userType, forKey: "userType")
        }
        if let userId = UserData.userId {
            user.setValue(userId, forKey: "userId")
        }
        if let registerVia = UserData.registerVia {
            user.setValue(registerVia, forKey: "registerVia")
        }
        if let password = UserData.password {
            user.setValue(password, forKey: "password")
        }
        if let nationality = UserData.nationality {
            user.setValue(nationality, forKey: "nationality")
        }
        if let mobileNumber = UserData.mobileNo {
            user.setValue(mobileNumber, forKey: "mobileNumber")
        }
        if let serviceProviderLongitude = UserData.serviceProviderLongitude {
            user.setValue(serviceProviderLongitude, forKey: "serviceProviderLongitude")
        }
        if let serviceProviderLatitude = UserData.serviceProviderLatitude {
            user.setValue(serviceProviderLatitude, forKey: "serviceProviderLatitude")
        }
        if let lastName = UserData.lastName {
            user.setValue(lastName, forKey: "lastName")
        }
        if let language = UserData.language {
            user.setValue(language, forKey: "language")
        }
        if let gender = UserData.gender {
            user.setValue(gender, forKey: "gender")
        }
        if let firstName = UserData.firstName {
            user.setValue(firstName, forKey: "firstName")
        }
        if let email = UserData.email {
            user.setValue(email, forKey: "email")
        }
        if let deviceType = UserData.deviceType {
            user.setValue(deviceType, forKey: "deviceType")
        }
        if let dateOfBirth = UserData.DOB {
            user.setValue(dateOfBirth, forKey: "dateOfBirth")
        }
        if let countryCode = UserData.countryCode {
            user.setValue(countryCode, forKey: "countryCode")
        }
        if let authenticateToken = UserData.authenticateToken {
            user.setValue(authenticateToken, forKey: "authenticateToken")
        }
        if let applicationId = UserData.applicationId {
            user.setValue(applicationId, forKey: "applicationId")
        }
        if let telephoneNo = UserData.telephoneNo {
            user.setValue(telephoneNo, forKey: "telephoneNo")
        }
        if let address = UserData.address {
            user.setValue(address, forKey: "address")
        }
        if let postCode = UserData.postCode {
            user.setValue(postCode, forKey: "postCode")
        }
        if let website = UserData.website {
            user.setValue(website, forKey: "website")
        }
        if let registerUnder = UserData.registerUnder {
            user.setValue(registerUnder, forKey: "registerUnder")
        }
        
        if let companyId = UserData.companyId {
            user.setValue(companyId, forKey: "companyId")
        }
        if let companyName = UserData.companyName {
            user.setValue(companyName, forKey: "companyName")
        }
        if let companyAddress = UserData.companyAddress {
            user.setValue(companyAddress, forKey: "companyAddress")
        }
        if let companyRegistrationNo = UserData.companyRegisterationNumber {
            user.setValue(companyRegistrationNo, forKey: "companyRegistrationNo")
        }
        if let serviceTypeID = UserData.serviceTypeId {
            user.setValue(serviceTypeID, forKey: "serviceTypeID")
        }
        if let serviceTypeName = UserData.serviceTypeName {
            user.setValue(serviceTypeName, forKey: "serviceTypeName")
        }
        if let driverLicenceExpiryDate = UserData.driverLicenseExpiryDate {
            user.setValue(driverLicenceExpiryDate, forKey: "driverLicenceExpiryDate")
        }
        if let conductNo = UserData.conductNo {
            user.setValue(conductNo, forKey: "conductNo")
        }
        if let photoIdentityImage = UserData.photoIdentityImage {
            user.setValue(photoIdentityImage, forKey: "photoIdentityImage")
        }
        if let socialMediaId = UserData.socialMediaId {
            user.setValue(socialMediaId, forKey: "socialMediaId")
        }
        if let issueNo = UserData.issueNo {
            user.setValue(issueNo, forKey: "issueNo")
        }
        if let birthCertificateORPassportNo = UserData.birthCeriticateOrPassportNo {
            user.setValue(birthCertificateORPassportNo, forKey: "birthCertificateORPassportNo")
        }
        if let userProfileImage = UserData.userProfileImage {
            user.setValue(userProfileImage, forKey: "userProfileImage")
        }
        if let hostUserId = UserData.hostUserId {
            user.setValue(hostUserId, forKey: "hostUserId")
        }
        
        //4
        do {
            try managedContext.save()
            //5
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    //save property details
//    class func savePropertyDetails(arrProperty: NSArray) {
//
//        print(arrProperty.count)
//
//        //check for previous enteries
//        let arr = self.fetchPropertyDetails()
//        if arr.count > 0 {
//            self.deleteAllData("PropertyDetails")
//        }
//        
//        //1
//        let appDelegate =
//            UIApplication.sharedApplication().delegate as! AppDelegate
//        let managedContext = appDelegate.managedObjectContext
//        
//        //2
//        for item in arrProperty {
//        
//        let entity =  NSEntityDescription.entityForName("PropertyDetails",
//                                                        inManagedObjectContext:managedContext)
//        
//        let propertyObj = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext) as! PropertyDetails
//        
//            if let accommodation = item.objectForKey("accommodation") as? String {
//                propertyObj.setValue(accommodation, forKey: "accommodation")
//            }
//            if let additionalMenities = item.objectForKey("additionalMenities") as? String {
//                propertyObj.setValue(additionalMenities, forKey: "additionalMenties")
//            }
//            if let apartment = item.objectForKey("apartment") as? String {
//                propertyObj.setValue(apartment, forKey: "apartment")
//            }
//            if let bathRoom = item.objectForKey("bathRoom") as? String {
//                propertyObj.setValue(bathRoom, forKey: "bathRoom")
//            }
//            if let bedRoom = item.objectForKey("bedRoom") as? String {
//                propertyObj.setValue(bedRoom, forKey: "bedRoom")
//            }
//            if let beds = item.objectForKey("beds") as? String {
//                propertyObj.setValue(beds, forKey: "beds")
//            }
//            if let city = item.objectForKey("city") as? String {
//                propertyObj.setValue(city, forKey: "city")
//            }
//            if let commomAmenities = item.objectForKey("commomAmenities") as? String {
//                propertyObj.setValue(commomAmenities, forKey: "commonAmenties")
//            }
//            if let country = item.objectForKey("country") as? String {
//                propertyObj.setValue(country, forKey: "country")
//            }
//            if let currency = item.objectForKey("currency") as? String {
//                propertyObj.setValue(currency, forKey: "currency")
//            }
//            if let emergencyExit = item.objectForKey("emergencyExit")  {
//                propertyObj.setValue(emergencyExit as? String, forKey: "emergencyExit")
//            }
//            if let fireAlarm = item.objectForKey("fireAlarm")  {
//                propertyObj.setValue(fireAlarm as? String, forKey: "fireAlarm")
//            }
//            if let fireExtingusher = item.objectForKey("fireExtingusher")  {
//                propertyObj.setValue(fireExtingusher as? String, forKey: "fireExtinguisher")
//            }
//            if let gasShutoffValve = item.objectForKey("gasShutoffValve")  {
//                propertyObj.setValue(gasShutoffValve as? String, forKey: "gasShutoffValve")
//            }
//            if let homeSafety = item.objectForKey("homeSafety") as? String {
//                propertyObj.setValue(homeSafety, forKey: "homeSafety")
//            }
//            if let id = item.objectForKey("id") as? String {
//                propertyObj.setValue(id, forKey: "id")
//            }
//            if let profileName = item.objectForKey("profileName") as? String {
//                propertyObj.setValue(profileName, forKey: "profileName")
//            }
//            if let propertyLatitude = item.objectForKey("propertyLatitude") as? String {
//                propertyObj.setValue(propertyLatitude, forKey: "propertyLatitude")
//            }
//            if let propertyLongitude = item.objectForKey("propertyLongitude") as? String {
//                propertyObj.setValue(propertyLongitude, forKey: "propertyLongitude")
//            }
//            if let propertyType = item.objectForKey("propertyType") as? String {
//                propertyObj.setValue(propertyType, forKey: "propertyType")
//            }
//            if let roomType = item.objectForKey("roomType") as? String {
//                propertyObj.setValue(roomType, forKey: "roomType")
//            }
//            if let setPrice = item.objectForKey("setPrice") as? String {
//                propertyObj.setValue(setPrice, forKey: "setPrice")
//            }
//            if let specialFeature = item.objectForKey("specialFeature") as? String {
//                propertyObj.setValue(specialFeature, forKey: "specialFeature")
//            }
//            if let state = item.objectForKey("state") as? String {
//                propertyObj.setValue(state, forKey: "state")
//            }
//            if let streetAddress = item.objectForKey("streetAddress") as? String {
//                propertyObj.setValue(streetAddress, forKey: "streetAddress")
//            }
//            if let summary = item.objectForKey("summary") as? String {
//                propertyObj.setValue(summary, forKey: "summary")
//            }
//            if let typeOfBooking = item.objectForKey("typeOfBooking") as? String {
//                propertyObj.setValue(typeOfBooking, forKey: "typeOfBooking")
//            }
//            if let userId = item.objectForKey("userId") as? String {
//                propertyObj.setValue(userId, forKey: "userId")
//            }
//            if let zipCode = item.objectForKey("zipCode") as? String {
//                propertyObj.setValue(zipCode, forKey: "zipCode")
//            }
//            
//            //to save images comma separated by name
//            if let arr = item.objectForKey("propertyImages") as? NSArray {
//                print(arr.count)
//                
//                for img in arr {
//                    if let imgName = img.objectForKey("propertyImage") as? String {
//                        propertyObj.setValue("\(imgName) ,", forKey: "images")
//                        
//                    }
//                    
//                }
//            }
//            
//        //4
//        do {
//            try managedContext.save()
//            //5
//        } catch let error as NSError  {
//            print("Could not save \(error), \(error.userInfo)")
//            }}
//    }
    
    
    
    
    
    
    //MARK:- FETCH DATA FUNCTIONS
    
    
    //fetch countries
    class func fetchCountries() -> [MyCountry]{
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("SelectedCountry", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var arrCountries = [MyCountry]()
        do {
            let result = try managedContext.executeFetchRequest(fetchRequest) as! [MyCountry]
            arrCountries = result
            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return arrCountries
    }
    
    
    //fetch property types
    class func fetchPropertyType() -> [PropertyType]{
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("PropertyType", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var arrPropertyType = [PropertyType]()
        do {
            let result = try managedContext.executeFetchRequest(fetchRequest) as! [PropertyType]
            arrPropertyType = result
            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return arrPropertyType
    }
    
    //fetch accommodation types
    class func fetchAccommodationTypes() -> [AccommodationType]{
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("AccommodationType", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var arrAccommodationType = [AccommodationType]()
        do {
            let result = try managedContext.executeFetchRequest(fetchRequest) as! [AccommodationType]
            arrAccommodationType = result
            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return arrAccommodationType
    }
    
    
    //fetch Room type
    class func fetchRoomType() -> [RoomType]{
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("RoomType", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var arrRoomType = [RoomType]()
        do {
            let result = try managedContext.executeFetchRequest(fetchRequest) as! [RoomType]
            arrRoomType = result
            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return arrRoomType
    }
    
    
    //fetch user registration data
    class func fetchUserRegistrationData() -> [UserRegistration]{
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("UserRegistration", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var arrUserData = [UserRegistration]()
        do {
            let result = try managedContext.executeFetchRequest(fetchRequest) as! [UserRegistration]
            arrUserData = result
            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return arrUserData
    }
    
    //fetch user registration data
    class func fetchServiceProviderUserRegistrationData() -> [ServiceProviderRegistrationDB]{
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("ServiceProviderRegistrationDB", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var arrUserData = [ServiceProviderRegistrationDB]()
        do {
            let result = try managedContext.executeFetchRequest(fetchRequest) as! [ServiceProviderRegistrationDB]
            arrUserData = result
            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return arrUserData
    }
    
    
    //fetch property details
//    class func fetchPropertyDetails() -> [PropertyDetails]{
//        
//        let appDelegate =
//            UIApplication.sharedApplication().delegate as! AppDelegate
//        let managedContext = appDelegate.managedObjectContext
//        // Initialize Fetch Request
//        let fetchRequest = NSFetchRequest()
//        
//        // Create Entity Description
//        let entityDescription = NSEntityDescription.entityForName("PropertyDetails", inManagedObjectContext: managedContext)
//        
//        // Configure Fetch Request
//        fetchRequest.entity = entityDescription
//        var arrPropertyList = [PropertyDetails]()
//        do {
//            let result = try managedContext.executeFetchRequest(fetchRequest) as! [PropertyDetails]
//            arrPropertyList = result
//            print(result)
//            
//        } catch {
//            let fetchError = error as NSError
//            print(fetchError)
//        }
//        return arrPropertyList
//    }
    
    
    
    
    //MARK:- DELETE
    class func deleteAllData(entity: String)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.deleteObject(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
