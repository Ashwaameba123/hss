//
//  MPGTextField.m
//
//  Created by Gaurav Wadhwani on 05/06/14.
//  Copyright (c) 2014 Mappgic. All rights reserved.
//

#import "MPGTextField.h"
#import "HSS-Swift.h"

@implementation MPGTextField


MyCountry *selectedCountry;

//Private declaration of UITableViewController that will present the results in a popover depending on the search query typed by the user.
UITableViewController *results;
UITableViewController *tableViewController;

int checkCount;
//Private declaration of NSArray that will hold the data supplied by the user for showing results in search popover.
NSArray *data;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textColor = [UIColor blackColor];
    self.backgroundColor = [UIColor whiteColor];
    tableViewController.tableView.tableFooterView = [UIView new];
    if (self.text.length > 0 && self.isFirstResponder) {
        //User entered some text in the textfield. Check if the delegate has implemented the required method of the protocol. Create a popover and show it around the MPGTextField.
        
        if ([self.delegate respondsToSelector:@selector(dataForPopoverInTextField:)]) {
            data = [[self delegate] dataForPopoverInTextField:self];
            [self provideSuggestions];
        }
        else{
            NSLog(@"<MPGTextField> WARNING: You have not implemented the requred methods of the MPGTextField protocol.");
        }
    }
    else{
        //No text entered in the textfield. If -textFieldShouldSelect is YES, select the first row from results using -handleExit method.tableView and set the displayText on the text field. Check if suggestions view is visible and dismiss it.
        if ([tableViewController.tableView superview] != nil) {
            [tableViewController.tableView removeFromSuperview];
        }
    }
    
   
}

//Override UITextField -resignFirstResponder method to ensure the 'exit' is handled properly.
- (BOOL)resignFirstResponder
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         [tableViewController.tableView setAlpha:0.0];
                     }
                     completion:^(BOOL finished){
                         [tableViewController.tableView removeFromSuperview];
                         tableViewController = nil;
                     }];
    [self handleExit];
    return [super resignFirstResponder];
}

//This method checks if a selection needs to be made from the suggestions box using the delegate method -textFieldShouldSelect. If a user doesn't tap any search suggestion, the textfield automatically selects the top result. If there is no result available and the delegate method is set to return YES, the textfield will wrap the entered the text in a NSDictionary and send it back to the delegate with 'CustomObject' key set to 'NEW'
- (void)handleExit
{
    [tableViewController.tableView removeFromSuperview];
    if ([[self delegate] respondsToSelector:@selector(textFieldShouldSelect:)]) {
        if ([[self delegate] textFieldShouldSelect:self]) {
                if ([[self delegate] respondsToSelector:@selector(textField:didEndEditingWithSelection:)]) {
                    if ([self.text isEqualToString:@""])
                    {
                        NSDictionary *dict = @{@"Object": @""};
                        [[self delegate] textField:self didEndEditingWithSelection:dict];
                    }
                    else{
                        if (selectedCountry!= nil){
                            NSDictionary *dict = @{@"Object": selectedCountry};
                            [[self delegate] textField:self didEndEditingWithSelection:dict];
                        }
                        else{
                            NSDictionary *dict = @{@"Object": @""};
                            [[self delegate] textField:self didEndEditingWithSelection:dict];
                        }
                    }
         
                }
        }
    }
}


#pragma mark UITableView DataSource & Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = [[self applyFilterWithSearchQuery:self.text] count];
    checkCount = count;
    if (count == 0) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             [tableViewController.tableView setAlpha:0.0];
                         }
                         completion:^(BOOL finished){
                             [tableViewController.tableView removeFromSuperview];
                             tableViewController = nil;
                         }];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MPGResultsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    MyCountry *dataForRowAtIndexPath = [[self applyFilterWithSearchQuery:self.text] objectAtIndex:indexPath.row];
    [cell setBackgroundColor:[UIColor clearColor]];
    [[cell textLabel] setText:dataForRowAtIndexPath.countryName];

    return cell;
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        [self changeFrame];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCountry = [[self applyFilterWithSearchQuery:self.text] objectAtIndex:indexPath.row];
    self.text = selectedCountry.countryName;
    [self resignFirstResponder];
}

#pragma mark Filter Method

- (NSArray *)applyFilterWithSearchQuery:(NSString *)filter
{
    NSString *capitalisedSentence = @"";
    if (filter && filter.length > 0) {
        // Yes, it does.
        
        capitalisedSentence = [filter stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                  withString:[[filter substringToIndex:1] capitalizedString]];
    }

    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:data];
    [array filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(MyCountry *evaluatedObject, NSDictionary *bindings) {
        return [evaluatedObject.countryName hasPrefix:capitalisedSentence];
    }]];
    return array;
}

#pragma mark Popover Method(s)

- (void)provideSuggestions
{
    //Providing suggestions
    if (tableViewController.tableView.superview == nil && [[self applyFilterWithSearchQuery:self.text] count] > 0) {
        //Add a tap gesture recogniser to dismiss the suggestions view when the user taps outside the suggestions view
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [tapRecognizer setNumberOfTapsRequired:1];
        [tapRecognizer setCancelsTouchesInView:NO];
        [tapRecognizer setDelegate:self];
        [self.superview addGestureRecognizer:tapRecognizer];
        
        tableViewController = [[UITableViewController alloc] init];
        [tableViewController.tableView setDelegate:self];
        [tableViewController.tableView setDataSource:self];
        if (self.backgroundColor == nil) {
            //Background color has not been set by the user. Use default color instead.
            [tableViewController.tableView setBackgroundColor:[UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0]];
        }
        else{
            [tableViewController.tableView setBackgroundColor:self.backgroundColor];
        }
        
        [tableViewController.tableView setSeparatorColor:self.seperatorColor];
        [self changeFrame];
        [[self superview] addSubview:tableViewController.tableView];
        tableViewController.tableView.alpha = 0.0;
        [UIView animateWithDuration:0.3
                         animations:^{
                             [tableViewController.tableView setAlpha:1.0];
                         }
                         completion:^(BOOL finished){
                             
                         }];
        
        
    }
    else{
        [tableViewController.tableView reloadData];
    }
}


-(void)changeFrame{
    if (self.popoverSize.size.height == 0.0) {
        //PopoverSize frame has not been set. Use default parameters instead.
        CGRect frameForPresentation = [self frame];
        frameForPresentation.origin.y += self.frame.size.height;
        frameForPresentation.size.height = 44 * 3;
        if (checkCount<=3) {
            frameForPresentation.size.height = 44 * checkCount;
        }
        [tableViewController.tableView setFrame:frameForPresentation];
    }
    else{
        [tableViewController.tableView setFrame:self.popoverSize];
    }
}

- (void)tapped:(UIGestureRecognizer *)gesture
{
    
}


@end
