//
//  Messages_List_Table_Cell.swift
//  HSS
//
//  Created by Rajni on 21/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class Messages_List_Table_Cell: UITableViewCell {
    
    @IBOutlet weak var lbl_last_msg: UILabel!
    @IBOutlet weak var img_vw_person: UIImageView!
    @IBOutlet weak var lbl_person_name: UILabel!
}
