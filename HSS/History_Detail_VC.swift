//
//  History_Detail_VC.swift
//  HSS
//
//  Created by Rajni on 14/03/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import FTIndicator

class History_Detail_VC: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var lbl_start_date: UILabel!
    @IBOutlet weak var lbl_end_date: UILabel!
    @IBOutlet weak var lbl_accommodation: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var imgVw_user: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var btn_viewDetails: UIButton!
    
    @IBOutlet weak var vw_to: UIView!
    @IBOutlet weak var vw_from: UIView!
    @IBOutlet weak var vw_booking_details: UIView!
    
    @IBOutlet weak var lbl_details_name: UILabel!
    @IBOutlet weak var vw_user_details: UIView!
    //constraints
    
    @IBOutlet weak var cons_top_btn_viewPropertyDetails: NSLayoutConstraint!
    @IBOutlet weak var cons_height_viewPropertyDetails: NSLayoutConstraint!
    @IBOutlet weak var cons_top_cancelBooking: NSLayoutConstraint!
    @IBOutlet weak var cons_height_cancelBooking: NSLayoutConstraint!
    @IBOutlet weak var cons_top_makePayment: NSLayoutConstraint!
    @IBOutlet weak var cons_height_makePayment: NSLayoutConstraint!
    @IBOutlet weak var cons_top_reviewRequest: NSLayoutConstraint!
    @IBOutlet weak var cons_height_reviewRequest: NSLayoutConstraint!
    
    @IBOutlet weak var cons_top_refundPayment: NSLayoutConstraint!
    @IBOutlet weak var cons_height_refundPayment: NSLayoutConstraint!
    //---- buttons
    
    @IBOutlet weak var btn_property_details: UIButton!
    @IBOutlet weak var btn_cancel_booking: UIButton!
    
    @IBOutlet weak var btn_make_payment: UIButton!
    
    @IBOutlet weak var btn_review_request: UIButton!
    
    @IBOutlet weak var btn_payment_refund: UIButton!
    
    
    //MARK:- Variables
    var booking: Property_Booking!
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- View Life cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationItem.leftBarButtonItem = nil
 }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        self.initView()
        self.toFillDetailsData()
    }
    
    func initView() {
        CommonFunctions.makeRoundCornerOfView(vw_booking_details)
        CommonFunctions.makeRoundCornerOfView(vw_user_details)
        CommonFunctions.makeRoundCornerOfView(vw_from)
        CommonFunctions.makeRoundCornerOfView(vw_to)
        CommonFunctions.makeRoundCornerOfButton(btn_review_request)
        CommonFunctions.makeRoundCornerOfButton(btn_property_details)
        CommonFunctions.makeRoundCornerOfButton(btn_cancel_booking)
        CommonFunctions.makeRoundCornerOfButton(btn_make_payment)
        CommonFunctions.makeRoundCornerOfButton(btn_payment_refund)
        
        
        //make round person image view
        imgVw_user.layer.cornerRadius = imgVw_user.frame.size.width / 2
        imgVw_user.clipsToBounds = true
        imgVw_user.layer.borderWidth = 1.0
        imgVw_user.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        imgVw_user.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        imgVw_user.contentMode = .ScaleAspectFit
        
        //conditions for buttons options for USER / HOST
        if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue && booking.isApproved == "false" && booking.isCancel == "false" && booking.paymentWith == "user") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
             btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
             btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.HostApprovalPending
         }
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue && booking.isApproved == "true" && booking.isCancel == "false" && booking.paymentWith == "admin") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
             btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.PaymentWithAdminFromUserSide
        }
        
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue && booking.isApproved == "true" && booking.isCancel == "true" && booking.paymentWith == "admin") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.PaymentWithAdminToUserSide
        }
        
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue && booking.isApproved == "true" && booking.isCancel == "false" && booking.paymentWith == "user") {
            cons_top_makePayment.constant = 10
            cons_height_makePayment.constant = 44
            btn_make_payment.hidden = false
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.MessageUserToMakePayment
        }
        
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "false" && booking.paymentWith == "host") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 10
            cons_height_cancelBooking.constant = 44
            btn_cancel_booking.hidden = false
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.MessageUserToForSuccessfullBooking
        }
        
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "true" && booking.paymentWith == "host") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.BookingCancelledPaymentPending
        }
        
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "false" && booking.paymentWith == "user") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.BookingApprovedPaymentPendingFromUser
        }
        
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "false" && booking.paymentWith == "admin") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.PaymentWithAdminToHostSide
        }
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "true" && booking.paymentWith == "admin") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.PaymentWithAdminFromHost
        }
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "true" && booking.paymentWith == "host") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 10
            cons_height_refundPayment.constant = 44
            btn_payment_refund.hidden = false
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.BookingCancelledByUser
        }
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "false" && booking.isCancel == "false" && booking.paymentWith == "user") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 10
            cons_height_reviewRequest.constant = 44
            btn_review_request.hidden = false
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.HostApprovalPendingMessage
        }
        else if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && booking.isApproved == "true" && booking.isCancel == "false" && booking.paymentWith == "host") {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + CommonFunctions.constants.propertyBookedSuccess
        }
        else {
            cons_top_makePayment.constant = 0
            cons_height_makePayment.constant = 0
            btn_make_payment.hidden = true
            cons_top_cancelBooking.constant = 0
            cons_height_cancelBooking.constant = 0
            btn_cancel_booking.hidden = true
            cons_top_refundPayment.constant = 0
            cons_height_refundPayment.constant = 0
            btn_payment_refund.hidden = true
            cons_top_reviewRequest.constant = 0
            cons_height_reviewRequest.constant = 0
            btn_review_request.hidden = true
            lbl_status.text = "Status :" + " " + booking.paymentWith
        }
        
    }
    
    
    func toFillDetailsData() {
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
           lbl_details_name.text = "User Details"
        }
        else {
             lbl_details_name.text = "Host Details"
        }
        
        lbl_start_date.text = CommonFunctions.dateFromString(booking.bookingStartDate)
        lbl_end_date.text = CommonFunctions.dateFromString(booking.bookingEndDate)
        lbl_accommodation.text = "Booked Accommodation :" + " " + booking.bookedRoom
        if let price = booking.setPrice {
        lbl_price.text = "Booked Price :" + " " + price
        }
        
        //to set profile image
        let imgName = booking.userProfileImage
        print(imgName)
        
        if imgName != "" {
            let urlHostString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imgName)"
            print(urlHostString)
            let escapedAddressHost = urlHostString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            print(escapedAddressHost)
            
            imgVw_user.sd_setImageWithURL(NSURL(string: escapedAddressHost!), placeholderImage:UIImage(named: "avatar.png"))
        }
        else {
            imgVw_user.image = UIImage(named: "avatar.png")
        }
        
        lbl_name.text = booking.userName
        
     }
    
    
    //MARK:- WEB SERVICES
    func callToCancelBooking(){
        var id: String = ""
        
        id = booking.propertyId
        
        let params = [
            URLParams.BOOKED_PROPERTY_ID: id
        ]
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        WebserviceUtils.callPostRequest(URLConstants.TO_CANCEL_BOOKING, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                FTIndicator.dismissProgress()
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    func callToMakePayment(){
        var id: String = ""
        
        id = booking.propertyId
        
        let params = [
            URLParams.BOOKED_PROPERTY_ID: id
        ]
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        WebserviceUtils.callPostRequest(URLConstants.TO_BOOKING_PAYMENT, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                FTIndicator.dismissProgress()
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    
    
    //MARK:- Button Actions
    
    @IBAction func btn_View_details_click(sender: UIButton) {
        let show_Detail_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailProvider_Controller") as! DetailProvider_Controller
        show_Detail_provider_obj.showDetailsViaHistory = true
        
         if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            show_Detail_provider_obj.userID = booking.userId
        }
         else {
        show_Detail_provider_obj.userID = booking.hostId
        }
        let arr = DatabaseBLFunctions.fetchUserRegistrationData()
        if arr.count > 0 {
            show_Detail_provider_obj.userData = arr[0]
        }
        self.navigationController?.pushViewController(show_Detail_provider_obj, animated: true)
    }
    @IBAction func btn_view_property_details(sender: UIButton) {
        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
        show_Detail_obj.noti_property_id = booking.propertyId
        let arr = DatabaseBLFunctions.fetchUserRegistrationData()
        if arr.count > 0 {
            show_Detail_obj.userData = arr[0]
        }
        show_Detail_obj.isToShowViaNoti = true
        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
    }
    @IBAction func btn_cancel_booking(sender: UIButton) {
        self.callToCancelBooking()
    }
    @IBAction func btn_make_payment(sender: UIButton) {
        //show paypal
    }
    @IBAction func btn_review_request(sender: UIButton) {
        let show_Detail_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailProvider_Controller") as! DetailProvider_Controller
        show_Detail_provider_obj.isShowUserDetails = true
        show_Detail_provider_obj.isHostViaHistory = true
        show_Detail_provider_obj.userID = booking.userId
        show_Detail_provider_obj.booking = self.booking
        let arr = DatabaseBLFunctions.fetchUserRegistrationData()
        if arr.count > 0 {
            show_Detail_provider_obj.userData = arr[0]
        }
        self.navigationController?.pushViewController(show_Detail_provider_obj, animated: true)
    }
    @IBAction func btn_payment_refund_click(sender: UIButton) {
        //pending
    }
    
}