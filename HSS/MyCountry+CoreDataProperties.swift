//
//  MyCountry+CoreDataProperties.swift
//  
//
//  Created by Ameba on 03/01/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MyCountry {

    @NSManaged var callingCode: String?
    @NSManaged var countryID: NSNumber?
    @NSManaged var countryName: String?
    @NSManaged var currencyCode: String?
    @NSManaged var currencySymbol: String?
    @NSManaged var isoAlpha2: String?

}
