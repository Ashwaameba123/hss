//
//  NearBy_attractions_table_cell.swift
//  HSS
//
//  Created by Rajni on 21/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class NearBy_attractions_table_cell: UITableViewCell{
    
    @IBOutlet weak var lbl_location_name: UILabel!
    @IBOutlet weak var btn_view_on_map: UIButton!
    
}