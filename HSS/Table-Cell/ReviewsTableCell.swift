//
//  ReviewsTableCell.swift
//  HSS
//
//  Created by Rajni on 21/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import Cosmos
import UIKit

class ReviewsTableCell: UITableViewCell {
    
    @IBOutlet weak var imgVwPersonImg: UIImageView!
    @IBOutlet weak var lblPersonImg: UILabel!
    
    @IBOutlet weak var txtVwUserReview: UITextView!
    
    
    @IBOutlet weak var lbl_reviews_count: UILabel!
    
    
    @IBOutlet weak var cosmoViewTblCell: CosmosView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    
    
}