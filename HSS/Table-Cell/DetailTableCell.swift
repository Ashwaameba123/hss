//
//  DetailTableCell.swift
//  HSS
//
//  Created by Rajni on 21/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class DetailTableCell: UITableViewCell {
    
    @IBOutlet weak var imgVwPropertyHost: UIImageView!
    
    @IBOutlet weak var lblHostName: UILabel!
    
    
    @IBOutlet weak var lblRoomType: UILabel!
    @IBOutlet weak var lblNoOfBeds: UILabel!
    @IBOutlet weak var lblNoOfGuest: UILabel!
    
    @IBOutlet weak var txtVwAbout: UITextView!
    
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var btn_service_providers: UIButton!
    //property detail
    
    @IBOutlet weak var lbl_property_name: UILabel!
    @IBOutlet weak var lbl_host_email: UILabel!
    @IBOutlet weak var txtVw_location: UITextView!
    
    @IBOutlet weak var btn_details: UIButton!
    @IBOutlet weak var btn_map_view: UIButton!
    @IBOutlet weak var lbl_Currency: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_comn_amenities: UILabel!
    @IBOutlet weak var lbl_add_amenities: UILabel!
    @IBOutlet weak var lbl_special_feature: UILabel!
    @IBOutlet weak var lbl_home_safety: UILabel!
    @IBOutlet weak var lbl_fire_extinguishers: UILabel!
    @IBOutlet weak var lbl_fire_alarm: UILabel!
    @IBOutlet weak var lbl_gas_shuot_valve: UILabel!
    @IBOutlet weak var lbl_emergency_exit: UILabel!
    
    
    @IBOutlet weak var lbl_booking_type: UILabel!
    @IBOutlet weak var lbl_booking_description: UILabel!
    
    @IBOutlet weak var lbl_no_accommodation: UILabel!
   
    @IBOutlet weak var lbl_furnishes: UILabel!
    
    @IBOutlet weak var lbl_policy_description: UILabel!
    @IBOutlet weak var lbl_policy_name: UILabel!
}
