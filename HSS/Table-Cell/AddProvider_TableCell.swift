//
//  AddProvider_TableCell.swift
//  HSS
//
//  Created by Rajni on 31/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import UIKit

class AddProvider_TableCell: UITableViewCell {
    @IBOutlet weak var imgVw_ProviderImage: UIImageView!

    @IBOutlet weak var lbl_ProviderMessage: UIView!
    @IBOutlet weak var lbl_ProviderName: UILabel!
    @IBOutlet weak var btn_Check: UIButton!
    @IBOutlet weak var lbl_provider_serviceTypeName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
