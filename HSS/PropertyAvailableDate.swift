//
//  PropertyAvailableDate.swift
//  HSS
//
//  Created by Rajni on 27/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class PropertyAvailableDate: NSObject {
    
    var availableRoom: String!
    var bookedRoom: String!
    var bookingEndDate: String!
    var bookingStartDate: String!
    var bookingType: String!
    var id: String!
    var totalRoom: String!
    var userId: String!
}
