//
//  Constants.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation

class Constants {
    
    
    struct userData {
        static var arrUserData = [Registration]()
    }
    
    struct property {
        
        static var arrPropertyData = [PropertyData]()
        
    }
    struct map_property {
        static var property = PropertyDetailsModel()
    }
    
    struct ServiceProviderRegistrationData {
        static var ServiceProviderUserData = ServiceProviderRegistration()
    }
    
    //selected property
    struct selectedProperty {
        static var selectedPropertyDetails = PropertyDetailsModel()
    }
    
    
    
    class userType {
        static let user = 1
        static let host = 2
        static let serviceProvider = 3
    }
    
    class Colors {
        static let BACKGROUND_COLOUR = UIColor(red: 10/255, green: 38/255, blue: 62/255, alpha: 1)
        static let GREEN_COLOUR = UIColor(red: 39/255, green: 206/255, blue: 102/255, alpha: 1)
    }
    
}
