//
//  Enumerations.swift
//  HSS
//
//  Created by Rajni on 04/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class Enumerations {
    
    enum USER_TYPE: String {
        case user = "user"
        case host = "host"
        case serviceProvider = "serviceProvider"
        case guest = "guest"
    }
    
    
    enum REGISTER_VIA: String {
        case manual = "manual"
        case facebook = "facebook"
        case twitter = "twitter"
        case googlePlus = "googleplus"
    }
    
    
    
    
}