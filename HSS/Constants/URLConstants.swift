//
//  URLConstants.swift
//  HSS
//
//  Created by Rajni on 02/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
class URLConstants {
    
    
    
    //************* TEST SERVER ************
    //static let BASE_URL = "http://192.168.88.203:1337"
    static let BASE_URL = "http://112.196.34.42:1337"
    
    
    //**********   LIVE SERVER ***********
    //static let BASE_URL = "http://112.196.34.43:1337"
    
    
    static let USER_FORGOT = "\(BASE_URL)/signUp/forgetPassword"
    static let GET_COUNTRIES = "\(BASE_URL)/country/getCountries"
    static let USER_REGISTRATION = "\(BASE_URL)/signUp/addUser"
    static let USER_LOGIN = "\(BASE_URL)/login/loginMethod"
    static let USER_VERIFICATION = "\(BASE_URL)/signUp/getverificationStatus"
    
    
    //to add property
    static let GET_ACCOMMODATION = "\(BASE_URL)/accommodation/getAccommodation"
    static let GET_PROPERTY_TYPE = "\(BASE_URL)/propertyType/getPropertyType"
    static let GET_ROOM_TYPE = "\(BASE_URL)/roomType/getRoomType"
    static let ADD_PROPERTY = "\(BASE_URL)/property/addProperty"
    
    // to get all properties list
    static let GET_PROPERTY_LIST =  "\(BASE_URL)/property/getProperties"
    
    // to get property list by user id
    static let GET_PROPERTY_LIST_BY_USERID = "\(BASE_URL)/property/getUserProperties"
    
    // to get property list by user id for service provider
    static let GET_PROPERTY_LIST_BY_USERID_FOR_SERVICE_PROVIDER = "\(BASE_URL)/serviceProvider/getPropertyServiceProvider"
    
    //to get authenticate token for guest
    static let GET_GUEST_AUTHENTICATE_TOKEN = "\(BASE_URL)/signUp/getGuestToken"
    
    // to get reviews of property
    static let GET_PROPERTY_REVIEWS = "\(BASE_URL)/property/getPropertyReview"
    
    //to write review to property
    static let WRITE_PROPERTY_REVIEW = "\(BASE_URL)/property/addPropertyReview"
    
    //to add contact
    static let GET_CONTACT_BY_USERID = "\(BASE_URL)/userContact/getUserContactByUserId"
    static let ADD_CONTACT =  "\(BASE_URL)/userContact/addUserContact"
    static let UPDATE_CONTACT = "\(BASE_URL)/userContact/updateUserContact"
    
    
    
    //to get profile photo
   // static let GET_PROFILE_PHOTO = "\(BASE_URL)/HSSImages/documentProof/"
    static let GET_PROFILE_PHOTO = "\(BASE_URL)/HSSImages/profileImages/"
    
    //to get property image
    static let GET_PROPERTY_IMAGE = "\(BASE_URL)/HSSImages/propertyImages/"
    
    //to get ID proof image
    static let GET_ID_PROOF_IMAGE = "\(BASE_URL)/HSSImages/photoIdentityImages/"
    
    //milestone 3
    static let SERVICE_PROVIDER_REGISTRATION = "\(BASE_URL)/serviceProvider/addServiceProvider"
    static let SERVICE_PROVIDER_REGISTRATION_BY_HOST = "\(BASE_URL)/serviceProvider/addServiceProvider"
    static let GET_ALL_COMPANIES = "\(BASE_URL)/company/getAllCompany"
    static let GET_ALL_SERVICE_TYPES = "\(BASE_URL)/serviceType/getServiceType"
    static let SERVICE_PROVIDER_ADD_PHOTO_IDENTITY = "\(BASE_URL)/signUp/addPhotoIdentity"
        
    //to get service provider
    static let GET_SERVICE_PROVIDER_FOR_PARTICULAR_PROPERTY = "\(BASE_URL)/propertyServiceProvider/getPropertyServiceProviderByProperty"
    static let GET_ALL_SERVICE_PROVIDERS = "\(BASE_URL)/serviceProvider/getAllServiceProvider"
    static let ADD_SERVICE_PROVIDER_TO_PROPERTY = "\(BASE_URL)/propertyServiceProvider/addPropertyServcieProvider"
    
    
    //change password
    static let CHANGE_PASSWORD = "\(BASE_URL)/login/changePassword"
    
    
    //edit profile host / user
    static let EDIT_PROFILE = "\(BASE_URL)/login/editProfile"
    
    //edit property
    static let EDIT_PROPERTY = "\(BASE_URL)/property/updateProperty"
    
    //to delete particular property image 
    static let DELETE_PARTICULAR_PROPERTY_IMAGE = "\(BASE_URL)/property/deletePropertyImage"
    
    static let CONFIRM_VERIFICATION = "\(BASE_URL)/signUp/confirmVerification"//Will Use it at later stage
    
    //to get user details by user id
    static let GET_USER_INFO = "\(BASE_URL)/user/getUser"
    
    //to get property list by filter
    static let GET_PROPERTY_LIST_BY_FILTER = "\(BASE_URL)/property/searchProperty"
    
    //to check property booking
    static let CHECK_PROPERTY_BOOKING = "\(BASE_URL)/property/getBookedPropertyByPropertyId"
    
    //to book property
    static let TO_BOOK_PROPERTY = "\(BASE_URL)/property/propertyBooking"
    
    //to get recent chat / or start chat 
    static let GET_RECENT_CHAT = "\(BASE_URL)/signUp/getAllChats"
    
    //to send message
    static let SEND_MESSAGE = "\(BASE_URL)/signUp/startChat"
        
    //to get all chats
    static let GET_ALL_CHATS = "\(BASE_URL)/signUp/getRecentChat"
    
    //to reset property bookings by host
    static let RESET_PROPERTY_BOOKINGS = "\(BASE_URL)/property/resetBooking"
    
    //to update device token
    static let UPDATE_DEVICE_TOKEN = "\(BASE_URL)/signUp/updateApplicationByUser"
    
    //to get notifications 
    static let GET_NOTIFICATIONS = "\(BASE_URL)/property/getNotification"
    
    //to confirm property booking by host
    static let TO_CONFIRM_BOOKING_BY_HOST = "\(BASE_URL)/property/confirmPropertyBooking"
    
    //to decline property booking by host
    static let TO_DECLINE_BOOKING_BY_HOST = "\(BASE_URL)/property/declineBooking"
    
    //to get partivular property details
    static let TO_GET_PARTICULAR_PROPERTY_DETAILS = "\(BASE_URL)/property/getProperty"
    
    //to booking payment
    static let TO_BOOKING_PAYMENT = "\(BASE_URL)/property/bookingPayment"
    
    //to get all bookings for host and user
    static let TO_GET_ALL_BOOKINGS = "\(BASE_URL)/property/getMyBookings"
    
    //to cancel booking by user
    static let TO_CANCEL_BOOKING = "\(BASE_URL)/property/cancelBooking"
}

