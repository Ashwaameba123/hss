//
//  URLParams.swift
//  HSS
//
//  Created by Rajni on 02/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
class URLParams {
    
    static let USER_ID = "userId"
    //static let SERVICE_PROVIDER_ID = "serviceProviderId"
    
    static let FIRST_NAME = "firstName"
    static let LAST_NAME = "lastName"
    static let PASSWORD = "password"
    static let EMAIL = "emailId"
    static let LANGUAGE = "language"
    static let GENDER = "gender"
    static let MOBILE_NUMBER = "mobileNumber"
    static let CONTACT_NAME = "contactName"
    static let RELATIONSHIP_STATUS = "relationshipStatus"
    static let DOB = "DOB"
    static let NATIONALITY = "nationality"
    static let LOCATION_NAME = "LocationName"
    static let LOCATION_LATITUDE = "locationLatitude"
    static let LOCATION_LONGITUDE = "locationLongitude"
    static let PHOTO_IDENTITY = "photoIdentity"
    static let COUNTRY_CODE = "countryCode"
    static let USER_TYPE = "userType"
    static let REGISTER_VIA = "registerVia"
    static let SOCIAL_MEDIA_ID = "socialMediaId"
    
    static let DEVICE_TYPE = "deviceType"
    static let APPLICATION_ID = "applicationId"
    
    // add property
    static let AUTHENTICATE_TOKEN = "authenticateToken"
    static let PROPERTY_TYPE = "propertyType"
    static let ROOM_TYPE = "roomType"
    static let ACCOMMODATION_TYPE = "accommodation"
    static let BEDROOM_COUNT = "bedRoom"
    static let BEDS_COUNT = "beds"
    static let BATHROOM_COUNT = "bathRoom"
    static let COUNTRY_NAME = "country"
    static let TYPE_OF_BOOKING = "typeOfBooking"
    static let CURRENCY = "currency"
    static let SET_PRICE = "setPrice"
    static let FIRE_EXTINGUISHERS = "fireExtingusher"
    static let FIRE_ALARM = "fireAlarm"
    static let GAS_SHUT_OFF_VALVA = "gasShutoffValve"
    static let EMERGENCY_EXIT = "emergencyExit"
    static let PROPERTY_LATITUDE = "propertyLatitude"
    static let PROPERTY_LONGITUDE = "propertyLongitude"
    static let PROFILE_NAME = "profileName"
    static let SUMMARY = "summary"
    static let STREET_ADDRESS = "streetAddress"
    static let APT_SUITE_BLDG = "apartment"
    static let CITY = "city"
    static let STATE = "state"
    static let ZIPCODE = "zipCode"
    static let COMMON_AMENTIES = "commomAmenities"
    static let ADDITIONAL_AMENTIES = "additionalMenities"
    static let SPECIAL_FEATURE = "specialFeature"
    static let HOME_SAFETY = "homeSafety"
    
    static let PAGE_NUMBER = "pageNumber"
    static let PROPERTY_ID = "propertyId"
    static let CANCELLATION_POLICY = "cancellationPolicy"
    static let FURNISHING = "furnishing"
    
    
    //add contact params
    //static let CONTACT_NAME = "contactName"
    static let CONTACT_RELATIONSHIP = "contactRelationShip"
    static let CONTACT_PHONE_NO = "contactPhonenumber"
    static let USER_CONTACT_ID = "userContactId"

    static let RATING = "ratings"
    static let REVIEW_MSG = "reviewMessage"
    
    //service provider registration
    static let ADDRESS = "address"
    static let WEBSITE = "website"
    static let POST_CODE = "postCode"
    static let TELEPHONE_NUMBER = "telephoneNumber"
    static let REGISTER_UNDER = "registerUnder"
    
    static let COMPANY_ID = "companyId"
    static let COMPANY_NAME = "companyName"
    static let COMPANY_ADDRESS = "companyAddress"
    static let COMPANY_REGISTRATION_NO = "companyRegisterationNumber"
    static let SERVICE_TYPE_ID = "serviceTypeId"
    static let DRIVING_LICENCE_EXPIRY_DATE = "driverLicenseExpiryDate"
    static let CONDUCT_NO = "conductNo"
    static let ISSUE_NO = "issueNo"
    static let BIRTH_CERTIFICATE_OR_PASSPORT_NO = "birthCeriticateOrPassportNo"
    static let SERVICE_PROVIDER_LATITUDE = "serviceProviderLatitude"
    static let SERVICE_PROVIDER_LONGITUDE = "serviceProviderLongitude"
    static let USER_PROFILE_IMAGE = "userProfileImage"
    static let SERVICE_TYPE_NAME = "serviceTypeName"
    
    static let SERVICE_PROVIDER_ID = "serviceProviderId"
    static let HOST_USER_ID = "hostUserId"
    
    //change password
    static let CURRENT_PASSWORD = "currentPassword"
    static let NEW_PASSWORD = "newPassword"
    
    //edit profile 
    static let PHOTO_REMOVE = "photoRemove"
    
    //edit property
    static let OLD_PROPERTY_IMAGES = "oldPropertyImages"
    
    //delete particular property image
    static let IMAGE_ID = "imageId"
    
    //to search property
    static let CHECK_IN_DATE = "checkInDate"
    static let CHECK_OUT_DATE = "checkOutDate"
    static let MIN_PRICE = "minimumPriceRange"
    static let MAX_PRICE = "maximumPriceRange"
    static let MIN_DISTANCE = "minimumDistanceRange"
    static let MAX_DISTANCE = "maximumDistanceRange"
    
    //to check booking
    static let BOOKING_START_DATE = "bookingStartDate"
    static let BOOKING_END_DATE  = "bookingEndDate"
    static let BOOKING_TYPE  = "bookingType"
    
    //chats
    static let USER_SENDER_ID = "userSenderId"
    static let USER_RECEIVER_ID = "userReceiverId"
    static let SERVICE_PROVIDER_SENDER_ID = "serviceProviderSenderId"
    static let SERVICE_PROVIDER_RECEIVER_ID = "serviceProviderReceiverId"
    static let MESSAGE = "message"
    
    static let TOTAL_ROOM = "totalRoom"
    static let BOOKED_ROOM = "bookedRoom"
    static let HOST_ID = "hostId"
    
    static let BOOKED_PROPERTY_ID = "bookedPropertyId"
    static let IS_APPROVED = "isApproved"
    
    static let AVAILABLE_ROOM = "availableRoom"
    static let DECLINE_REQUEST_ID = "declineRequestId"
}
