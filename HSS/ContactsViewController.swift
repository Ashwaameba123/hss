//
//  ContactsViewController.swift
//  HSS
//
//  Created by Rajni on 24/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import Contacts
import ContactsUI
import QuartzCore

class ContactsViewController: UIViewController , CNContactPickerDelegate{
//    
//    //MARK:- OUTLETS
//    @IBOutlet weak var tableView: UITableView!
//    
//    //MARK:- VARIABLES
//    var contactStore = CNContactStore()
//    var arrContacts = [Contact]()
//    
//   //MARK:- VIEW LIFE CYCLE
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        //ask for contact access 
//        self.askForContactAccess()
//        saveContactsIntoLocalArr()
//    }
//    
//    
//    //MARK:- CUSTOM METHODS
//    // - Contact Access Permission Method
//    func askForContactAccess() {
//        let authorizationStatus = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts)
//        
//        switch authorizationStatus {
//        case .Denied, .NotDetermined:
//            self.contactStore.requestAccessForEntityType(CNEntityType.Contacts, completionHandler: { (access, accessError) -> Void in
//                if !access {
//                    if authorizationStatus == CNAuthorizationStatus.Denied {
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
//                            let alertController = UIAlertController(title: "Contacts", message: message, preferredStyle: UIAlertControllerStyle.Alert)
//                            
//                            let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
//                            }
//                            
//                            alertController.addAction(dismissAction)
//                            
//                            self.presentViewController(alertController, animated: true, completion: nil)
//                        })
//                    }
//                }
//            })
//            break
//            
//        //when allow
//        case .Authorized:
//            //self.fetchPhoneContacts()
//            break
//            
//        default:
//            break
//        }
//    }
//    
//    
//    func fetchPhoneContacts() -> [CNContact]{
//       
//            let contactStore = CNContactStore()
//            let keysToFetch = [
//                CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName),
//                CNContactEmailAddressesKey,
//                CNContactPhoneNumbersKey,
//                CNContactImageDataAvailableKey,
//                CNContactThumbnailImageDataKey]
//            
//            // Get all the containers
//            var allContainers: [CNContainer] = []
//            do {
//                allContainers = try contactStore.containersMatchingPredicate(nil)
//            } catch {
//                print("Error fetching containers")
//            }
//            
//            var results: [CNContact] = []
//            
//            // Iterate all containers and append their contacts to our results array
//            for container in allContainers {
//                let fetchPredicate = CNContact.predicateForContactsInContainerWithIdentifier(container.identifier)
//                
//                do {
//                    let containerResults = try contactStore.unifiedContactsMatchingPredicate(fetchPredicate, keysToFetch: keysToFetch)
//                    print("contact-------- \(containerResults)")
//                    results.appendContentsOf(containerResults)
//                } catch {
//                    print("Error fetching results for container")
//                }
//            }
//            
//            return results
//    }
//    
//    func saveContactsIntoLocalArr() {
//        let arrPhnContacts = fetchPhoneContacts()
//        print(arrPhnContacts.count)
//        
//        for contactIndex in arrPhnContacts {
//            let contactObject = Contact()
//            
//            if let name: String = contactIndex.givenName {
//                print(name)
//                contactObject.name = name
//            }
//            
//            for phoneNumber:CNLabeledValue in contactIndex.phoneNumbers {
//                let number  = phoneNumber.value as! CNPhoneNumber
//                let lable :String  =  CNLabeledValue.localizedStringForLabel(phoneNumber.label )
//                print("\(lable)  \(number.stringValue)")
//                contactObject.phoneNumber = number.stringValue
//            }
//            
//            
//            //var imageData = NSData?()
//            if contactIndex.thumbnailImageData != nil{
//                contactObject.imageData = contactIndex.thumbnailImageData!
//            }
//            
//            
//            
//            //save contact into local array
//            arrContacts.append(contactObject)
//        }
//        
//        self.tableView.reloadData()
//    }
//    
//    
//    
//    
//    
//    
//}
//}
////MARK:- TABLE VIEW DELEGATES AND DATASOURCE
//extension ContactsViewController : UITableViewDelegate, UITableViewDataSource  {
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//        return arrContacts.count
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//    let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
//        
//        let contact = arrContacts[indexPath.row]
//        
//        let cellImageLayer: CALayer?  = cell.imageView!.layer
//        cellImageLayer!.cornerRadius = cell.imageView!.frame.size.width/2
//        cellImageLayer!.masksToBounds = true
//    
//        cell.selectionStyle = UITableViewCellSelectionStyle.None
//        
//        cell.textLabel!.text! = contact.name
//        cell.detailTextLabel!.text = contact.phoneNumber
//        
//        if contact.imageData != nil {
//        let image : UIImage = UIImage(data: contact.imageData)!
//            cell.textLabel
//           cell.imageView?.image = image
//        }
//        else {
//            cell.imageView!.image = UIImage(named: "avatar")
//        }
//        
////        //make round image view
////        cell.imageView!.layer.cornerRadius = cell.imageView!.frame.size.width/2
////        cell.imageView!.clipsToBounds = true
//        
//        return cell
//        
//    }
//    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        NSLog("You selected cell #\(indexPath.row)!")
//    }
//    
//    
    
    
}

