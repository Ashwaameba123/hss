//
//  Available_Dates_For_Property_VC.swift
//  HSS
//
//  Created by Rajni on 24/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator

class Available_Dates_For_Property_VC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var table_View: UITableView!
    
    //MARK:- VARIABLES
    var arr_available_dates = [PropertyAvailableDate]()
    var property: PropertyDetailsModel!
    var userID: String!
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table_View.separatorColor = UIColor.clearColor()
        //self.title = "Available Dates"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
    }
    
    //MARK:- TABLE VIEW DATA SOURCE AND DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr_available_dates.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! Available_Property_Dates_Tbl_Cell
        CommonFunctions.makeRoundCornerOfView(cell.vw_background)
        
        let date = arr_available_dates[indexPath.row]
        
        cell.lbl_start_date.text = CommonFunctions.dateFromString(date.bookingStartDate)
        cell.lbl_end_date.text = CommonFunctions.dateFromString(date.bookingEndDate)
        cell.lbl_total_room.text = "Total Rooms: \(date.totalRoom)"
        cell.lbl_booked_room.text = "Available Rooms: \(date.availableRoom)"
        cell.btn_continue.tag = indexPath.row
        cell.btn_continue.addTarget(self, action: #selector(Available_Dates_For_Property_VC.buttonContinueClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        //check
        if date.availableRoom == "0" {
            cell.btn_continue.hidden = true
            cell.btn_continue.userInteractionEnabled = false
        }
        else {
            cell.btn_continue.hidden = false
            cell.btn_continue.userInteractionEnabled = true
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        footerView.backgroundColor = UIColor.clearColor()
        let requestBtn: UIButton = UIButton()
        
        requestBtn.setTitle("CHECK ANOTHER DATES", forState: .Normal)
        requestBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        requestBtn.backgroundColor = Constants.Colors.GREEN_COLOUR
        requestBtn.frame = CGRect(x:0,y:0,width:tableView.frame.size.width - 20,height:40)
        requestBtn.layer.cornerRadius = 10
        requestBtn.clipsToBounds = true
        requestBtn.titleLabel!.font = UIFont.boldSystemFontOfSize(16)
        requestBtn.center = footerView.center
        requestBtn.addTarget(self, action: #selector(Available_Dates_For_Property_VC.btnCheckAnotherDatesClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        footerView.addSubview(requestBtn)
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    func btnCheckAnotherDatesClick(sender:UIButton!)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func buttonContinueClick(sender:UIButton!) {
        let tag = sender.tag
        self.callToBookProperty(tag)
    }
    
    //MARK:- WEB SERVICES
    func callToBookProperty(tag: Int){
        var propertyID: String = ""
        var startDate: String = ""
        var endDate: String = ""
        var bookingType: String = ""
        var totalRoom: String = ""
        var bookedRoom: String = ""
        var userId: String = ""
        var hostId: String = ""
        
        
        
        if let id = property.id{
            propertyID = id
        }
        if let date = arr_available_dates[tag].bookingStartDate {
            startDate = date
        }
        if let date = arr_available_dates[tag].bookingEndDate{
            endDate = date
        }
        if let id = userID {
            userId = id
        }
        if let type = property.typeOfBooking {
            bookingType = type
        }
        if let total_room = property.accommodation {
            totalRoom = total_room
        }
        if let booked = arr_available_dates[tag].availableRoom {
            bookedRoom = booked
        }
        if let id = property.userId {
            hostId = id
        }
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.PROPERTY_ID: propertyID,
            URLParams.BOOKING_START_DATE: startDate,
            URLParams.BOOKING_END_DATE: endDate,
            URLParams.BOOKING_TYPE: bookingType,
            URLParams.TOTAL_ROOM: totalRoom,
            URLParams.BOOKED_ROOM: bookedRoom,
            URLParams.HOST_ID: hostId
        ]
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.TO_BOOK_PROPERTY, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        
                        if self.property.typeOfBooking.containsString("review") {
                            FTIndicator.showInfoWithMessage("Your request has been sent to host for review.")
                        }
                        else {
                            FTIndicator.showInfoWithMessage("Property has been booked.")
                        }
                        CommonFunctions.globals.isUserBookedProperty = true
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
}