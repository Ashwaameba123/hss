//
//  Property_Booking.swift
//  HSS
//
//  Created by Rajni on 14/03/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class Property_Booking: NSObject {
    
    var availableRoom: String!
    var bookedRoom: String!
    var bookingEndDate: String!
    var bookingStartDate: String!
    var bookingType: String!
    var createdAt: String!
    var hostId: String!
    var id: String!
    var isApproved: String!
    var isCancel: String!
    var paymentWith: String!
    var profileName: String!
    var propertyId: String!
    var propertyImage: String!
    var streetAddress: String!
    var totalRoom: String!
    var updatedAt: String!
    var userId: String!
    var userName: String!
    var userProfileImage: String!
    var userType: String!
    var setPrice: String!
    var currency: String!
    
}
