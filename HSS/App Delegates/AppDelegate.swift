//
//  AppDelegate.swift
//  HSS
//
//  Created by Rajni on 01/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import Fabric
import TwitterKit
import IQKeyboardManagerSwift
import FTIndicator
import FBSDKLoginKit
import Crashlytics
import Firebase
import FirebaseMessaging
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var uni_vc: UIViewController?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        AFNetworkReachabilityManager.sharedManager().startMonitoring()
        AFNetworkReachabilityManager.sharedManager().setReachabilityStatusChangeBlock{(status: AFNetworkReachabilityStatus?) in
            
            switch status!.hashValue {
            case AFNetworkReachabilityStatus.NotReachable.hashValue:
                
                print("no internet")
                let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                
            case AFNetworkReachabilityStatus.ReachableViaWiFi.hashValue,AFNetworkReachabilityStatus.ReachableViaWWAN.hashValue:
                print("with internet")
            default:
                print("unknown")
            }
        }
        
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        
        let notificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        application.registerForRemoteNotifications()
        
        FIRApp.configure()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)), name: kFIRInstanceIDTokenRefreshNotification, object: nil)
        
        //2017-01-20T06:13:40.435Z
        
        // Override point for customization after application launch.
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        //IQKeyboard Manager::--
        IQKeyboardManager.sharedManager().enable = true
        
        LocationService.sharedInstance().startUpdatingLocation()
        FTIndicator.setIndicatorStyle(.Dark)
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: .Default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().translucent = true
        
        application.setStatusBarHidden(false, withAnimation: .Slide)
        application.statusBarStyle = .LightContent
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("isLogin") == nil{
            
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isLogin")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        else{
            if NSUserDefaults.standardUserDefaults().boolForKey("isLogin") == true{
                toMakeRevealVwInitial()
            }
        }
        
        
        Fabric.with([Twitter.self , Crashlytics.self])
        
        // TODO: Move this to where you establish a user session
        self.logUser()
        
        //firebase
        //FIRApp.configure()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
   
    
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("smartamebadevelopers123@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("Ameba[2016]")
        Crashlytics.sharedInstance().setUserName("Rajni iOS")
    }
    
    
    
    func toMakeRevealVwInitial(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.window?.rootViewController = yourVC
        self.window?.makeKeyAndVisible()
    }
    
    
    // Handle refresh notification token
    func tokenRefreshNotification(notification: NSNotification) {
        let refreshedToken = FIRInstanceID.instanceID().token()
        print("InstanceID token: \(refreshedToken)")
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.DEVICE_TOKEN, value: refreshedToken)
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        if (refreshedToken != nil)
        {
            connectToFcm()
            
            FIRMessaging.messaging().subscribeToTopic("/topics/topic")
        }
        
    }
    
    
    // Connect to FCM
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        //FIRMessaging.messaging().disconnect()
        //print("Disconnected from FCM.")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
        connectToFcm()
        application.applicationIconBadgeNumber = 0;
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        FBSDKLoginManager().logOut()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        if let loginType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) {
            if loginType == Enumerations.REGISTER_VIA.facebook.rawValue {
                return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
            }
            else if loginType == Enumerations.REGISTER_VIA.googlePlus.rawValue{
                return GPPURLHandler.handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
            }
        }
        return false
        
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Prod)
        print(FIRInstanceID.instanceID().token())
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        for i in 0..<deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        print("tokenString: \(tokenString)")
        //PrefrencesUtils.saveStringToPrefs(PreferenceConstant.DEVICE_TOKEN, value: tokenString)
        
    }
    
    
    
    
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Failed to get Device Token")
    }
    
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("Push Notification Recieved")
        var flag: NSString?
        var senderId: String = ""
        var sen_img: String = ""
        var sen_name: String = ""
        var sen_type: String = ""
        var dictonary:NSDictionary?
        var noti_msg:String = ""
        var title_show: String = ""
        var updatedAt: String = ""
        
        
        /* public static final String HOST_APPROVAL_FOR_PROPERTY = "hostApprovalForProperty"; //done
         public static final String HOST_NEW_REQUEST_FROM_USER = "newRequestFromUser";   //done
         public static final String HOST_DECLINE_PROPERTY_REQUEST = "declineRequestFromHost"; //done
         public static final String NEW_CHAT_MESSAGE= "newChatMessage";  //done
         public static final String BOOKING_CANCEL_FROM_USER= "bookingCancelFromUser";  //todo
         public static final String INSTANT_BOOKING_FROM_USER="instantBookingFromUser"; //done
         public static final String HOST_SERVICE_PROVIDER_ADDED_IN_PROPERTY = "servicePropertyAddedInProperty"; // need to implement from backend
         public static final String KEY_MessageFlag = "messageFlag"; */
        
        print(userInfo)
        if userInfo["messageFlag"] != nil {
            flag = userInfo["messageFlag"] as? NSString
        }
        
        
        // when app is in background state
        if (application.applicationState == UIApplicationState.Inactive){
            
            
            // 1-----chat
            if flag!.containsString("newChatMessage") {
                
                //to get chat dic
                if let chat = userInfo["chatDetail"]  {
                    
                    if let data = chat.dataUsingEncoding(NSUTF8StringEncoding) {
                        
                        do {
                            dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                            
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                    
                    print(dictonary)
                    
                    if let sender = dictonary!.objectForKey("userSenderId") as? String {
                        print(sender)
                        senderId = sender
                    }
    
                    if let img = dictonary!
                        .objectForKey("senderProfileImage") as? String {
                        print(img)
                        sen_img = img
                    }
                    
                    if let name = dictonary!
                        .objectForKey("senderName") as? String {
                        print(name)
                        sen_name = name
                    }
                    
                    if let userType = dictonary!
                        .objectForKey("userType") as? String {
                        print(userType)
                        sen_type = userType
                    }
                    if let update = dictonary!
                        .objectForKey("updatedAt") as? String {
                        print(update)
                        updatedAt = update
                    }
                    
                    
                }
                
                
                let chat = Chat_Model()
                
                
                //when user and host
                if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue{
                    
                    let arr = DatabaseBLFunctions.fetchUserRegistrationData()
                    if arr.count > 0 {
                        
                        chat.senderId = arr[0].userId
                        chat.senderAuthToken = arr[0].authenticateToken
                        chat.senderType = arr[0].userType
                    }
                }
                //service provider
                else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                    let arr = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
                    if arr.count > 0 {
                        
                        chat.senderId = arr[0].userId
                        chat.senderAuthToken = arr[0].authenticateToken
                        chat.senderType = arr[0].userType
                    }
                }
                
                chat.reciverId = senderId
                chat.receiverName = sen_name
                chat.receiverImage = sen_img
                chat.receiverType = sen_type
                
                CommonFunctions.chat.noti_chat = chat
                self.toMakeRevealVwInitial()
            }
            
            // 2-----new request from user
           if flag!.containsString("newRequestFromUser") {
            
            //to get new request dic
            if let property = userInfo["bookedPropertyDetail"]  {
                if let data = property.dataUsingEncoding(NSUTF8StringEncoding) {
                    do {
                        dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                        
                    } catch let error as NSError {
                        print(error)
                    }
                }
            print(dictonary)
            
            CommonFunctions.globals.isNewRequestFrmUser = true
            let obj = CommonDataParsingMethods.toParseNotificationDic(dictonary!)
            CommonFunctions.NewRequest.Noti_obj = obj
            self.toMakeRevealVwInitial()
           }
        }
            
            // 3-----HOST_DECLINE_PROPERTY_REQUEST
            if flag!.containsString("declineRequestFromHost") {
                
                //to get new request dic
                if let property = userInfo["bookedPropertyDetail"]  {
                    if let data = property.dataUsingEncoding(NSUTF8StringEncoding) {
                        do {
                            dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                            
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                    print(dictonary)
                    
                    CommonFunctions.globals.isNewRequestFrmUser = true
                    let obj = CommonDataParsingMethods.toParseNotificationDic(dictonary!)
                    CommonFunctions.NewRequest.Noti_obj = obj
                    self.toMakeRevealVwInitial()
                }
            }
            
            // 4-----HOST_ACCEPT_PROPERTY_REQUEST
            if flag!.containsString("hostApprovalForProperty") {
                
                //to get new request dic
                if let property = userInfo["bookedPropertyDetail"]  {
                    if let data = property.dataUsingEncoding(NSUTF8StringEncoding) {
                        do {
                            dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                            
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                    print(dictonary)
                    
                    CommonFunctions.globals.isNewRequestFrmUser = true
                    let obj = CommonDataParsingMethods.toParseNotificationDic(dictonary!)
                    CommonFunctions.NewRequest.Noti_obj = obj
                    self.toMakeRevealVwInitial()
                }
            }
            
            // 5-----INSTANT-BOOKING-FROM-USER
            if flag!.containsString("instantBookingFromUser") {
                
                //to get new request dic
                if let property = userInfo["bookedPropertyDetail"]  {
                    if let data = property.dataUsingEncoding(NSUTF8StringEncoding) {
                        do {
                            dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                            
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                    print(dictonary)
                    
                    CommonFunctions.globals.isNewRequestFrmUser = true
                    let obj = CommonDataParsingMethods.toParseNotificationDic(dictonary!)
                    CommonFunctions.NewRequest.Noti_obj = obj
                    self.toMakeRevealVwInitial()
                }
            }
           //TODO // 6-----Booking cancel by user
            if flag!.containsString("bookingCancelFromUser") {
                
                //to get new request dic
                if let property = userInfo["bookedPropertyDetail"]  {
                    if let data = property.dataUsingEncoding(NSUTF8StringEncoding) {
                        do {
                            dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                            
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                    print(dictonary)
                    
                    CommonFunctions.globals.isNewRequestFrmUser = true
                    let obj = CommonDataParsingMethods.toParseNotificationDic(dictonary!)
                    CommonFunctions.NewRequest.Noti_obj = obj
                    self.toMakeRevealVwInitial()
                }
            }
        }
            
            
         //***** when app in active state
        else {
            
            if let aps = userInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    if let body = alert["body"] as? NSString {
                        print(body)
                        noti_msg = body as String
                    }
                    if let title = alert["title"] as? NSString {
                        print(title)
                        title_show = title as String
                    }
                }
            }
            
            print("current vc" , uni_vc)
            
            //1--chat
            if flag!.containsString("newChatMessage") {
                if uni_vc?.isKindOfClass(LGChatController) == true {
                    NSNotificationCenter.defaultCenter().postNotificationName("newChatMessage", object: nil, userInfo: userInfo)
                }
                else {
                   FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
                }
            }
            
            //2--new request from user
            if flag!.containsString("newRequestFromUser") {
                if uni_vc?.isKindOfClass(NotificationsViewController) == true {
                NSNotificationCenter.defaultCenter().postNotificationName("newRequestFromUser", object: nil, userInfo: userInfo)
                }
                else {
                    FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
                }
            }
            
            //3--host decline user request
            if flag!.containsString("declineRequestFromHost") {
                if uni_vc?.isKindOfClass(NotificationsViewController) == true {
                NSNotificationCenter.defaultCenter().postNotificationName("declineRequestFromHost", object: nil, userInfo: userInfo)
                }
                else {
                    FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
                }
            }
            //4--host accept user request
            if flag!.containsString("hostApprovalForProperty") {
                if uni_vc?.isKindOfClass(NotificationsViewController) == true {
                NSNotificationCenter.defaultCenter().postNotificationName("hostApprovalForProperty", object: nil, userInfo: userInfo)
                }
                else {
                    FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
                }
            }
            //5--instant booking from user
            if flag!.containsString("instantBookingFromUser") {
                if uni_vc?.isKindOfClass(NotificationsViewController) == true {
                NSNotificationCenter.defaultCenter().postNotificationName("instantBookingFromUser", object: nil, userInfo: userInfo)
                }
                else {
                    FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
                }
            }
            
         }
        
    }
    
    
    
    
    
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "ameba.softwares.HSS" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("HSS", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        
        print("********url***** \(url)")
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
}

