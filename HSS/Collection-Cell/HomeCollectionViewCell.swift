//
//  HomeCollectionViewCell.swift
//  HSS
//
//  Created by Rajni on 06/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import Cosmos
import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgVwPlace: UIImageView!
    
    
    @IBOutlet weak var imgVwSelectImage: UIImageView!
    
    
    @IBOutlet weak var txtVw_locationName: UITextView!
    
    
    @IBOutlet weak var imgPropertyHost: UIImageView!

    @IBOutlet weak var activity_indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var vw_rating: CosmosView!
}