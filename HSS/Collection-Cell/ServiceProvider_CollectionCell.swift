//
//  ServiceProvider_CollectionCell.swift
//  HSS
//
//  Created by Rajni on 31/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import UIKit

class ServiceProvider_CollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgVW_Service: UIImageView!
    
    @IBOutlet weak var lbl_Provider_Name: UILabel!
    @IBOutlet weak var lbl_Message_Provider: UILabel!
}
