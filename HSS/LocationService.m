//
//  LocationService.m
//  WishDrop
//
//  Created by Mandeep Sharma on 08/12/16.
//  Copyright © 2016 ameba. All rights reserved.
//

#import "LocationService.h"

@implementation LocationService
+(LocationService *) sharedInstance
{
    static LocationService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if(self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }
        self.locationManager.distanceFilter = 100; // meters
        self.locationManager.delegate = self;
        

    }
    return self;
}

- (void)startUpdatingLocation
{
    NSLog(@"Starting location updates");
    [self.locationManager startUpdatingLocation];
}
- (void)stopUpdatingLocation
{
    NSLog(@"Stop location updates");
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Location service failed with error %@", error);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray*)locations
{
    CLLocation *location = [locations lastObject];
    NSLog(@"Latitude %+.6f, Longitude %+.6f\n",
          location.coordinate.latitude,
          location.coordinate.longitude);
    self.currentLocation = location;
    
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *lng = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         NSString *strAdd = @"";
         NSString *strPin = @"";
         NSString *state = @"";
         NSString *city = @"";
         NSString *strAddress = @"";
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark *placemark = [placemarks lastObject];
             

             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strPin = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strPin = placemark.postalCode;
             }
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
                     city = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
                    state = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAddress = [NSString stringWithFormat:@"%@, %@",strAddress,[placemark subLocality]];
                 else
                     strAddress = placemark.subLocality;
             }
             
             
             
        }
         
         NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
         [dict setObject:lat forKey:@"latitude"];
         [dict setObject:lng forKey:@"longitude"];
         [dict setObject:strAdd forKey:@"locationName"];
        [dict setObject:strPin forKey:@"pincode"];
         [dict setObject:strAddress forKey:@"streetAddress"];
         [dict setObject:city forKey:@"city"];
         [dict setObject:state forKey:@"state"];
         
         [[NSUserDefaults standardUserDefaults]setObject:dict forKey:@"CurrentLocation"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         
         
         
         
     }];

    [self.locationManager stopUpdatingLocation];
    
}

@end
