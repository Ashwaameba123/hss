//
//  History_table_cell.swift
//  HSS
//
//  Created by Rajni on 14/03/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class History_table_cell: UITableViewCell {
    
    @IBOutlet weak var vw_background: UIView!
    @IBOutlet weak var imgVw_Property: UIImageView!
    @IBOutlet weak var lbl_property_name: UILabel!
    
    @IBOutlet weak var lbl_userName: UILabel!
    
    @IBOutlet weak var lbl_date_time: UILabel!
    
}