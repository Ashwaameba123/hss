//
//  LocationService.h
//  WishDrop
//
//  Created by Mandeep Sharma on 08/12/16.
//  Copyright © 2016 ameba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface LocationService : NSObject <CLLocationManagerDelegate>
+(LocationService *) sharedInstance;


@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;

- (void)stopUpdatingLocation;
- (void)startUpdatingLocation;



@end
