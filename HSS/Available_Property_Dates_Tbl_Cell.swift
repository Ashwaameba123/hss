//
//  Available_Property_Dates_Tbl_Cell.swift
//  HSS
//
//  Created by Rajni on 24/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class Available_Property_Dates_Tbl_Cell: UITableViewCell{
    
    @IBOutlet weak var lbl_booked_room: UILabel!
    @IBOutlet weak var lbl_total_room: UILabel!
    @IBOutlet weak var lbl_end_date: UILabel!
    @IBOutlet weak var lbl_start_date: UILabel!
    @IBOutlet weak var vw_background: UIView!
    @IBOutlet weak var btn_continue: UIButton!
    
}