//
//  SimpleChatController.swift
//  SimpleChat
//
//  Created by Logan Wright on 10/16/14.
//  Copyright (c) 2014 Logan Wright. All rights reserved.
//

/*
 Mozilla Public License
 Version 2.0
 https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)
 */

import UIKit
import IQKeyboardManagerSwift
import FTIndicator
import JTSImageViewController

// MARK: Message

class LGChatMessage : NSObject {
    
    enum SentBy : String {
        case User = "LGChatMessageSentByUser"
        case Opponent = "LGChatMessageSentByOpponent"
    }
    
    // MARK: ObjC Compatibility
    
    /*
     ObjC can't interact w/ enums properly, so this is used for converting compatible values.
     */
    
    class func SentByUserString() -> String {
        return LGChatMessage.SentBy.User.rawValue
    }
    
    class func SentByOpponentString() -> String {
        return LGChatMessage.SentBy.Opponent.rawValue
    }
    
    var sentByString: String {
        get {
            return sentBy.rawValue
        }
        set {
            if let sentBy = SentBy(rawValue: newValue) {
                self.sentBy = sentBy
            } else {
                
            }
        }
    }
    
    // MARK: Public Properties
    
    var sentBy: SentBy
    var content: String
    var timeStamp: NSTimeInterval?
    
    required init (content: String, sentBy: SentBy, timeStamp: NSTimeInterval? = nil){
        self.sentBy = sentBy
        self.timeStamp = timeStamp
        self.content = content
    }
    
    // MARK: ObjC Compatibility
    convenience init (content: String, sentByString: String) {
        if let sentBy = SentBy(rawValue: sentByString) {
            self.init(content: content, sentBy: sentBy, timeStamp: nil)
        } else {
            fatalError("LGChatMessage.FatalError : Initialization : Incompatible string set to SentByString!")
        }
    }
    
    convenience init (content: String, sentByString: String, timeStamp: NSTimeInterval) {
        if let sentBy = SentBy(rawValue: sentByString) {
            self.init(content: content, sentBy: sentBy, timeStamp: timeStamp)
        } else {
            fatalError("LGChatMessage.FatalError : Initialization : Incompatible string set to SentByString!")
        }
    }
}

// MARK: Message Cell

class LGChatMessageCell : UITableViewCell {
    
    // MARK: Global MessageCell Appearance Modifier
    struct Appearance {
        static var opponentColor = UIColor(red: 0.142954, green: 0.60323, blue: 0.862548, alpha: 0.88)
        static var userColor = UIColor(red: 0.14726, green: 0.838161, blue: 0.533935, alpha: 1)
        static var font: UIFont = UIFont.systemFontOfSize(17) //UIFont.systemFontOfSize(17.0)
    }
    
    /*
     These methods are included for ObjC compatibility.  If using Swift, you can set the Appearance variables directly.
     */
    
    class func setAppearanceOpponentColor(opponentColor: UIColor) {
        Appearance.opponentColor = opponentColor
    }
    
    class func setAppearanceUserColor(userColor: UIColor) {
        Appearance.userColor = userColor
    }
    
    class  func setAppearanceFont(font: UIFont) {
        Appearance.font = font
    }
    
    // MARK: Message Bubble TextView
    private lazy var textView: MessageBubbleTextView = {
        let textView = MessageBubbleTextView(frame: CGRectZero, textContainer: nil)
        self.contentView.addSubview(textView)
        return textView
    }()
    
    private class MessageBubbleTextView : UITextView {
        
        override init(frame: CGRect = CGRectZero, textContainer: NSTextContainer? = nil) {
            super.init(frame: frame, textContainer: textContainer)
            self.font = Appearance.font
            self.scrollEnabled = false
            self.editable = false
            self.textContainerInset = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            //            self.layer.cornerRadius = 15
            //            self.layer.borderWidth = 2.0
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    // MARK: ImageView
    
    private lazy var opponentImageView: UIImageView = {
        let opponentImageView = UIImageView()
        opponentImageView.hidden = true
        opponentImageView.bounds.size = CGSize(width: self.minimumHeight, height: self.minimumHeight)
        let halfWidth = CGRectGetWidth(opponentImageView.bounds) / 2.0
        let halfHeight = CGRectGetHeight(opponentImageView.bounds) / 2.0
        
        // Center the imageview vertically to the textView when it is singleLine
        let textViewSingleLineCenter = self.textView.textContainerInset.top + (Appearance.font.lineHeight / 2.0)
        opponentImageView.center = CGPointMake(self.padding + halfWidth, textViewSingleLineCenter)
        opponentImageView.backgroundColor = UIColor.lightTextColor()
        opponentImageView.layer.rasterizationScale = UIScreen.mainScreen().scale
        opponentImageView.layer.shouldRasterize = true
        opponentImageView.layer.cornerRadius = halfHeight
        opponentImageView.layer.masksToBounds = true
        self.contentView.addSubview(opponentImageView)
        return opponentImageView
    }()
    
    // MARK: Sizing
    
    private let padding: CGFloat = 5.0
    private let minimumHeight: CGFloat = 50.0 // arbitrary minimum height
    private var size = CGSizeZero
    private var maxSize: CGSize {
        get {
            let maxWidth = CGRectGetWidth(self.bounds) * 0.75 // Cells can take up to 3/4 of screen
            let maxHeight = CGFloat.max
            return CGSize(width: maxWidth, height: maxHeight)
        }
    }
    
    // MARK: Setup Call
    
    /*!
     Use this in cellForRowAtIndexPath to setup the cell.
     */
    func setupWithMessage(message: LGChatMessage) -> CGSize {
        
        textView.text = message.content
        size = textView.sizeThatFits(maxSize)
        if size.height < minimumHeight {
            size.height = minimumHeight
        }
        
        textView.bounds.size = size
        self.styleTextViewForSentBy(message.sentBy)
        return size
    }
    
    // MARK: TextBubble Styling
    private func styleTextViewForSentBy(sentBy: LGChatMessage.SentBy) {
        let halfTextViewWidth = CGRectGetWidth(self.textView.bounds) / 2.0
        let targetX = halfTextViewWidth + padding
        let halfTextViewHeight = CGRectGetHeight(self.textView.bounds) / 2.0
        switch sentBy {
        case .Opponent:
            let maskPath: UIBezierPath = UIBezierPath(roundedRect: textView.bounds, byRoundingCorners: ([.TopLeft, .BottomRight,.TopRight]), cornerRadii: CGSizeMake(10.0, 10.0))
            let maskLayer: CAShapeLayer = CAShapeLayer()
            maskLayer.frame = textView.bounds
            maskLayer.path = maskPath.CGPath
            textView.layer.mask = maskLayer
            self.textView.center.x = targetX
            self.textView.center.y = halfTextViewHeight
            self.textView.backgroundColor = UIColor.grayColor()
            self.textView.textColor = UIColor.whiteColor()
            
            
            if self.opponentImageView.image != nil {
                self.opponentImageView.hidden = false
                self.textView.center.x += CGRectGetWidth(self.opponentImageView.bounds) + padding
            }
            
        case .User:
            let maskPath: UIBezierPath = UIBezierPath(roundedRect: textView.bounds, byRoundingCorners: ([.TopLeft, .BottomLeft,.TopRight]), cornerRadii: CGSizeMake(10.0, 10.0))
            let maskLayer: CAShapeLayer = CAShapeLayer()
            maskLayer.frame = textView.bounds
            maskLayer.path = maskPath.CGPath
            textView.layer.mask = maskLayer
            self.opponentImageView.hidden = true
            self.textView.center.x = CGRectGetWidth(self.bounds) - targetX
            self.textView.center.y = halfTextViewHeight
            self.textView.backgroundColor = UIColor(red: 39/255, green: 206/255, blue: 102/255, alpha: 1)
            self.textView.textColor = UIColor.whiteColor()
            self.textView.layer.borderColor = Appearance.userColor.CGColor
        }
    }
}

// MARK: Chat Controller
@objc protocol LGChatControllerDelegate {
    optional func shouldChatController(chatController: LGChatController, addMessage message: LGChatMessage) -> Bool
    optional func chatController(chatController: LGChatController, didAddNewMessage message: LGChatMessage)
}

class LGChatController : UIViewController, UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate,LGChatInputDelegate {
    
    // MARK: Constants
    private struct Constants {
        static let MessagesSection: Int = 0;
        static let MessageCellIdentifier: String = "LGChatController.Constants.MessageCellIdentifier"
    }
    
    // MARK: Public Properties
    
    /*!
     Use this to set the messages to be displayed
     */
    var messages: [LGChatMessage] = []
    var dataDict:NSDictionary!
    var chat: Chat_Model!
    var opponentImage: UIImage!
    //var opponentID: String!
    var appDelegate : AppDelegate!
    var hostImage: UIImage!
    //var userId: String!
    //var isChatWithHost:Bool = false
    
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    
    
    weak var delegate: LGChatControllerDelegate?
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    // MARK: Private Properties
    private var popUpView:UIView!
    private let sizingCell = LGChatMessageCell()
    private let tableView: UITableView = UITableView()
    private let chatInput = LGChatInput(frame: CGRectZero)
    private var bottomChatInputConstraint: NSLayoutConstraint!
    
    let userDefault = NSUserDefaults.standardUserDefaults()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nc=NSNotificationCenter.defaultCenter()
        nc.addObserver(self, selector: #selector(refreshView(_:)), name: "newChatMessage", object: nil)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LGChatController.refreshView(_:)), name:"rajni", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        IQKeyboardManager.sharedManager().enable = false
        
        
        self.listenForKeyboardChanges()
        self.setup()
        //        userId = String(userDefault.objectForKey("UserInfo")!.valueForKey("user_id")!)
        //        if dataDict.valueForKey("user_details")?.valueForKey("user_id")! != nil {
        //            opponentID = String(dataDict.valueForKey("user_details")!.valueForKey("user_id")!)
        //        } else {
        //            let id = String(dataDict.valueForKey("sender_id")!)
        //
        //            if userId == id {
        //                opponentID = String(dataDict.valueForKey("receiver_id")!)
        //            } else {
        //                opponentID = String(dataDict.valueForKey("sender_id")!)
        //            }
        //        }
        appDelegate = (UIApplication.sharedApplication().delegate as? AppDelegate)!
        // appDelegate.opponentID=self.opponentID
        self.getAllChat()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.scrollToBottom()
    }
    
    override func viewWillDisappear(animated: Bool) {
        IQKeyboardManager.sharedManager().enable = true
        super.viewWillDisappear(animated)
        self.unregisterKeyboardObservers()
        CommonFunctions.chat.noti_chat = nil
        // appDelegate.opponentID="";
        
    }
    
    deinit {
        /*
         Need to remove delegate and datasource or they will try to send scrollView messages.
         */
        self.tableView.delegate = nil
        self.tableView.dataSource = nil
    }
    
    
    func getAllChat(){
        //        let postString = "user_id=\(userId)&friend_id=\(opponentID)"
        //
        //        let param : [String: AnyObject] = ["user_id":userId,"friend_id":opponentID]
        //        //WebData().getMethod("get_message", postString: postString, success: #selector(self.webResponAllChat(_:)), target: self , showHud: true,param: param)
        self.callToGetRecentChat()
    }
    
    func refreshView(notification:NSNotification){
        print("refresh")
        let info = notification.userInfo
        print(info)
        
        var senderId: String = ""
        var receiverId: String = ""
        var msg: String = ""
        var rec_img: String = ""
        var dictonary:NSDictionary?
        var noti_msg:String = ""
        var title_show: String = ""
        
        
        
        
        if let aps = info!["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
        if let body = alert["body"] as? NSString {
            print(body)
            noti_msg = body as String
        }
        if let title = alert["title"] as? NSString {
            print(title)
            title_show = title as String
        }
            }}
        
        //to get chat dic
        if let chat = info!["chatDetail"]  {
            
        if let data = chat.dataUsingEncoding(NSUTF8StringEncoding) {
                
                do {
                    dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
        print(dictonary)
            
            if let sender = dictonary!.objectForKey("userSenderId") as? String {
                print(sender)
                senderId = sender
            }
            
            if let rec = dictonary!.objectForKey("userReceiverId") as? String {
                print(rec)
                receiverId = rec
            }
            
            if let message = dictonary!.objectForKey("message") as? String {
                print(message)
                msg = message
            }
            
            if let sender_img = dictonary!
                .objectForKey("senderProfileImage") as? String {
                print(sender_img)
                rec_img = sender_img
            }
        }
        
        
        //to check chat conversation
        if let sen_id = chat.senderId {
            if let rec_id = chat.reciverId {
             
                if senderId == rec_id && receiverId == sen_id {
                   print("current chat")
                    
                    if (senderId == chat.reciverId){
                        let content = msg
                        let master = LGChatMessage(content: content, sentBy: .Opponent)
                        
                        addNewMessage(master)
                    }
                    
                    
                    
                }
                else {
                  FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
                }
            } }
    }
    // MARK: Setup
    private func setup() {
        self.setupTableView()
        //self.setUpPopUpView()
        self.setupChatInput()
        self.setupNavigationBar()
        self.setupLayoutConstraints()
    }
    
    private func setupNavigationBar(){
        //Left Button
        let leftButton = UIButton()
        leftButton.setImage(UIImage(named: "back_icon.png"), forState: .Normal)
        leftButton.frame = CGRectMake(0, 0, 30, 30)
        leftButton.tag=1
        leftButton.addTarget(self, action: #selector(action(_:)), forControlEvents: .TouchUpInside)
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = leftButton
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        //RightButton
        let rtButton = UIButton()
        rtButton.setImage(UIImage(named: "Dot_Icon"), forState: .Normal)
        rtButton.frame = CGRectMake(0, 0, 30, 30)
        rtButton.tag=2
        let rtBarButton = UIBarButtonItem()
        rtBarButton.customView = rtButton
        self.navigationItem.rightBarButtonItem = rtBarButton
        
        //UserImage and Name label
        let  view = UIView(frame: CGRectMake(self.view.frame.origin.x+30, 0,self.view.frame.width-100 ,44))
        let imageView = UIImageView(frame:CGRectMake(0, 0, 44, 44))
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.greenColor().CGColor
        imageView.contentMode = .ScaleAspectFit
        imageView.userInteractionEnabled = true
        //let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(LGChatController.imageTapped(_:)))
        //imageView.addGestureRecognizer(tapRecognizer)
        
        //        if let img = opponentImage {
        //            imageView.image = img
        //            hostImage = img
        //        }
        //        else {
        //            imageView.image=UIImage(named: "avatar.png")
        //            hostImage = imageView.image
        //        }
        
        if chat.receiverImage != "" {
            let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(chat.receiverImage)"
            let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            
            imageView.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        }
        else {
            imageView.image = UIImage(named: "avatar.png")
        }
        
        let nameLabel = UILabel(frame: CGRectMake(self.view.frame.width/2-180, 0, 300 ,44))
        nameLabel.font =  UIFont(name: "Lato-SemiBold", size: 13)
        nameLabel.textColor=UIColor.whiteColor()
        nameLabel.textAlignment = NSTextAlignment.Center
        if let name = chat.receiverName {
            nameLabel.text = name
        }
        
        //        userId = String(userDefault.objectForKey("UserInfo")!.valueForKey("user_id")!)
        //        if dataDict.valueForKey("user_details")?.valueForKey("user_id")! != nil {
        //            nameLabel.text = String(dataDict.valueForKey("user_details")!.valueForKey("name")!)
        //        } else {
        //            let id = String(dataDict.valueForKey("sender_id")!)
        //            if userId == id {
        //                nameLabel.text=String(dataDict .objectForKey("receiver_name")!)
        //            } else {
        //                opponentID = String(dataDict.valueForKey("sender_id")!)
        //                nameLabel.text=String(dataDict .objectForKey("sender_name")!)
        //            }
        //        }
        
        view.addSubview(nameLabel)
        view.addSubview(imageView)
        // let recognizer = UITapGestureRecognizer(target: self, action:#selector(LGChatController.handleTap(_:)))
        //recognizer.delegate = self
        // view.addGestureRecognizer(recognizer)
        self.navigationItem.titleView = view
    }
    
    //    func imageTapped(img: AnyObject)
    //    {
    //        if let selectedImg:UIImage = hostImage {
    //            let imageInfo = JTSImageInfo()
    //            imageInfo.image = selectedImg
    //            //imageInfo.referenceRect = cell.img_vw_person.frame
    //            //imageInfo.referenceView = cell.img_vw_person.superview
    //            imageInfo.title = "ID"
    //            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
    //            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
    //    }}
    
    
    private func setupTableView() {
        tableView.allowsSelection = false
        tableView.separatorStyle = .None
        tableView.frame = self.view.bounds
        tableView.backgroundColor=UIColor.clearColor()
        
        // Add a background view to the table view
        //        let backgroundImage = UIImage(named: "splash.png")
        //        let imageView = UIImageView(image: backgroundImage)
        //        self.tableView.backgroundView = imageView
        //        imageView.contentMode = .ScaleAspectFill
        //
        //        tableView.backgroundView = imageView
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.registerClass(LGChatMessageCell.classForCoder(), forCellReuseIdentifier: "identifier")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.view.addSubview(tableView)
    }
    
    private func setupChatInput() {
        chatInput.delegate = self
        self.view.addSubview(chatInput)
    }
    
    private func setupLayoutConstraints() {
        chatInput.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints(self.chatInputConstraints())
        self.view.addConstraints(self.tableViewConstraints())
    }
    private func chatInputConstraints() -> [NSLayoutConstraint] {
        self.bottomChatInputConstraint = NSLayoutConstraint(item: chatInput, attribute: .Bottom, relatedBy: .Equal, toItem: self.bottomLayoutGuide, attribute: .Top, multiplier: 1.0, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: chatInput, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1.0, constant: 0.0)
        let rightConstraint = NSLayoutConstraint(item: chatInput, attribute: .Right, relatedBy: .Equal, toItem: self.view, attribute: .Right, multiplier: 1.0, constant: 0.0)
        return [leftConstraint, self.bottomChatInputConstraint, rightConstraint]
    }
    private func tableViewConstraints() -> [NSLayoutConstraint] {
        let leftConstraint = NSLayoutConstraint(item: tableView, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1.0, constant: 0.0)
        let rightConstraint = NSLayoutConstraint(item: tableView, attribute: .Right, relatedBy: .Equal, toItem: self.view, attribute: .Right, multiplier: 1.0, constant: 0.0)
        let topConstraint = NSLayoutConstraint(item: tableView, attribute: .Top, relatedBy: .Equal, toItem: self.view, attribute: .Top, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: tableView, attribute: .Bottom, relatedBy: .Equal, toItem: chatInput, attribute: .Top, multiplier: 1.0, constant: 0)
        return [rightConstraint, leftConstraint, topConstraint, bottomConstraint]//, rightConstraint, bottomConstraint]
    }
    
    //    //MARK:-Hide PopUp View
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //        popUpView.hidden=true
    //    }
    
    //MARK:-Handle Tap
    func handleTap(sender:UIGestureRecognizer)  {
        //        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        //        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("ID_View_Post") as! View_Post
        //        nextViewController.infodataDict = dataDict
        //        nextViewController.fromVIew = "chat"
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        // self.presentViewController(nextViewController, animated:true, completion:nil)
    }
    
    //MARK:-Action Button
    func action(sender: UIButton) {
        switch  sender.tag {
        case 1:
            // messagesCount = 0
            self.navigationController?.popViewControllerAnimated(true)
        case 3:
            //            //Clear Chat
            //            if #available(iOS 8.0, *) {
            //                let alert = UIAlertController(title: "Wish Drop", message: "Clear Messages", preferredStyle: UIAlertControllerStyle.Alert)
            //                alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Cancel, handler:nil))
            //                alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            //                let postString = "user_id=\(self.userId)&friend_id=\(self.opponentID)"
            //
            //                      let param : [String: AnyObject] = ["user_id":self.userId,"friend_id":self.opponentID]
            //
            //                    //WebData().postMethod("clear_chat", postString: postString, success: #selector(self.webClearChatResponse(_:)), target: self,param: param , showHud : true)
            //                }))
            //                self.presentViewController(alert, animated: true, completion: { })
            //            } else {
            //                let blockAlert: UIAlertView = UIAlertView()
            //                blockAlert.delegate = self
            //                blockAlert.title = "WishDrop"
            //                blockAlert.message = "Sure want to clear all chat"
            //                blockAlert.addButtonWithTitle("YES")
            //                blockAlert.addButtonWithTitle("NO")
            //                blockAlert.tag=101
            //                blockAlert.show()
            //            }
            print("clear chat")
        case 4:
            //Block User
            //            if #available(iOS 8.0, *) {
            //                let alert = UIAlertController(title: "Wish Drop", message: "Are you sure you want to block this user", preferredStyle: UIAlertControllerStyle.Alert)
            //                alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Cancel, handler:nil))
            //                alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            //                    let postString = "user_id=\(self.userId)&blocked_user_id=\(self.opponentID)"
            //                      let param : [String: AnyObject] = ["user_id":self.userId,"blocked_user_id":self.opponentID]
            //                   // WebData().postMethod("block_user", postString: postString, success: #selector(self.webBlockUserResponse(_:)), target: self,param: param , showHud : true)
            //                }))
            //                self.presentViewController(alert, animated: true, completion: { })
            //            } else {
            //                let blockAlert: UIAlertView = UIAlertView()
            //                blockAlert.delegate = self
            //                blockAlert.title = "WishDrop"
            //                blockAlert.message = "Are you sure you want to block this user"
            //                blockAlert.addButtonWithTitle("YES")
            //                blockAlert.addButtonWithTitle("NO")
            //                blockAlert.tag=100
            //                blockAlert.show()
            //            }
            print("block chat")
        default: break
        }
    }
    //MARK:-ActionSheet Delegate
    //    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
    //        if alertView.tag==100 && buttonIndex==0{
    //            let postString = ["CustomerIdBy":userId,"CustomerIdTo":opponentID,"IsBlocked":"1"] as Dictionary<String, String>
    //            //WebData().postMethod("Customer/SaveBlockUnblock", postString: postString, success: #selector(webBlockUserResponse(_:)), target: self)
    //        }
    //        else  if alertView.tag==101 && buttonIndex==0{
    //            let postString = ["CustomerIdBy":userId,"CustomerIdTo":opponentID] as Dictionary<String, String>
    //            //WebData().postMethod("Chat/ClearAllChat", postString: postString, success: #selector(webClearChatResponse(_:)), target: self)
    //        }
    //    }
    //MARK:-Api Response
    //Block User
    func webBlockUserResponse(result:NSDictionary) {
        let status = String(result["status"]!)
        if status == "Success" {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            let message = String(result.valueForKey("message")!)
            showError(message)
        }
    }
    //clear Chat
    func webClearChatResponse(result:NSDictionary) {
        let status = String(result["status"]!)
        if status == "Success" {
            messages.removeAll()
            tableView.reloadData()
            //popUpView.hidden=true
            //self.dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            let message = String(result.valueForKey("message")!)
            showError(message)
        }
    }
    
    //Send Message
    func webResponseSend(result:NSDictionary) {
        let status = String(result["status"]!)
        if status == "Success" {
        }
    }
    
    //Send Message
    func webResponAllChat(result:NSDictionary) {
        var sender_Id: String = ""
        
        if chat.senderType == Enumerations.USER_TYPE.serviceProvider.rawValue {
            sender_Id = String(result.objectForKey("serviceProviderSenderId")!)
        }
        else {
            sender_Id = String(result.objectForKey("userSenderId")!)
        }
        
        let timestamp = result.objectForKey("updatedAt") as! String
        let form_time = self.dateFromString(timestamp)
        
        let content=(result.objectForKey("message")as! String)
        if (chat.senderId == sender_Id){
            //let master = LGChatMessage(content: content, sentBy: .User, timeStamp: form_time)
            let master  = LGChatMessage(content: content, sentBy: .User)
            addNewMessage(master)
        } else {
            //let master = LGChatMessage(content: content, sentBy: .Opponent, timeStamp: form_time)
            let master  = LGChatMessage(content: content, sentBy: .Opponent)
            addNewMessage(master)
        }
    }
    
    
    func dateFromString (dateString : String) -> NSTimeInterval{
        var date : String!
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dat : NSDate = dateFormatter.dateFromString(dateString)!
        //dateFormatter = NSDateFormatter()
        //dateFormatter.dateFormat = "YYYY-MM-dd"
        //date = dateFormatter.stringFromDate(dat)
        
        let interval = dat.timeIntervalSince1970
        print("interval-----" , interval)
        return interval
    }
    
    
    
    
    // MARK: - User Defined Methods
    func showError(message:String) {
        if #available(iOS 8.0, *) {
            let alertView = UIAlertController(title: "Wish Drop", message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil));
            presentViewController(alertView, animated: true, completion: nil);
        }
        else {
            let createAccountErrorAlert: UIAlertView = UIAlertView()
            createAccountErrorAlert.title = "Wish Drop"
            createAccountErrorAlert.message = message
            createAccountErrorAlert.addButtonWithTitle("Ok")
            createAccountErrorAlert.show()
        }
    }
    
    // MARK: Keyboard Notifications
    private func listenForKeyboardChanges() {
        let defaultCenter = NSNotificationCenter.defaultCenter()
        defaultCenter.addObserver(self, selector: #selector(LGChatController.keyboardWillChangeFrame(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    private func unregisterKeyboardObservers() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillChangeFrame(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo!
        let duration = keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        var keyboardFrame = (keyboardAnimationDetail[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        if let window = self.view.window {
            keyboardFrame = window.convertRect(keyboardFrame, toView: self.view)
        }
        let animationCurve = keyboardAnimationDetail[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        
        self.tableView.scrollEnabled = false
        self.tableView.decelerationRate = UIScrollViewDecelerationRateFast
        
        self.view.layoutIfNeeded()
        
        var chatInputOffset = -((CGRectGetHeight(self.view.bounds) - self.bottomLayoutGuide.length) - CGRectGetMinY(keyboardFrame))
        if chatInputOffset > 0 {
            chatInputOffset = 0
        }
        
        self.bottomChatInputConstraint.constant = chatInputOffset
        UIView.animateWithDuration(duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: animationCurve), animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.scrollToBottom()
            }, completion: {(finished) -> () in
                self.tableView.scrollEnabled = true
                self.tableView.decelerationRate = UIScrollViewDecelerationRateNormal
        })
    }
    
    // MARK: Rotation
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        super.willAnimateRotationToInterfaceOrientation(toInterfaceOrientation, duration: duration)
        self.tableView.reloadData()
    }
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        super.didRotateFromInterfaceOrientation(fromInterfaceOrientation)
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.scrollToBottom()
            }, completion: nil)
    }
    
    // MARK: Scrolling
    private func scrollToBottom() {
        if messages.count > 0 {
            var lastItemIdx = self.tableView.numberOfRowsInSection(Constants.MessagesSection) - 1
            if lastItemIdx < 0 {
                lastItemIdx = 0
            }
            let lastIndexPath = NSIndexPath(forRow: lastItemIdx, inSection: Constants.MessagesSection)
            self.tableView.scrollToRowAtIndexPath(lastIndexPath, atScrollPosition: .Bottom, animated: false)
        }
    }
    
    
    
    
    // MARK: New messages
    func addNewMessage(message: LGChatMessage) {
        messages += [message]
        tableView.reloadData()
        self.scrollToBottom()
        self.delegate?.chatController?(self, didAddNewMessage: message)
    }
    
    // MARK: SwiftChatInputDelegate
    func chatInputDidResize(chatInput: LGChatInput) {
        self.scrollToBottom()
    }
    
    func chatInput(chatInput: LGChatInput, didSendMessage message: String) {
        let newMessage = LGChatMessage(content: message, sentBy: .User)
        //let userId = String(userDefaults.objectForKey("UserInfo")!.valueForKey("user_id")!)
        
        // let postString = "sender_id=\(userId)&receiver_id=\(opponentID)&message=\(message)"
        //let param : [String: AnyObject] = ["sender_id":userId,"receiver_id":opponentID,"message":message]
        //WebData().postMethod("send_message", postString: postString, success: #selector(webResponseSend(_:)), target: self,param: param , showHud : false)
        
        var str = message
        str = str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        //str = str.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        print(str)
        
        if str.characters.count < 1 {
            FTIndicator.showInfoWithMessage("Please enter message")
            return
        }
        
        self.callToSendMsg(message)
        
        var shouldSendMessage = true
        if let value = self.delegate?.shouldChatController?(self, addMessage: newMessage) {
            shouldSendMessage = value
        }
        
        if shouldSendMessage {
            self.addNewMessage(newMessage)
        }
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let padding: CGFloat = 10.0
        sizingCell.bounds.size.width = CGRectGetWidth(self.view.bounds)
        let height = self.sizingCell.setupWithMessage(messages[indexPath.row]).height + padding;
        return height
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.dragging {
            self.chatInput.textView.resignFirstResponder()
        }
        
        
        //         if (scrollView.contentOffset.y < 0) {
        //
        //            if isMoreDataLeft{
        //                self.loadMoreMsgs()
        //                isMoreDataLeft = false
        //            }
        //        }
        
        //         if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        //            if isMoreDataLeft{
        //                self.loadMoreMsgs()
        //                isMoreDataLeft = false
        //            }
        //        }
    }
    
    
    
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("identifier", forIndexPath: indexPath) as! LGChatMessageCell
        let message = self.messages[indexPath.row]
        cell.backgroundColor=UIColor.clearColor()
        cell.opponentImageView.contentMode = .ScaleAspectFill
        
        if chat.receiverImage != "" {
            let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(chat.receiverImage)"
            let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            
            cell.opponentImageView.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        }
        else {
            cell.opponentImageView.image = UIImage(named: "avatar.png")
        }
        
        
        //cell.opponentImageView.image = message.sentBy == .Opponent ? opponentImage : nil
        //cell.opponentImageView.image = chat.receiverImage
        cell.setupWithMessage(message)
        return cell;
    }
    
    ///////
    var arr_dic_msgs = [AnyObject]()
    
    func callToGetRecentChat(){
        var param_sender: String = ""
        var param_reciever: String = ""
        
        var userSenderId: String = ""
        var userReceiverId: String = ""
        var pageNo: String = ""
        
        //set param for sender
        if chat.senderType == Enumerations.USER_TYPE.user.rawValue || chat.senderType == Enumerations.USER_TYPE.host.rawValue {
            param_sender = URLParams.USER_SENDER_ID
        }
        else if chat.senderType == Enumerations.USER_TYPE.serviceProvider.rawValue{
            param_sender = URLParams.SERVICE_PROVIDER_SENDER_ID
            
        }
        
        //set param for reciver
        if chat.receiverType == Enumerations.USER_TYPE.user.rawValue || chat.receiverType == Enumerations.USER_TYPE.host.rawValue {
            param_reciever = URLParams.USER_RECEIVER_ID
        }
        else if chat.receiverType == Enumerations.USER_TYPE.serviceProvider.rawValue{
            param_reciever = URLParams.SERVICE_PROVIDER_RECEIVER_ID
        }
        
        //to set sender ID
        if let id = chat.senderId{
            userSenderId = id
        }
        
        //to set reciver ID
        if let id = chat.reciverId{
            userReceiverId = id
        }
        
        pageNo = String(pageNumber)
        
        let params = [
            param_sender: userSenderId ,
            param_reciever: userReceiverId ,
            URLParams.PAGE_NUMBER: pageNo
        ]
        
        print(params)
        if arr_dic_msgs.count < 1 {
            FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        }
        WebserviceUtils.callPostRequest(URLConstants.GET_RECENT_CHAT, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        let dataArray = json["chat"] as! NSArray
                        if (dataArray.count>0){
                            
                            if dataArray.count < 10 {
                                self.isMoreDataLeft = false
                                
                                if self.arr_dic_msgs.count > 0 {
                                    let dataArr_Reverse = self.arr_dic_msgs.reverse()
                                    for dict in dataArr_Reverse {
                                        print(dict)
                                        let dico : NSDictionary = dict as! NSDictionary
                                        self.webResponAllChat(dico)
                                        
                                    }
                                }
                                else {
                                    let dataArr_Reverse = dataArray.reverse()
                                    for dict in dataArr_Reverse {
                                        print(dict)
                                        let dico : NSDictionary = dict as! NSDictionary
                                        self.webResponAllChat(dico)
                                        
                                    }}
                                
                              FTIndicator.dismissProgress()
                                
                            }
                            else {
                                for item in dataArray {
                                    self.arr_dic_msgs.append(item)
                                }
                                self.isMoreDataLeft = true
                                self.loadMoreMsgs()
                                
                            }
                            
                            
                        }
                    }
                        //while error reveived
                    else {
                        let dataArr_Reverse = self.arr_dic_msgs.reverse()
                        for dict in dataArr_Reverse {
                            print(dict)
                            let dico : NSDictionary = dict as! NSDictionary
                            self.webResponAllChat(dico)
                        }
                    }
                }
                
            }
            }, failure: {
                (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    func loadMoreMsgs(){
        pageNumber += 1
        self.callToGetRecentChat()
    }
    
    
    
    
    
    func callToSendMsg(msg: String){
        var param_sender: String = ""
        var param_reciever: String = ""
        
        var userSenderId: String = ""
        var userReceiverId: String = ""
        var pageNo: String = ""
        var message: String = ""
        var authenticateToken = ""
        
        //set param for sender
        if chat.senderType == Enumerations.USER_TYPE.user.rawValue || chat.senderType == Enumerations.USER_TYPE.host.rawValue {
            param_sender = URLParams.USER_SENDER_ID
        }
        else if chat.senderType == Enumerations.USER_TYPE.serviceProvider.rawValue{
            param_sender = URLParams.SERVICE_PROVIDER_SENDER_ID
            
        }
        
        //set param for reciver
        if chat.receiverType == Enumerations.USER_TYPE.user.rawValue || chat.receiverType == Enumerations.USER_TYPE.host.rawValue {
            param_reciever = URLParams.USER_RECEIVER_ID
        }
        else if chat.receiverType == Enumerations.USER_TYPE.serviceProvider.rawValue{
            param_reciever = URLParams.SERVICE_PROVIDER_RECEIVER_ID
        }
        
        //to set sender ID
        if let id = chat.senderId{
            userSenderId = id
        }
        
        //to set reciver ID
        if let id = chat.reciverId{
            userReceiverId = id
        }
        //to set auth token
        if let token = chat.senderAuthToken{
            authenticateToken = token
        }
        
        message = msg
        
        
        
        //        if chat.receiverType == "host" {
        //            userSenderId = chat.senderId
        //            userReceiverId = chat.reciverId
        //            authenticateToken = chat.senderAuthToken
        //            params = [
        //                URLParams.USER_SENDER_ID: userSenderId ,
        //                URLParams.USER_RECEIVER_ID: userReceiverId ,
        //                URLParams.MESSAGE: msg,
        //                URLParams.AUTHENTICATE_TOKEN: authenticateToken
        //            ]
        //        }
        
        let params = [
            param_sender: userSenderId ,
            param_reciever: userReceiverId ,
            URLParams.MESSAGE: msg,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken
        ]
        
        print(params)
        WebserviceUtils.callPostRequest(URLConstants.SEND_MESSAGE, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        if let dic  = json.objectForKey("chat") as? NSDictionary {
                            //self.webResponseSend(dic)
                        }
                    }
                    
                }}
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
}

// MARK: Chat Input
protocol LGChatInputDelegate : class {
    func chatInputDidResize(chatInput: LGChatInput)
    func chatInput(chatInput: LGChatInput, didSendMessage message: String)
}

class LGChatInput : UIView, LGStretchyTextViewDelegate {
    
    // MARK: Styling
    struct Appearance {
        static var includeBlur = true
        static var tintColor = UIColor(red: 0.0, green: 120 / 255.0, blue: 255 / 255.0, alpha: 1.0)
        static var backgroundColor = UIColor(red: 10/255, green: 38/255, blue: 62/255, alpha: 1)
        static var textViewFont = UIFont(name: "Montserrat-Regular", size: 17)
        static var textViewTextColor = UIColor.darkTextColor()
        static var textViewBackgroundColor = UIColor.whiteColor()
    }
    
    /*
     These methods are included for ObjC compatibility.  If using Swift, you can set the Appearance variables directly.
     */
    
    class func setAppearanceIncludeBlur(includeBlur: Bool) {
        Appearance.includeBlur = includeBlur
    }
    
    class func setAppearanceTintColor(color: UIColor) {
        Appearance.tintColor = color
    }
    
    class func setAppearanceBackgroundColor(color: UIColor) {
        Appearance.backgroundColor = color
    }
    
    class func setAppearanceTextViewFont(textViewFont: UIFont) {
        Appearance.textViewFont = textViewFont
    }
    
    class func setAppearanceTextViewTextColor(textColor: UIColor) {
        Appearance.textViewTextColor = textColor
    }
    
    class func setAppearanceTextViewBackgroundColor(color: UIColor) {
        Appearance.textViewBackgroundColor = color
    }
    
    // MARK: Public Properties
    var textViewInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    weak var delegate: LGChatInputDelegate?
    
    // MARK: Private Properties
    private let textView = LGStretchyTextView(frame: CGRectZero, textContainer: nil)
    private let sendButton = UIButton(type: .System)
    private let blurredBackgroundView: UIToolbar = UIToolbar()
    private var heightConstraint: NSLayoutConstraint!
    private var sendButtonHeightConstraint: NSLayoutConstraint!
    
    // MARK: Initialization
    override init(frame: CGRect = CGRectZero) {
        super.init(frame: frame)
        self.setup()
        self.stylize()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup
    func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setupSendButton()
        self.setupSendButtonConstraints()
        self.setupTextView()
        self.setupTextViewConstraints()
        self.setupBlurredBackgroundView()
        self.setupBlurredBackgroundViewConstraints()
    }
    
    func setupTextView() {
        textView.bounds = UIEdgeInsetsInsetRect(self.bounds, self.textViewInsets)
        textView.stretchyTextViewDelegate = self
        textView.keyboardType = UIKeyboardType.Alphabet
        textView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
        self.styleTextView()
        self.addSubview(textView)
    }
    
    func styleTextView() {
        textView.layer.rasterizationScale = UIScreen.mainScreen().scale
        textView.layer.shouldRasterize = true
        textView.layer.cornerRadius = 5.0
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor(white: 0.0, alpha: 0.2).CGColor
    }
    
    func setupSendButton() {
        self.sendButton.enabled = false
        self.sendButton.setImage(UIImage(named: "send.png"), forState: .Normal)
        self.sendButton.addTarget(self, action: #selector(LGChatInput.sendButtonPressed(_:)), forControlEvents: .TouchUpInside)
        self.sendButton.bounds = CGRect(x: 0, y: 0, width: 40, height: 1)
        self.addSubview(sendButton)
    }
    
    func setupSendButtonConstraints() {
        self.sendButton.translatesAutoresizingMaskIntoConstraints = false
        self.sendButton.removeConstraints(self.sendButton.constraints)
        
        // TODO: Fix so that button height doesn't change on first newLine
        let rightConstraint = NSLayoutConstraint(item: self, attribute: .Right, relatedBy: .Equal, toItem: self.sendButton, attribute: .Right, multiplier: 1.0, constant: textViewInsets.right)
        let bottomConstraint = NSLayoutConstraint(item: self, attribute: .Bottom, relatedBy: .Equal, toItem: self.sendButton, attribute: .Bottom, multiplier: 1.0, constant: textViewInsets.bottom)
        let widthConstraint = NSLayoutConstraint(item: self.sendButton, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 40)
        sendButtonHeightConstraint = NSLayoutConstraint(item: self.sendButton, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 30)
        self.addConstraints([sendButtonHeightConstraint, widthConstraint, rightConstraint, bottomConstraint])
    }
    
    func setupTextViewConstraints() {
        self.textView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: self, attribute: .Top, relatedBy: .Equal, toItem: self.textView, attribute: .Top, multiplier: 1.0, constant: -textViewInsets.top)
        let leftConstraint = NSLayoutConstraint(item: self, attribute: .Left, relatedBy: .Equal, toItem: self.textView, attribute: .Left, multiplier: 1, constant: -textViewInsets.left)
        let bottomConstraint = NSLayoutConstraint(item: self, attribute: .Bottom, relatedBy: .Equal, toItem: self.textView, attribute: .Bottom, multiplier: 1, constant: textViewInsets.bottom)
        let rightConstraint = NSLayoutConstraint(item: self.textView, attribute: .Right, relatedBy: .Equal, toItem: self.sendButton, attribute: .Left, multiplier: 1, constant: -textViewInsets.right)
        heightConstraint = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .Height, multiplier: 1.00, constant: 40)
        self.addConstraints([topConstraint, leftConstraint, bottomConstraint, rightConstraint, heightConstraint])
    }
    
    func setupBlurredBackgroundView() {
        self.addSubview(self.blurredBackgroundView)
        self.sendSubviewToBack(self.blurredBackgroundView)
    }
    
    func setupBlurredBackgroundViewConstraints() {
        self.blurredBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: self, attribute: .Top, relatedBy: .Equal, toItem: self.blurredBackgroundView, attribute: .Top, multiplier: 1.0, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: self, attribute: .Left, relatedBy: .Equal, toItem: self.blurredBackgroundView, attribute: .Left, multiplier: 1.0, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: self, attribute: .Bottom, relatedBy: .Equal, toItem: self.blurredBackgroundView, attribute: .Bottom, multiplier: 1.0, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: self, attribute: .Right, relatedBy: .Equal, toItem: self.blurredBackgroundView, attribute: .Right, multiplier: 1.0, constant: 0)
        self.addConstraints([topConstraint, leftConstraint, bottomConstraint, rightConstraint])
    }
    
    // MARK: Styling
    func stylize() {
        self.textView.backgroundColor = Appearance.textViewBackgroundColor
        self.sendButton.tintColor = Appearance.tintColor
        self.textView.tintColor = Appearance.tintColor
        self.textView.font = Appearance.textViewFont
        self.textView.textColor = Appearance.textViewTextColor
        self.blurredBackgroundView.hidden = !Appearance.includeBlur
        self.backgroundColor = Appearance.backgroundColor
    }
    
    // MARK: StretchyTextViewDelegate
    func stretchyTextViewDidChangeSize(textView: LGStretchyTextView) {
        let textViewHeight = CGRectGetHeight(textView.bounds)
        if textView.text.characters.count == 0 {
            self.sendButtonHeightConstraint.constant = textViewHeight
        }
        let targetConstant = textViewHeight + textViewInsets.top + textViewInsets.bottom
        self.heightConstraint.constant = targetConstant
        self.delegate?.chatInputDidResize(self)
    }
    
    func stretchyTextView(textView: LGStretchyTextView, validityDidChange isValid: Bool) {
        self.sendButton.enabled = isValid
    }
    
    // MARK: Button Presses
    func sendButtonPressed(sender: UIButton) {
        if self.textView.text.characters.count > 0 {
            self.delegate?.chatInput(self, didSendMessage: self.textView.text)
            self.textView.text = ""
        }
        
    }
}

// MARK: Text View
@objc protocol LGStretchyTextViewDelegate {
    func stretchyTextViewDidChangeSize(chatInput: LGStretchyTextView)
    optional func stretchyTextView(textView: LGStretchyTextView, validityDidChange isValid: Bool)
}

class LGStretchyTextView : UITextView, UITextViewDelegate {
    
    // MARK: Delegate
    weak var stretchyTextViewDelegate: LGStretchyTextViewDelegate?
    
    // MARK: Public Properties
    var maxHeightPortrait: CGFloat = 160
    var maxHeightLandScape: CGFloat = 60
    var maxHeight: CGFloat {
        get {
            return UIInterfaceOrientationIsPortrait(UIApplication.sharedApplication().statusBarOrientation) ? maxHeightPortrait : maxHeightLandScape
        }
    }
    
    // MARK: Private Properties
    private var maxSize: CGSize {
        get {
            return CGSize(width: CGRectGetWidth(self.bounds), height: self.maxHeightPortrait)
        }
    }
    
    private var isValid: Bool = false {
        didSet {
            if isValid != oldValue {
                stretchyTextViewDelegate?.stretchyTextView?(self, validityDidChange: isValid)
            }
        }
    }
    
    private let sizingTextView = UITextView()
    
    // MARK: Property Overrides
    override var contentSize: CGSize {
        didSet {
            resize()
        }
    }
    
    override var font: UIFont! {
        didSet {
            sizingTextView.font = font
        }
    }
    
    override var textContainerInset: UIEdgeInsets {
        didSet {
            sizingTextView.textContainerInset = textContainerInset
        }
    }
    
    // MARK: Initializers
    override init(frame: CGRect = CGRectZero, textContainer: NSTextContainer? = nil) {
        super.init(frame: frame, textContainer: textContainer);
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup
    func setup() {
        font = UIFont.systemFontOfSize(17.0)
        textContainerInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        delegate = self
    }
    
    // MARK: Sizing
    
    func resize() {
        bounds.size.height = self.targetHeight()
        stretchyTextViewDelegate?.stretchyTextViewDidChangeSize(self)
    }
    
    func targetHeight() -> CGFloat {
        /*
         There is an issue when calling `sizeThatFits` on self that results in really weird drawing issues with aligning line breaks ("\n").  For that reason, we have a textView whose job it is to size the textView. It's excess, but apparently necessary.  If there's been an update to the system and this is no longer necessary, or if you find a better solution. Please remove it and submit a pull request as I'd rather not have it.
         */
        
        sizingTextView.text = self.text
        let targetSize = sizingTextView.sizeThatFits(maxSize)
        let targetHeight = targetSize.height
        let maxHeight = self.maxHeight
        return targetHeight < maxHeight ? targetHeight : maxHeight
    }
    
    // MARK: Alignment
    func align() {
        guard let end = self.selectedTextRange?.end, let caretRect: CGRect = self.caretRectForPosition(end) else { return }
        
        let topOfLine = CGRectGetMinY(caretRect)
        let bottomOfLine = CGRectGetMaxY(caretRect)
        
        let contentOffsetTop = self.contentOffset.y
        let bottomOfVisibleTextArea = contentOffsetTop + CGRectGetHeight(self.bounds)
        
        /*
         If the caretHeight and the inset padding is greater than the total bounds then we are on the first line and aligning will cause bouncing.
         */
        
        let caretHeightPlusInsets = CGRectGetHeight(caretRect) + self.textContainerInset.top + self.textContainerInset.bottom
        if caretHeightPlusInsets < CGRectGetHeight(self.bounds) {
            var overflow: CGFloat = 0.0
            if topOfLine < contentOffsetTop + self.textContainerInset.top {
                overflow = topOfLine - contentOffsetTop - self.textContainerInset.top
            } else if bottomOfLine > bottomOfVisibleTextArea - self.textContainerInset.bottom {
                overflow = (bottomOfLine - bottomOfVisibleTextArea) + self.textContainerInset.bottom
            }
            self.contentOffset.y += overflow
        }
    }
    
    // MARK: UITextViewDelegate
    func textViewDidChangeSelection(textView: UITextView) {
        self.align()
    }
    
    func textViewDidChange(textView: UITextView) {
        
        // TODO: Possibly filter spaces and newlines
        self.isValid = textView.text.characters.count > 0
    }
}
