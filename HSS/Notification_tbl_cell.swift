//
//  Notification_tbl_cell.swift
//  HSS
//
//  Created by Rajni on 27/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class Notification_tbl_cell: UITableViewCell {
    
    @IBOutlet weak var img_vw_notification: UIImageView!
    @IBOutlet weak var lbl_notification_title: UILabel!
    @IBOutlet weak var txtVw_Notification_descriptn: UITextView!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var vw_background: UIView!
    
}