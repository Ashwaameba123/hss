//
//  Notification.swift
//  HSS
//
//  Created by Rajni on 27/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class Notification: NSObject {
    
    var id: String!
    var propertyId: String!
    var userId: String!
    var userName: String!
    var userProfileImage: String!
    var updatedAt: String!
    var bookingStartDate: String!
    var bookingEndDate: String!
    var isCancel: String!
    var availableRoom: String!
    var totalRoom: String!
    var bookedRoom: String!
    var setPrice: String!
    var bookingType: String!
    var currency: String!
    var isApproved: String!
    var paymentWith: String!
    var hostId: String!
    
}