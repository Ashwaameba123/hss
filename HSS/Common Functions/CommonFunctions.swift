//
//  CommonFunctions.swift
//  HSS
//
//  Created by Rajni on 05/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit

class CommonFunctions {
    
    //globals
    struct globals {
        static var isHostAdd_EditProperty: Bool = false
        static var isUserBookedProperty:Bool = false
        static var isNeedToRefreshNotifications:Bool = false
        static var isNewRequestFrmUser: Bool = false
    }
    
    //notification chat
    struct chat {
        static var noti_chat: Chat_Model!
    }
    
    //notification new request
    struct NewRequest {
        static var Noti_obj: Notification!
    }
    
    
    
    //to check email validation
    class func isEmailValid(txtFeildString: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
       return emailTest.evaluateWithObject(txtFeildString)
    }
    
    //to round corner of view 
    class func makeRoundCornerOfView(view: UIView ){
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
    }
    
    // to round corner of button
    class func makeRoundCornerOfButton(button: UIButton ){
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        button.clipsToBounds = true
    }
    
    //to add border to view
    class func addBorderToView(view: UIView ){
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    //to get all countries calling codes
    class func getCountriesCallingCode() -> [String]{
     let arrCountries = DatabaseBLFunctions.fetchCountries()
        var arrCallingCodes = [String]()
        for item in arrCountries{
            arrCallingCodes.append(item.callingCode!)
        }
       return arrCallingCodes
    }
    
    class constants {
        static let MessageUserToMakePayment = "Host has approved your request to book the property. Make the payment and book this property now."
        static let HostApprovalPending = "Property booking request approval is pending from host side."
        static let MessageUserToForSuccessfullBooking = "Property has been successfully booked by you for the mentioned dates."
       static let BookingCancelledByUser = "Booking has been cancelled by the user. Proceed to refund the payment."
        static let BookingCancelledPaymentPending = "The payment refund has been in progress from host end."
       static let BookingApprovedPaymentPendingFromUser = "The payment is pending from user side yet."
        static let MessageForHostToApproveRequest = "You need to review the user property booking request to proceed."
        static let HostApprovalPendingMessage = "Property booking request approval is pending from your side. Click to view user request."
        static let PaymentWithAdminFromUserSide = "Payment is with Home Stay Safari admin and yet to be paied to host."
        static let PaymentWithAdminToHostSide = "Payment is with Home Stay Safari admin and yet to be paied to you."
        static let PaymentWithAdminToUserSide = "Host successfully refund the payment, Payment is with Home Stay Safari admin and yet to be paied to you back."
        static let PaymentWithAdminFromHost = "Payment is with Home Stay Safari admin and yet to be paied to user back."
        static let propertyBookedSuccess = "Property Successfully Booked."
        static let noPropertyFound = "No property found, according to your required search."
        
    }
    
    
   class func getDataFromUrl(url: NSURL, completion: (data: NSData?,  response: NSURLResponse?,  error: NSError?) -> Void) {
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {
            (data, response, error) -> Void in
            completion(data: data, response: response, error: error)
        })
        task.resume()
    }
    
    
    class func dateFromString (dateString : String) -> String{
        var date : String!
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dat : NSDate = dateFormatter.dateFromString(dateString)!
        dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        date = dateFormatter.stringFromDate(dat)
        return date
    }
    
   class func dateAndTimeFromString (dateString : String) -> String{
        var date : String!
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dat : NSDate = dateFormatter.dateFromString(dateString)!
        dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd,HH:mm"
        date = dateFormatter.stringFromDate(dat)
        return date
    }
    
    
    
}

