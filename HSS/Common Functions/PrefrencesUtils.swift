//
//  PrefrencesUtils.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation

class PrefrencesUtils {
    
    
    // MARK: Save Values
    class func saveStringToPrefs(key: String, value: String?){
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: key)
    }
    
    class func saveBoolToPrefs(key: String, value: Bool){
        NSUserDefaults.standardUserDefaults().setBool(value, forKey: key)
    }
    class func saveImageToPrefs(key: String, value: NSData){
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: key)
    }
    
    class func saveIntToPrefs(key: String, value: Int?){
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: key)
    }
    
    class func saveUserDataIntoPrefrences(key: String, value: Registration ) {
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: key)
    }
    
   
    
    
    
    // MARK: Get Values
    class func getStringFromPrefs(key:String) -> String? {
        return NSUserDefaults.standardUserDefaults().valueForKey(key) as? String
    }
    
    class func getBoolFromPrefs(key: String) -> Bool {
        return NSUserDefaults.standardUserDefaults().boolForKey(key)
    }
    class func getImageFromPrefs(key:String) -> NSData? {
        return NSUserDefaults.standardUserDefaults().valueForKey(key) as? NSData
    }
    
    class func getIntFromPrefs(key:String) -> Int? {
        return NSUserDefaults.standardUserDefaults().valueForKey(key) as? Int
    }
    
    class func getUserDataFromPrefrences(key:String) -> Registration? {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) as? Registration
    }
    
    
    
    
    
}
