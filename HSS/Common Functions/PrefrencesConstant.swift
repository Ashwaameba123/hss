//
//  PrefrencesConstant.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation

class PreferenceConstant {
    static let USER_TYPE = "userType"
    static let REGISTER_VIA = "registerVia"
    static let USER_ID = "userId"
    static let AUTHENTICATE_TOKEN = "authenticateToken"
    
    //user data
    static let USER_DATA = "userData"

    
    //social media credentials
    static let SOCIAL_MEDIA_ID = "id"
    static let SOCIAL_MEDIA_EMAIL = "email"
    static let SOCIAL_MEDIA_FIRST_NAME = "userFirstName"
    static let SOCIAL_MEDIA_LAST_NAME = "userLastName"
    
    //guest authenticate token
    static let GUEST_AUTHENTICATE_TOKEN = "guestAuthenticateToken"
    
    
    //service provider added by host
    static let IS_SERVICE_PROVIDER_ADDED_BY_HOST = "isServiceProviderAddedByHost"
    static let AUTHENTICATE_TOKEN_HOST = "authenticateTokenHost"
    static let HOST_ID = "hostID"
    static let HOST_PROPERTY_ID = "hostPropertyId"
    
    //get current location 
    static let CURRENT_LOCATION_NAME = "currentLocationName"
    
    //to check filter view active
    static let IS_FILTER_VIEW_ACTIVE = "isFilterViewActive"
    
    //device token
    static let DEVICE_TOKEN = "deviceToken"
    
    //default
    static let IS_DID_SELECT_MAP = "isDidSelectMap"
    
    

    

}
