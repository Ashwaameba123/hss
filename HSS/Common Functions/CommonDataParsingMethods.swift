//
//  CommonDataParsingMethods.swift
//  HSS
//
//  Created by Rajni on 11/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class CommonDataParsingMethods {
    
    class func saveDicToUserRegistrationType(dic: NSMutableDictionary) -> Registration{
        let user = Registration()
        
        if let applicationId = dic.objectForKey("applicationId") {
            user.applicationId = applicationId as! String
        }
        if let contactName = dic.objectForKey("contactName") {
            user.contactName = contactName as! String
        }
        if let countryCode = dic.objectForKey("countryCode") {
            user.countryCode = countryCode as! String
        }
        if let dateOfBirth = dic.objectForKey("DOB") {
            user.DOB = dateOfBirth as! String
        }
        if let deviceType = dic.objectForKey("deviceType") {
            user.deviceType = deviceType as! String
        }
        
        if let email = dic.objectForKey("emailId") {
            //condition for manual or social media registration
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) == Enumerations.REGISTER_VIA.manual.rawValue {
                user.email = email as! String
            }
            else {
                user.email = ""
            }
        }
        if let firstName = dic.objectForKey("firstName") {
            user.firstName = firstName as! String
        }
        if let gender = dic.objectForKey("gender") {
            user.gender = gender as! String
        }
        if let language = dic.objectForKey("language") {
            user.language = language as! String
        }
        if let lastName = dic.objectForKey("lastName") {
            user.lastName = lastName as! String
        }
        if let locationLatitude = dic.objectForKey("locationLatitude") {
            user.locationLatitude = locationLatitude as! String
        }
        if let locationLongitude = dic.objectForKey("locationLongitude") {
            user.locationLongitude = locationLongitude as! String
        }
        if let locationName = dic.objectForKey("LocationName") {
            user.locationName = locationName as! String
        }
        if let mobileNumber = dic.objectForKey("mobileNumber") {
            user.mobileNumber = mobileNumber as! String
        }
        if let nationality = dic.objectForKey("nationality") {
            user.nationality = nationality as! String
        }
        if let password = dic.objectForKey("password") {
            user.password = password as! String
        }
        if let userProfileImage = dic.objectForKey("userProfileImage") {
            user.userProfileImage = userProfileImage as! String
        }
        if let photoIdentity = dic.objectForKey("photoIdentity") {
            user.photoIdentity = photoIdentity as! String
        }
        if let registerVia = dic.objectForKey("registerVia") {
            user.registerVia = registerVia as! String
        }
        if let relationshipStatus = dic.objectForKey("relationshipStatus") {
            user.relationshipStatus = relationshipStatus as! String
        }
        if let socialMediaId = dic.objectForKey("socialMediaId") {
            user.socialMediaId = socialMediaId as! String
        }
        if let userId = dic.objectForKey("id") {
            user.userId = userId as! String
        }
        if let userType = dic.objectForKey("userType") {
            user.userType = userType as! String
        }
        if let authenticateToken = dic.objectForKey("authenticateToken") {
            user.authenticateToken = authenticateToken as! String
        }
        return user
    }
    
    
    
    class func saveDicToServiceProviderRegistrationType(dic: NSMutableDictionary) -> ServiceProviderRegistration{
        let user = ServiceProviderRegistration()
        
        if let DOB = dic.objectForKey("DOB") {
            user.DOB = DOB as! String
        }
        if let address = dic.objectForKey("address") {
            user.address = address as! String
        }
        if let authenticateToken = dic.objectForKey("authenticateToken") {
            user.authenticateToken = authenticateToken as! String
        }
        if let birthCeriticateOrPassportNo = dic.objectForKey("birthCeriticateOrPassportNo") {
            user.birthCeriticateOrPassportNo = birthCeriticateOrPassportNo as! String
        }
        if let companyId = dic.objectForKey("companyId") {
            user.companyId = companyId as! String
        }
        if let conductNo = dic.objectForKey("conductNo") {
            user.conductNo = conductNo as! String
        }
        if let countryCode = dic.objectForKey("countryCode") {
            user.countryCode = countryCode as! String
        }
        if let deviceType = dic.objectForKey("deviceType") {
            user.deviceType = deviceType as! String
        }
        if let driverLicenseExpiryDate = dic.objectForKey("driverLicenseExpiryDate") {
            user.driverLicenseExpiryDate = driverLicenseExpiryDate as! String
        }
        if let emailId = dic.objectForKey("emailId") {
            user.email = emailId as! String
        }
        if let firstName = dic.objectForKey("firstName") {
            user.firstName = firstName as! String
        }
        if let gender = dic.objectForKey("gender") {
            user.gender = gender as! String
        }
        if let id = dic.objectForKey("id") {
            user.userId = id as! String
        }
        if let issueNo = dic.objectForKey("issueNo") {
            user.issueNo = issueNo as! String
        }
        if let language = dic.objectForKey("language") {
            user.language = language as! String
        }
        if let lastName = dic.objectForKey("lastName") {
            user.lastName = lastName as! String
        }
        if let mobileNumber = dic.objectForKey("mobileNumber") {
            user.mobileNo = mobileNumber as! String
        }
        if let telephoneNo = dic.objectForKey("telephoneNumber") {
            user.telephoneNo = telephoneNo as! String
        }
        if let nationality = dic.objectForKey("nationality") {
            user.nationality = nationality as! String
        }
        if let password = dic.objectForKey("password") {
            user.password = password as! String
        }
        if let photoIdentity = dic.objectForKey("photoIdentity") {
            user.photoIdentityImage = photoIdentity as! String
        }
        if let postCode = dic.objectForKey("postCode") {
            user.postCode = postCode as! String
        }
        if let registerUnder = dic.objectForKey("registerUnder") {
            user.registerUnder = registerUnder as! String
        }
        
        if let registerVia = dic.objectForKey("registerVia") {
            user.registerVia = registerVia as! String
        }
        if let serviceProviderLatitude = dic.objectForKey("serviceProviderLatitude") {
            user.serviceProviderLatitude = serviceProviderLatitude as! String
        }
        if let serviceProviderLongitude = dic.objectForKey("serviceProviderLongitude") {
            user.serviceProviderLongitude = serviceProviderLongitude as! String
        }
        if let serviceTypeId = dic.objectForKey("serviceTypeId") {
            user.serviceTypeId = serviceTypeId as! String
        }
        if let serviceTypeName = dic.objectForKey("serviceTypeName") {
            user.serviceTypeName = serviceTypeName as! String
        }
        if let socialMediaId = dic.objectForKey("socialMediaId") {
            user.socialMediaId = socialMediaId as! String
        }
        if let userProfileImage = dic.objectForKey("userProfileImage") {
            user.userProfileImage = userProfileImage as! String
        }
        if let userType = dic.objectForKey("userType") {
            user.userType = userType as! String
        }
        
        if let website = dic.objectForKey("website") {
            user.website = website as! String
        }
        if let hostUserId = dic.objectForKey("hostUserId") {
            user.hostUserId = hostUserId as! String
        }
        
        
        return user
    }
    
    
    
   class func toParseNotificationDic(dic: NSDictionary) -> Notification {
        
        let obj = Notification()
        if let id = dic.objectForKey("id") as? String {
            obj.setValue(id, forKey: "id")
        }
        if let propertyId = dic.objectForKey("propertyId") as? String {
            obj.setValue(propertyId, forKey: "propertyId")
        }
        if let userId = dic.objectForKey("userId") as? String {
            obj.setValue(userId, forKey: "userId")
        }
        if let userName = dic.objectForKey("userName") as? String {
            obj.setValue(userName, forKey: "userName")
        }
        if let userProfileImage = dic.objectForKey("userProfileImage") as? String {
            obj.setValue(userProfileImage, forKey: "userProfileImage")
        }
        else {
            obj.setValue("", forKey: "userProfileImage")
        }
        if let updatedAt = dic.objectForKey("updatedAt") as? String {
            obj.setValue(updatedAt, forKey: "updatedAt")
        }
        
        if let bookingStartDate = dic.objectForKey("bookingStartDate") as? String {
            let date = CommonFunctions.dateFromString(bookingStartDate)
            obj.setValue(date, forKey: "bookingStartDate")
        }
        
        if let bookingEndDate = dic.objectForKey("bookingEndDate") as? String {
            let date = CommonFunctions.dateFromString(bookingEndDate)
            obj.setValue(date, forKey: "bookingEndDate")
        }
    if let isCancel = dic.objectForKey("isCancel") {
        let str = String(isCancel)
        if str == "0" {
            obj.setValue("false", forKey: "isCancel")
        }
        else {
            obj.setValue("true", forKey: "isCancel")
        }
    }
    if let isApproved = dic.objectForKey("isApproved") {
        let str = String(isApproved)
        if str == "0" {
            obj.setValue("false", forKey: "isApproved")
        }
        else {
            obj.setValue("true", forKey: "isApproved")
        }
    }
    if let availableRoom = dic.objectForKey("availableRoom") {
        let str = String(availableRoom)
        obj.setValue(str, forKey: "availableRoom")
    }
    if let hostId = dic.objectForKey("hostId") {
        let str = String(hostId)
        obj.setValue(str, forKey: "hostId")
    }
    if let totalRoom = dic.objectForKey("totalRoom") {
        let str = String(totalRoom)
        obj.setValue(str, forKey: "totalRoom")
    }
    if let bookedRoom = dic.objectForKey("bookedRoom") {
        let str = String(bookedRoom)
        obj.setValue(str, forKey: "bookedRoom")
    }
    if let setPrice = dic.objectForKey("setPrice") {
        let str = String(setPrice)
        obj.setValue(str, forKey: "setPrice")
    }
    if let bookingType = dic.objectForKey("bookingType") {
        let str = String(bookingType)
        obj.setValue(str, forKey: "bookingType")
    }
    if let currency = dic.objectForKey("currency") {
        let str = String(currency)
        obj.setValue(str, forKey: "currency")
    }
    if let paymentWith = dic.objectForKey("paymentWith") {
        let str = String(paymentWith)
        obj.setValue(str, forKey: "paymentWith")
    }
    
       return obj
    }
    
}