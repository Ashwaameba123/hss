//
//  PropertyReviewsViewController.swift
//  HSS
//
//  Created by Rajni on 21/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator
import Cosmos

class PropertyReviewsViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cosmoViewHalf: CosmosView!
    @IBOutlet weak var txtVwReview: UITextView!
    @IBOutlet var vwAddReview: UIView!
    
    
    //MARK:- VARIABLES
    var userData: UserRegistration!
    var userDataServiceProvider: ServiceProviderRegistrationDB!
    var property: PropertyDetailsModel!
    var arrPropertyReviews = [PropertyReview]()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var scrollView = UIScrollView(frame: CGRectMake(0, 0, 320, 300))
    var arrPropertyImagesName = [String]()
    var frame: CGRect = CGRectMake(0, 0, 0, 0)
    var pageControl : UIPageControl = UIPageControl(frame: CGRectMake(50, 300, 200, 50))
    var dynamicView:UIView = UIView()
    var ratingValue : Float = 0.0
    private var startRating:Float = 0.0
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    
    //- IMAGES
    let imgHome = UIImage(named: "home")
    let imgMessage = UIImage(named: "message")
    let imgNotification = UIImage(named: "notification")
    let imgSetting = UIImage(named: "setting")
    let imgLogout = UIImage(named: "logout")
    let imgAvtar = UIImage(named: "avatar")
    var height:CGFloat = 0.0

    
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = true
        
        //get array of property images
        self.toGetPropertyImagesArr()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        pageNumber = 0
        arrPropertyReviews = []
        //to get property reviews
        callToGetPropertyReviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- COSMO RATING METHODS
    private func updateRating() {
        let value = Double(ratingValue)
        cosmoViewHalf.rating = value
        
       // ratingValue = PropertyReviewsViewController
          //  .formatValue(value)
    }
    
    private class func formatValue(value: Double) -> String {
        return String(format: "%.2f", value)
    }
    
    private func didTouchCosmos(rating: Double) {
        ratingValue = Float(rating)
        print(ratingValue)
    }
    
    private func didFinishTouchingCosmos(rating: Double) {
        ratingValue = Float(rating)
        print(ratingValue)
    }
    
    
    //MARK:- CUSTOM METHODS
    func toGetPropertyImagesArr() {
        if let strImages = property.images {
            let imgNameArr = strImages.characters.split{$0 == ","}.map(String.init)
            
            print(imgNameArr.count)
            for imgName in imgNameArr{
                print(imgName)
                arrPropertyImagesName.append(imgName)
            }
            
        }
    }
    
    func toRefreshTable(){
        self.callToGetPropertyReviews()
    }
    
    func adjustUITextViewHeight(arg : UITextView) -> CGFloat
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.scrollEnabled = false
        let height = arg.frame.size.height
        
        return height
    }
    
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            print("reach bottom")
            
            if isMoreDataLeft{
                self.loadMoreReviews()
                isMoreDataLeft = false
            }
        }
    }
    
    func loadMoreReviews(){
        pageNumber += 1
        self.callToGetPropertyReviews()
    }
    
    //MARK:- WEB SERVICES
    
    func callToGetPropertyReviews(){
        var authenticateToken: String = ""
        var propertyId: String = ""
        let page_no: String = String(pageNumber)
        var userType: String = "serviceProvider"
        
        
        //if guest user 
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN) {
                authenticateToken = token
            }
            userType = Enumerations.USER_TYPE.guest.rawValue
        }
            
        //if service provider
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            
            if let token = userDataServiceProvider.authenticateToken {
                authenticateToken = token
            }
            userType = Enumerations.USER_TYPE.serviceProvider.rawValue
        }
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            if let token = userData.authenticateToken {
                authenticateToken = token
            }
            if let type = userData.userType {
                userType = type
            }
        }
        if let id = property.id {
            propertyId = id
        }
        
        
        
       let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PROPERTY_ID: propertyId,
            URLParams.PAGE_NUMBER: page_no,
            URLParams.USER_TYPE: userType
        ]
        
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_REVIEWS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success"){
                        
                        if let arr = json.objectForKey("propertyReview") as? NSArray {
                            if arr.count > 0 {
                                self.toParsePropertyReviews(arr)
                            }
                            else {
                                FTIndicator.showInfoWithMessage("No Reviews Found")
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                            
                        }
                    }
                    else {
                        if let msg = json.objectForKey("message") as? String
                        {
                        FTIndicator.showInfoWithMessage(msg)
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    
    func toParsePropertyReviews(arrReviews: NSArray) {
        
        print(arrReviews.count)
        if pageNumber == 0{
            arrPropertyReviews.removeAll()
        }
        
        
        for item in arrReviews {
            
            let propertyObj = PropertyReview()
            
            if let id = item.objectForKey("id") as? String {
                propertyObj.setValue(id, forKey: "id")
            }
            if let propertyId = item.objectForKey("propertyId") as? String {
                propertyObj.setValue(propertyId, forKey: "propertyId")
            }
            if let ratings = item.objectForKey("ratings") {
                let str: String = String(ratings)
                propertyObj.setValue(str , forKey: "ratings")
            }
            if let time = item.objectForKey("updatedAt") {
                let str: String = String(time)
                propertyObj.setValue(str , forKey: "time")
            }
            if let reviewMessage = item.objectForKey("reviewMessage") as? String {
                propertyObj.setValue(reviewMessage, forKey: "reviewMessage")
            }
            if let userId = item.objectForKey("userId") as? String {
                propertyObj.setValue(userId, forKey: "userId")
            }
            if let userName = item.objectForKey("userName") as? String {
                propertyObj.setValue(userName, forKey: "userName")
            }
            if let userProfileImage = item.objectForKey("userProfileImage") as? String {
                propertyObj.setValue(userProfileImage, forKey: "userProfileImage")
            }
            arrPropertyReviews.append(propertyObj)
            self.tableView.delegate = self
            self.tableView.dataSource = self
            
            if pageNumber == 0{
                self.tableView.reloadData()
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                self.tableView.beginUpdates()
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                self.tableView.endUpdates()
                
            }
            else {
            self.tableView.beginUpdates()
            self.tableView.insertRowsAtIndexPaths([
                        NSIndexPath(forRow: arrPropertyReviews.count-1, inSection: 0)
                        ], withRowAnimation: .Automatic)
            self.tableView.endUpdates()
            }
        }
        
    }
    
    
    func callToWritePropertyReviews(){
        var authenticateToken: String = ""
        var propertyId: String = ""
        var userID: String = ""
        var rating: String = ""
        
        
        if let token = userData.authenticateToken {
            authenticateToken = token
        }
        if let id = property.id {
            propertyId = id
        }
        if let userId  = userData.userId {
            userID = userId
        }
        
        let reviewText: String = txtVwReview.text!
        rating = "\(ratingValue)"
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PROPERTY_ID: propertyId,
            URLParams.USER_ID: userID,
            URLParams.RATING: rating,
            URLParams.REVIEW_MSG: reviewText
        ]
        
        print(params)
        
        //FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.WRITE_PROPERTY_REVIEW, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
               // FTIndicator.dismissProgress()
                if let status = json.objectForKey("status"){
                    if status as! String == "success"{
                        
                        
                        self.tableView.userInteractionEnabled = true
                        self.dynamicView.removeFromSuperview()
                        self.startRating = 0.0
                        self.toRefreshTable()
                    }
                }
                
            }
            }, failure: { (error) in
               // FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    
    
    //MARK:- ADD REVIEW BUTTON ACTIONS
        @IBAction func btnSubmitReviewClick(sender: UIButton) {
        
        var reviewTxt: String = txtVwReview.text!
        let trimmedString = reviewTxt.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        )
        reviewTxt = trimmedString
        
        if reviewTxt.characters.count > 0 && !reviewTxt.containsString("Write Review here") {
            callToWritePropertyReviews()
        }
        else {
            FTIndicator.showToastMessage("Please write review")
        }
        
    }
    @IBAction func btnCloseWriteReviewScreenClick(sender: UIButton) {
        self.tableView.userInteractionEnabled = true
        dynamicView.removeFromSuperview()
    }
    
    func btnWriteReviewClick(sender:UIButton!)
    {
        print("Button Clicked")
        
        dynamicView = UIView(frame: CGRectMake(0, 0, app.window!.bounds.size.width , app.window!.bounds.size.height))
        self.view.addSubview(dynamicView)
        self.tableView.userInteractionEnabled = false
        
        dynamicView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        vwAddReview.frame.size = CGSize(width: view.frame.size.width - 40 , height: 235)
        vwAddReview.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        vwAddReview.layer.cornerRadius = 8
        vwAddReview.clipsToBounds = true
        vwAddReview.center = dynamicView.center
        dynamicView.addSubview(vwAddReview)
        
        // Register touch handlers
        cosmoViewHalf.didTouchCosmos = didTouchCosmos
        cosmoViewHalf.didFinishTouchingCosmos = didFinishTouchingCosmos
        cosmoViewHalf.settings.fillMode = .Half
        
        ratingValue = startRating
        
        updateRating()
        
        
        //set placeholder to text view
        txtVwReview.text = "Write Review here"
        txtVwReview.textColor = UIColor.lightGrayColor()
        
    }
    
}


//MARK:- TABLE VIEW DELEGATES AND DATASOURCE
extension PropertyReviewsViewController : UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 50
        }
        else {
            return 170
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return arrPropertyReviews.count + 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
           let cell = tableView.dequeueReusableCellWithIdentifier("cellOne", forIndexPath: indexPath) as! ReviewsTableCell
            print("\(arrPropertyReviews.count)")
            let count: String = "\(arrPropertyReviews.count)"
            print(count)
            cell.lbl_reviews_count.text = count
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellTwo", forIndexPath: indexPath) as! ReviewsTableCell
            
            let review = arrPropertyReviews[indexPath.row - 1]
            
            cell.lblPersonImg.text = review.userName
            cell.txtVwUserReview.text = review.reviewMessage
            cell.txtVwUserReview.userInteractionEnabled = true
            
            
            //make round host image view
            cell.imgVwPersonImg.layer.cornerRadius = cell.imgVwPersonImg.frame.size.width / 2
            cell.imgVwPersonImg.clipsToBounds = true
            cell.imgVwPersonImg.layer.borderWidth = 1.0
            cell.imgVwPersonImg.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        
        // Register touch handlers
        cell.cosmoViewTblCell.settings.fillMode = .Half
        cell.cosmoViewTblCell.settings.updateOnTouch = false
        
        let numberFloatValue: Float = Float(review.ratings)!
       // print(numberFloatValue)
         cell.cosmoViewTblCell.rating = Double(numberFloatValue)
        
            
            //set time
            cell.lblTime.text = CommonFunctions.dateAndTimeFromString(review.time)
        
            //to set profile image
            let imageName = review.userProfileImage
            //print(imageName)
            
            let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
            print(urlString)
            let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
            cell.imgVwPersonImg.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
            
//            height = 0.0
//            height = self.adjustUITextViewHeight(cell.txtVwUserReview)
//            print(height)
            
             return cell
        }
    }
    
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        footerView.backgroundColor = UIColor.clearColor()
        
        let reviewBtn: UIButton = UIButton()
        reviewBtn.setTitle("WRITE REVIEW", forState: .Normal)
        reviewBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        reviewBtn.backgroundColor = Constants.Colors.GREEN_COLOUR
        reviewBtn.frame = CGRect(x:0,y:0,width:tableView.frame.size.width - 20,height:40)
        reviewBtn.layer.cornerRadius = 10
        reviewBtn.clipsToBounds = true
        reviewBtn.titleLabel!.font = UIFont.boldSystemFontOfSize(16)
        reviewBtn.addTarget(self, action: #selector(PropertyReviewsViewController.btnWriteReviewClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        reviewBtn.center = footerView.center
        footerView.addSubview(reviewBtn)
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            return nil
        }
        else {
            return footerView
        }
    }
    
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
                   return 0
        }
        else {
                    return 40.0
        }

    }
}

extension PropertyReviewsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        
        
        if textView != txtVwReview {
            textView.resignFirstResponder()
            return
        }
    
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write Review here"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
}