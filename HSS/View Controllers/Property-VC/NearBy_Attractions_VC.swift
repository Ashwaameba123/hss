//
//  NearBy_Attractions_VC.swift
//  HSS
//
//  Created by Rajni on 21/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class NearBy_Attractions_VC: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var table_view: UITableView!
    
    //MARK:- VARIABLES
    var arr_nearBY_att = [NearBy_Attrraction]()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.toSetValuesInNearByAtt()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
    }
    
    
    //MARK:- CUSTOM METHODS
    func toSetValuesInNearByAtt(){
        let obj_1 = NearBy_Attrraction()
        obj_1.locationName = "Elante Mall , Chandigarh"
        obj_1.loc_lat = "30.705587"
        obj_1.loc_long =  "76.801271"
        arr_nearBY_att.append(obj_1)
        
        let obj_2 = NearBy_Attrraction()
        obj_2.locationName = "Rock Garden , Chandigarh"
        obj_2.loc_lat = "30.752535"
        obj_2.loc_long =  "76.810104"
        arr_nearBY_att.append(obj_2)
        
        let obj_3 = NearBy_Attrraction()
        obj_3.locationName = "Sukhna Lake , Chandigarh"
        obj_3.loc_lat = "30.742138"
        obj_3.loc_long =  "76.818756"
        arr_nearBY_att.append(obj_3)
        
        let obj_4 = NearBy_Attrraction()
        obj_4.locationName = "The North Country Mall , Chandigarh"
        obj_4.loc_lat = "30.737710"
        obj_4.loc_long =  "76.678528"
        arr_nearBY_att.append(obj_4)
        
        self.table_view.reloadData()
    }
    
    
    //MARK:- TableView_DataSource & Delegates---------
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arr_nearBY_att.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("cell",forIndexPath: indexPath ) as! NearBy_attractions_table_cell
        
        let near_by = arr_nearBY_att[indexPath.row]
        
        cell.lbl_location_name.text = near_by.locationName
        cell.btn_view_on_map.tag = indexPath.row
        cell.btn_view_on_map.addTarget(self, action: #selector(NearBy_Attractions_VC.buttonViewOnMapClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        return cell
    }
    
    
    //MARK:- BUTTON ACTIONS
    func buttonViewOnMapClick(sender:UIButton!) {
        print("click map")
        let tag = sender.tag
        let map_vc = self.storyboard?.instantiateViewControllerWithIdentifier("Map_View_Controller") as! Map_View_Controller
        map_vc.isNearBYAtt = true
        map_vc.near_by_att_obj = arr_nearBY_att[tag]
        print(arr_nearBY_att[tag])
        self.navigationController?.pushViewController(map_vc, animated: true)
    }
}