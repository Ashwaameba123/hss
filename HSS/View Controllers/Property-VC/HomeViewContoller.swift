//
//  HomeViewContoller.swift
//  HSS
//
//  Created by Rajni on 06/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator
import Crashlytics
import SDWebImage
import LocationPicker
import SwiftRangeSlider

class HomeViewContoller:UIViewController , UIGestureRecognizerDelegate , UIPickerViewDelegate , UINavigationControllerDelegate , UIPickerViewDataSource {
    //MARK:- OUTLETS
    
    @IBOutlet weak var barBtnMenu: UIBarButtonItem!
    @IBOutlet weak var CollectionView: UICollectionView!
    
    @IBOutlet weak var barBtn_Filter: UIBarButtonItem!
    @IBOutlet weak var barBtn_map: UIBarButtonItem!
    @IBOutlet weak var btnAddItems: UIButton!
    
    ///--- search module
    @IBOutlet var vw_filter: UIView!
    @IBOutlet weak var txtField_location: UITextField!
    @IBOutlet weak var txtField_roomType: UITextField!
    @IBOutlet weak var txtField_check_IN: UITextField!
    @IBOutlet weak var txtField_check_out: UITextField!
    @IBOutlet weak var lbl_min_distance: UILabel!
    @IBOutlet weak var lbl_max_distance: UILabel!
    
    @IBOutlet weak var lbl_min_price: UILabel!
    
    @IBOutlet weak var lbl_max_price: UILabel!
    
    @IBOutlet weak var slider_distance: RangeSlider!
    
    @IBOutlet weak var slider_price: RangeSlider!
    
    @IBOutlet weak var vw_distance_back: UIView!
    
    @IBOutlet weak var vw_price_back: UIView!
    
    
    //MARK:- VARIABLES
    var arrUserData = [UserRegistration]()
    var arrUserDataServiceProvider = [ServiceProviderRegistrationDB]()
    var selectedProperty: PropertyDetailsModel!
    var arrProperties = [PropertyDetailsModel]()
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var dynamicView:UIView = UIView()
    let codeBtn : UIButton = UIButton()
    public var presentCountry = Country.currentCountry
    var arrRoomType = [RoomType]()
    var pickerView = UIPickerView()
    var univTxtField = UITextField()
    var userCoordinates : CLLocationCoordinate2D!
    var isFilterActive:Bool = false
    var isHostAddOrEditProperty: Bool = false
    var checkIndate: NSDate!
    
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            self.revealViewController().rearViewRevealWidth = 200
            barBtnMenu.target = self.revealViewController()
            barBtnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //while new message notification received
        if CommonFunctions.chat.noti_chat != nil
        {
            self.performSegueWithIdentifier("messages", sender: self)
        }
        
        //while new notification received
        if CommonFunctions.globals.isNewRequestFrmUser {
            self.performSegueWithIdentifier("notifications", sender: self)
        }
        
        
        pageNumber = 0
        arrProperties = []
        CommonFunctions.globals.isHostAdd_EditProperty = false
        
        
        //save initial value
        userCoordinates = CLLocationCoordinate2DMake(0.00000, 0.00000)
        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE, value: false)
        
        let screenWidth : CGFloat = UIScreen.mainScreen().bounds.width
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 5, bottom: 10, right: 5)
        layout.itemSize = CGSize(width: (screenWidth - 15) / 2, height: (screenWidth - 15) / 2)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        CollectionView!.collectionViewLayout = layout
        initView()
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        
        print(CommonFunctions.globals.isHostAdd_EditProperty)
        if CommonFunctions.globals.isHostAdd_EditProperty {
            pageNumber = 0
            self.toRefreshPropertyForHost()
            CommonFunctions.globals.isHostAdd_EditProperty = false
        }
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        //to check is filter view active or not
        if PrefrencesUtils.getBoolFromPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE) == true {
            if self.revealViewController() != nil {
                self.view.removeGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.navigationController?.navigationBar.userInteractionEnabled = false
            }
            PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE, value: false)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        univTxtField.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- To Refresh view
    func toRefreshPropertyForHost(){
        self.arrProperties.removeAll()
        self.callToGetPropertyListByHost()
    }
    
    
    //MARK:- WEB SERVICE
    func callToGetPropertyList(){
        var authenticateToken: String = ""
        
        let page_no: String = String(pageNumber)
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
        }
        
        
        //when guest user
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN) {
                authenticateToken = token
            }
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PAGE_NUMBER: page_no
        ]
        
        
        //FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_LIST, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                //FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success"){
                        if let arr = json.objectForKey("properties") as? NSArray {
                            
                            if arr.count > 0 {
                                self.toParseProperty(arr)
                            }
                            else {
                                FTIndicator.showInfoWithMessage("No Properties Found", userInteractionEnable: true)
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                        }
                        
                    }
                }
                
                
                
                
            }
            }, failure: { (error) in
                //FTIndicator.dismissProgress()
        })
        
        
    }
    
    func callToGetPropertyListByServiceProvider(){
        var authenticateToken: String = ""
        var userID: String = ""
        
        let page_no: String = String(pageNumber)
        
        if arrUserDataServiceProvider.count > 0 {
            authenticateToken = arrUserDataServiceProvider[0].authenticateToken!
        }
        
        if arrUserDataServiceProvider.count > 0 {
            userID = arrUserDataServiceProvider[0].userId!
        }
        
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.SERVICE_PROVIDER_ID: userID,
            URLParams.PAGE_NUMBER: page_no
        ]
        
        
        //FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_LIST_BY_USERID_FOR_SERVICE_PROVIDER, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                //FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success"){
                        if let arr = json.objectForKey("property") as? NSArray {
                            
                            if arr.count > 0 {
                                self.toParseProperty(arr)
                            }
                            else {
                                FTIndicator.showInfoWithMessage("No Properties Found", userInteractionEnable: true)
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                        }
                        
                    }
                    else {
                        
                        if let msg: String = json.objectForKey("message") as? String {
                            if msg.containsString("not exists"){
                                FTIndicator.showInfoWithMessage(msg)
                            }
                        }
                    }
                    
                }
                
                
                
                
                
                
                
            }
            }, failure: { (error) in
                //FTIndicator.dismissProgress()
        })
        
        
    }
    
    func callToGetPropertyListByHost(){
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().clearDisk()
        var authenticateToken: String = ""
        var userID: String = ""
        
        let page_no: String = String(pageNumber)
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userID = arrUserData[0].userId!
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken ,
            URLParams.USER_ID: userID,
            URLParams.PAGE_NUMBER: page_no
        ]
        
        //FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_LIST_BY_USERID, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                //FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String{
                    if status.containsString("success"){
                        if let arr = json.objectForKey("properties") as? NSArray {
                            if arr.count > 0 {
                                self.toParseProperty(arr)
                                
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                            
                        }
                    }
                    else{
                        if let msg = json.objectForKey("message") as? String {
                            if msg.containsString("No") {
                                //FTIndicator.showInfoWithMessage(msg, userInteractionEnable: true)
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                //FTIndicator.dismissProgress()
        })
    }
    
    //to get property type
    func callGetPropertyType(){
        let token = arrUserData[0].authenticateToken!
        let params = [
            URLParams.AUTHENTICATE_TOKEN: token
        ]
        
        
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_TYPE, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                let arr = json.objectForKey("propertyTypes")
                print(arr!.count)
                
                //save property type into database
                if arr!.count > 0 {
                    DatabaseBLFunctions.savePropertyType(arr as! NSArray)
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    func callGetRoomType(){
        var authentication_token: String = ""
        
        //when guest user
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN) {
                authentication_token = token
            }
        }
        else {
            authentication_token = arrUserData[0].authenticateToken!
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authentication_token
        ]
        
        WebserviceUtils.callPostRequest(URLConstants.GET_ROOM_TYPE, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                let arr = json.objectForKey("roomTypes")
                print(arr!.count)
                
                if arr!.count > 0 {
                    //save property type into database
                    DatabaseBLFunctions.saveRoomType(arr as! NSArray)
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    //MARK:- SERVICE RESPONSE PARSING METHODS
    func toParseProperty(arrProperty: NSArray) {
        for item in arrProperty {
            
            let propertyObj = PropertyDetailsModel()
            propertyObj.setValue("0.0000", forKey: "propertyLatitude")
            propertyObj.setValue("0.0000", forKey: "propertyLongitude")
            if let accommodation = item.objectForKey("accommodation") as? String {
                propertyObj.setValue(accommodation, forKey: "accommodation")
            }
            if let additionalMenities = item.objectForKey("additionalMenities") as? String {
                propertyObj.setValue(additionalMenities, forKey: "additionalMenties")
            }
            if let apartment = item.objectForKey("apartment") as? String {
                propertyObj.setValue(apartment, forKey: "apartment")
            }
            if let bathRoom = item.objectForKey("bathRoom") as? String {
                propertyObj.setValue(bathRoom, forKey: "bathRoom")
            }
            if let bedRoom = item.objectForKey("bedRoom") as? String {
                propertyObj.setValue(bedRoom, forKey: "bedRoom")
            }
            if let beds = item.objectForKey("beds") as? String {
                propertyObj.setValue(beds, forKey: "beds")
            }
            if let city = item.objectForKey("city") as? String {
                propertyObj.setValue(city, forKey: "city")
            }
            if let commomAmenities = item.objectForKey("commomAmenities") as? String {
                propertyObj.setValue(commomAmenities, forKey: "commonAmenties")
            }
            if let country = item.objectForKey("country") as? String {
                propertyObj.setValue(country, forKey: "country")
            }
            if let currency = item.objectForKey("currency") as? String {
                propertyObj.setValue(currency, forKey: "currency")
            }
            
            
            if let emergencyExit = item.objectForKey("emergencyExit")    {
                let emergency: String = "\(emergencyExit)"
                if emergency == "0"{
                    propertyObj.setValue("false" , forKey: "emergencyExit")
                }
                else {
                    propertyObj.setValue("true" , forKey: "emergencyExit")
                }
            }
            if let fireAlarm = item.objectForKey("fireAlarm")  {
                let alarm: String = "\(fireAlarm)"
                if alarm == "0"{
                    propertyObj.setValue("false" , forKey: "fireAlarm")
                }
                else {
                    propertyObj.setValue("true" , forKey: "fireAlarm")
                }
            }
            if let fireExtingusher = item.objectForKey("fireExtingusher")  {
                let fire: String = "\(fireExtingusher)"
                if fire == "0"{
                    propertyObj.setValue("false" , forKey: "fireExtinguisher")
                }
                else {
                    propertyObj.setValue("true" , forKey: "fireExtinguisher")
                }
            }
            if let gasShutoffValve = item.objectForKey("gasShutoffValve")  {
                let gasShut: String = "\(gasShutoffValve)"
                if gasShut == "0"{
                    propertyObj.setValue("false" , forKey: "gasShutoffValve")
                }
                else {
                    propertyObj.setValue("true" , forKey: "gasShutoffValve")
                }
            }
            if let homeSafety = item.objectForKey("homeSafety") as? String {
                propertyObj.setValue(homeSafety, forKey: "homeSafety")
            }
            if let id = item.objectForKey("id") as? String {
                propertyObj.setValue(id, forKey: "id")
            }
            if let profileName = item.objectForKey("profileName") as? String {
                propertyObj.setValue(profileName, forKey: "profileName")
            }
            if let propertyLatitude = item.objectForKey("propertyLatitude") as? String {
                propertyObj.setValue(propertyLatitude, forKey: "propertyLatitude")
            }
            
            if let propertyLongitude = item.objectForKey("propertyLongitude") as? String {
                propertyObj.setValue(propertyLongitude, forKey: "propertyLongitude")
            }
            
            if let propertyType = item.objectForKey("propertyType") as? String {
                propertyObj.setValue(propertyType, forKey: "propertyType")
            }
            if let roomType = item.objectForKey("roomType") as? String {
                propertyObj.setValue(roomType, forKey: "roomType")
            }
            if let setPrice = item.objectForKey("setPrice") {
                let str = String(setPrice)
                propertyObj.setValue(str, forKey: "setPrice")
            }
            if let specialFeature = item.objectForKey("specialFeature") as? String {
                propertyObj.setValue(specialFeature, forKey: "specialFeature")
            }
            if let state = item.objectForKey("state") as? String {
                propertyObj.setValue(state, forKey: "state")
            }
            if let streetAddress = item.objectForKey("streetAddress") as? String {
                propertyObj.setValue(streetAddress, forKey: "streetAddress")
            }
            if let summary = item.objectForKey("summary") as? String {
                propertyObj.setValue(summary, forKey: "summary")
            }
            if let typeOfBooking = item.objectForKey("typeOfBooking") as? String {
                propertyObj.setValue(typeOfBooking, forKey: "typeOfBooking")
            }
            if let userId = item.objectForKey("userId") as? String {
                propertyObj.setValue(userId, forKey: "userId")
            }
            if let zipCode = item.objectForKey("zipCode") as? String {
                propertyObj.setValue(zipCode, forKey: "zipCode")
            }
            if let userName = item.objectForKey("userName") as? String {
                propertyObj.setValue(userName, forKey: "hostName")
            }
            if let userProfileImage = item.objectForKey("userProfileImage") as? String {
                propertyObj.setValue(userProfileImage, forKey: "hostProfile")
            }
            if let furnishing = item.objectForKey("furnishing") as? String {
                propertyObj.setValue(furnishing, forKey: "furnishing")
            }
            if let cancellationPolicy = item.objectForKey("cancellationPolicy") as? String {
                propertyObj.setValue(cancellationPolicy, forKey: "cancellationPolicy")
            }
            
            //rating stars
            if let totalPropertyReview = item.objectForKey("totalPropertyReview") {
                let reviewCount = String(totalPropertyReview)
                propertyObj.setValue(reviewCount, forKey: "totalPropertyReview")
            }
            if let totalReviewCount = item.objectForKey("totalReviewCount") {
                let reviewCount = String(totalReviewCount)
                propertyObj.setValue(reviewCount, forKey: "totalReviewCount")
            }
            
            
            
            //to save images comma separated by name
            if let arr = item.objectForKey("propertyImages") as? NSArray {
                
                
                
                let mutString : NSMutableString = NSMutableString()
                for img in arr {
                    if let imgName = img.objectForKey("propertyImage") as? String {
                        mutString.appendString("\(imgName),")
                    }
                }
                
                var strImgName: String = mutString as String
                if strImgName.characters.count > 0 {
                    strImgName.removeAtIndex(strImgName.endIndex.predecessor())
                    
                    propertyObj.setValue("\(strImgName)", forKey: "images")
                }
                
                
                //to save property image array into model
                for img in arr {
                    
                    let imageObj = PropertyImage()
                    
                    if let id = img.objectForKey("id") as? String {
                        imageObj.id = id
                    }
                    if let propertyId = img.objectForKey("propertyId") as? String {
                        imageObj.propertyId = propertyId
                    }
                    if let propertyImage = img.objectForKey("propertyImage") as? String {
                        imageObj.propertyImage = propertyImage
                    }
                    if let userId = img.objectForKey("userId") as? String {
                        imageObj.userId = userId
                    }
                    
                    propertyObj.arrImages.append(imageObj)
                }
                
                if arr.count == 0 {
                    propertyObj.setValue("", forKey: "images")
                }
                
            }
            arrProperties.append(propertyObj)
            if pageNumber == 0{
                
                self.CollectionView.reloadData()
                
            }
            else{
                let insertIndexPath = NSIndexPath(forItem: arrProperties.count - 1, inSection: 0)
                CollectionView.insertItemsAtIndexPaths([insertIndexPath])
            }
            
            
        }
    }
    
    
    //MARK:- CUSTOM METHODS
    func initView(){
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue ||  PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        else if  PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            arrUserDataServiceProvider = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
        }
        
        let userType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)
        
        
        
        
        if userType == Enumerations.USER_TYPE.user.rawValue || userType == Enumerations.USER_TYPE.guest.rawValue  {
            self.callToGetPropertyList()
        }
        else if userType == Enumerations.USER_TYPE.host.rawValue {
            self.callToGetPropertyListByHost()
        }
        else if userType == Enumerations.USER_TYPE.serviceProvider.rawValue {
            self.callToGetPropertyListByServiceProvider()
        }
        
        
        
        if userType == Enumerations.USER_TYPE.host.rawValue {
            btnAddItems.userInteractionEnabled = true
            btnAddItems.hidden = false
        }
        else if userType == Enumerations.USER_TYPE.user.rawValue || userType == Enumerations.USER_TYPE.serviceProvider.rawValue {
            btnAddItems.hidden = true
            btnAddItems.userInteractionEnabled = false
        }
        else if userType == Enumerations.USER_TYPE.guest.rawValue {
            btnAddItems.hidden = true
            btnAddItems.userInteractionEnabled = false
        }
        
        self.title = "HOME"
        
        if arrUserData.count > 0 {
            //save user id
            if arrUserData[0].userId != "" && arrUserData[0].userId != nil {
                PrefrencesUtils.saveStringToPrefs(PreferenceConstant.USER_ID, value: arrUserData[0].userId)
            }
            else{
                PrefrencesUtils.saveStringToPrefs(PreferenceConstant.USER_ID, value: "")
            }
            
            //save authenticate token
            if arrUserData[0].authenticateToken != ""{
                PrefrencesUtils.saveStringToPrefs(PreferenceConstant.AUTHENTICATE_TOKEN, value: arrUserData[0].authenticateToken)
            }
            else{
                PrefrencesUtils.saveStringToPrefs(PreferenceConstant.AUTHENTICATE_TOKEN, value: "")
            }}
        
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            self.navigationItem.rightBarButtonItems = nil
        }
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            //call property type webservice
            if DatabaseBLFunctions.fetchRoomType().count < 1 {
                self.callGetRoomType()
                
            }
            //fetch data from DB and save it to local array
            if DatabaseBLFunctions.fetchRoomType().count > 0 {
                self.arrRoomType = DatabaseBLFunctions.fetchRoomType()
            }}
        
        
        //call room type webservice for user / guest / host
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            
            if DatabaseBLFunctions.fetchRoomType().count < 1 {
                self.callGetRoomType()
            }
        }
        
        //call to get property type for host
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            if DatabaseBLFunctions.fetchPropertyType().count < 1 {
                self.callGetPropertyType()
            }
        }
        
        
        
        
        
    }
    
    
    //MARK:- COLLECTION VIEW DATA SOURCE AND DELEGATES
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrProperties.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! HomeCollectionViewCell
        
        //make round host image view
        cell.imgPropertyHost.layer.cornerRadius = cell.imgPropertyHost.frame.size.width / 2
        cell.imgPropertyHost.clipsToBounds = true
        cell.imgPropertyHost.layer.borderWidth = 1.0
        cell.imgPropertyHost.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        
        //WHEN SERVICE PROVIDER ***************
        //        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
        //            cell.imgVwPlace.image = UIImage(named: "splash-5s.png")
        //            cell.imgPropertyHost.image = UIImage(named: "avatar.png")
        //            cell.lblLocationName.text! = "Chandigarh"
        //        }
        //        else {
        
        //URLConstants.BASE_URL
        let obj : PropertyDetailsModel = arrProperties[indexPath.row]
        if let imageName : String = obj.images as String {
            
            if imageName != "" {
                
                cell.activity_indicator.hidden = false
                cell.activity_indicator.startAnimating()
                let imgNameArr = imageName.characters.split{$0 == ","}.map(String.init)
                
                let str:String = imgNameArr[0]
                
                let urlString:String = "\(URLConstants.GET_PROPERTY_IMAGE)\(str)"
                let escapedAddress: String = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
                
                
                cell.imgVwPlace.sd_setImageWithURL(NSURL(string: escapedAddress)!, completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!)  -> Void in
                    if error != nil{
                        CommonFunctions.getDataFromUrl(NSURL(string: escapedAddress)!) { (data, response, error) in
                            dispatch_async(dispatch_get_main_queue()) {
                                
                                if let img = UIImage(data:data!){
                                    cell.imgVwPlace.image = img
                                }
                                else{
                                    cell.imgVwPlace.image = UIImage(named: "logoIcon")
                                }
                            }
                        }
                        print(error.description)
                    }
                    cell.activity_indicator.hidden = true
                    cell.activity_indicator.stopAnimating()
                    cell.imgVwPlace.image = image
                })
                
                
                
            }
            else {
                cell.imgVwPlace.image = UIImage(named: "logoIcon")
            }
        }
        
        
        
        //to set profile image
        let hostImgName = arrProperties[indexPath.row].hostProfile!
        
        let urlHostString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(hostImgName)"
        let escapedAddressHost = urlHostString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        
        cell.imgPropertyHost.sd_setImageWithURL(NSURL(string: escapedAddressHost!), placeholderImage:UIImage(named: "avatar.png"))
        
        //to show location name
        if let name = arrProperties[indexPath.row].streetAddress  {
            cell.txtVw_locationName.text! = name
        }
        
        //to set rating stars
        cell.vw_rating.settings.updateOnTouch = false
        cell.vw_rating.settings.fillMode = .Half
        var total_rating: Double = 0.0
        var total_count: Double = 0.0
        var average:Double = 0.0
        
        if let total = arrProperties[indexPath.row].totalPropertyReview {
            total_rating = Double(total)!
        }
        if let count = arrProperties[indexPath.row].totalReviewCount {
            total_count = Double(count)!
        }
        
        if total_count != 0.0 {
            average = total_rating / total_count
        }
        
        print(average)
        
        cell.vw_rating.rating = average
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = UIScreen.mainScreen().bounds.size
        
        // 8 - space between 3 collection cells
        // 4 - 4 times gap will appear between cell.
        return CGSize(width: (size.width - 3 * 8)/2, height: 220
        )
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        selectedProperty = arrProperties[indexPath.row]
        
        //save property details locally
        Constants.selectedProperty.selectedPropertyDetails = self.selectedProperty
        
        //move to detail screen
        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) != Enumerations.USER_TYPE.guest.rawValue {
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                show_Detail_obj.userDataServiceProvider = arrUserDataServiceProvider[0]
            }
            else {
                show_Detail_obj.userData = arrUserData[0]
            }
        }
        
        show_Detail_obj.property = selectedProperty
        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
        
    }
    
    
    //MARK:- BUTTON ACTIONS
    
    
    @IBAction func btnAddItemsClick(sender: AnyObject) {
        
        let add_property = self.storyboard?.instantiateViewControllerWithIdentifier("AddPropertyViewController") as! AddPropertyViewController
        self.navigationController?.pushViewController(add_property, animated: true)
    }
    
    
    @IBAction func btnMapVCClick(sender: AnyObject) {
        
        let map_preprty = self.storyboard?.instantiateViewControllerWithIdentifier("Property_Map_Controller") as! Property_Map_Controller
        map_preprty.mapProperties = arrProperties
        self.navigationController?.pushViewController(map_preprty, animated: true)
    }
    
    
    @IBAction func btnFilterClick(sender: AnyObject) {
        //FTIndicator.showInfoWithMessage("Under Development")
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            self.openFilterView()
        }
    }
    
    @IBAction func btn_close_filter_click(sender: UIButton) {
        vw_filter.removeFromSuperview()
        dynamicView.removeFromSuperview()
        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE, value: false)
        self.navigationController?.navigationBar.userInteractionEnabled = true
        isFilterActive = false
    }
    
    @IBAction func btn_search_click(sender: UIButton) {
        vw_filter.removeFromSuperview()
        dynamicView.removeFromSuperview()
        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE, value: false)
        self.navigationController?.navigationBar.userInteractionEnabled = true
        arrProperties.removeAll()
        self.callToGetPropertyListByFilter()
        
    }
    
    //slider actions
    @IBAction func slider_distance_action(sender: RangeSlider) {
        let min = Int(sender.lowerValue)
        lbl_min_distance.text = String(min) + "Km"
        
        let max = Int(sender.upperValue)
        lbl_max_distance.text = String(max) + "Km"
        
        if sender.lowerValue == sender.upperValue {
            sender.lowerValue = 200
            let min = Int(sender.lowerValue)
            lbl_min_distance.text = String(min) + "Km"
            let seconds = 0.6
            let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
            let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            
            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                
                // here code perfomed with delay
                sender.upperValue = 500
                let max = Int(sender.upperValue)
                self.lbl_max_distance.text = String(max) + "Km"
            })
            
            
        }
        
    }
    
    @IBAction func slider_price_action(sender: RangeSlider) {
        let min = Int(sender.lowerValue)
        lbl_min_price.text = String(min)
        
        let max = Int(sender.upperValue)
        lbl_max_price.text = String(max)
        
        if sender.lowerValue == sender.upperValue {
            sender.lowerValue = 200
            let min = Int(sender.lowerValue)
            lbl_min_price.text = String(min)
            let seconds = 0.6
            let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
            let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            
            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                
                // here code perfomed with delay
                sender.upperValue = 500
                let max = Int(sender.upperValue)
                self.lbl_max_price.text = String(max)
            })
        }
    }
    
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            
            if isMoreDataLeft{
                self.loadMoreProperty()
                isMoreDataLeft = false
            }
        }
    }
    
    func loadMoreProperty(){
        
        if isFilterActive{
            pageNumber += 1
            self.callToGetPropertyListByFilter()
        }
        else {
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
                pageNumber += 1
                self.callToGetPropertyListByHost()
            }
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
                pageNumber += 1
                self.callToGetPropertyList()
            }
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                pageNumber += 1
                self.callToGetPropertyListByServiceProvider()
            }}
        
    }
    
    //MARK:- FILTER
    func callToGetPropertyListByFilter(){
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().clearDisk()
        
        var locationName:String = ""
        var locationLatitude:String = ""
        var locationLongitude:String = ""
        var roomType:String = ""
        var checkInDate:String = ""
        var checkOutDate:String = ""
        var minPrice:String = ""
        var maxPrice:String = ""
        var minDistance:String = ""
        var maxDistance:String = ""
        var authenticateToken: String = ""
        var userType: String = ""
        
        
        if let lName = txtField_location.text {
            locationName = lName
        }
        if let lat: Double = userCoordinates.latitude {
            locationLatitude = String(lat)
        }
        if let long: Double = userCoordinates.longitude {
            locationLongitude = String(long)
        }
        if let rType = txtField_roomType.text {
            roomType = rType
        }
        if let checkIn = txtField_check_IN.text {
            checkInDate = checkIn
        }
        if let checkOut = txtField_check_out.text {
            checkOutDate = checkOut
        }
        if let pMin = lbl_min_price.text {
            minPrice = pMin
        }
        if let pMax = lbl_max_price.text {
            maxPrice = pMax
        }
        if let dMin = lbl_min_distance.text {
            minDistance = dMin
        }
        if let dMax = lbl_max_distance.text {
            maxDistance = dMax
        }
        
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userType = arrUserData[0].userType!
        }
        
        
        //when guest user
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN) {
                authenticateToken = token
            }
            userType = "guest"
        }
        
        
        
        let page_no: String = String(pageNumber)
        
        let params = [
            URLParams.LOCATION_NAME: locationName,
            URLParams.LOCATION_LATITUDE: locationLatitude,
            URLParams.LOCATION_LONGITUDE: locationLongitude,
            URLParams.ROOM_TYPE: roomType,
            URLParams.CHECK_IN_DATE: checkInDate,
            URLParams.CHECK_OUT_DATE: checkOutDate,
            URLParams.MIN_PRICE: minPrice,
            URLParams.MAX_PRICE: maxPrice,
            URLParams.MIN_DISTANCE: minDistance,
            URLParams.MAX_DISTANCE: maxDistance,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken ,
            URLParams.USER_TYPE: userType,
            URLParams.PAGE_NUMBER: page_no
        ]
        
        print(params)
        
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_LIST_BY_FILTER, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                FTIndicator.dismissProgress()
                
                print(json)
                if let status = json.objectForKey("status") as? String{
                    if status.containsString("success"){
                        if let arr = json.objectForKey("searchedProperties") as? NSArray {
                            
                            print("*******************" , arr.count)
                            if arr.count > 0 {
                                self.toParseProperty(arr)
                                
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                            
                        }
                    }
                    else{
                        if let msg = json.objectForKey("message") as? String {
                            if msg.containsString("No") {
                                FTIndicator.showInfoWithMessage(msg, userInteractionEnable: true)
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
        })
    }
    
    //MARK:- TO GENEARTE SEARCH VIEW CUSTOM METHODS
    func openFilterView(){
        print("Button Clicked")
        
        //initial all values blank
        let Locale = NSLocale(localeIdentifier: "en_US")
        let country = Locale.displayNameForKey(NSLocaleCountryCode, value: presentCountry.countryCode)!
        print("MY Country : \(country)")
        
        if NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") != nil{
            let locationDictionary : NSDictionary = NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") as! NSDictionary
            
            let locationName : String = locationDictionary["locationName"] as! String
            let locationPinCode: String = locationDictionary["pincode"] as! String
            let streetAddress: String = locationDictionary["streetAddress"] as! String
            let city: String = locationDictionary["city"] as! String
            let state: String = locationDictionary["state"] as! String
            let latitude: String = locationDictionary["latitude"] as! String
            let longitude: String = locationDictionary["longitude"] as! String
            userCoordinates = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            //remove comma
            let strAddress = String(streetAddress.characters.dropFirst())
            print("my street address" , strAddress)
            
            txtField_location.text = strAddress
        }
        txtField_roomType.text = ""
        txtField_check_IN.text = ""
        txtField_check_out.text = ""
        slider_distance.lowerValue = 50
        slider_distance.upperValue = 1000
        lbl_min_distance.text = "0 Km"
        lbl_max_distance.text = "1000 Km"
        slider_price.lowerValue = 50
        slider_price.upperValue = 1000
        lbl_min_price.text = "0"
        lbl_max_price.text = "1000"
        pageNumber = 0
        isFilterActive = true
        
        
        
        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE, value: true)
        
        
        dynamicView = UIView(frame: CGRectMake(0, 0, app.window!.bounds.size.width , app.window!.bounds.size.height))
        self.view.addSubview(dynamicView)
        
        dynamicView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        vw_filter.frame = CGRect(x: 8, y: 20, width: view.frame.size.width - 14, height: 480)
        //vw_filter.frame.size = CGSize(width: view.frame.size.width - 14 , height: 460)
        vw_filter.backgroundColor = UIColor.whiteColor()
        vw_filter.layer.cornerRadius = 8
        vw_filter.clipsToBounds = true
        // vw_filter.center = dynamicView.center
        
        self.vw_price_back.layer.borderWidth = 1
        self.vw_price_back.layer.borderColor = UIColor.grayColor().CGColor
        self.vw_distance_back.layer.borderWidth = 1
        self.vw_distance_back.layer.borderColor = UIColor.grayColor().CGColor
        self.dropIcon(txtField_roomType)
        self.rtIcon(txtField_location, imageName: "location-dark")
        self.rtIcon(txtField_roomType, imageName: "location-dark")
        self.rtIcon(txtField_check_IN, imageName: "calender")
        self.rtIcon(txtField_check_out, imageName: "calender")
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        txtField_roomType.inputView = pickerView
        
        
        //        //add tap gesture
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewContoller.handleTap(_:)))
        //        tap.delegate = self
        //        dynamicView.addGestureRecognizer(tap)
        
        dynamicView.addSubview(vw_filter)
        
        if self.revealViewController() != nil {
            self.view.removeGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.navigationController?.navigationBar.userInteractionEnabled = false
        }
        
        
    }
    
    //    func handleTap(sender: UITapGestureRecognizer? = nil) {
    //        // handling code
    //        vw_filter.removeFromSuperview()
    //        dynamicView.removeFromSuperview()
    //        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_FILTER_VIEW_ACTIVE, value: false)
    //        self.navigationController?.navigationBar.userInteractionEnabled = true
    //    }
    
    func rtIcon(textField : UITextField , imageName : String) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 40, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(10, 10, 20, 20)
        imgView.image = UIImage(named: imageName)
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        rtPad.addSubview(imgView)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddPropertyViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on drop down")
        
        switch tappedImageView.tag {
        case 1:
            // txtFieldPropertyType.becomeFirstResponder()
            print("invalid selection")
            
        default:
            print("invalid selection")
            
        }
    }
    
    func addDatePicker(textField: UITextField){
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
        let todayDate = NSDate()
        let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
        let minimumYear = (components?.year)! - 1917
        let minimumMonth = (components?.month)! - 1
        let minimumDay = (components?.day)! - 1
        let comps = NSDateComponents();
        comps.year = -minimumYear
        comps.month = -minimumMonth
        comps.day = -minimumDay
        let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        let maxYear = (components?.year)! - 2017
        let maxMonth = (components?.month)! - 1
        let maxDay = (components?.day)! - 1
        let maxcomps = NSDateComponents();
        maxcomps.year = -maxYear
        maxcomps.month = -maxMonth
        maxcomps.day = -maxDay
        let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        //to set minimum date
        if textField == txtField_check_IN{
            datePicker.minimumDate = maxDate
        }
        
        //to set minimum date for check out
        if textField == txtField_check_out{
            datePicker.minimumDate = checkIndate
        }
        
        //datePicker.minimumDate = minDate
        //datePicker.maximumDate = maxDate
        
        
        textField.inputView = datePicker
    }
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        univTxtField.text! = formattor.stringFromDate(sender.date)
        
        if univTxtField == txtField_check_IN {
            checkIndate = sender.date
        }
        print("Selected value \(univTxtField.text!)")
        
    }
    
    func openLocationPicker(textField: UITextField){
        
        let locationPicker = LocationPickerViewController()
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        locationPicker.currentLocationButtonBackground = Constants.Colors.BACKGROUND_COLOUR
        locationPicker.showCurrentLocationInitially = true // default: true
        locationPicker.mapType = .Standard // default: .Hybrid
        
        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false
        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
        
        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
        locationPicker.resultRegionDistance = 500 // default: 600
        
        locationPicker.completion = { location in
            
            
            let locationName : String = (location?.address)!
            self.userCoordinates  = (location?.coordinate)!
            
            let countryName : String = (location?.placemark.country)!
            
            textField.text = locationName
            //self.txtFieldNationality.text = countryName
            
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        
        univTxtField = textField
        
        print("called")
        if textField == txtField_location {
            openLocationPicker(textField)
        }
        if textField == txtField_check_IN {
            addDatePicker(txtField_check_IN)
        }
        if textField == txtField_check_out {
            addDatePicker(txtField_check_out)
        }
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if univTxtField == txtField_roomType {
            return arrRoomType.count
        }
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if univTxtField == txtField_roomType {
            return arrRoomType[row].name
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let indexPath = NSIndexPath(forRow: univTxtField.tag , inSection: 0)
        print(indexPath)
        
        if univTxtField == txtField_roomType {
            txtField_roomType.text = arrRoomType[row].name
        }
    }
    
    
}
