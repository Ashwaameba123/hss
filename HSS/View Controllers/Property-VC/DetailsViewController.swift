//
//  DetailsViewController.swift
//  HSS
//
//  Created by Rajni on 06/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator
import SDWebImage
import JTSImageViewController

class DetailsViewController:UIViewController , UIScrollViewDelegate , UIPickerViewDelegate , UINavigationControllerDelegate , UIPickerViewDataSource  {
    
    //MARK:- OUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    //outlets for check booking
    @IBOutlet var vw_check_booking: UIView!
    @IBOutlet weak var txtField_checkIN_date: UITextField!
    @IBOutlet weak var txtField_checkOUT_date: UITextField!
    @IBOutlet weak var txtField_NOOfRooms: UITextField!
    
    //MARK:- VARIABLES
    var userData: UserRegistration!
    var userDataServiceProvider: ServiceProviderRegistrationDB!
    var property: PropertyDetailsModel!
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var scrollView = UIScrollView(frame: CGRectMake(0, 0, 320, 300))
    var arrPropertyImagesName = [String]()
    var frame: CGRect = CGRectMake(0, 0, 0, 0)
    var pageControl : UIPageControl = UIPageControl(frame: CGRectMake(50, 300, 200, 50))
    var dynamicView:UIView = UIView()
    var pickerView = UIPickerView()
    var univTxtField = UITextField()
    var arrRoomNo = [String]()
    var selectedP_ImgVw: UIImageView!
    var arr_available_dates = [PropertyAvailableDate]()
    var noti_property_id: String!
    var checkIndate: NSDate!
    var isToShowViaNoti:Bool = false
    
    
    //- IMAGES
    let imgHome = UIImage(named: "home")
    let imgMessage = UIImage(named: "message")
    let imgNotification = UIImage(named: "notification")
    let imgSetting = UIImage(named: "setting")
    let imgLogout = UIImage(named: "logout")
    let imgAvtar = UIImage(named: "avatar")
    var arr_p_images = [UIImage]()
    
    
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        CommonFunctions.globals.isUserBookedProperty = false
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        self.title = "Property Details"
        let attr = NSDictionary(object: UIFont(name: "Montserrat-Regular", size: 10.0)!, forKey: NSFontAttributeName)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject : AnyObject] , forState: .Normal)
        
        if property == nil {
            callToGetPropertyDetailsById()
        }
        else {
            //get array of property images
            self.toGetPropertyImagesArr()
            self.toGetNoOfRooms()
        }
        
        if CommonFunctions.globals.isUserBookedProperty {
            vw_check_booking.removeFromSuperview()
            dynamicView.removeFromSuperview()
            self.navigationController?.navigationBar.userInteractionEnabled = true
            CommonFunctions.globals.isUserBookedProperty = false
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- WEB SERVICES
    func callToGetPropertyDetailsById(){
        var propertyID: String = ""
        var auth_token: String = ""
        
        if let p_id = noti_property_id {
            propertyID = p_id
        }
        
        //to fetch user data
        let arr = DatabaseBLFunctions.fetchUserRegistrationData()
        if arr.count > 0 {
            if let token = arr[0].authenticateToken {
                auth_token = token
            }
        }
        
        
        let params = [
            URLParams.PROPERTY_ID: propertyID,
            URLParams.AUTHENTICATE_TOKEN: auth_token
        ]
        print(params)
        
        
        WebserviceUtils.callPostRequest(URLConstants.TO_GET_PARTICULAR_PROPERTY_DETAILS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        
                        if let dic = json.objectForKey("properties") as? NSDictionary{
                            self.toParseProperty(dic)
                        }
                        //get array of property images
                        self.toGetPropertyImagesArr()
                        self.toGetNoOfRooms()
                        self.tableView.reloadData()
                        
                    }}}
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    
    //MARK:- TO PARSE WEB SERVICE RESPONSE
    
    func toParseProperty(dic: NSDictionary) {
        
        let propertyObj = PropertyDetailsModel()
        propertyObj.setValue("0.0000", forKey: "propertyLatitude")
        propertyObj.setValue("0.0000", forKey: "propertyLongitude")
        
        if let accommodation = dic.objectForKey("accommodation") as? String {
            propertyObj.setValue(accommodation, forKey: "accommodation")
        }
        if let additionalMenities = dic.objectForKey("additionalMenities") as? String {
            propertyObj.setValue(additionalMenities, forKey: "additionalMenties")
        }
        if let apartment = dic.objectForKey("apartment") as? String {
            propertyObj.setValue(apartment, forKey: "apartment")
        }
        if let bathRoom = dic.objectForKey("bathRoom") as? String {
            propertyObj.setValue(bathRoom, forKey: "bathRoom")
        }
        if let bedRoom = dic.objectForKey("bedRoom") as? String {
            propertyObj.setValue(bedRoom, forKey: "bedRoom")
        }
        if let beds = dic.objectForKey("beds") as? String {
            propertyObj.setValue(beds, forKey: "beds")
        }
        if let city = dic.objectForKey("city") as? String {
            propertyObj.setValue(city, forKey: "city")
        }
        if let commomAmenities = dic.objectForKey("commomAmenities") as? String {
            propertyObj.setValue(commomAmenities, forKey: "commonAmenties")
        }
        if let country = dic.objectForKey("country") as? String {
            propertyObj.setValue(country, forKey: "country")
        }
        if let currency = dic.objectForKey("currency") as? String {
            propertyObj.setValue(currency, forKey: "currency")
        }
        
        
        if let emergencyExit = dic.objectForKey("emergencyExit")    {
            let emergency: String = "\(emergencyExit)"
            if emergency == "0"{
                propertyObj.setValue("false" , forKey: "emergencyExit")
            }
            else {
                propertyObj.setValue("true" , forKey: "emergencyExit")
            }
        }
        if let fireAlarm = dic.objectForKey("fireAlarm")  {
            let alarm: String = "\(fireAlarm)"
            if alarm == "0"{
                propertyObj.setValue("false" , forKey: "fireAlarm")
            }
            else {
                propertyObj.setValue("true" , forKey: "fireAlarm")
            }
        }
        if let fireExtingusher = dic.objectForKey("fireExtingusher")  {
            let fire: String = "\(fireExtingusher)"
            if fire == "0"{
                propertyObj.setValue("false" , forKey: "fireExtinguisher")
            }
            else {
                propertyObj.setValue("true" , forKey: "fireExtinguisher")
            }
        }
        if let gasShutoffValve = dic.objectForKey("gasShutoffValve")  {
            let gasShut: String = "\(gasShutoffValve)"
            if gasShut == "0"{
                propertyObj.setValue("false" , forKey: "gasShutoffValve")
            }
            else {
                propertyObj.setValue("true" , forKey: "gasShutoffValve")
            }
        }
        if let homeSafety = dic.objectForKey("homeSafety") as? String {
            propertyObj.setValue(homeSafety, forKey: "homeSafety")
        }
        if let id = dic.objectForKey("id") as? String {
            propertyObj.setValue(id, forKey: "id")
        }
        if let profileName = dic.objectForKey("profileName") as? String {
            propertyObj.setValue(profileName, forKey: "profileName")
        }
        if let propertyLatitude = dic.objectForKey("propertyLatitude") as? String {
            propertyObj.setValue(propertyLatitude, forKey: "propertyLatitude")
        }
        
        if let propertyLongitude = dic.objectForKey("propertyLongitude") as? String {
            propertyObj.setValue(propertyLongitude, forKey: "propertyLongitude")
        }
        
        if let propertyType = dic.objectForKey("propertyType") as? String {
            propertyObj.setValue(propertyType, forKey: "propertyType")
        }
        if let roomType = dic.objectForKey("roomType") as? String {
            propertyObj.setValue(roomType, forKey: "roomType")
        }
        if let setPrice = dic.objectForKey("setPrice") as? String {
            propertyObj.setValue(setPrice, forKey: "setPrice")
        }
        if let specialFeature = dic.objectForKey("specialFeature") as? String {
            propertyObj.setValue(specialFeature, forKey: "specialFeature")
        }
        if let state = dic.objectForKey("state") as? String {
            propertyObj.setValue(state, forKey: "state")
        }
        if let streetAddress = dic.objectForKey("streetAddress") as? String {
            propertyObj.setValue(streetAddress, forKey: "streetAddress")
        }
        if let summary = dic.objectForKey("summary") as? String {
            propertyObj.setValue(summary, forKey: "summary")
        }
        if let typeOfBooking = dic.objectForKey("typeOfBooking") as? String {
            propertyObj.setValue(typeOfBooking, forKey: "typeOfBooking")
        }
        if let userId = dic.objectForKey("userId") as? String {
            propertyObj.setValue(userId, forKey: "userId")
        }
        if let zipCode = dic.objectForKey("zipCode") as? String {
            propertyObj.setValue(zipCode, forKey: "zipCode")
        }
        if let userName = dic.objectForKey("userName") as? String {
            propertyObj.setValue(userName, forKey: "hostName")
        }
        if let userProfileImage = dic.objectForKey("userProfileImage") as? String {
            propertyObj.setValue(userProfileImage, forKey: "hostProfile")
        }
        if let furnishing = dic.objectForKey("furnishing") as? String {
            propertyObj.setValue(furnishing, forKey: "furnishing")
        }
        if let cancellationPolicy = dic.objectForKey("cancellationPolicy") as? String {
            propertyObj.setValue(cancellationPolicy, forKey: "cancellationPolicy")
        }
        
        //rating stars
        if let totalPropertyReview = dic.objectForKey("totalPropertyReview") {
            let reviewCount = String(totalPropertyReview)
            propertyObj.setValue(reviewCount, forKey: "totalPropertyReview")
        }
        if let totalReviewCount = dic.objectForKey("totalReviewCount") {
            let reviewCount = String(totalReviewCount)
            propertyObj.setValue(reviewCount, forKey: "totalReviewCount")
        }
        
        
        
        //to save images comma separated by name
        if let arr = dic.objectForKey("propertyImages") as? NSArray {
            
            
            
            let mutString : NSMutableString = NSMutableString()
            for img in arr {
                if let imgName = img.objectForKey("propertyImage") as? String {
                    mutString.appendString("\(imgName),")
                }
            }
            
            var strImgName: String = mutString as String
            if strImgName.characters.count > 0 {
                strImgName.removeAtIndex(strImgName.endIndex.predecessor())
                
                propertyObj.setValue("\(strImgName)", forKey: "images")
            }
            
            
            //to save property image array into model
            for img in arr {
                
                let imageObj = PropertyImage()
                
                if let id = img.objectForKey("id") as? String {
                    imageObj.id = id
                }
                if let propertyId = img.objectForKey("propertyId") as? String {
                    imageObj.propertyId = propertyId
                }
                if let propertyImage = img.objectForKey("propertyImage") as? String {
                    imageObj.propertyImage = propertyImage
                }
                if let userId = img.objectForKey("userId") as? String {
                    imageObj.userId = userId
                }
                
                propertyObj.arrImages.append(imageObj)
            }
            
            if arr.count == 0 {
                propertyObj.setValue("", forKey: "images")
            }
            
        }
        
        property = propertyObj
    }
    
    
    
    
    
    //MARK:- CUSTOM METHODS
    func toGetNoOfRooms(){
        arrRoomNo.removeAll()
        if let count = property.accommodation {
            let c:Int = Int(count)!
            print(c)
            
            for i in 1  ... c  {
                arrRoomNo.append(String(i))
            }
        }
    }
    
    func toGetPropertyImagesArr() {
        arrPropertyImagesName.removeAll()
        if let strImages = property.images {
            let imgNameArr = strImages.characters.split{$0 == ","}.map(String.init)
            
            print(imgNameArr.count)
            for imgName in imgNameArr{
                print(imgName)
                arrPropertyImagesName.append(imgName)
            }
            
        }
    }
    
    func configurePageControl(numbers : Int,yPosition : CGFloat) {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRectMake(0, 120, app.window!.bounds.size.width , 50))
        self.pageControl.numberOfPages = numbers
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.redColor()
        self.pageControl.pageIndicatorTintColor = UIColor.blackColor()
        self.pageControl.currentPageIndicatorTintColor = UIColor.greenColor()
    }
    
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPointMake(x, 0), animated: true)
    }
    
    func toShowAvailableDatesVC(){
        let avail_dates_vc = self.storyboard?.instantiateViewControllerWithIdentifier("Available_Dates_For_Property_VC") as! Available_Dates_For_Property_VC
        avail_dates_vc.userID = userData.userId
        avail_dates_vc.property = self.property
        avail_dates_vc.arr_available_dates = self.arr_available_dates
        self.navigationController?.pushViewController(avail_dates_vc, animated: true)
    }
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    
    //MARK:- SEGMENT CONTROL ACTION
    @IBAction func SegmentControlAction(sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            print("information")
        case 1:
            let review_obj = self.storyboard?.instantiateViewControllerWithIdentifier("PropertyReviewsViewController") as! PropertyReviewsViewController
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) != Enumerations.USER_TYPE.guest.rawValue {
                if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                    review_obj.userDataServiceProvider = self.userDataServiceProvider
                }
                else {
                    review_obj.userData = self.userData
                }
            }
            review_obj.property = self.property
            print("property to pass" , property)
            
            self.navigationController?.pushViewController(review_obj, animated: true)
            sender.selectedSegmentIndex = 0
        case 2:
            
            FTIndicator.showInfoWithMessage("static enteries")
            let nearBy_vc_obj = self.storyboard?.instantiateViewControllerWithIdentifier("NearBy_Attractions_VC") as! NearBy_Attractions_VC
            self.navigationController?.pushViewController(nearBy_vc_obj, animated: true)
            sender.selectedSegmentIndex = 0
            
        default:
            break;
        }
        
    }
    
    
    //MARK:- CHECK BOOKING  BUTTON ACTIONS
    @IBAction func btn_OK_click(sender: UIButton) {
        if toCheckBookingValidation() {
            self.callCheckPropertyBooking()
        }
    }
    
    @IBAction func btn_cancel_click(sender: UIButton) {
        vw_check_booking.removeFromSuperview()
        dynamicView.removeFromSuperview()
        self.navigationController?.navigationBar.userInteractionEnabled = true
    }
    
    
    //MARK:- VALIDATIONS
    func toCheckBookingValidation() -> Bool {
        if txtField_checkIN_date.text?.characters.count < 1 {
            FTIndicator.showToastMessage("Please select check in date")
            return false
        }
        if txtField_checkOUT_date.text?.characters.count < 1 {
            FTIndicator.showToastMessage("Please select check out date")
            return false
        }
        if txtField_NOOfRooms.text?.characters.count < 1 {
            FTIndicator.showToastMessage("Please select no. of rooms")
            return false
        }
        
        
        return true
    }
    
    
    
    //MARK:- TO GENERATE CHECK BOOKING VIEW
    func openCheckBookingView(){
        print("Button Clicked")
        
        //initial all values blank
        txtField_NOOfRooms.text = ""
        txtField_checkIN_date.text = ""
        txtField_checkOUT_date.text = ""
        
        dynamicView = UIView(frame: CGRectMake(0, 0, app.window!.bounds.size.width , app.window!.bounds.size.height))
        self.view.addSubview(dynamicView)
        
        dynamicView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        //vw_filter.frame = CGRect(x: 8, y: 20, width: view.frame.size.width - 14, height: 480)
        vw_check_booking.frame.size = CGSize(width: view.frame.size.width - 14 , height: 192)
        vw_check_booking.backgroundColor = UIColor.lightGrayColor()
        vw_check_booking.layer.cornerRadius = 8
        vw_check_booking.clipsToBounds = true
        vw_check_booking.center = dynamicView.center
        
        self.dropIcon(txtField_NOOfRooms)
        self.rtIcon(txtField_checkIN_date, imageName: "calender")
        self.rtIcon(txtField_checkOUT_date, imageName: "calender")
        self.rtIcon(txtField_NOOfRooms, imageName: "room")
        
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        txtField_NOOfRooms.inputView = pickerView
        
        
        dynamicView.addSubview(vw_check_booking)
        
        self.navigationController?.navigationBar.userInteractionEnabled = false
        
    }
    
    
    func rtIcon(textField : UITextField , imageName : String) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 40, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(10, 10, 20, 20)
        imgView.image = UIImage(named: imageName)
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        rtPad.addSubview(imgView)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddPropertyViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on drop down")
        
        switch tappedImageView.tag {
        case 1:
            txtField_NOOfRooms.becomeFirstResponder()
            print("invalid selection")
            
        default:
            print("invalid selection")
            
        }
    }
    
    
    func addDatePicker(textField: UITextField){
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
        let todayDate = NSDate()
        let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
        let minimumYear = (components?.year)! - 1917
        let minimumMonth = (components?.month)! - 1
        let minimumDay = (components?.day)! - 1
        let comps = NSDateComponents();
        comps.year = -minimumYear
        comps.month = -minimumMonth
        comps.day = -minimumDay
        let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        let maxYear = (components?.year)!
        let maxMonth = (components?.month)!
        let maxDay = (components?.day)!
        let maxcomps = NSDateComponents();
        maxcomps.year = -maxYear
        maxcomps.month = -maxMonth
        maxcomps.day = -maxDay
        let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        
        //to set minimum date
        if textField == txtField_checkIN_date{
            datePicker.minimumDate = maxDate
        }
        
        //to set minimum date for check out
        if textField == txtField_checkOUT_date{
            datePicker.minimumDate = checkIndate
        }
        
        
        //datePicker.minimumDate = maxDate
        // datePicker.maximumDate = maxDate
        
        
        textField.inputView = datePicker
    }
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        univTxtField.text! = formattor.stringFromDate(sender.date)
        
        if univTxtField == txtField_checkIN_date {
            checkIndate = sender.date
        }
        print("Selected value \(univTxtField.text!)")
        
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        
        univTxtField = textField
        
        print("called")
        if textField == txtField_checkIN_date || textField == txtField_checkOUT_date {
            addDatePicker(textField)
        }
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if univTxtField == txtField_NOOfRooms {
            return arrRoomNo.count
        }
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if univTxtField == txtField_NOOfRooms {
            return arrRoomNo[row]
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTxtField == txtField_NOOfRooms {
            txtField_NOOfRooms.text = arrRoomNo[row]
        }
    }
    
    //MARK:- WEB SERVICE TO CHECK PROPERTY BOOKING
    func callCheckPropertyBooking(){
        var propertyID: String = ""
        var startDate: String = ""
        var endDate: String = ""
        var avail_room: String = ""
        
        
        if let id = property.id{
            propertyID = id
        }
        if let date = txtField_checkIN_date.text {
            startDate = date
        }
        if let date = txtField_checkOUT_date.text{
            endDate = date
        }
        if let room = txtField_NOOfRooms.text {
            avail_room = room
        }
        
        
        
        let params = [
            URLParams.PROPERTY_ID: propertyID,
            URLParams.CHECK_IN_DATE: startDate,
            URLParams.CHECK_OUT_DATE: endDate,
            URLParams.AVAILABLE_ROOM: avail_room
        ]
        print(params)
        FTIndicator.showInfoWithMessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.CHECK_PROPERTY_BOOKING, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        if let arr = json.objectForKey("bookedProperty") as? NSArray {
                            self.toParsePropertyAvailableDates(arr)
                        }
                        self.toShowAvailableDatesVC()
                    }
                    else {
                        self.callToBookProperty()
                    }
                    
                }
                
                
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    func toParsePropertyAvailableDates(arr: NSArray) {
        
        print(arr.count)
        self.arr_available_dates = []
        
        for item in arr {
            
            let propertyObj = PropertyAvailableDate()
            
            if let availableRoom = item.objectForKey("availableRoom") {
                let str: String = String(availableRoom)
                propertyObj.setValue(str , forKey: "availableRoom")
            }
            if let bookedRoom = item.objectForKey("bookedRoom") {
                let str: String = String(bookedRoom)
                propertyObj.setValue(str , forKey: "bookedRoom")
            }
            if let bookingEndDate = item.objectForKey("bookingEndDate") as? String {
                propertyObj.setValue(bookingEndDate, forKey: "bookingEndDate")
            }
            if let bookingStartDate = item.objectForKey("bookingStartDate") as? String {
                propertyObj.setValue(bookingStartDate, forKey: "bookingStartDate")
            }
            if let bookingType = item.objectForKey("bookingType") as? String {
                propertyObj.setValue(bookingType, forKey: "bookingType")
            }
            if let id = item.objectForKey("id") as? String {
                propertyObj.setValue(id, forKey: "id")
            }
            if let totalRoom = item.objectForKey("totalRoom") {
                let str: String = String(totalRoom)
                propertyObj.setValue(str , forKey: "totalRoom")
            }
            if let userId = item.objectForKey("userId") as? String {
                propertyObj.setValue(userId, forKey: "userId")
            }
            
            arr_available_dates.append(propertyObj)
        }
        
    }
    
    
    func callToBookProperty(){
        var propertyID: String = ""
        var startDate: String = ""
        var endDate: String = ""
        var bookingType: String = ""
        var totalRoom: String = ""
        var bookedRoom: String = ""
        var userId: String = ""
        var hostId: String = ""
        var avail_room: String = ""
        var price: String = ""
        var currency: String = ""
        
        
        
        
        
        
        
        if let id = property.id{
            propertyID = id
        }
        if let date = txtField_checkIN_date.text {
            startDate = date
        }
        if let date = txtField_checkOUT_date.text{
            endDate = date
        }
        if let id = userData.userId {
            userId = id
        }
        if let type = property.typeOfBooking {
            bookingType = type
        }
        if let total_room = property.accommodation {
            totalRoom = total_room
        }
        if let booked = txtField_NOOfRooms.text {
            bookedRoom = booked
        }
        if let id = property.userId {
            hostId = id
        }
        if let pri = property.setPrice {
            price = pri
        }
        if let curren = property.currency {
            currency = curren
        }
        
        let total:Int = Int(totalRoom)!
        let booked:Int = Int(bookedRoom)!
        let avail = (total - booked)
        avail_room = String(avail)
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.PROPERTY_ID: propertyID,
            URLParams.BOOKING_START_DATE: startDate,
            URLParams.BOOKING_END_DATE: endDate,
            URLParams.BOOKING_TYPE: bookingType,
            URLParams.TOTAL_ROOM: totalRoom,
            URLParams.BOOKED_ROOM: bookedRoom,
            URLParams.HOST_ID: hostId,
            URLParams.AVAILABLE_ROOM: avail_room,
            URLParams.SET_PRICE: price,
            URLParams.CURRENCY: currency
        ]
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.TO_BOOK_PROPERTY, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        
                        if self.property.typeOfBooking.containsString("review") {
                            FTIndicator.showInfoWithMessage("Your request has been sent to host for review.")
                        }
                        else {
                            FTIndicator.showInfoWithMessage("Property has been booked.")
                        }
                        self.vw_check_booking.removeFromSuperview()
                        self.dynamicView.removeFromSuperview()
                        self.navigationController?.navigationBar.userInteractionEnabled = true
                    }
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
}

//MARK:- TABLE VIEW DELEGATES AND DATASOURCE
extension DetailsViewController : UITableViewDelegate, UITableViewDataSource  {
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 160.0
        }
        else if indexPath.row == 1 {
            return 46.0
        }
        else if indexPath.row == 2 {
            
            return 89.0
        }
        else if indexPath.row == 3 {
            return 787.0
        }
        else {
            return 175.0
        }
    }
    
    func imageTapped(img: AnyObject)
    {
        //        let index = pageControl.currentPage
        //
        //        if let selectedImg:UIImage = arr_p_images[index]{
        //            let imageInfo = JTSImageInfo()
        //            imageInfo.image = selectedImg
        //            imageInfo.referenceRect = selectedP_ImgVw.frame
        //            imageInfo.referenceView = selectedP_ImgVw.superview
        //            imageInfo.title = "ID"
        //            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
        //            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
        //        }
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if property != nil {
            return 5
        }
        return 0
    }
    /////
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellImage", forIndexPath: indexPath) as! DetailTableCell
            
            let customView: UIView = UIView(frame: CGRectMake(0, 0, app.window!.bounds.size.width , 160))
            
            //        //set shadow
            customView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            customView.layer.shadowOpacity = 1
            customView.layer.shadowOffset = CGSize.zero
            customView.layer.shadowRadius = 2
            customView.layer.shadowPath = UIBezierPath(rect: customView.bounds).CGPath
            customView.layer.shouldRasterize = true
            
            
            scrollView = UIScrollView(frame: CGRectMake(0, 0, app.window!.bounds.size.width , 160))
            scrollView.delegate = self
            //            let subViews = self.scrollView.subviews
            //            for subview in subViews{
            //                subview.removeFromSuperview()
            //            }
            arr_p_images.removeAll()
            self.view.addSubview(scrollView)
            
            
            var imageView: UIImageView!
            var frm : CGRect = CGRect(x: 0, y: 0,width: UIScreen.mainScreen().bounds.width,height: self.scrollView.frame.size.height)
            var imagesCount : Int = 0
            if arrPropertyImagesName.count < 1 {
                frame.size = self.scrollView.frame.size
                self.scrollView.showsHorizontalScrollIndicator = false
                self.scrollView.pagingEnabled = false
                
                imageView = UIImageView(frame: frm)
                imageView.image = UIImage(named:"avatar.png")
                imageView.contentMode = UIViewContentMode.ScaleAspectFit
                imageView.userInteractionEnabled = true
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.imageTapped(_:)))
                imageView.addGestureRecognizer(tapRecognizer)
                selectedP_ImgVw = imageView
                // arr_p_images.append(imageView.image!)
                self.scrollView.addSubview(imageView)
            }
            
            for index in 0..<arrPropertyImagesName.count {
                frm.origin.x = self.scrollView.frame.size.width * CGFloat(index)
                frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
                frame.size = self.scrollView.frame.size
                self.scrollView.showsHorizontalScrollIndicator = false
                self.scrollView.pagingEnabled = true
                
                imageView = UIImageView(frame: frm)
                imageView.image = UIImage(named:"logo.png")
                imagesCount = imagesCount + 1
                imageView.contentMode = UIViewContentMode.ScaleAspectFit
                imageView.userInteractionEnabled = true
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.imageTapped(_:)))
                imageView.addGestureRecognizer(tapRecognizer)
                
                let str:String = arrPropertyImagesName[index]
                let urlString:String = "\(URLConstants.BASE_URL)/HSSImages/propertyImages/\(str)"
                let escapedAddress: String = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
                print("addressssssssss",escapedAddress)
                //imageView.sd_setImageWithURL(NSURL(string: escapedAddress), placeholderImage:UIImage(named:"logo.png"))
                
                imageView.sd_setImageWithURL(NSURL(string: escapedAddress)!, completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!)  -> Void in
                    if error != nil{
                        CommonFunctions.getDataFromUrl(NSURL(string: escapedAddress)!) { (data, response, error) in
                            dispatch_async(dispatch_get_main_queue()) {
                                
                                if let img = UIImage(data:data!){
                                    imageView.image = img
                                }
                                else{
                                    imageView.image = UIImage(named: "logoIcon")
                                }
                            }
                        }
                        print(error.description)
                    }
                    imageView.image = image
                })
                selectedP_ImgVw = imageView
                //arr_p_images.append(imageView.image!)
                self.scrollView.addSubview(imageView)
                
            }
            self.scrollView.backgroundColor = UIColor.whiteColor()
            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * CGFloat(imagesCount), self.scrollView.frame.size.height)
            pageControl.addTarget(self, action: #selector(DetailsViewController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
            let yPosition : CGFloat = scrollView.frame.size.height - 50
            customView.addSubview(scrollView)
            if imagesCount > 1{
                configurePageControl(imagesCount, yPosition: yPosition)
                customView.addSubview(pageControl)
            }
            
            cell.addSubview(customView)
            
            //set the data here
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellOne", forIndexPath: indexPath) as! DetailTableCell
            
            //set the data here
            return cell
        }
        else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCellWithIdentifier("cellTwo", forIndexPath: indexPath) as! DetailTableCell
            
            //****describe host
            if let hostName = property.hostName {
                cell.lblHostName.text! = hostName
            }
            cell.lbl_host_email.text = ""
            
            //make round host image view
            cell.imgVwPropertyHost.layer.cornerRadius = cell.imgVwPropertyHost.frame.size.width / 2
            cell.imgVwPropertyHost.clipsToBounds = true
            cell.imgVwPropertyHost.layer.borderWidth = 1.0
            cell.imgVwPropertyHost.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
            cell.imgVwPropertyHost.contentMode = .ScaleAspectFit
            cell.btn_details.addTarget(self, action: #selector(DetailsViewController.buttonDetailsClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            //to set profile image
            let hostImgName = property.hostProfile!
            print(hostImgName)
            
            let urlHostString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(hostImgName)"
            print(urlHostString)
            let escapedAddressHost = urlHostString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            print(escapedAddressHost)
            
            cell.imgVwPropertyHost.sd_setImageWithURL(NSURL(string: escapedAddressHost!), placeholderImage:UIImage(named: "avatar.png"))
            
            return cell
            
        }
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellThree", forIndexPath: indexPath) as! DetailTableCell
            
            //*** describe property
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                cell.btn_service_providers.hidden = true
                cell.btn_service_providers.userInteractionEnabled = false
            }
            else {
                cell.btn_service_providers.hidden = false
                cell.btn_service_providers.userInteractionEnabled = true
            }
            
            if let propertyName = property.profileName {
                cell.lbl_property_name.text! =  propertyName
            }
            //set location name
            if let country = property.streetAddress {
                cell.txtVw_location.text! = country
            }
            //set room type
            if let roomType = property.roomType {
                cell.lblRoomType.text! = roomType
            }
            
            //set no of guest
            if let accom = property.accommodation {
                cell.lblNoOfGuest.text! = "\(accom) Guest"
            }
            //set no of beds
            if let beds = property.beds {
                
                if beds == "1" {
                    cell.lblNoOfBeds.text! = "\(beds) Bed"
                }
                else {
                    cell.lblNoOfBeds.text! = "\(beds) Beds"
                }
            }
            //set no of accommodation
            if let no = property.accommodation{
                cell.lbl_no_accommodation.text = ("Accommodation : " + no)
            }
            
            //set currency type
            if let currency = property.currency {
                cell.lbl_Currency.text! = ": \(currency)"
            }
            //set price
            if let price = property.setPrice {
                cell.lbl_price.text! = ": \(price)"
            }
            //set common amenities
            if let commonAmenties = property.commonAmenties {
                cell.lbl_comn_amenities.text! = ": \(commonAmenties)"
            }
            //set additional amenities
            if let additionalMenties = property.additionalMenties {
                cell.lbl_add_amenities.text! = ": \(additionalMenties)"
            }
            //set special feature
            if let specialFeature = property.specialFeature {
                cell.lbl_special_feature.text! = ": \(specialFeature)"
            }
            //set home safety
            if let homeSafety = property.homeSafety {
                cell.lbl_home_safety.text! = ": \(homeSafety)"
            }
            
            if let fireExtinguisher = property.fireExtinguisher {
                if fireExtinguisher.containsString("true"){
                    cell.lbl_fire_extinguishers.text = ": Available"
                }
                else {
                    cell.lbl_fire_extinguishers.text = ": Not Available"
                }
            }
            if let fireAlarm = property.fireAlarm {
                if fireAlarm.containsString("true"){
                    cell.lbl_fire_alarm.text = ": Available"
                }
                else {
                    cell.lbl_fire_alarm.text = ": Not Available"
                }
            }
            if let emergencyExit = property.emergencyExit {
                if emergencyExit.containsString("true"){
                    cell.lbl_emergency_exit.text = ": Available"
                }
                else {
                    cell.lbl_emergency_exit.text = ": Not Available"
                }
            }
            if let gasShutoffValve = property.gasShutoffValve {
                if gasShutoffValve.containsString("true"){
                    cell.lbl_gas_shuot_valve.text = ": Available"
                }
                else {
                    cell.lbl_gas_shuot_valve.text = ": Not Available"
                }
            }
            
            cell.btn_service_providers.addTarget(self, action: #selector(DetailsViewController.buttonServiceProvidersClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btn_map_view.addTarget(self, action: #selector(DetailsViewController.buttonViewOnMapClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            let bookingType  = property.typeOfBooking
            if bookingType.containsString("review") {
                cell.lbl_booking_type.text = "Host to Review Request"
                cell.lbl_booking_description.text = "Details: Host will review each request, So hurry up sent your book request to host and make this property yours."
            }
            else {
                cell.lbl_booking_type.text = "Book Instantly"
                cell.lbl_booking_description.text = "Details: You can book this property right away."
            }
            
            
            //to set furnishing
            if let fur = property.furnishing {
                cell.lbl_furnishes.text = "Furnishing : " + fur
            }
            
            //to set cancellation policy
            if let policy = property.cancellationPolicy {
                
                if policy.containsString("strict") == true {
                    cell.lbl_policy_name.text = "Strict"
                    cell.lbl_policy_description.text = "Percentage of Deduction from Total Payable Amount - 20%."
                }
                else if policy.containsString("moderate") == true {
                    cell.lbl_policy_name.text = "Moderate"
                    cell.lbl_policy_description.text = "Percentage of Deduction from Total Payable Amount - 15%."
                }
                else {
                    cell.lbl_policy_name.text = "Flexible"
                    cell.lbl_policy_description.text = "Percentage of Deduction from Total Payable Amount - 10%."
                }
                
            }
            else {
                cell.lbl_policy_name.text = "Strict"
                cell.lbl_policy_description.text = "Percentage of Deduction from Total Payable Amount - 20%."
            }
            
            
            
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellFour", forIndexPath: indexPath) as! DetailTableCell
            //set the data here
            
            //set summary name
            if let summary = property.summary {
                cell.txtVwAbout.text! = summary
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        footerView.backgroundColor = UIColor.clearColor()
        if (PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue ) && !isToShowViaNoti  {
            
            
            let requestBtn: UIButton = UIButton()
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
                requestBtn.setTitle("EDIT PROPERTY", forState: .Normal)
            }
            else {
                requestBtn.setTitle("REQUEST TO BOOK", forState: .Normal)
            }
            requestBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            requestBtn.backgroundColor = Constants.Colors.GREEN_COLOUR
            requestBtn.frame = CGRect(x:0,y:0,width:tableView.frame.size.width - 20,height:40)
            requestBtn.layer.cornerRadius = 10
            requestBtn.clipsToBounds = true
            requestBtn.titleLabel!.font = UIFont.boldSystemFontOfSize(16)
            requestBtn.center = footerView.center
            requestBtn.addTarget(self, action: #selector(DetailsViewController.btnRequestToBookClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            footerView.addSubview(requestBtn)
            
            isToShowViaNoti = false
            return footerView
        }
        else {
            return footerView
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    
    
    //MARK:- BUTTONS ACTIONS
    func buttonTapAction(sender:UIButton!)
    {
        print("Button is working")
        //        let chatController = LGChatController()
        //        //let dict:Dictionary = arr_OfData[indexPath.row] as! Dictionary<String, String>
        //        chatController.opponentImage = UIImage(named: "avatar.png")
        //        //chatController.dataDict = dict
        //
        //        self.navigationController?.pushViewController(chatController, animated: true)
        
    }
    func buttonDetailsClick(sender:UIButton!)
    {
        print("Button detail is working")
        
        //move to detail screen
        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailProvider_Controller") as! DetailProvider_Controller
        show_Detail_obj.isShowHostDetails = true
        if let hostID = property.userId {
            show_Detail_obj.hostId = hostID
            
            //if service provider
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                
                if let id = userDataServiceProvider.userId {
                    show_Detail_obj.userID = id
                }
                
            }
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
                if let id = userData.userId {
                    show_Detail_obj.userID = id
                }
            }
            
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) != Enumerations.USER_TYPE.guest.rawValue {
                if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                    show_Detail_obj.userDataServiceProvider = self.userDataServiceProvider
                }
                else {
                    show_Detail_obj.userData = self.userData
                }
            }
        }
        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
    }
    
    func buttonViewOnMapClick(sender:UIButton!) {
        let map_vc = self.storyboard?.instantiateViewControllerWithIdentifier("Map_View_Controller") as! Map_View_Controller
        map_vc.mapProperty = self.property
        print("property to pass" , property)
        self.navigationController?.pushViewController(map_vc, animated: true)
    }
    
    
    func buttonServiceProvidersClick(sender:UIButton!) {
        //when guest user
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            FTIndicator.showErrorWithMessage("First Sign Up")
        }
            
            //when service provider
            //when guest user
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            let service_provider_vc = self.storyboard?.instantiateViewControllerWithIdentifier("ServiceProvider_Controller") as! ServiceProvider_Controller
            service_provider_vc.serviceProviderUserData = self.userDataServiceProvider
            
            service_provider_vc.property = self.property
            print("property to pass" , property)
            self.navigationController?.pushViewController(service_provider_vc, animated: true)
        }
        else {
            let service_provider_vc = self.storyboard?.instantiateViewControllerWithIdentifier("ServiceProvider_Controller") as! ServiceProvider_Controller
            service_provider_vc.userData = self.userData
            service_provider_vc.property = self.property
            print("property to pass" , property)
            self.navigationController?.pushViewController(service_provider_vc, animated: true)
        }}
    
    
    func btnRequestToBookClick(sender:UIButton!)
    {
        if sender.titleLabel!.text?.containsString("EDIT PROPERTY") == true {
            let add_property = self.storyboard?.instantiateViewControllerWithIdentifier("AddPropertyViewController") as! AddPropertyViewController
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) != Enumerations.USER_TYPE.guest.rawValue {
                add_property.add_PropertyArr = self.property
                print("property to pass" , property)
            }
            self.navigationController?.pushViewController(add_property, animated: true)
        }
        else {
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
                openCheckBookingView()
            }
            else {
                FTIndicator.showErrorWithMessage("First Sign Up")
            }
        }
    }
    
    
}


