//
//  SignUpServiceProviderViewController.swift
//  HSS
//
//  Created by Rajni on 08/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit


class SignUpServiceProviderViewController: UIViewController , CountriesViewControllerDelegate {
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var txtFieldGender: UITextField!
    @IBOutlet weak var txtFieldDOB: UITextField!
    @IBOutlet weak var txtFieldTelephoneNo: UITextField!
    @IBOutlet weak var txtFieldMobileNo: UITextField!
    
    @IBOutlet weak var constraint_txtField_password_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_txtField_password_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_txtField_email_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_txtField_email_top: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_vwBackground_height: NSLayoutConstraint!
    
    
    //MARK:- VARIABLES
    public var presentCountry = Country.currentCountry
    var pickerView = UIPickerView()
    var arrGender = ["Male", "Female"]
    let codeBtn : UIButton = UIButton()
    var univTextField : UITextField!
    var isLoginViaSocialMedia:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var authenticateToken_whilenewSPAdded: String = ""
    var propertyId_whilenewSPAdded: String = ""
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLoginViaSocialMedia == true {
            constraint_txtField_email_top.constant = 0
            constraint_txtField_email_height.constant = 0
            constraint_txtField_password_top.constant = 0
            constraint_txtField_password_height.constant = 0
            constraint_vwBackground_height.constant = (470 - 100)
            self.fillEnteriesViaSocialMedia()
        }
        else {
            constraint_txtField_email_top.constant = 12
            constraint_txtField_password_top.constant = 12
            constraint_txtField_email_height.constant = 44
            constraint_txtField_password_height.constant = 44
            constraint_vwBackground_height.constant = 470
        }
        self.initView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
    }
    
    
    func fillEnteriesViaSocialMedia(){
        
      //  txtFieldEmail.hidden = true
       // txtFieldPassword.hidden = true
        
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME) != nil {
            txtFieldFirstName.text! = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME)!
        }
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) == Enumerations.REGISTER_VIA.facebook.rawValue {
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_LAST_NAME) != nil {
                txtFieldLastName.text! = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_LAST_NAME)!
            }}
    }
    
    //MARK:- CUSTOM METHODS
   func initView(){
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    
    //Add PickerView
    pickerView.delegate = self
    pickerView.dataSource = self
    pickerView.backgroundColor = UIColor.clearColor()
    
    //Add PickerView Input
    univTextField = txtFieldFirstName
    txtFieldGender.inputView = pickerView
    txtFieldEmail.autocorrectionType = .No
    
    //Add Drop Icon
    self.dropIcon(txtFieldGender)
    
    self.layerFields(txtFieldFirstName)
    self.layerFields(txtFieldLastName)
    self.layerFields(txtFieldEmail)
    self.layerFields(txtFieldPassword)
    self.layerFields(txtFieldGender)
    self.layerFields(txtFieldDOB)
    self.layerFields(txtFieldTelephoneNo)
    self.layerFields(txtFieldMobileNo)
    CommonFunctions.makeRoundCornerOfButton(btnNext)
    
    self.rtIcon(txtFieldFirstName, imageName: "user-name")
    self.rtIcon(txtFieldLastName, imageName: "user-name")
    self.rtIcon(txtFieldEmail, imageName: "mail")
    self.rtIcon(txtFieldPassword, imageName: "password")
    self.rtIcon(txtFieldGender, imageName: "gender")
    self.rtIcon(txtFieldDOB, imageName: "calender")
    self.rtIcon(txtFieldTelephoneNo, imageName: "telephone")
    self.rtIcon(txtFieldMobileNo, imageName: "phone")
    
    let datePicker: UIDatePicker = UIDatePicker()
    datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
    datePicker.backgroundColor = UIColor.whiteColor()
    datePicker.datePickerMode = UIDatePickerMode.Date
    datePicker.addTarget(self, action: #selector(SignUpSubmitViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
    
    let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
    let todayDate = NSDate()
    let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
    let minimumYear = (components?.year)! - 1917
    let minimumMonth = (components?.month)! - 1
    let minimumDay = (components?.day)! - 1
    let comps = NSDateComponents();
    comps.year = -minimumYear
    comps.month = -minimumMonth
    comps.day = -minimumDay
    let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
    
    
    let maxYear = (components?.year)! - 2017
    let maxMonth = (components?.month)! - 1
    let maxDay = (components?.day)! - 1
    let maxcomps = NSDateComponents();
    maxcomps.year = -maxYear
    maxcomps.month = -maxMonth
    maxcomps.day = -maxDay
    let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
    
    
    datePicker.minimumDate = minDate
    datePicker.maximumDate = maxDate
    
    
    self.txtFieldDOB.inputView = datePicker

    
    btnNext.backgroundColor = Constants.Colors.GREEN_COLOUR
    self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
    }
    
    
    func rtIcon(textField : UITextField , imageName : String) {
        if textField == txtFieldMobileNo{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 90, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            
            codeBtn.frame = CGRectMake(40, 0, 50, 40)
            codeBtn.setTitle("+\(presentCountry.phoneExtension)-", forState: .Normal)
            codeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            codeBtn.titleLabel!.font =  UIFont.systemFontOfSize(15)
            codeBtn.addTarget(self, action: #selector(SignUpViewController.pressed(_:)), forControlEvents: .TouchUpInside)
            codeBtn.contentHorizontalAlignment = .Left;
            
            rtPad.addSubview(codeBtn)
            
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
        else{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
    }
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        txtFieldDOB.text! = formattor.stringFromDate(sender.date)
        print("Selected value \(txtFieldDOB.text!)")
    }
    
    func pressed(sender: UIButton!) {
        let countriesViewController = CountriesViewController.standardController()
        countriesViewController.cancelBarButtonItemHidden = true
        countriesViewController.delegate = self
        countriesViewController.majorCountryLocaleIdentifiers = ["GB", "US", "IT", "DE", "RU", "BR", "IN"]
        
        navigationController?.pushViewController(countriesViewController, animated: true)
    }
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        if tappedImageView.tag == 1 {
            txtFieldGender.becomeFirstResponder()
        }
    }
    
    

    //MARK:- BUTTON ACTIONS

    @IBAction func btnNextClick(sender: AnyObject) {
        if Validate() == true {
            
            //to save user data in global array
           // let user = ServiceProviderRegistration()
            //Constants.ServiceProviderRegistrationData.ServiceProviderUserData
            
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.firstName = txtFieldFirstName.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.lastName  =  txtFieldLastName.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.gender = txtFieldGender.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.DOB = txtFieldDOB.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.telephoneNo = txtFieldTelephoneNo.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.countryCode = codeBtn.titleLabel!.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.mobileNo = txtFieldMobileNo.text!
            
            if !isLoginViaSocialMedia {
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.email = txtFieldEmail.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.password = txtFieldPassword.text!
            }
            else{
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.email = ""
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.password = ""
            }
            
            let service_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpOneServiceProviderViewController") as! SignUpOneServiceProviderViewController
            service_provider_obj.isLoginViaSocialMedia = self.isLoginViaSocialMedia
            self.navigationController?.pushViewController(service_provider_obj, animated: true)
        }
    }
    
    
    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        // to check validation of first name
        if txtFieldFirstName.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldFirstName.attributedPlaceholder = NSAttributedString(string: "Please enter first name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of last name
        if txtFieldLastName.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldLastName.attributedPlaceholder = NSAttributedString(string: "Please enter last name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        if !isLoginViaSocialMedia {
        //to check validation of Email
        if txtFieldEmail.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldEmail.attributedPlaceholder = NSAttributedString(string: "Please enter email id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }else{
            valid = CommonFunctions.isEmailValid(txtFieldEmail.text!)
            if !valid{
                txtFieldEmail.text = ""
                self.AnimationShakeTextField(txtFieldEmail)
                txtFieldEmail.attributedPlaceholder = NSAttributedString(string: "Please enter valid email id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
        }
        
        //to check validation of password
        if txtFieldPassword.text!.utf16.count==0{
            // Change the placeholder color to red for textfield passWord
            txtFieldPassword.attributedPlaceholder = NSAttributedString(string: "Please enter password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }else{
            let newLength = txtFieldPassword.text!.utf16.count
            if newLength < 8{
                valid = false
                txtFieldPassword.text=""
                self.AnimationShakeTextField(self.txtFieldPassword)
                txtFieldPassword.attributedPlaceholder = NSAttributedString(string: "Please enter password minimum of 8 characters", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                
            }
            }}
        
        // to check validation of gender
        if txtFieldGender.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldGender.attributedPlaceholder = NSAttributedString(string: "Please select gender", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of Date of Birth
        if txtFieldDOB.text!.utf16.count==0 {
            //Change the placeholder color to red for textfield email if
            txtFieldDOB.attributedPlaceholder = NSAttributedString(string: "Please select DOB", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of telephone no
        if txtFieldTelephoneNo.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldTelephoneNo.attributedPlaceholder = NSAttributedString(string: "Please enter telephone no.", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of mobile no
        if txtFieldMobileNo.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldMobileNo.attributedPlaceholder = NSAttributedString(string: "Please enter mobile no.", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
       return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }
    
    internal func countriesViewControllerDidCancel(countriesViewController: CountriesViewController) { }
    
    internal func countriesViewController(countriesViewController: CountriesViewController, didSelectCountry country: Country) {
        navigationController?.popViewControllerAnimated(true)
        
        print(country.phoneExtension)
        codeBtn.setTitle("+\(country.phoneExtension)-", forState: .Normal)
    }
    
    
    
    
    
    
    
    
    
    
}
//MARK:- Textfield Delegates/Datasource
extension SignUpServiceProviderViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        univTextField = textField
        
     if (textField == txtFieldGender) {
        textField.tintColor = UIColor.clearColor()
        textField.text = arrGender[0]
        }
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
         let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if textField.text?.characters.count > 100 {
            return false
        }
        
        if textField == txtFieldMobileNo || textField == txtFieldTelephoneNo {
            
            if string.rangeOfCharacterFromSet(characterset.invertedSet) != nil {
                    print("string contains special characters")
                    return false
                }
                else {
                    if string == ""{
                        return true
                    }
                    if textField.text?.characters.count >= 15 {
                        return false
                    }
                }
        }
        
        return true
    }
    
}

//MARK:- PickerView Delegates/Datasource
extension SignUpServiceProviderViewController: UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if univTextField == txtFieldGender{
            return arrGender.count
        }
        return 0
    }
}

extension SignUpServiceProviderViewController: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if univTextField == txtFieldGender {
            return arrGender[row]
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTextField == txtFieldGender {
            txtFieldGender.text! = arrGender[row]
        }
    }
}

