//
//  SignUpOneServiceProviderViewController.swift
//  HSS
//
//  Created by Rajni on 08/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import LocationPicker
import CoreLocation
import FTIndicator
import JTSImageViewController

class SignUpOneServiceProviderViewController: UIViewController , UINavigationControllerDelegate , MPGTextFieldDelegate  {
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var constraint_TxtField_CompanyName_Height: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_txtField_CompanyName_Top: NSLayoutConstraint!
    
    @IBOutlet weak var vwCompany: UIView!
    @IBOutlet weak var vwIndividual: UIView!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var btnIndividual: UIButton!
    //view 1
   
    @IBOutlet weak var txtField_Nationality1: MPGTextField!
    @IBOutlet weak var txtField_Address1: UITextField!
    @IBOutlet weak var txtField_PostCode1: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtField_Language1: UITextField!
    @IBOutlet weak var txtField_website1: UITextField!
    
    
    //view 2
    @IBOutlet weak var btnNextVw2: UIButton!
    @IBOutlet weak var txtField_selectCompany: UITextField!
    @IBOutlet weak var txtField_CompanyName: UITextField!
    @IBOutlet weak var txtField_CompanyAddress2: UITextField!
    @IBOutlet weak var txtField_RegistrationNo: UITextField!
    @IBOutlet weak var txtField_Nationality2: MPGTextField!
    @IBOutlet weak var txtField_Address2: UITextField!
    @IBOutlet weak var txtField_PostCode2: UITextField!
    @IBOutlet weak var txtField_Language2: UITextField!
    @IBOutlet weak var txtField_Website2: UITextField!
    @IBOutlet weak var vw_dummy_nationality_1: UIView!
    @IBOutlet weak var vw_dummy_nationality_2: UIView!
    
    //MARK:- VARIABLES
    // Images
    let checkedImage = UIImage(named: "radio-selected")! as UIImage
    let uncheckedImage = UIImage(named: "radio-unselected")! as UIImage
    var userCoordinates : CLLocationCoordinate2D!
    var arrCompany = [Company]()
    var companyName: String = ""
    var isLoginViaSocialMedia:Bool!

    
    public var presentCountry = Country.currentCountry
    var pickerView = UIPickerView()
    var arrGender = ["Male", "Female"]
    let codeBtn : UIButton = UIButton()
    var univTextField : UITextField!
    var arrLanguages = ["English", "French", "Italian"]
    
    var tableArray = [String]()
    var pickerTxtField = UITextField()
    var isViewOneActive:Bool = true
    var coutriesList : NSArray = []
    var pickerType: String = ""
    var companyID: String = ""
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fectCoutries()
        self.callToRetrieveAllCompanies()
        btnIndividual.setImage(checkedImage, forState: .Normal)
        btnCompany.setImage(uncheckedImage, forState: .Normal)
        userCoordinates = CLLocationCoordinate2DMake(0.00000, 0.00000)
        initView()
    }
    
    override func viewWillAppear(animated: Bool) {
        app.uni_vc = self
        txtField_Address1.resignFirstResponder()
        txtField_Address2.resignFirstResponder()
        self.view.endEditing(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        txtField_Address1.resignFirstResponder()
        txtField_Address2.resignFirstResponder()
        self.view.endEditing(true)

    }
    
    //MARK:- CUSTOM METHODS
    func initView(){
        vwCompany.hidden = true
        vwCompany.userInteractionEnabled = false
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        
        //Add PickerView Input
        univTextField = txtField_Nationality1
        txtField_Language1.inputView = pickerView
        
        // add dummy background vw
        vw_dummy_nationality_1.layer.cornerRadius = 8
        vw_dummy_nationality_1.clipsToBounds = true
        
//        //add nationality 
        self.txtField_Nationality1.delegate = self
        self.txtField_Nationality1.backgroundColor = UIColor.redColor()
        self.txtField_Nationality1.textColor = UIColor.blackColor()
        let Locale = NSLocale(localeIdentifier: "en_US")
        let country = Locale.displayNameForKey(NSLocaleCountryCode, value: presentCountry.countryCode)!
        print("MY Country : \(country)")
        self.txtField_Nationality1.text = country
    
        if NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") != nil{
            let locationDictionary : NSDictionary = NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") as! NSDictionary
            
            let locationName : String = locationDictionary["locationName"] as! String
            let locationPinCode: String = locationDictionary["pincode"] as! String
            let latitude: String = locationDictionary["latitude"] as! String
            let longitude: String = locationDictionary["longitude"] as! String
            userCoordinates = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            
            txtField_Address1.text = locationName
            txtField_PostCode1.text = locationPinCode
            
        }
        
        
        self.layerFields(txtField_Nationality1)
        self.layerFields(txtField_Address1)
        self.layerFields(txtField_PostCode1)
        self.layerFields(txtField_Language1)
        self.layerFields(txtField_website1)
        
        self.rtIcon(txtField_Nationality1, imageName: "flag")
        self.rtIcon(txtField_Address1, imageName: "address")
        self.rtIcon(txtField_PostCode1, imageName: "post-code")
        self.rtIcon(txtField_Language1, imageName: "country-language")
        self.rtIcon(txtField_website1, imageName: "website")
        
        self.dropIcon(txtField_Language1)
        CommonFunctions.makeRoundCornerOfButton(btnNext)
        btnNext.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        
    }
    
    func initView2(){
        
        vwIndividual.hidden = true
        vwIndividual.userInteractionEnabled = false
    
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        
        //Add PickerView Input
        univTextField = txtField_Nationality1
        txtField_Language2.inputView = pickerView
        txtField_selectCompany.inputView = pickerView

        // add dummy background vw
        vw_dummy_nationality_2.layer.cornerRadius = 8
        vw_dummy_nationality_2.clipsToBounds = true
        
        //add nationality
        self.txtField_Nationality2.delegate = self
        self.txtField_Nationality2.backgroundColor = UIColor.whiteColor()
        self.txtField_Nationality2.textColor = UIColor.blackColor()
       let Locale = NSLocale(localeIdentifier: "en_US")
        let country = Locale.displayNameForKey(NSLocaleCountryCode, value: presentCountry.countryCode)!
        print("MY Country : \(country)")
        self.txtField_Nationality2.text = country
        
        if NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") != nil{
            let locationDictionary : NSDictionary = NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") as! NSDictionary
            
            let locationName : String = locationDictionary["locationName"] as! String
            let locationPinCode: String = locationDictionary["pincode"] as! String
            let latitude: String = locationDictionary["latitude"] as! String
            let longitude: String = locationDictionary["longitude"] as! String
            userCoordinates = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            
            txtField_Address2.text = locationName
            txtField_PostCode2.text = locationPinCode
            
        }
        
        self.layerFields(txtField_selectCompany)
        self.layerFields(txtField_CompanyName)
        self.layerFields(txtField_CompanyAddress2)
        self.layerFields(txtField_RegistrationNo)
        self.layerFields(txtField_Nationality2)
        self.layerFields(txtField_Address2)
        self.layerFields(txtField_PostCode2)
        self.layerFields(txtField_Language2)
        self.layerFields(txtField_Website2)
        
        self.rtIcon(txtField_selectCompany, imageName: "company")
        self.rtIcon(txtField_CompanyName, imageName: "company")
        self.rtIcon(txtField_CompanyAddress2, imageName: "address")
        self.rtIcon(txtField_RegistrationNo, imageName: "passport")
        self.rtIcon(txtField_Nationality2, imageName: "flag")
        self.rtIcon(txtField_Address2, imageName: "address")
        self.rtIcon(txtField_PostCode2, imageName: "post-code")
        self.rtIcon(txtField_Language2, imageName: "country-language")
        self.rtIcon(txtField_Website2, imageName: "website")
        
        self.dropIcon(txtField_Language2)
        self.dropIcon(txtField_selectCompany)
        CommonFunctions.makeRoundCornerOfButton(btnNextVw2)
        btnNextVw2.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
    }
    
    
    func rtIcon(textField : UITextField , imageName : String) {
        
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        
    }
    
    
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        if tappedImageView.tag == 1 {
            txtField_Language1.becomeFirstResponder()
        }
    }
    
    func textField(textField: MPGTextField!, didEndEditingWithSelection result: [NSObject : AnyObject]!) {
        print(result)
        let dic : NSDictionary = result as NSDictionary
        
        if isViewOneActive {
        if let country : MyCountry = dic.objectForKey("Object") as? MyCountry{
            self.txtField_Nationality1.text = country.countryName
        }
        else{
            self.txtField_Nationality1.text = ""
            }}
        else {
            if let country : MyCountry = dic.objectForKey("Object") as? MyCountry{
                self.txtField_Nationality2.text = country.countryName
            }
            else{
                self.txtField_Nationality2.text = ""
            }
        }
    }
    
    func dataForPopoverInTextField(textField: MPGTextField!) -> [AnyObject]! {
        return coutriesList as [AnyObject];
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnNextClick(sender: AnyObject) {
        if Validate() == true {
          
            //save data local
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.nationality = txtField_Nationality1.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.address = txtField_Address1.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.serviceProviderLatitude = String(userCoordinates.latitude)
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.serviceProviderLongitude = String(userCoordinates.longitude)
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.postCode = txtField_PostCode1.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.language = txtField_Language1.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.website = txtField_website1.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.registerUnder = "asIndividual"
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyId = companyID
            
            if companyName == "" {
                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyName = txtField_CompanyName.text!
            }
            else {
                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyName = companyName
            }
            
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyAddress = txtField_CompanyAddress2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyRegisterationNumber = txtField_RegistrationNo.text!
            
            let service_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpTwoServiceProviderViewController") as! SignUpTwoServiceProviderViewController
            service_provider_obj.isLoginViaSocialMedia = self.isLoginViaSocialMedia
            self.navigationController?.pushViewController(service_provider_obj, animated: true)
        }
    }
    
    @IBAction func btnNextVw2Click(sender: AnyObject) {
        if Validate() == true {
            //save data local
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.nationality = txtField_Nationality2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.address = txtField_Address2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.serviceProviderLatitude = String(userCoordinates.latitude)
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.serviceProviderLongitude = String(userCoordinates.longitude)
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.postCode = txtField_PostCode2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.language = txtField_Language2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.website = txtField_Website2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.registerUnder = "underCompany"
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyId = companyID
            
            if companyName == "" {
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyName = txtField_CompanyName.text!
            }
            else {
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyName = companyName
            }
            
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyAddress = txtField_CompanyAddress2.text!
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.companyRegisterationNumber = txtField_RegistrationNo.text!
            
            let service_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpTwoServiceProviderViewController") as! SignUpTwoServiceProviderViewController
            service_provider_obj.isLoginViaSocialMedia = self.isLoginViaSocialMedia
            self.navigationController?.pushViewController(service_provider_obj, animated: true)
        }
    }
    
    
    
    
    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        if isViewOneActive {
            
        //to check validation of nationality
        if txtField_Nationality1.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtField_Nationality1.attributedPlaceholder = NSAttributedString(string: "Please select nationality", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
            // to check validation of address
            if txtField_Address1.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_Address1.attributedPlaceholder = NSAttributedString(string: "Please enter address", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            // to check validation of post code
            if txtField_PostCode1.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_PostCode1.attributedPlaceholder = NSAttributedString(string: "Please enter post code", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            // to check validation of language
            if txtField_Language1.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_Language1.attributedPlaceholder = NSAttributedString(string: "Please select language", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
        
        }
        else {
            // to check validation of company name option
            if txtField_selectCompany.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_selectCompany.attributedPlaceholder = NSAttributedString(string: "Please select company name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            
            if txtField_selectCompany.text?.containsString("Other") == true {
            // to check validation of company name
            if txtField_CompanyName.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_CompanyName.attributedPlaceholder = NSAttributedString(string: "Please enter company name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
                }}
            
            // to check validation of company address
            if txtField_CompanyAddress2.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_CompanyAddress2.attributedPlaceholder = NSAttributedString(string: "Please enter company address", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            
                //to check validation of registration no
                if txtField_RegistrationNo.text!.utf16.count==0{
                    //Change the placeholder color to red for textfield email if
                    txtField_RegistrationNo.attributedPlaceholder = NSAttributedString(string: "Please enter registration no", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                    valid = false
                }
            
            //to check validation of nationality
            if txtField_Nationality2.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_Nationality2.attributedPlaceholder = NSAttributedString(string: "Please select nationality", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            // to check validation of address
            if txtField_Address2.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_Address2.attributedPlaceholder = NSAttributedString(string: "Please enter address", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            // to check validation of post code
            if txtField_PostCode2.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_PostCode2.attributedPlaceholder = NSAttributedString(string: "Please enter post code", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
            
            // to check validation of language
            if txtField_Language2.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtField_Language2.attributedPlaceholder = NSAttributedString(string: "Please select language", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    }
        
        return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }
    
    
    
    //MARK:- TO SELECT DIFFERENT VIEWS
    @IBAction func btnIndividualClick(sender: AnyObject) {
        
        btnIndividual.setImage(checkedImage, forState: .Normal)
        btnCompany.setImage(uncheckedImage, forState: .Normal)
        
        initView()
        vwIndividual.hidden = false
        vwIndividual.userInteractionEnabled = true
        isViewOneActive = true
    }
    
    
    @IBAction func btnCompanyClick(sender: AnyObject) {
        btnCompany.setImage(checkedImage, forState: .Normal)
        btnIndividual.setImage(uncheckedImage, forState: .Normal)
        initView2()
        vwCompany.hidden = false
        vwCompany.userInteractionEnabled = true
        isViewOneActive = false
    }
    
    func fectCoutries(){
        if let art : NSArray = DatabaseBLFunctions.fetchCountries() {
            if art.count>0{
                let mutArt : NSMutableArray = NSMutableArray(array: art)
                mutArt.removeObjectAtIndex(0)
                coutriesList = mutArt
            }
        }
    }
    
    
    
    
    
    //MARK:- WEB SERVICE
    func callToRetrieveAllCompanies(){
            WebserviceUtils.callPostRequestWithEmptyParams(URLConstants.GET_ALL_COMPANIES, params: "", success: { (response) in
                if let json = response as? NSDictionary {
                    print(json)
                    if let status = json.objectForKey("status") as? String{
                        if status.containsString("success"){
                            if let arr = json.objectForKey("company") as? NSArray {
                                print("ghsdkajlshas.kj",arr)
                                if arr.count > 0 {
                                    self.toParseCompanies(arr)
                                }
                            }
                        }
                    }
                    
                }
                }, failure: { (error) in
                    print(error.localizedDescription)
            })
    }
    
    func toParseCompanies(arrProperty: NSArray) {
        
        print(arrProperty.count)
        arrCompany = []
        
        for item in arrProperty {
            let companyObj = Company()
            
            if let companyAddress = item.objectForKey("companyAddress") as? String {
                companyObj.setValue(companyAddress, forKey: "companyAddress")
            }
            if let companyName = item.objectForKey("companyName") as? String {
                companyObj.setValue(companyName, forKey: "companyName")
            }
            if let companyRegisterationNumber = item.objectForKey("companyRegisterationNumber") as? String {
                companyObj.setValue(companyRegisterationNumber, forKey: "companyRegisterationNumber")
            }
            if let id = item.objectForKey("id") as? String {
                companyObj.setValue(id, forKey: "id")
            }
            arrCompany.append(companyObj)
        }
        
    }
    
    
    
}


//MARK:- Textfield Delegates/Datasource
extension SignUpOneServiceProviderViewController: UITextFieldDelegate {
    
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        univTextField = textField
        
    if (textField == txtField_Language1) || (textField == txtField_Language2) {
            textField.tintColor = UIColor.clearColor()
            textField.text = arrLanguages[0]
       }
        if textField == txtField_Nationality1 || textField == txtField_Nationality2{
            pickerType = "nationality"
        }
        else if textField == txtField_Address1 || textField == txtField_Address2 {
            
            openLocationPicker()
        }
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
    }
    
    func openLocationPicker(){
        
        let locationPicker = LocationPickerViewController()
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        locationPicker.currentLocationButtonBackground = Constants.Colors.BACKGROUND_COLOUR
        locationPicker.showCurrentLocationInitially = true // default: true
        locationPicker.mapType = .Standard // default: .Hybrid
        
        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false
        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
        
        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
        locationPicker.resultRegionDistance = 500 // default: 600
        
        locationPicker.completion = { location in
            
            var postalCode:String = ""
            let locationName : String = (location?.address)!
            self.userCoordinates  = (location?.coordinate)!
            
            let countryName : String = (location?.placemark.country)!
            
            if let postalCodeObj: String = (location?.placemark.postalCode){
                postalCode = postalCodeObj
            }
            
            if self.isViewOneActive{
               self.txtField_Nationality1.text! = countryName
               self.txtField_Address1.text = locationName
              self.txtField_PostCode1.text = postalCode
            }
            else {
                self.txtField_Nationality2.text! = countryName
                self.txtField_Address2.text = locationName
                self.txtField_PostCode2.text = postalCode
            }
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if (textField == txtField_Nationality1){
            txtField_Address1.text = ""
            txtField_PostCode1.text = ""
        }
        
        if (textField == txtField_Nationality2) {
            txtField_Address2.text = ""
            txtField_PostCode2.text = ""
        }
        
        textField.resignFirstResponder();
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if textField.text?.characters.count > 100 {
            return false
        }
        
        if textField == txtField_PostCode2 || textField == txtField_PostCode1 {
            
            if string.rangeOfCharacterFromSet(characterset.invertedSet) != nil {
                print("string contains special characters")
                return false
            }
            else {
                if string == ""{
                    return true
                }
                if textField.text?.characters.count >= 15 {
                    return false
                }
            }
        }
        return true
    }
    
}

//MARK:- PickerView Delegates/Datasource
extension SignUpOneServiceProviderViewController: UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if univTextField == txtField_Language1 || univTextField == txtField_Language2 {
            return arrLanguages.count
        }
        if pickerType == "nationality"{
            return coutriesList.count
        }
        
        if univTextField == txtField_selectCompany {
            return arrCompany.count + 1
        }
        
       return 0
    }
}

extension SignUpOneServiceProviderViewController: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if univTextField == txtField_Language1 || univTextField == txtField_Language2 {
            return arrLanguages[row]
        }
        if pickerType == "nationality"{
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            return countryDetail.countryName
        }
        
        if univTextField == txtField_selectCompany {
            
            if row == arrCompany.count {
               return "Other"
            }
            else {
            return arrCompany[row].companyName
            }
        }

       return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTextField == txtField_Language1 || univTextField == txtField_Language2 {
            univTextField.text! = arrLanguages[row]
        }
        if univTextField == txtField_selectCompany {
            if row == arrCompany.count {
            constraint_TxtField_CompanyName_Height.constant = 44
            constraint_txtField_CompanyName_Top.constant = 12
            univTextField.text! = "Other"
            companyID = ""
                txtField_CompanyAddress2.text = ""
                txtField_RegistrationNo.text = ""
                txtField_CompanyName.text = ""
            }
            else {
            constraint_TxtField_CompanyName_Height.constant = 0
            constraint_txtField_CompanyName_Top.constant = 0
            univTextField.text! = arrCompany[row].companyName
            txtField_CompanyAddress2.text! = arrCompany[row].companyAddress
            txtField_RegistrationNo.text! = arrCompany[row].companyRegisterationNumber
            companyID = arrCompany[row].id
            companyName = arrCompany[row].companyName
                
            }
        }
        if pickerType == "nationality"{
            if coutriesList.count > 0 {
                let countryDetail : MyCountry = coutriesList[row] as! MyCountry
               // txtFieldNationality.text = countryDetail.countryName
        }
    }
}
}

 