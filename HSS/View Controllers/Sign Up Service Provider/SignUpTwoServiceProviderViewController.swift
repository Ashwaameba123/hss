//
//  SignUpTwoServiceProviderViewController.swift
//  HSS
//
//  Created by Rajni on 08/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator

class SignUpTwoServiceProviderViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var Constraint_TxtField_LicenceNo_Height: NSLayoutConstraint!
    @IBOutlet weak var constraint_txtField_LicenceNo_Top: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_conductNo_height: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_conductNo_top: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_issue_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_issue_height: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_bckVw_height: NSLayoutConstraint!
    
    //view
    @IBOutlet weak var imgVwProfilePic: UIImageView!
    @IBOutlet weak var imgVwId: UIImageView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtField_ServiceType: UITextField!
    @IBOutlet weak var txtField_licenceExpiryDate: UITextField!
    @IBOutlet weak var txtField_ConductNo: UITextField!
    @IBOutlet weak var txtField_IssueNo: UITextField!
    @IBOutlet weak var txtField_BirthCertificate_PassportNo: UITextField!
    @IBOutlet weak var vw_Profile: UIView!
    @IBOutlet weak var vw_Id: UIView!
    
    //MARK:- VARIABLES
    let arrLicenseTime = ["11" , "12" , "13" , "14" , "15"]
    var tableArray = [String]()
    var pickerView = UIPickerView()
    var univTextField : UITextField!
    let imagePicker = UIImagePickerController()
    var isProfilePicImgVwTapped: Bool = false
    var arrServiceTypes = [ServiceType]()
    var serviceTypeID: String = ""
     var isLoginViaSocialMedia:Bool!
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callToRetrieveAllServiceTypes()
        imagePicker.delegate = self
        self.imgVwProfilePic.layer.cornerRadius = self.imgVwProfilePic.frame.size.width / 2
        self.imgVwProfilePic.clipsToBounds = true
        self.imgVwProfilePic.layer.borderWidth = 1.0
        self.imgVwProfilePic.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        self.imgVwId.layer.cornerRadius = self.imgVwId.frame.size.width / 2
        self.imgVwId.clipsToBounds = true
        self.imgVwId.layer.borderWidth = 1.0
        self.imgVwId.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        initView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
    }
    
    //MARK:- BUTTON ACTIONS
    
    
    
    @IBAction func btnSubmitClick(sender: AnyObject) {
        if Validate() {
       if validationForProfilePhotoAndIdProof() == true {
        
        //save data local
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.serviceTypeId = serviceTypeID
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.serviceTypeName = txtField_ServiceType.text!
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.driverLicenseExpiryDate = txtField_licenceExpiryDate.text!
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.conductNo = txtField_ConductNo.text!
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.issueNo = txtField_IssueNo.text!
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.birthCeriticateOrPassportNo = txtField_BirthCertificate_PassportNo.text!
        
        if !isLoginViaSocialMedia{
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.registerVia = Enumerations.REGISTER_VIA.manual.rawValue
        }
        else {
            if let register: String = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) {
                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.registerVia = register
            }
        }
    
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.deviceType = "Ios"
        
        //to save device token
        
        if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.DEVICE_TOKEN) {
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.applicationId = token
        }
        else {
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.applicationId = "123456"
        }
        Constants.ServiceProviderRegistrationData.ServiceProviderUserData.userType = Enumerations.USER_TYPE.serviceProvider.rawValue
        
        
        
        self.callToPostServiceProviderUserRegistrationData()
        
        
            }}
    }
    
    
    //MARK:- WEB SERVICE USER REGISTRATION
    func callToPostServiceProviderUserRegistrationData(){
        var socialMediaId:String = ""
        var registerVia:String = ""
        var params = [ String: String]()
        
        let user = Constants.ServiceProviderRegistrationData.ServiceProviderUserData
        
        //start loader
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        if isLoginViaSocialMedia == true {
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_ID) != nil{
                socialMediaId = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_ID)!
            }
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) != nil {
                registerVia = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA)!
            }
        }
        else {
            Constants.ServiceProviderRegistrationData.ServiceProviderUserData.socialMediaId = ""
            socialMediaId = ""
            registerVia = "manual"
        }
        
        //When service provider added by host
        if PrefrencesUtils.getBoolFromPrefs(PreferenceConstant.IS_SERVICE_PROVIDER_ADDED_BY_HOST) == true {
            params = [
                URLParams.FIRST_NAME: user.firstName!,
                URLParams.LAST_NAME: user.lastName!,
                URLParams.PASSWORD: user.password!,
                URLParams.EMAIL: user.email!,
                URLParams.ADDRESS: user.address!,
                URLParams.WEBSITE: user.website!,
                URLParams.POST_CODE: user.postCode!,
                URLParams.TELEPHONE_NUMBER: user.telephoneNo!,
                URLParams.REGISTER_UNDER: user.registerUnder!,
                URLParams.LANGUAGE: user.language!,
                URLParams.GENDER: user.gender!,
                URLParams.MOBILE_NUMBER: user.mobileNo!,
                URLParams.COUNTRY_CODE: user.countryCode!,
                URLParams.COMPANY_ID: user.companyId!,
                URLParams.COMPANY_NAME: user.companyName!,
                URLParams.COMPANY_ADDRESS: user.companyAddress!,
                URLParams.COMPANY_REGISTRATION_NO: user.companyRegisterationNumber!,
                URLParams.DOB: user.DOB!,
                URLParams.SERVICE_TYPE_ID: user.serviceTypeId!,
                URLParams.SERVICE_TYPE_NAME: user.serviceTypeName!,
                URLParams.NATIONALITY: user.nationality!,
                URLParams.DRIVING_LICENCE_EXPIRY_DATE: user.driverLicenseExpiryDate!,
                URLParams.CONDUCT_NO: user.conductNo!,
                URLParams.ISSUE_NO: user.issueNo!,
                URLParams.BIRTH_CERTIFICATE_OR_PASSPORT_NO: user.birthCeriticateOrPassportNo!,
                URLParams.SERVICE_PROVIDER_LATITUDE: user.serviceProviderLatitude!,
                URLParams.SERVICE_PROVIDER_LONGITUDE: user.serviceProviderLongitude!,
                URLParams.REGISTER_VIA: registerVia,
                URLParams.APPLICATION_ID: user.applicationId!,
                URLParams.DEVICE_TYPE: user.deviceType!,
                URLParams.SOCIAL_MEDIA_ID: socialMediaId,
                URLParams.USER_TYPE: user.userType!,
                URLParams.HOST_USER_ID: user.hostUserId!
            ]
        }
        else {
         params = [
            URLParams.FIRST_NAME: user.firstName!,
            URLParams.LAST_NAME: user.lastName!,
            URLParams.PASSWORD: user.password!,
            URLParams.EMAIL: user.email!,
            URLParams.ADDRESS: user.address!,
            URLParams.WEBSITE: user.website!,
            URLParams.POST_CODE: user.postCode!,
            URLParams.TELEPHONE_NUMBER: user.telephoneNo!,
            URLParams.REGISTER_UNDER: user.registerUnder!,
            URLParams.LANGUAGE: user.language!,
            URLParams.GENDER: user.gender!,
            URLParams.MOBILE_NUMBER: user.mobileNo!,
            URLParams.COUNTRY_CODE: user.countryCode!,
            URLParams.COMPANY_ID: user.companyId!,
            URLParams.COMPANY_NAME: user.companyName!,
            URLParams.COMPANY_ADDRESS: user.companyAddress!,
            URLParams.COMPANY_REGISTRATION_NO: user.companyRegisterationNumber!,
            URLParams.DOB: user.DOB!,
            URLParams.SERVICE_TYPE_ID: user.serviceTypeId!,
            URLParams.SERVICE_TYPE_NAME: user.serviceTypeName!,
            URLParams.NATIONALITY: user.nationality!,
            URLParams.DRIVING_LICENCE_EXPIRY_DATE: user.driverLicenseExpiryDate!,
            URLParams.CONDUCT_NO: user.conductNo!,
            URLParams.ISSUE_NO: user.issueNo!,
            URLParams.BIRTH_CERTIFICATE_OR_PASSPORT_NO: user.birthCeriticateOrPassportNo!,
            URLParams.SERVICE_PROVIDER_LATITUDE: user.serviceProviderLatitude!,
            URLParams.SERVICE_PROVIDER_LONGITUDE: user.serviceProviderLongitude!,
            URLParams.REGISTER_VIA: registerVia,
            URLParams.APPLICATION_ID: user.applicationId!,
            URLParams.DEVICE_TYPE: user.deviceType!,
            URLParams.SOCIAL_MEDIA_ID: socialMediaId,
            URLParams.USER_TYPE: user.userType!,
        ]
        }
        
        
        print(params)
        let image = imgVwProfilePic.image
        
        WebserviceUtils.callPostRequestForUserRegistration(URLConstants.SERVICE_PROVIDER_REGISTRATION ,image: image, params: params , imageParam: "userProfileImage" , success: { (response) in
            
            FTIndicator.dismissProgress()
            if let json = response as? NSDictionary {
                
                print(json)
                if let message = json.objectForKey("message"){
                    FTIndicator.showToastMessage(message as! String)
                }
                
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                       if let dic = json.objectForKey("serviceProvider"){
                            
                            if let userId = dic.objectForKey("id"){
                                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.userId = userId as! String
                            }
                            if let authenticateToken = dic.objectForKey("authenticateToken"){
                               Constants.ServiceProviderRegistrationData.ServiceProviderUserData.authenticateToken = authenticateToken as! String
                            }
                            
                            self.callToAddPhotoIdentityServiceProvider()
                    }
                        
        
                }
                
                
                
                }}
            }, failure: { (error) in
                print(error.localizedDescription)
                FTIndicator.dismissProgress()
                FTIndicator.showToastMessage(error.localizedDescription)
        })
        
        
    }
    
    
    
    func callToAddPhotoIdentityServiceProvider(){
        
        let user = Constants.ServiceProviderRegistrationData.ServiceProviderUserData
        
        
        let params = [
            URLParams.USER_TYPE: user.userType!,
            URLParams.AUTHENTICATE_TOKEN: user.authenticateToken!,
            URLParams.USER_ID: user.userId!
            ]
        
        
        print(params)
        let image = imgVwId.image
        
        WebserviceUtils.callPostRequestForUserRegistration(URLConstants.SERVICE_PROVIDER_ADD_PHOTO_IDENTITY ,image: image, params: params , imageParam: "photoIdentity" , success: { (response) in
            
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                        if let dic = json.objectForKey("serviceProvider"){
                            
                            if let photoIdentity = dic.objectForKey("photoIdentity"){
                                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.photoIdentityImage = photoIdentity as! String
                            }
                            if let userProfileImage = dic.objectForKey("userProfileImage"){
                                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.userProfileImage = userProfileImage as! String
                            }
                            if let hostUserId = dic.objectForKey("hostUserId"){
                                Constants.ServiceProviderRegistrationData.ServiceProviderUserData.hostUserId = hostUserId as! String
                            }
                            
                        }
                        
                        //to add service provider to property
                        if PrefrencesUtils.getBoolFromPrefs(PreferenceConstant.IS_SERVICE_PROVIDER_ADDED_BY_HOST) == true {
                            self.callToAddServiceProvider()
                        }
                        else {
                            
                        //save user data in DB
                        DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(Constants.ServiceProviderRegistrationData.ServiceProviderUserData)
                        //move to verification screen
                        self.toMoveToNextScreenAccordingToUserRegistrationType()
                            
                        }
                        
                    }
                
                }}
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    func callToAddServiceProvider(){
        var userId: String = ""
        var authenticateToken: String = ""
        var propertyId: String = ""
        var serviceProviderId: String = ""
        
        //get user id
        if let user_id = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.HOST_ID) {
            userId = user_id
        }
        //get authenticate token
        if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.AUTHENTICATE_TOKEN_HOST) {
            authenticateToken = token
        }
        //get propertyId
        if let property_id = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.HOST_PROPERTY_ID){
            propertyId = property_id
        }
        
        serviceProviderId = Constants.ServiceProviderRegistrationData.ServiceProviderUserData.userId
        
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PROPERTY_ID: propertyId,
            URLParams.SERVICE_PROVIDER_ID: serviceProviderId
        ]
        print(params)
        
        WebserviceUtils.callToPostUrl(URLConstants.ADD_SERVICE_PROVIDER_TO_PROPERTY, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let msg = json.objectForKey("message"){
                    FTIndicator.showToastMessage(msg as! String)
                }
                
                PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_SERVICE_PROVIDER_ADDED_BY_HOST, value: false)
                self.toMoveToServiceProviderListScreen()
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    
    func toMoveToServiceProviderListScreen(){
    performSegueWithIdentifier("SHOW_ADD_SERVICE_PROVIDER_VC_BY_HOST", sender: self)
    }
    
    
    
    func toMoveToNextScreenAccordingToUserRegistrationType(){
        if !isLoginViaSocialMedia{
            let verification_vc = self.storyboard?.instantiateViewControllerWithIdentifier("VerificationViewController") as! VerificationViewController
            self.navigationController?.pushViewController(verification_vc, animated: true)
        }
        else {
            FTIndicator.showSuccessWithMessage("User Created Successfully")
            toMakeRevealVwInitial()
        }
    }
    
    func toMakeRevealVwInitial(){
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isLogin")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        appDelegate.window?.rootViewController = yourVC
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    
    
    func validationForProfilePhotoAndIdProof() -> Bool {
        let placeholderImage:UIImage = UIImage(named: "upload-photo")!
        if imgVwProfilePic.image == placeholderImage {
            FTIndicator.showToastMessage("Please select profile photo")
            return false
        }
        else {
            if imgVwId.image == placeholderImage {
                FTIndicator.showToastMessage("Please select ID proof")
                return false
            }
            else{
                return true
            }
        }
    }
   
    
    //MARK:- CUSTOM METHODS
    func initView(){
        btnSubmit.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        CommonFunctions.makeRoundCornerOfButton(btnSubmit)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        //initially hide driving licence field
        Constraint_TxtField_LicenceNo_Height.constant = 0
        constraint_txtField_LicenceNo_Top.constant = 0
        constraint_conductNo_height.constant = 0
        constraint_conductNo_top.constant = 0
        constraint_issue_height.constant = 0
        constraint_issue_top.constant = 0
        constraint_bckVw_height.constant = (407 - 165)
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        
        //Add PickerView Input
        univTextField = txtField_ServiceType
        txtField_ServiceType.inputView = pickerView
        
        //Add Drop Icon
        self.dropIcon(txtField_ServiceType)
        self.dropIcon(txtField_licenceExpiryDate)
        
        self.layerFields(txtField_ServiceType)
        self.layerFields(txtField_licenceExpiryDate)
        self.layerFields(txtField_ConductNo)
        self.layerFields(txtField_IssueNo)
        self.layerFields(txtField_BirthCertificate_PassportNo)
        vw_Id.layer.cornerRadius = 8
        vw_Id.clipsToBounds = true
        vw_Profile.layer.cornerRadius = 8
        vw_Profile.clipsToBounds = true
        
        self.rtIcon(txtField_ServiceType, imageName: "service-type")
        self.rtIcon(txtField_licenceExpiryDate, imageName: "license")
        self.rtIcon(txtField_ConductNo, imageName: "contact-number")
        self.rtIcon(txtField_IssueNo, imageName: "issue")
        self.rtIcon(txtField_BirthCertificate_PassportNo, imageName: "passport")
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(SignUpTwoServiceProviderViewController.tappedOnImageVw))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(SignUpTwoServiceProviderViewController.tappedOnImageVw))
        imgVwProfilePic.addGestureRecognizer(tap1)
        imgVwProfilePic.userInteractionEnabled = true
        imgVwProfilePic.tag = 3
        imgVwId.addGestureRecognizer(tap2)
        imgVwId.userInteractionEnabled = true
        imgVwId.tag = 4
    }
    
    
    func addDatePicker(textField: UITextField){
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
        let todayDate = NSDate()
        let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
        let minimumYear = (components?.year)! - 1917
        let minimumMonth = (components?.month)! - 1
        let minimumDay = (components?.day)! - 1
        let comps = NSDateComponents();
        comps.year = -minimumYear
        comps.month = -minimumMonth
        comps.day = -minimumDay
        let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        let maxYear = (components?.year)! - 2017
        let maxMonth = (components?.month)! - 1
        let maxDay = (components?.day)! - 1
        let maxcomps = NSDateComponents();
        maxcomps.year = -maxYear
        maxcomps.month = -maxMonth
        maxcomps.day = -maxDay
        let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        
        textField.inputView = datePicker
    }
    
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        txtField_licenceExpiryDate.text! = formattor.stringFromDate(sender.date)
        print("Selected value \(txtField_licenceExpiryDate.text!)")
    }
    
    func tappedOnImageVw(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        var titleString:String = ""
        
        if tappedImageView.tag == 3 {
          titleString = "Select your profile photo"
          isProfilePicImgVwTapped = true
        }
        else if tappedImageView.tag == 4 {
            titleString = "Select your ID proof"
            isProfilePicImgVwTapped = false
        }
        
        let  value:String = NSLocalizedString( titleString , comment: "")
        let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(0)
        }
        let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(1)
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
            print("Cancel")
        }
        sheet.addAction(cameraAction)
        sheet.addAction(galleryAction)
        sheet.addAction(cancelAction)
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            if let popoverController = sheet.popoverPresentationController {
                popoverController.sourceView = tappedImageView
                popoverController.sourceRect = tappedImageView.bounds
            }
            self.presentViewController(sheet, animated: true, completion: nil)
        }
        else{
            presentViewController(sheet, animated: true, completion: nil)
        }
        
        
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageView.contentMode = .ScaleAspectFit
            //imageView.image = pickedImage
            if isProfilePicImgVwTapped {
               imgVwProfilePic.image = pickedImage
            }
            else {
               imgVwId.image = pickedImage
            }
            //selectedIdentityImg = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func openCamorGallary(type : Int){
        if type == 0 {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        presentViewController(imagePicker, animated: true, completion: imagePickerHandler)
    }
    
    func imagePickerHandler(){
        imagePicker.navigationBar.translucent = false
        imagePicker.navigationBar.barTintColor = Constants.Colors.BACKGROUND_COLOUR
    }
    
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        if tappedImageView.tag == 2 {
            txtField_ServiceType.becomeFirstResponder()
        }
    }
    
    func rtIcon(textField : UITextField , imageName : String) {
        
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
    
    
    //MARK:- WEB SERVICE
    func callToRetrieveAllServiceTypes(){
        WebserviceUtils.callPostRequestWithEmptyParams(URLConstants.GET_ALL_SERVICE_TYPES, params: "", success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String{
                    if status.containsString("success"){
                        if let arr = json.objectForKey("serviceTypes") as? NSArray {
                            print("ghsdkajlshas.kj",arr)
                            if arr.count > 0 {
                                self.toParseServiceTypes(arr)
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    func toParseServiceTypes(arrProperty: NSArray) {
        
        print(arrProperty.count)
        arrServiceTypes = []
        
        for item in arrProperty {
            let serviceObj = ServiceType()
            
            if let id = item.objectForKey("id") as? String {
                serviceObj.setValue(id, forKey: "id")
            }
            if let serviceName = item.objectForKey("serviceName") as? String {
                serviceObj.setValue(serviceName, forKey: "serviceName")
            }
            
            arrServiceTypes.append(serviceObj)
        }
    }
    
    
//    //MARK:- NAVIGATION METHODS
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SHOW_ADD_SERVICE_PROVIDER_VC_BY_HOST"{
            let vc = segue.destinationViewController as! ServiceProvider_Controller
            vc.selectedPropertyId = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.HOST_PROPERTY_ID)!
            vc.isAddedByHost = true
        }
    }
    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        // to check validation of service type
        if txtField_ServiceType.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtField_ServiceType.attributedPlaceholder = NSAttributedString(string: "Please select service type", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of driving licenec no
        if txtField_ServiceType.text?.containsString("Driver") == true {
        if txtField_licenceExpiryDate.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtField_licenceExpiryDate.attributedPlaceholder = NSAttributedString(string: "Please enter licence expiry date", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
            }
        
        // to check validation of conduct no
        if txtField_ConductNo.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtField_ConductNo.attributedPlaceholder = NSAttributedString(string: "Please enter conduct no.", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of issue no
        if txtField_IssueNo.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtField_IssueNo.attributedPlaceholder = NSAttributedString(string: "Please enter issue no.", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
            }}
        
        // to check validation of birth certification OR passport
        if txtField_BirthCertificate_PassportNo.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtField_BirthCertificate_PassportNo.attributedPlaceholder = NSAttributedString(string: "Please enter value", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        
        return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }
    
}



func textFieldShouldReturn(textField: UITextField!) -> Bool {
    textField.resignFirstResponder()
    return true
}

    
    
    
    


//MARK:- PickerView Delegates/Datasource
extension SignUpTwoServiceProviderViewController: UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if univTextField == txtField_ServiceType {
            return arrServiceTypes.count
        }
        return 0
    }
}

extension SignUpTwoServiceProviderViewController: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if univTextField == txtField_ServiceType {
            return arrServiceTypes[row].serviceName
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTextField == txtField_ServiceType {
            serviceTypeID = arrServiceTypes[row].id
            txtField_ServiceType.text! = arrServiceTypes[row].serviceName
        if arrServiceTypes[row].serviceName.containsString("Driver") {
            constraint_txtField_LicenceNo_Top.constant = 12
            Constraint_TxtField_LicenceNo_Height.constant = 44
            constraint_conductNo_height.constant = 44
            constraint_conductNo_top.constant = 12
            constraint_issue_height.constant = 44
            constraint_issue_top.constant = 12
            constraint_bckVw_height.constant = 407
        }
        else {
            Constraint_TxtField_LicenceNo_Height.constant = 0
            constraint_txtField_LicenceNo_Top.constant = 0
            constraint_conductNo_height.constant = 0
            constraint_conductNo_top.constant = 0
            constraint_issue_height.constant = 0
            constraint_issue_top.constant = 0
            constraint_bckVw_height.constant = (407 - 165)
        }
    }
}
}

//MARK:- Textfield Delegates/Datasource
extension SignUpTwoServiceProviderViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        univTextField = textField
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
        if (textField == txtField_ServiceType) || (textField == txtField_licenceExpiryDate)  {
            textField.tintColor = UIColor.clearColor()
            
            if textField == txtField_ServiceType {
            textField.text = arrServiceTypes[0].serviceName
            }
            else {
               self.addDatePicker(textField)
            }
        }
        
        
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
       // let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if textField.text?.characters.count > 100 {
            return false
        }
        
        return true
    }
    
}



