//
//  AddPropertySubmitViewController.swift
//  HSS
//
//  Created by Rajni on 18/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator


class AddPropertySubmitViewController: UIViewController , UIPickerViewDelegate {
    
    //MARK:- OUTLETS
    @IBOutlet weak var txtFieldCommonAmenties: UITextField!
    
    @IBOutlet weak var txtField_furnishes: UITextField!
    @IBOutlet weak var txtFieldAdditionalAmenties: UITextField!
    @IBOutlet weak var txtField_cancellation_policy: UITextField!
    @IBOutlet weak var txtFieldSpecialFeature: UITextField!
    @IBOutlet weak var txtFieldHomeSafety: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    
    @IBOutlet weak var switch_fireExtinguishers: UISwitch!
    
    @IBOutlet weak var switch_fireAlarm: UISwitch!
    
    @IBOutlet weak var switch_caseShutOff: UISwitch!
    
    @IBOutlet weak var switch_emergencyExit: UISwitch!
    
    @IBOutlet weak var btn_reset_property_bookings: UIButton!
    //MARK:- VARIABLES
    var fireExtinguisherValue: String = "false"
    var fireAlarmValue: String = "false"
    var caseShutOffValue: String = "false"
    var emergencyExitValue: String = "false"
    var data_Detail_Property: PropertyDetailsModel!
    var data_after_editing: PropertyData!
    var arr_furnishing = ["Furnished" , "Unfurnished"]
    var arr_cancellation = ["Strict (20% deduction)" , "Moderate (15% deduction)" , "Flexible (10% deduction)"]
    var univTextField : UITextField!
    var pickerView = UIPickerView()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = "Add Property"
        //set back button
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(AddPropertySubmitViewController.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.initView()
        if data_Detail_Property == nil{
            
            btn_reset_property_bookings.hidden = true
            btn_reset_property_bookings.userInteractionEnabled = false
        }
        else {
            btn_reset_property_bookings.hidden = false
            btn_reset_property_bookings.userInteractionEnabled = true
            fill_data()
        }
        
        
    }
    
    
    
    func initView(){
        //Add Drop Icon
        self.dropIcon(txtField_furnishes)
        self.dropIcon(txtField_cancellation_policy)
        
        // Add padding to textfield
        self.addPadding(txtField_furnishes)
        self.addPadding(txtField_cancellation_policy)
        self.addPadding(txtFieldCommonAmenties)
        self.addPadding(txtFieldAdditionalAmenties)
        self.addPadding(txtFieldHomeSafety)
        self.addPadding(txtFieldSpecialFeature)
        
        //picker view
        univTextField = txtField_furnishes
        pickerView.delegate = self
        txtField_furnishes.inputView = pickerView
        txtField_cancellation_policy.inputView = pickerView
    }
    
    func addPadding(textField: UITextField){
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 10, 40)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    func back(sender: UIBarButtonItem) {
        
        // Go back to the previous ViewController
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        btn_submit.userInteractionEnabled = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        FTIndicator.dismissProgress()
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddPropertyOneViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on drop down")
        
        switch tappedImageView.tag {
        case 1:
            txtField_furnishes.becomeFirstResponder()
        case 2:
            txtField_cancellation_policy.becomeFirstResponder()
        default:
            print("invalid selection")
            
        }
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        
        univTextField = textField
        
        if (textField == txtField_cancellation_policy) || (textField == txtField_furnishes) {
            textField.tintColor = UIColor.clearColor()
        }
        
//        if (textField == txtField_furnishes) {
//            txtFieldBookingType.text! = arrBookingType[0]
//            pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: true)
//        }
        
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if string == ""{
            return true
        }
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        switch univTextField {
        case txtField_furnishes:
            return arr_furnishing.count
        case txtField_cancellation_policy:
            return arr_cancellation.count
        default:
            print("invalid")
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch univTextField {
        case txtField_furnishes:
            return arr_furnishing[row]
        case txtField_cancellation_policy:
            return arr_cancellation[row]
        default:
            print("invalid")
        }
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTextField == txtField_furnishes {
            txtField_furnishes.text = arr_furnishing[row]
        }
      else if univTextField == txtField_cancellation_policy {
        txtField_cancellation_policy.text! = arr_cancellation[row]
        }
    }

    
    // Edit Property ----------
    func fill_data(){
        
        self.title = "Edit Property"
        let common_Amenties: String = data_Detail_Property.commonAmenties
        let additional_Amenities : String = data_Detail_Property.additionalMenties
        let special_Feature : String = data_Detail_Property.specialFeature
        let home_Safety : String = data_Detail_Property.homeSafety
        let fire_Extinguishers : String = data_Detail_Property.fireExtinguisher
        if fire_Extinguishers.containsString("true"){
            switch_fireExtinguishers.setOn(true, animated: false)
        }
        else {
            switch_fireExtinguishers.setOn(false, animated: false)
        }
        let fireAlaram : String = data_Detail_Property.fireAlarm
        if fireAlaram.containsString("true"){
            switch_fireAlarm.setOn(true, animated: false)
        }
        else {
            switch_fireAlarm.setOn(false, animated: false)
        }
        let case_Shutoff_Valve : String = data_Detail_Property.gasShutoffValve
        if case_Shutoff_Valve.containsString("true"){
            switch_caseShutOff.setOn(true, animated: false)
        }
        else {
            switch_caseShutOff.setOn(false, animated: false)
        }
        let emergencyExit : String = data_Detail_Property.emergencyExit
        if emergencyExit.containsString("true"){
            switch_emergencyExit.setOn(true, animated: false)
        }
        else {
            switch_emergencyExit.setOn(false, animated: false)
        }
        
        txtFieldCommonAmenties.text = common_Amenties
        txtFieldAdditionalAmenties.text = additional_Amenities
        txtFieldSpecialFeature.text = special_Feature
        txtFieldHomeSafety.text = home_Safety
        
        
        //to set furnishes
        txtField_furnishes.text = data_Detail_Property.furnishing
        
        //to set cancellation policy
        if let policy = data_Detail_Property.cancellationPolicy {
            if policy.containsString("strict"){
              txtField_cancellation_policy.text = arr_cancellation[0]
            }
            else if policy.containsString("moderate") {
                txtField_cancellation_policy.text = arr_cancellation[1]
            }
            else {
               txtField_cancellation_policy.text = arr_cancellation[2]
            }
        }
        else {
            txtField_cancellation_policy.text = arr_cancellation[0]
        }
        
        
        if data_Detail_Property.fireExtinguisher == nil{  //1
        fireExtinguisherValue = "false"
        }
        else{
            if data_Detail_Property.fireExtinguisher == "false"{
                fireExtinguisherValue = "false"
            }
            else if data_Detail_Property.fireExtinguisher == "true" {
                //let fire_Extinguishers : String = data_Detail_Property.fireExtinguisher
                fireExtinguisherValue = "true"//fire_Extinguishers
            }
        }

            if data_Detail_Property.fireAlarm == nil{ //2
                fireAlarmValue = "false"
            }
            else{
                if data_Detail_Property.fireAlarm == "false"{
                    fireAlarmValue = "false"
                }
                else if data_Detail_Property.fireAlarm == "true" {
                     //let fire_Alaram : String = data_Detail_Property.fireAlarm
                    fireAlarmValue = "true"//fire_Alaram
                }
            }
            
                if data_Detail_Property.gasShutoffValve == nil{ //3
                    caseShutOffValue = "false"
                }
            
                else{
                    if data_Detail_Property.gasShutoffValve == "false"{
                        caseShutOffValue = "false"
                    }
                    else if data_Detail_Property.gasShutoffValve == "true" {
                        //let case_Shutoff_Valve : String = data_Detail_Property.gasShutoffValve
                        caseShutOffValue = "true"//case_Shutoff_Valve
                    }
                }

                
                    if data_Detail_Property.emergencyExit == nil{ // 4
                        emergencyExitValue = "false"
                    }
                    else{
                        if data_Detail_Property.emergencyExit == "false"{
                            emergencyExitValue = "false"
                        }
                        else if data_Detail_Property.emergencyExit != "true" {
                            //let emergencyExit : String = data_Detail_Property.emergencyExit
                            emergencyExitValue = "true"//emergencyExit
                        }
                    }


        
    }

    
    //MARk:- SWITCH ACTIONS
    @IBAction func switchFireExtinguisherAction(sender: UISwitch) {
        if sender.on {
            print("ON")
            fireExtinguisherValue = "true"
        }
        else {
            print("OFF")
            fireExtinguisherValue = "false"
        }
    }
    
    @IBAction func switchFireAlarmAction(sender: UISwitch) {
        if sender.on {
            print("ON")
            fireAlarmValue = "true"
        }
        else {
            print("OFF")
            fireAlarmValue = "false"
        }
    }
    
    @IBAction func switchCaseShutOffAction(sender: UISwitch) {
        if sender.on {
            print("ON")
            caseShutOffValue = "true"
        }
        else {
            print("OFF")
            caseShutOffValue = "false"
        }
    }
    @IBAction func switchEmergencyExitAction(sender: UISwitch) {
        if sender.on {
            print("ON")
            emergencyExitValue = "true"
        }
        else {
            print("OFF")
            emergencyExitValue = "false"
        }
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnSubmitClick(sender: AnyObject) {
        
        if Validate() {
            
            btn_submit.userInteractionEnabled = false

            if data_Detail_Property == nil{
                self.savePropertyDataLocally()
                self.callToAddPropertyWebService()
            }
            else {
                self.savePropertyDataLocally()
                self.callToEditPropertyWebService()
            }
        }
        
    }
    
    
    @IBAction func btn_reset_property_bookings_click(sender: UIButton) {
        self.callToResetPropertyBookings()
    }
    
    
    func callToResetPropertyBookings(){
        var propertyId: String = ""
        
        // get property id
        if let property_id = data_Detail_Property.id {
            propertyId = property_id
        }
    
        
        let params = [
            URLParams.PROPERTY_ID: propertyId
        ]
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.RESET_PROPERTY_BOOKINGS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                
                print("reset-----" , json)
                if let status = json.objectForKey("status"){
                    if status as! String == "success"{
                        
                        if let msg = json.objectForKey("message") as? String {
                            FTIndicator.showInfoWithMessage(msg)
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    //MARK:- CUSTOM METHODS
    
    func savePropertyDataLocally(){
        Constants.property.arrPropertyData[0].commonAmenties = txtFieldCommonAmenties.text!
        Constants.property.arrPropertyData[0].addAmenties = txtFieldAdditionalAmenties.text!
        Constants.property.arrPropertyData[0].spclFeature = txtFieldSpecialFeature.text!
        Constants.property.arrPropertyData[0].homeSafety = txtFieldHomeSafety.text!
        Constants.property.arrPropertyData[0].fireExtinguisher = fireExtinguisherValue
        Constants.property.arrPropertyData[0].fireAlarm = fireAlarmValue
        Constants.property.arrPropertyData[0].gasShutOffValve = caseShutOffValue
        Constants.property.arrPropertyData[0].emergencyExit = emergencyExitValue
        
        if let fur = txtField_furnishes.text {
           Constants.property.arrPropertyData[0].furnishing = fur
        }
        
        if let policy = txtField_cancellation_policy.text {
            if policy.containsString("20") == true {
               Constants.property.arrPropertyData[0].cancellation_policy = "Strict"
            }
            else if policy.containsString("15") == true {
              Constants.property.arrPropertyData[0].cancellation_policy = "Moderate"
            }
            else {
              Constants.property.arrPropertyData[0].cancellation_policy = "Flexible"
            }
        }
    }
    
    
    
    
    
    //MARK:- WEB SERVICE ADD PROPERTY
    func callToAddPropertyWebService(){
        var userId: String = ""
        var authenticateToken = ""
        var arrOfImages = [UIImage]()
        var cancel_policy: String = ""
        
        //get user id from prefrences
        if let id = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_ID){
            userId = id
        }
        
        // get authenticate token from prefrences
        if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.AUTHENTICATE_TOKEN) {
            authenticateToken = token
        }
        
        if let policy = txtField_cancellation_policy.text {
            if policy.containsString("20") == true {
                cancel_policy = "Strict"
            }
            else if policy.containsString("15") == true {
                cancel_policy = "Moderate"
            }
            else {
                cancel_policy = "Flexible"
            }
        }
     
        
        //start loader
        //FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PROPERTY_TYPE: Constants.property.arrPropertyData[0].propertyType!,
            URLParams.FURNISHING: Constants.property.arrPropertyData[0].furnishing! ,
            URLParams.ROOM_TYPE: Constants.property.arrPropertyData[0].roomType!,
            URLParams.ACCOMMODATION_TYPE: Constants.property.arrPropertyData[0].accommodationType!,
            URLParams.BEDROOM_COUNT: Constants.property.arrPropertyData[0].bedRoomCount!,
            URLParams.BEDS_COUNT: Constants.property.arrPropertyData[0].bedsCount!,
            URLParams.BATHROOM_COUNT: Constants.property.arrPropertyData[0].bathRoomCount!,
            URLParams.PROFILE_NAME: Constants.property.arrPropertyData[0].profileName!,
            URLParams.SUMMARY: Constants.property.arrPropertyData[0].summary!,
            URLParams.COUNTRY_NAME: Constants.property.arrPropertyData[0].countryName!,
            URLParams.PROPERTY_LATITUDE: Constants.property.arrPropertyData[0].latitude!,
            URLParams.PROPERTY_LONGITUDE: Constants.property.arrPropertyData[0].longitude!,
            URLParams.STREET_ADDRESS: Constants.property.arrPropertyData[0].streetAdd!,
            URLParams.APT_SUITE_BLDG: Constants.property.arrPropertyData[0].AptOrSuite!,
            URLParams.CITY: Constants.property.arrPropertyData[0].city!,
            URLParams.STATE: Constants.property.arrPropertyData[0].state!,
            URLParams.ZIPCODE: Constants.property.arrPropertyData[0].zipCode!,
            URLParams.TYPE_OF_BOOKING: Constants.property.arrPropertyData[0].bookingType!,
            URLParams.CURRENCY: Constants.property.arrPropertyData[0].currencyType!,
            URLParams.SET_PRICE: Constants.property.arrPropertyData[0].price!,
            URLParams.COMMON_AMENTIES: Constants.property.arrPropertyData[0].commonAmenties!,
            URLParams.ADDITIONAL_AMENTIES: Constants.property.arrPropertyData[0].addAmenties!,
            URLParams.SPECIAL_FEATURE: Constants.property.arrPropertyData[0].spclFeature!,
            URLParams.HOME_SAFETY: Constants.property.arrPropertyData[0].homeSafety!,
            URLParams.FIRE_EXTINGUISHERS: Constants.property.arrPropertyData[0].fireExtinguisher!,
            URLParams.FIRE_ALARM: Constants.property.arrPropertyData[0].fireAlarm!,
            URLParams.GAS_SHUT_OFF_VALVA: Constants.property.arrPropertyData[0].gasShutOffValve!,
            URLParams.EMERGENCY_EXIT: Constants.property.arrPropertyData[0].emergencyExit!,
            URLParams.CANCELLATION_POLICY: cancel_policy
            
           // URLParams.CANCELLATION_POLICY: Constants.property.arrPropertyData[0].cancellation_policy ,
           // URLParams.FURNISHING: Constants.property.arrPropertyData[0].furnishing
        ]
        
        print(params)
        
        arrOfImages = Constants.property.arrPropertyData[0].arrImges!
        print(arrOfImages.count)
        
        WebserviceUtils.callToAddProperty(URLConstants.ADD_PROPERTY , arrImages: arrOfImages, params: params , success: { (response) in
            
            
            if let json = response as? NSDictionary {
            print(json)
            
               // FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String {
                
                if status.containsString("success"){
                if let msg = json.objectForKey("message") {
                    print(msg)
                    FTIndicator.showSuccessWithMessage(msg as! String)
                    self.toOpenHomeVC()
                    
                }}
                }
            }
            
            },
            failure: { (error) in
            print(error.localizedDescription)
           // FTIndicator.dismissProgress()
            FTIndicator.showToastMessage(error.localizedDescription)
                                            
        })
        
    }
    
    
    //MARK:- WEB SERVICE EDIT PROPERTY
    func callToEditPropertyWebService(){
        var userId: String = ""
        var authenticateToken = ""
        var arrOfImages = [UIImage]()
        var propertyID: String = ""
        var cancel_policy: String = ""
        
        //get user id from prefrences
        if let id = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_ID){
            userId = id
        }
        
        // get authenticate token from prefrences
        if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.AUTHENTICATE_TOKEN) {
            authenticateToken = token
        }
        
        // get property id
        if let property_id = data_Detail_Property.id {
            propertyID = property_id
        }
        
        if let policy = txtField_cancellation_policy.text {
            if policy.containsString("20") == true {
                cancel_policy = "Strict"
            }
            else if policy.containsString("15") == true {
                cancel_policy = "Moderate"
            }
            else {
                cancel_policy = "Flexible"
            }
        }
        
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PROPERTY_TYPE: Constants.property.arrPropertyData[0].propertyType!,
            URLParams.ROOM_TYPE: Constants.property.arrPropertyData[0].roomType!,
            URLParams.ACCOMMODATION_TYPE: Constants.property.arrPropertyData[0].accommodationType!,
            URLParams.BEDROOM_COUNT: Constants.property.arrPropertyData[0].bedRoomCount!,
            URLParams.BEDS_COUNT: Constants.property.arrPropertyData[0].bedsCount!,
            URLParams.BATHROOM_COUNT: Constants.property.arrPropertyData[0].bathRoomCount!,
            URLParams.PROFILE_NAME: Constants.property.arrPropertyData[0].profileName!,
            URLParams.SUMMARY: Constants.property.arrPropertyData[0].summary!,
            URLParams.COUNTRY_NAME: Constants.property.arrPropertyData[0].countryName!,
            URLParams.PROPERTY_LATITUDE: Constants.property.arrPropertyData[0].latitude!,
            URLParams.PROPERTY_LONGITUDE: Constants.property.arrPropertyData[0].longitude!,
            URLParams.STREET_ADDRESS: Constants.property.arrPropertyData[0].streetAdd!,
            URLParams.APT_SUITE_BLDG: Constants.property.arrPropertyData[0].AptOrSuite!,
            URLParams.CITY: Constants.property.arrPropertyData[0].city!,
            URLParams.STATE: Constants.property.arrPropertyData[0].state!,
            URLParams.ZIPCODE: Constants.property.arrPropertyData[0].zipCode!,
            URLParams.TYPE_OF_BOOKING: Constants.property.arrPropertyData[0].bookingType!,
            URLParams.CURRENCY: Constants.property.arrPropertyData[0].currencyType!,
            URLParams.SET_PRICE: Constants.property.arrPropertyData[0].price!,
            URLParams.COMMON_AMENTIES: Constants.property.arrPropertyData[0].commonAmenties!,
            URLParams.ADDITIONAL_AMENTIES: Constants.property.arrPropertyData[0].addAmenties!,
            URLParams.SPECIAL_FEATURE: Constants.property.arrPropertyData[0].spclFeature!,
            URLParams.HOME_SAFETY: Constants.property.arrPropertyData[0].homeSafety!,
            URLParams.FIRE_EXTINGUISHERS: Constants.property.arrPropertyData[0].fireExtinguisher!,
            URLParams.FIRE_ALARM: Constants.property.arrPropertyData[0].fireAlarm!,
            URLParams.GAS_SHUT_OFF_VALVA: Constants.property.arrPropertyData[0].gasShutOffValve!,
            URLParams.EMERGENCY_EXIT: Constants.property.arrPropertyData[0].emergencyExit!,
            URLParams.PROPERTY_ID: propertyID,
            URLParams.FURNISHING: Constants.property.arrPropertyData[0].furnishing! ,
            URLParams.CANCELLATION_POLICY: cancel_policy
        ]
        
        print(params)
        arrOfImages = Constants.property.arrPropertyData[0].arrImges!
        print(arrOfImages.count)
        
        WebserviceUtils.callToAddProperty(URLConstants.EDIT_PROPERTY , arrImages: arrOfImages, params: params , success: { (response) in
            
            
            if let json = response as? NSDictionary {
                print(json)
                
                // FTIndicator.dismissProgress()
                if let status = json.objectForKey("status") as? String {
                    
                    if status.containsString("success"){
                        if let msg = json.objectForKey("message") {
                            print(msg)
                            FTIndicator.showSuccessWithMessage(msg as! String)
                            self.toOpenHomeVC()
                            
                        }}
                }
            }
            
            },
                                          failure: { (error) in
                                            print(error.localizedDescription)
                                            // FTIndicator.dismissProgress()
                                            FTIndicator.showToastMessage(error.localizedDescription)
                                            
        })
        
    }
    
    
    func toOpenHomeVC(){
        CommonFunctions.globals.isHostAdd_EditProperty = true
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    
    //    //MARK:- VALIDATION METHODS
        func Validate() -> Bool{
            var valid:Bool = true
    
            // to check validation of common amenties
            if txtFieldCommonAmenties.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldCommonAmenties.attributedPlaceholder = NSAttributedString(string: "Please enter common amenties", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of additional amenties
            if txtFieldAdditionalAmenties.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldAdditionalAmenties.attributedPlaceholder = NSAttributedString(string: "Please enter additional amenties", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of special feature
            if txtFieldSpecialFeature.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldSpecialFeature.attributedPlaceholder = NSAttributedString(string: "Please enter special feature", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of home safety
            if txtFieldHomeSafety.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldHomeSafety.attributedPlaceholder = NSAttributedString(string: "Please enter home safety", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            return valid
        }
    
        func AnimationShakeTextField(textField:UITextField){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
            animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
            textField.layer.addAnimation(animation, forKey: "position")
        }
}
