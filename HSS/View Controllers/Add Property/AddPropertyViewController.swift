//
//  AddPropertyViewController.swift
//  HSS
//
//  Created by Rajni on 08/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import FTIndicator

class AddPropertyViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtFieldPropertyType: UITextField!
    @IBOutlet weak var txtFieldRoomType: UITextField!
    @IBOutlet weak var txtFieldAccommodation: UITextField!
    @IBOutlet weak var txtFieldBedRoom: UITextField!
    @IBOutlet weak var txtFieldBeds: UITextField!
    @IBOutlet weak var txtFieldBathRooms: UITextField!
    @IBOutlet weak var txtFieldProfileName: UITextField!
    @IBOutlet weak var txtVwSummary: UITextView!
    
    var arrPropertyImagesName = [String]()
    let escapedAddress = String()
    var isEditPropertyBtnClicked: Bool = false
    
    
    
    //MARK:- VARIABLES
    var otherOptionTxtField: UITextField!
    let imagePicker = UIImagePickerController()
    var arrSelectedImages = [UIImage]()
    var arrUserData: [UserRegistration] = []
    var pickerView = UIPickerView()
    var univTextField : UITextField!
    var arrPropertyType = [PropertyType]()
    var arrRoomType = [RoomType]()
    var arrPropertyImageModel = [PropertyImage]()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var arrBedRoom = ["1 Room" , "2 Rooms" , "3 Rooms" , "4 Rooms" , "5 Rooms" , "6 Rooms" , "7 Rooms" , "8 Rooms", "Other"]
    var arrBeds = ["1 Bed" , "2 Beds" , "3 Beds" , "4 Beds" , "5 Beds" , "6 Beds" , "7 Beds" , "8 Beds" , "Other"]
    var arrBathRooms = ["1 Bath Room" , "2 Bath Rooms" , "3 Bath Rooms" , "4 Bath Rooms" , "5 Bath Rooms" , "6 Bath Rooms" , "7 Bath Rooms" , "8 Bath Rooms" , "Other"]
    var arrAccommodationPrsn = ["1 Person" , "2 Persons" , "3 Persons" , "4 Persons" , "5 Persons" , "6 Persons" , "7 Persons" , "8 Persons" , "Other"]
    
    var add_PropertyArr : PropertyDetailsModel!
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = "Add Property"
        
        //set back button
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(AddPropertyViewController.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        
        if add_PropertyArr == nil{
           
        }
        else{
            self.title = "Edit Property"
            self.fill_data()
        }
        
        
        imagePicker.delegate = self
        
        arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
 //Edit Property------
        
        
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        
        //Add PickerView Input
        univTextField = txtFieldPropertyType
        txtFieldPropertyType.inputView = pickerView
        txtFieldAccommodation.inputView = pickerView
        txtFieldRoomType.inputView = pickerView
        txtFieldBedRoom.inputView = pickerView
        txtFieldBathRooms.inputView = pickerView
        txtFieldBeds.inputView = pickerView
        
        //Add Drop Icon
        self.dropIcon(txtFieldPropertyType)
        self.dropIcon(txtFieldAccommodation)
        self.dropIcon(txtFieldRoomType)
        self.dropIcon(txtFieldBedRoom)
        self.dropIcon(txtFieldBeds)
        self.dropIcon(txtFieldBathRooms)
        
        // Add padding to textfield
        self.addPadding(txtFieldPropertyType)
        self.addPadding(txtFieldRoomType)
        self.addPadding(txtFieldAccommodation)
        self.addPadding(txtFieldBedRoom)
        self.addPadding(txtFieldBeds)
        self.addPadding(txtFieldBathRooms)
        self.addPadding(txtFieldProfileName)
        
        //self.toGetPropertyImagesArr()
        
    }
    
    
    func back(sender: UIBarButtonItem) {
        
        if isEditPropertyBtnClicked {
          CommonFunctions.globals.isHostAdd_EditProperty = true
          self.navigationController?.popToRootViewControllerAnimated(true)
        }
        else {
        // Go back to the previous ViewController
        self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    
    func toGetPropertyImagesArr() {
        if let strImages = add_PropertyArr.images {
            let imgNameArr = strImages.characters.split{$0 == ","}.map(String.init)
            
            print(imgNameArr.count)
            for imgName in imgNameArr{
                print(imgName)
                arrPropertyImagesName.append(imgName)
            }
            
        }
    }

    
    func fill_data(){
        

            let roomTypeTxt : String = add_PropertyArr.roomType
            let accommodationTxt : String = add_PropertyArr.accommodation
            let bedRoomTxt : String = add_PropertyArr.bedRoom
            let bedsTxt : String = add_PropertyArr.beds
            let bathRoomTxt : String = add_PropertyArr.bathRoom
            let profileNameTxt : String = add_PropertyArr.profileName
            let summaryTxt : String = add_PropertyArr.summary
            let imags : String = add_PropertyArr.images  // hostProfile
            let propertyTypeTxt : String = add_PropertyArr.propertyType
            print("jkghkjghkj",(imags))
        
            txtFieldPropertyType.text = propertyTypeTxt
            txtFieldRoomType.text = roomTypeTxt
            txtFieldAccommodation.text = accommodationTxt
            txtFieldBedRoom.text = bedRoomTxt
            txtFieldBathRooms.text = bathRoomTxt
            txtFieldProfileName.text = profileNameTxt
            txtVwSummary.text = summaryTxt
            txtFieldBeds.text = bedsTxt
        
            //print(add_PropertyArr.images)
           //let arrPropertyImages = add_PropertyArr.images.characters.split{$0 == ","}.map(String.init)
           //print(arrPropertyImages.count)
        
           //save to local arr property images name
           //self.arrPropertyImagesName = arrPropertyImages
            arrPropertyImageModel = add_PropertyArr.arrImages
           isEditPropertyBtnClicked = true
           self.collectionView.reloadData()
        }

    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
           app.uni_vc = self
            //call property type webservice
            if DatabaseBLFunctions.fetchPropertyType().count < 1 {
                self.callGetPropertyType()
            }
            if DatabaseBLFunctions.fetchRoomType().count < 1 {
                self.callGetRoomType()
  
            }
            
            //fetch data from DB and save it to local array
            if DatabaseBLFunctions.fetchPropertyType().count > 0 {
                self.arrPropertyType = DatabaseBLFunctions.fetchPropertyType()
            }
            if DatabaseBLFunctions.fetchRoomType().count > 0 {
                self.arrRoomType = DatabaseBLFunctions.fetchRoomType()
            }
            
     
       
  }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    
    //MARK:- BUTTON ACTIONS
    @IBAction func btnNextClick(sender: AnyObject) {
        
        if Validate() {
        savePropertyDataLocally()
        }
    }
    
    func savePropertyDataLocally(){
        
        Constants.property.arrPropertyData = []
        let propertyData = PropertyData()
        
        propertyData.propertyType = txtFieldPropertyType.text!
        propertyData.roomType = txtFieldRoomType.text!
        propertyData.accommodationType = txtFieldAccommodation.text!
        propertyData.bedRoomCount = txtFieldBedRoom.text!
        propertyData.bedsCount = txtFieldBeds.text!
        propertyData.bathRoomCount = txtFieldBathRooms.text!
        propertyData.profileName = txtFieldProfileName.text!
        propertyData.summary = txtVwSummary.text!
        propertyData.arrImges = []
        if isEditPropertyBtnClicked{
         arrSelectedImages = []
            for item in arrPropertyImageModel{
                if item.id == "" {
                arrSelectedImages.append(item.selectedImage)
                }
            }
            print(arrSelectedImages.count)
            propertyData.arrImges = arrSelectedImages
        }
        else {
        propertyData.arrImges = arrSelectedImages
        }
        
        
        Constants.property.arrPropertyData.append(propertyData)
        
        //move to next screen
        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("AddPropertyOneViewController") as! AddPropertyOneViewController
        show_Detail_obj.data_Property_detail = add_PropertyArr
        show_Detail_obj.data_after_editing = Constants.property.arrPropertyData[0]
        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            if isEditPropertyBtnClicked{
                let imgObj = PropertyImage()
                imgObj.id = ""
                imgObj.selectedImage = pickedImage
                arrPropertyImageModel.append(imgObj)
            }
            else {
            //add images to local array
            arrSelectedImages.append(pickedImage)
            }
            self.collectionView.reloadData()
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func openCamorGallary(type : Int){
        if type == 0 {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        presentViewController(imagePicker, animated: true, completion: imagePickerHandler)
    }
    
    func imagePickerHandler(){
        imagePicker.navigationBar.translucent = false
        imagePicker.navigationBar.barTintColor = Constants.Colors.BACKGROUND_COLOUR
    }
    
    func addPadding(textField: UITextField){
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 10, 40)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddPropertyViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on drop down")
        
        switch tappedImageView.tag {
        case 1:
            txtFieldPropertyType.becomeFirstResponder()
        case 2:
            txtFieldRoomType.becomeFirstResponder()
        case 3:
            txtFieldAccommodation.becomeFirstResponder()
        case 4:
            txtFieldBedRoom.becomeFirstResponder()
        case 5:
            txtFieldBeds.becomeFirstResponder()
        case 6:
            txtFieldBathRooms.becomeFirstResponder()
        default:
            print("invalid selection")
            
        }
    }
    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
            var valid:Bool = true
    
            // to check validation of property type
            if txtFieldPropertyType.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldPropertyType.attributedPlaceholder = NSAttributedString(string: "Please select property type", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of room type
            if txtFieldRoomType.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldRoomType.attributedPlaceholder = NSAttributedString(string: "Please select room type", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of accommodation
            if txtFieldAccommodation.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldAccommodation.attributedPlaceholder = NSAttributedString(string: "Please select acc. type", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of bed room
            if txtFieldBedRoom.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldBedRoom.attributedPlaceholder = NSAttributedString(string: "Please select no. of bed rooms", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of beds
            if txtFieldBeds.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldBeds.attributedPlaceholder = NSAttributedString(string: "Please select no. of beds ", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
    
            // to check validation of bath rooms
            if txtFieldBathRooms.text!.utf16.count==0{
                //Change the placeholder color to red for textfield email if
                txtFieldBathRooms.attributedPlaceholder = NSAttributedString(string: "Please select no. of bath rooms", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }
        
        // to check validation of profile name
        if txtFieldProfileName.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldProfileName.attributedPlaceholder = NSAttributedString(string: "Please enter profile name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of profile name
        if txtVwSummary.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtVwSummary.text! = "Please enter summary"
            txtVwSummary.textColor = UIColor.redColor()
            valid = false
        }
        
        if isEditPropertyBtnClicked {
            if arrPropertyImageModel.count==0{
                if txtFieldPropertyType.text!.characters.count > 0 && txtFieldRoomType.text!.characters.count > 0 && txtFieldAccommodation.text!.characters.count > 0 && txtFieldBedRoom.text!.characters.count > 0  && txtFieldBeds.text!.characters.count > 0  && txtFieldBathRooms.text!.characters.count > 0 && txtFieldProfileName.text!.characters.count > 0 && txtVwSummary.text!.characters.count > 0 {
                    
                    FTIndicator.showToastMessage("Please select minimum one image")
                    
                }
                valid = false
            }
            
        }
        else {
        // to check validation of images selected
        if arrSelectedImages.count==0{
            if txtFieldPropertyType.text!.characters.count > 0 && txtFieldRoomType.text!.characters.count > 0 && txtFieldAccommodation.text!.characters.count > 0 && txtFieldBedRoom.text!.characters.count > 0  && txtFieldBeds.text!.characters.count > 0  && txtFieldBathRooms.text!.characters.count > 0 && txtFieldProfileName.text!.characters.count > 0 && txtVwSummary.text!.characters.count > 0 {
            
                FTIndicator.showToastMessage("Please select minimum one image")
            
            }
            valid = false
            }
        
            
        }
    
       
            return valid
        }
    
        func AnimationShakeTextField(textField:UITextField){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
            animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
            textField.layer.addAnimation(animation, forKey: "position")
        }
    
    //MARK:- WEB SERVICE
    
    func callGetPropertyType(){
        let token = arrUserData[0].authenticateToken!
        let params = [
            URLParams.AUTHENTICATE_TOKEN: token
        ]
        
        
        WebserviceUtils.callPostRequest(URLConstants.GET_PROPERTY_TYPE, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                let arr = json.objectForKey("propertyTypes")
                print(arr!.count)
                
                //save property type into database
                if DatabaseBLFunctions.fetchPropertyType().count == 0 {
                   DatabaseBLFunctions.savePropertyType(arr as! NSArray)
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    func callGetRoomType(){
        let token = arrUserData[0].authenticateToken!
        let params = [
            URLParams.AUTHENTICATE_TOKEN: token
        ]
        
        
        WebserviceUtils.callPostRequest(URLConstants.GET_ROOM_TYPE, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                let arr = json.objectForKey("roomTypes")
                print(arr!.count)
                
                //save property type into database
                if DatabaseBLFunctions.fetchRoomType().count == 0 {
                    DatabaseBLFunctions.saveRoomType(arr as! NSArray)
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    func callToDeleteParticularPropertyImage(imageID: String , row: Int ){
        let token = arrUserData[0].authenticateToken!
        let userId = arrUserData[0].userId!
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: token,
            URLParams.USER_ID: userId,
            URLParams.IMAGE_ID: imageID
        ]
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.DELETE_PARTICULAR_PROPERTY_IMAGE , params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        self.arrPropertyImageModel.removeAtIndex(row)
                        self.collectionView.reloadData()
                    }
                }
                
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    
    
    
    
}

//MARK:- UICOLLECTION-VIEW DATASOURCE AND DELEGATES
extension AddPropertyViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //when edit property
        if isEditPropertyBtnClicked {
            if arrPropertyImageModel.count == 0{
                return 1
            }
            else if arrPropertyImageModel.count >= 5{
                return 5
            }
            else if arrPropertyImageModel.count < 5 {
              return arrPropertyImageModel.count + 1
            }
            
        }
        else {
        
        ///when add property
        if arrSelectedImages.count == 0{
            return 1
        }
        else if arrSelectedImages.count >= 5{
            return 5
        }
        else {
        return arrSelectedImages.count + 1
        }
        }
        return 0

    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! HomeCollectionViewCell
        cell.imgVwSelectImage.contentMode = .ScaleAspectFill
        
        
        if isEditPropertyBtnClicked {
            
            if indexPath.row == arrPropertyImageModel.count{
               cell.imgVwSelectImage.image = UIImage(named: "avatar.png")
            }
            else {
                
                
           if arrPropertyImageModel[indexPath.row].id == ""
           {
            cell.imgVwSelectImage.image = arrPropertyImageModel[indexPath.row].selectedImage
           }
           else {
           let str:String = arrPropertyImageModel[indexPath.row].propertyImage
           let urlString:String = "\(URLConstants.BASE_URL)/HSSImages/propertyImages/\(str)"
           let escapedAddress: String = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
           cell.imgVwSelectImage.sd_setImageWithURL(NSURL(string: escapedAddress), placeholderImage:UIImage(named: "avatar.png"))
            }
            }
        
        }
        
        else {

        
        if arrSelectedImages.count > 0 {
            if indexPath.row == arrSelectedImages.count {
                
            cell.imgVwSelectImage.image = UIImage(named: "avatar.png")
                
            }
            else {
         cell.imgVwSelectImage.image = arrSelectedImages[indexPath.row]
            }
        }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
       let cell = collectionView.cellForItemAtIndexPath(indexPath) as! HomeCollectionViewCell
        print("did select called")
        
        if isEditPropertyBtnClicked {
            if indexPath.row == arrPropertyImageModel.count {
                openImagePickerAlert(false , indexPath: indexPath , cell: cell)
            }
            else {
                openImagePickerAlert(true , indexPath: indexPath, cell : cell)
            }
        }
        else {
        if indexPath.row == arrSelectedImages.count {
            openImagePickerAlert(false , indexPath: indexPath , cell: cell)
        }
        else {
            openImagePickerAlert(true , indexPath: indexPath, cell : cell)
        }
        }
    }
    
    func openImagePickerAlert(addDeleteBtn: Bool , indexPath: NSIndexPath ,cell : UICollectionViewCell){
        let  value:String = NSLocalizedString("Select Image", comment: "")
        let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(0)
        }
        let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(1)
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
            print("Cancel")
        }
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .Destructive) { (alert) -> Void in
            print("delete")
            
            if self.isEditPropertyBtnClicked {
                
                if self.arrPropertyImageModel[indexPath.row].id == "" {
                   self.arrPropertyImageModel.removeAtIndex(indexPath.row)
                    self.collectionView.reloadData()
                }
                else {
                let alert = UIAlertController(title: "Do you want to delete this image ?", message: "", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:self.handleCancel))
                alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler:{ (UIAlertAction) in
                    
                    self.callToDeleteParticularPropertyImage(self.arrPropertyImageModel[indexPath.row].id , row: indexPath.row)
                }))
                self.presentViewController(alert, animated: true, completion: {
                    print("completion block")
                })
                }
            }
            else {
            self.arrSelectedImages.removeAtIndex(indexPath.row)
            self.collectionView.deleteItemsAtIndexPaths([indexPath])
            //self.collectionView.reloadData()
            }
        }
        
        sheet.addAction(cameraAction)
        sheet.addAction(galleryAction)
        sheet.addAction(cancelAction)
        
        if addDeleteBtn == true {
          sheet.addAction(deleteAction)
        }
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            if let popoverController = sheet.popoverPresentationController {
                popoverController.sourceView = cell.contentView
                popoverController.sourceRect = cell.contentView.bounds
            }
            self.presentViewController(sheet, animated: true, completion: nil)
        }
        else{
            presentViewController(sheet, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- TEXT-VIEW DELEGATES
    
    func textViewDidBeginEditing(textView: UITextView) {
       textView.textColor = UIColor.blackColor()
        
        if textView == txtVwSummary {
       if textView.text!.containsString("Please enter"){
            textView.text! = ""
        }
        }
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        textView.resignFirstResponder()
    }
}


//MARK:- Textfield Delegates/Datasource
extension AddPropertyViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        univTextField = textField
        
        if (textField == txtFieldPropertyType) || (textField == txtFieldRoomType) {
            textField.tintColor = UIColor.clearColor()
        }
        
        if (textField == txtFieldPropertyType) || (textField == txtFieldAccommodation) || (textField == txtFieldRoomType) || (textField == txtFieldBedRoom) || (textField == txtFieldBeds) || (textField == txtFieldBathRooms) {
            
            switch univTextField {
            case txtFieldPropertyType:
                txtFieldPropertyType.text! = arrPropertyType[0].name!
            case txtFieldAccommodation:
                txtFieldAccommodation.text! = "\(arrAccommodationPrsn[0][arrAccommodationPrsn[0].startIndex.advancedBy(0)])"
            case txtFieldRoomType:
                txtFieldRoomType.text! = arrRoomType[0].name!
        
            case txtFieldBathRooms:
                txtFieldBathRooms.text! = "\(arrBathRooms[0][arrBathRooms[0].startIndex.advancedBy(0)])"
            case txtFieldBeds:
                txtFieldBeds.text! = "\(arrBeds[0][arrBeds[0].startIndex.advancedBy(0)])"
            case txtFieldBedRoom:
                txtFieldBedRoom.text! = "\(arrBedRoom[0][arrBedRoom[0].startIndex.advancedBy(0)])"
            default:
                print("invalid selection")
            }
            
            pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: true)
        }
        
        
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
        
        if textField == txtFieldBeds || textField == txtFieldBathRooms || textField == txtFieldBedRoom || textField == txtFieldAccommodation {
        if textField.text! == ""{
            enterOtherOption(textField)
        }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.characters.count > 100 {
            return false
        }
        
            return true
    }
    
    
}

//MARK:- PickerView Delegates/Datasource
extension AddPropertyViewController: UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch univTextField {
        case txtFieldPropertyType:
            return arrPropertyType.count
        case txtFieldAccommodation:
            return arrAccommodationPrsn.count
        case txtFieldRoomType:
            return arrRoomType.count
        case txtFieldBathRooms:
            return arrBathRooms.count
        case txtFieldBeds:
            return arrBeds.count
        case txtFieldBedRoom:
            return arrBedRoom.count
        default:
            print("invalid selection")
        }
        
        return 0
    }
}

extension AddPropertyViewController: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        switch univTextField {
        case txtFieldPropertyType:
            return arrPropertyType[row].name
        case txtFieldAccommodation:
            return arrAccommodationPrsn[row]
        case txtFieldRoomType:
            return arrRoomType[row].name
        case txtFieldBathRooms:
            return arrBathRooms[row]
        case txtFieldBeds:
            return arrBeds[row]
        case txtFieldBedRoom:
            return arrBedRoom[row]
        default:
            print("invalid selection")
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch univTextField {
        case txtFieldPropertyType:
            txtFieldPropertyType.text! = arrPropertyType[row].name!
        case txtFieldAccommodation:
            if arrAccommodationPrsn[row] != "Other" {
                let str = arrAccommodationPrsn[row]
                txtFieldAccommodation.text! = "\(str[str.startIndex.advancedBy(0)])"
            }
            else{
                txtFieldAccommodation.text! = ""
            }
        case txtFieldRoomType:
            txtFieldRoomType.text! = arrRoomType[row].name!
        case txtFieldBathRooms:
            if arrBathRooms[row] != "Other" {
             let str = arrBathRooms[row]
            txtFieldBathRooms.text! = "\(str[str.startIndex.advancedBy(0)])"
            }
            else{
               txtFieldBathRooms.text! = ""
            }
        case txtFieldBeds:
            if arrBeds[row] != "Other" {
            let str = arrBeds[row]
            txtFieldBeds.text! = "\(str[str.startIndex.advancedBy(0)])"
            }
            else{
                txtFieldBeds.text! = ""
            }
        case txtFieldBedRoom:
            if arrBedRoom[row] != "Other" {
            let str = arrBedRoom[row]
            txtFieldBedRoom.text! = "\(str[str.startIndex.advancedBy(0)])"
            }
            else {
                txtFieldBedRoom.text! = ""
            }
        default:
            print("invalid selection")
        }
    }
    
    func configurationTextField(textField: UITextField!)
    {
        if univTextField == txtFieldBathRooms{
          textField.placeholder =  "Enter no. of bath rooms"
        }
        else if univTextField == txtFieldBeds{
           textField.placeholder =  "Enter no. of beds"
        }
        else if univTextField == txtFieldAccommodation{
            textField.placeholder = "Enter no. of persons"
        }
        else{
            textField.placeholder =  "Enter no. of bed rooms"
        }
        print("generating the TextField")
        otherOptionTxtField = textField
        otherOptionTxtField.delegate = self
        otherOptionTxtField.keyboardType = UIKeyboardType.NumberPad
        otherOptionTxtField.becomeFirstResponder()
        
    }
    
            

    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    func enterOtherOption(textFld: UITextField){
        let alert = UIAlertController(title: "Enter Other Option", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Submit", style: .Default, handler:{ (UIAlertAction) in
            print("Item : \(self.otherOptionTxtField.text)")
            
            textFld.text! = "\(self.otherOptionTxtField.text!)"
            
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
    }
}










