//
//  AddPropertyOneViewController.swift
//  HSS
//
//  Created by Rajni on 08/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import LocationPicker
import CoreLocation
import FTIndicator

class AddPropertyOneViewController: UIViewController , UIPickerViewDelegate , MPGTextFieldDelegate , UITextFieldDelegate {
    
    //MARK:-OUTLETS
    
    @IBOutlet weak var txtFieldCountry: MPGTextField!
    //@IBOutlet weak var txtFieldCountry: MPGTextField!
//    @IBOutlet weak var txtFieldCountry: MPGTextField!
    @IBOutlet weak var txtViewStreetAddress: UITextView!
    @IBOutlet weak var txtFieldAptBldg: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldState: UITextField!
    @IBOutlet weak var txtFieldZipCode: UITextField!
    @IBOutlet weak var txtFieldSetPrice: UITextField!
    
    @IBOutlet weak var txtFieldBookingType: UITextField!
    
    @IBOutlet weak var txtFieldCurrencyType: UITextField!
    
    //MARK:- VARIABLES
     var userCoordinates : CLLocationCoordinate2D!
     var pickerView = UIPickerView()
     var coutriesList : NSArray = []
     var currencyList : NSArray = ["US Dollar(USD)","Indian Rupee(INR)","Canadian Dollar(CAD)"]
     var univTextField : UITextField!
     var arrBookingType = ["Review Each Request" , "Guest Book Instantly"]
     var currencyCode: String = ""
    var data_Property_detail: PropertyDetailsModel!
    var data_after_editing: PropertyData!
     public var presentCountry = Country.currentCountry
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.title = "Add Property"
        //set back button
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Bordered, target: self, action: #selector(AddPropertyOneViewController.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        self.fectCoutries()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        userCoordinates = CLLocationCoordinate2DMake(0.00000, 0.00000)
        
        
        //add nationality
        self.txtFieldCountry.backgroundColor = UIColor.whiteColor()
        self.txtFieldCountry.textColor = UIColor.blackColor()
        let Locale = NSLocale(localeIdentifier: "en_US")
        let country = Locale.displayNameForKey(NSLocaleCountryCode, value: presentCountry.countryCode)!
        print("MY Country : \(country)")
        self.txtFieldCountry.text = country
        self.txtFieldCountry.delegate = self
        
        if NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") != nil{
            let locationDictionary : NSDictionary = NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") as! NSDictionary
            
            let locationName : String = locationDictionary["locationName"] as! String
            let locationPinCode: String = locationDictionary["pincode"] as! String
            let streetAddress: String = locationDictionary["streetAddress"] as! String
            let city: String = locationDictionary["city"] as! String
            let state: String = locationDictionary["state"] as! String
            let latitude: String = locationDictionary["latitude"] as! String
            let longitude: String = locationDictionary["longitude"] as! String
             userCoordinates = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            //remove comma
            let strAddress = String(streetAddress.characters.dropFirst())
            print("my street address" , strAddress)
            
            txtViewStreetAddress.text = strAddress
            txtFieldCity.text = city
            txtFieldState.text = state
            txtFieldZipCode.text = locationPinCode
           
            
        }
        
        if data_Property_detail == nil {
            
        }
        else {
            
            fill_data()
        }

        //Add Drop Icon
        self.dropIcon(txtFieldCountry)
        self.dropIcon(txtFieldBookingType)
        self.dropIcon(txtFieldCurrencyType)
        
        
        // Add padding to textfield
        self.addPadding(txtFieldCountry)
        self.addPadding(txtFieldAptBldg)
        self.addPadding(txtFieldCity)
        self.addPadding(txtFieldState)
        self.addPadding(txtFieldZipCode)
        self.addPadding(txtFieldSetPrice)
        self.addPadding(txtFieldBookingType)
        self.addPadding(txtFieldCurrencyType)
        
        
        //picker view
        univTextField = txtFieldCountry
        pickerView.delegate = self
        //txtFieldCountry.inputView = pickerView
        txtFieldCurrencyType.inputView = pickerView
        txtFieldBookingType.inputView = pickerView
    }
    
    func back(sender: UIBarButtonItem) {
        
        // Go back to the previous ViewController
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        self.view.endEditing(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        txtViewStreetAddress.resignFirstResponder()
    }
    

    
    func textField(textField: MPGTextField!, didEndEditingWithSelection result: [NSObject : AnyObject]!) {
        print(result)
        let dic : NSDictionary = result as NSDictionary
        
        if let country : MyCountry = dic.objectForKey("Object") as? MyCountry{
            self.txtFieldCountry.text = country.countryName
            txtViewStreetAddress.text = ""
            txtFieldZipCode.text = ""
            txtFieldCity.text = ""
            txtFieldState.text = ""
            
        }
        else{
            self.txtFieldCountry.text = ""
        }
    }
    
    func dataForPopoverInTextField(textField: MPGTextField!) -> [AnyObject]! {
        return coutriesList as [AnyObject];
    }
    
    func fill_data(){
        
          self.title = "Edit Property"       
        txtFieldCountry.text = data_Property_detail.country
        txtViewStreetAddress.text = data_Property_detail.streetAddress
        txtFieldAptBldg.text = data_Property_detail.apartment
        txtFieldCity.text = data_Property_detail.city
        txtFieldState.text = data_Property_detail.state
        txtFieldZipCode.text = data_Property_detail.zipCode
        
        if data_Property_detail.typeOfBooking.containsString("Review Each"){
            txtFieldBookingType.text = "Review Each Request"
        }
        else {
            txtFieldBookingType.text = "Guest Book Instantly"
        }
        txtFieldCurrencyType.text = data_Property_detail.currency
        txtFieldSetPrice.text = data_Property_detail.setPrice
        
    }
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnNextClick(sender: AnyObject) {
        
        if Validate() {
        savePropertyDataLocally()
        }
        
    }
    
    func savePropertyDataLocally(){
        var lat :String = String(format:"%f", userCoordinates.latitude)
        var lng :String = String(format:"%f", userCoordinates.longitude)
        
        if data_Property_detail != nil {
        if lat.containsString("0.0"){
           lat = data_Property_detail.propertyLatitude
        }
        if lng.containsString("0.0") {
            lng = data_Property_detail.propertyLongitude
            }
            if currencyCode == "" {
                currencyCode = data_Property_detail.currency
            }
        
        }
        
        Constants.property.arrPropertyData[0].countryName = txtFieldCountry.text!
        Constants.property.arrPropertyData[0].latitude = lat
        Constants.property.arrPropertyData[0].longitude = lng
        Constants.property.arrPropertyData[0].streetAdd = txtViewStreetAddress.text!
        
        if txtFieldAptBldg.text! != "" {
        Constants.property.arrPropertyData[0].AptOrSuite = txtFieldAptBldg.text!
        }
        else {
          Constants.property.arrPropertyData[0].AptOrSuite = ""
        }
        Constants.property.arrPropertyData[0].city = txtFieldCity.text!
        Constants.property.arrPropertyData[0].state = txtFieldState.text!
        Constants.property.arrPropertyData[0].zipCode = txtFieldZipCode.text!
        
        if txtFieldBookingType.text!.containsString("Review Each"){
           Constants.property.arrPropertyData[0].bookingType = "reviewEachRequest"
        }
        else {
          Constants.property.arrPropertyData[0].bookingType = "bookInstantly"
        }
        
        Constants.property.arrPropertyData[0].currencyType = currencyCode
        Constants.property.arrPropertyData[0].price = txtFieldSetPrice.text!
        
        //move to next screen
        let submit_Property_Obj = self.storyboard?.instantiateViewControllerWithIdentifier("AddPropertySubmitViewController") as! AddPropertySubmitViewController
        submit_Property_Obj.data_Detail_Property = data_Property_detail
        submit_Property_Obj.data_after_editing = Constants.property.arrPropertyData[0]
        
        self.navigationController?.pushViewController(submit_Property_Obj, animated: true)
       
    }
    
    //MARK:- CUSTOM METHODS
    func addPadding(textField: UITextField){
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 10, 40)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    func fectCoutries(){
        if let art : NSArray = DatabaseBLFunctions.fetchCountries() {
            if art.count>0{
                let mutArt : NSMutableArray = NSMutableArray(array: art)
                mutArt.removeObjectAtIndex(0)
                coutriesList = mutArt
            }
        }
    }
    
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddPropertyOneViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on drop down")
        
        switch tappedImageView.tag {
        case 1:
            txtFieldCountry.becomeFirstResponder()
        case 2:
            txtFieldBookingType.becomeFirstResponder()
        case 3:
            txtFieldCurrencyType.becomeFirstResponder()
        
        default:
            print("invalid selection")
            
        }
    }
    
    //MARK:- TEXT-VIEW DELEGATES
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        
        if textView == txtViewStreetAddress {
            openLocationPicker()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        textView.resignFirstResponder()
    }
    
    
    
    
    
    
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        
         univTextField = textField
        
        if (textField == txtFieldBookingType) || (textField == txtFieldCurrencyType) {
            textField.tintColor = UIColor.clearColor()
        }
        
        if (textField == txtFieldBookingType) {
            txtFieldBookingType.text! = arrBookingType[0]
        }
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
    }
    
    func openLocationPicker(){
        
        let locationPicker = LocationPickerViewController()
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        locationPicker.currentLocationButtonBackground = Constants.Colors.BACKGROUND_COLOUR
        locationPicker.showCurrentLocationInitially = true // default: true
        locationPicker.mapType = .Standard // default: .Hybrid
        
        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false
        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
        
        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
        locationPicker.resultRegionDistance = 500 // default: 600
        
        locationPicker.completion = { location in
            
            print("******************************")
            print(location)
            
            let locationName : String = (location?.address)!
            self.userCoordinates  = (location?.coordinate)!
            self.txtViewStreetAddress.textColor = UIColor.blackColor()
            self.txtViewStreetAddress.text! = locationName
            
            if let city: String = (location?.placemark.locality){
                self.txtFieldCity.text! = city
            }
            else {
                self.txtFieldCity.text! = ""
            }
            
            if let state: String = (location?.placemark.administrativeArea){
                self.txtFieldState.text! = state
            }
            else {
                self.txtFieldState.text! = ""
            }
            
            
            if let postalCode: String = (location?.placemark.postalCode){
                self.txtFieldZipCode.text! = postalCode
            }
            else {
                self.txtFieldZipCode.text! = ""
            }
            
            if let country: String = (location?.placemark.country){
                self.txtFieldCountry.text! = country
            }
            else {
                self.txtFieldCountry.text! = ""
            }
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if (textField == txtFieldCountry){
            txtViewStreetAddress.text = ""
            txtFieldZipCode.text = ""
            txtFieldCity.text = ""
            txtFieldState.text = ""
        }
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if string == ""{
            return true
        }
        
        if textField == txtFieldZipCode {
            
                if string.rangeOfCharacterFromSet(characterset.invertedSet) != nil {
                    print("string contains special characters")
                    return false
                }
                else {
                    if string == ""{
                        return true
                    }
                    if textField.text?.characters.count >= 8 {
                        return false
                    }
                }
        }
        
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        switch univTextField {
        case txtFieldCountry:
            return coutriesList.count
        case txtFieldCurrencyType:
            return currencyList.count
        case txtFieldBookingType:
            return arrBookingType.count
        default:
            print("invalid")
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let countryDetail : MyCountry = coutriesList[row] as! MyCountry
        
        switch univTextField {
        case txtFieldCountry:
            return countryDetail.countryName
        case txtFieldCurrencyType:
            return currencyList[row] as? String
        case txtFieldBookingType:
            return arrBookingType[row]
        default:
            print("invalid")
        }
        
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTextField == txtFieldCountry {
            if coutriesList.count > 0 {
                let countryDetail : MyCountry = coutriesList[row] as! MyCountry
                txtFieldCountry.text = countryDetail.countryName
                txtViewStreetAddress.text = ""
                txtFieldZipCode.text = ""
                txtFieldCity.text = ""
                txtFieldState.text = ""
                
            }
        }
       else if univTextField == txtFieldCurrencyType {
            if coutriesList.count > 0 {
                //let countryDetail : MyCountry = coutriesList[row] as! MyCountry
                txtFieldCurrencyType.text = currencyList[row] as? String
                
                if txtFieldCurrencyType.text?.containsString("USD") == true {
                    currencyCode = "USD"
                }
                else if txtFieldCurrencyType.text?.containsString("INR") == true {
                    currencyCode = "INR"
                }
                else if txtFieldCurrencyType.text?.containsString("CAD") == true {
                    currencyCode = "CAD"
                }
                
            //countryDetail.currencyCode!
            }
        }
        else if univTextField == txtFieldBookingType {
            
            txtFieldBookingType.text! = arrBookingType[row]
        }
        
    }
    
//    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        // to check validation of country
        if txtFieldCountry.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldCountry.attributedPlaceholder = NSAttributedString(string: "Please enter country", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of address
        if txtViewStreetAddress.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            //set placeholder to text view
            txtViewStreetAddress.text = "Please select street address"
            txtViewStreetAddress.textColor = UIColor.redColor()
            valid = false
        }
        
        // to check validation of city
        if txtFieldCity.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldCity.attributedPlaceholder = NSAttributedString(string: "Please enter city", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of state
        if txtFieldState.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldState.attributedPlaceholder = NSAttributedString(string: "Please enter state", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of zip code
        if txtFieldZipCode.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldZipCode.attributedPlaceholder = NSAttributedString(string: "Please enter zip code", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        // to check validation of type of booking
        if txtFieldBookingType.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldBookingType.attributedPlaceholder = NSAttributedString(string: "Please select booking type", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        // to check validation of currency
        if txtFieldCurrencyType.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldCurrencyType.attributedPlaceholder = NSAttributedString(string: "Please select currency type", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of price
        if txtFieldSetPrice.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldSetPrice.attributedPlaceholder = NSAttributedString(string: "Please enter price", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }

}
