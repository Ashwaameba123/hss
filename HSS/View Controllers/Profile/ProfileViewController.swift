//
//  ProfileViewController.swift
//  HSS
//
//  Created by Rajni on 23/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import LocationPicker
import CoreLocation
import FTIndicator
import JTSImageViewController

class ProfileViewController: UIViewController , UIPickerViewDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate , UIPickerViewDataSource , CountriesViewControllerDelegate , MPGTextFieldDelegate , UITextFieldDelegate {
    //MARK:- OUTLETS
    @IBOutlet weak var lblEmail: UILabel!
    
    
    @IBOutlet weak var btn_uploadId: UIButton!
    @IBOutlet weak var imgVw_IDProof: UIImageView!
    //add contact view outlets
    @IBOutlet var vwAddContact: UIView!
    @IBOutlet weak var btnAddContact: UIButton!
    @IBOutlet weak var txtField_contactName: UITextField!
   
    @IBOutlet weak var txtField_relationship_addContact: UITextField!
    
    @IBOutlet weak var txtField_mobileNo: UITextField!
    
    
    @IBOutlet weak var imgVwProfile: UIImageView!
    
    @IBOutlet weak var txtFild_FirstName: UITextField!
    @IBOutlet weak var txtField_lastName: UITextField!
    @IBOutlet weak var txtField_language: UITextField!
    @IBOutlet weak var txtField_gender: UITextField!
    @IBOutlet weak var txtField_mobileno: UITextField!
    @IBOutlet weak var txtField_relationship: UITextField!
    @IBOutlet weak var txtField_location: UITextField!
    @IBOutlet weak var txtField_DOB: UITextField!
    @IBOutlet weak var collection_vw: UICollectionView!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var txtFieldNationality: MPGTextField!
    @IBOutlet weak var vw_dummy_nationality: UIView!
    
    
    //MARK:- VARIABLES
    var arrUserData = [UserRegistration]()
    var arrContacts = [Contact]()
    let codeBtn : UIButton = UIButton()
    let codeBtnOne : UIButton = UIButton()
    var userCoordinates : CLLocationCoordinate2D!
    public var presentCountry = Country.currentCountry
    var univTxtField = UITextField()
    var pickerView = UIPickerView()
    var arrGender = ["Male", "Female"]
    var arrLanguages = ["English", "French", "Italian"]
    var relationshipArray = ["Married","Single"]
    var coutriesList : NSArray = []
    var isEditBtnClicked:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var dynamicView:UIView = UIView()
    var isProfilePicImgVwTapped: Bool = false
    var isUploadedIdChanged:Bool = false
    
    //change password view textfield
    var currentPassTxtField = UITextField()
    var newPassTxtField = UITextField()
    var confirmPassTxtField = UITextField()
    var rightBarBtn = UIBarButtonItem()
    let imagePicker = UIImagePickerController()
    var newPassword: String = ""
    var isPhotoRemove: Bool = false
    var isUpdatePhoto: Bool = false
    var selectedContactId: String = ""
    
    //MARK:- VIEW LIFE CYCLE 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        
        if arrUserData.count > 0 {
            print("email \(arrUserData[0].email!)")
            //show username
            lblEmail.text! = arrUserData[0].email!
            fill_user_details()
            //get contact
            self.callToGetContactsByID()
        }
        else {
            lblEmail.text! = ""
        }
        
        //add right navigation button
        rightBarBtn = UIBarButtonItem(
            title: "Edit",
            style: .Plain,
            target: self,
            action: #selector(btnClicked(_:))
        )
        self.navigationItem.rightBarButtonItem = rightBarBtn
        
        
        self.fectCoutries()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        userCoordinates = CLLocationCoordinate2DMake(0.00000, 0.00000)

        //make round host image view
        imgVwProfile.layer.cornerRadius = imgVwProfile.frame.size.width / 2
        imgVwProfile.clipsToBounds = true
        imgVwProfile.layer.borderWidth = 2.0
        imgVwProfile.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.tappedOnImageVw))
        imgVwProfile.addGestureRecognizer(tap)
        imgVwProfile.tag = 1
        imgVwProfile.userInteractionEnabled = true
        
        //to set profile image
        let imageName = arrUserData[0].userProfileImage!
        //print(imageName)
        
        let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
       // print(urlString)
        let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        imgVwProfile.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.tappedOnImageVw))
        imgVw_IDProof.tag = 2
        imgVw_IDProof.addGestureRecognizer(tap2)
        imgVw_IDProof.userInteractionEnabled = true
        
        //to set id proof image
        if let idImageName = arrUserData[0].photoIdentity {
        //print(imageName)

        let urlString2:String = "\(URLConstants.GET_ID_PROOF_IMAGE)\(idImageName)"
        // print(urlString)
        let escapedAddress2 = urlString2.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        imgVw_IDProof.sd_setImageWithURL(NSURL(string: escapedAddress2!), placeholderImage:UIImage(named: "avatar.png"))
        }
        
        
        
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        txtField_language.inputView = pickerView
        txtField_gender.inputView = pickerView
        txtField_relationship.inputView = pickerView
        imagePicker.delegate = self
        
        initView()
        
        
        
    }
    
    
    
    
    func initView(){
        
        //Add Drop Icon
        self.dropIcon(txtField_language)
        self.dropIcon(txtField_gender)
        self.dropIcon(txtField_relationship)
        self.dropIcon(txtField_DOB)
        self.dropIcon(txtFieldNationality)
        
        self.layerFields(txtFild_FirstName)
        self.layerFields(txtField_lastName)
        self.layerFields(txtField_language)
        self.layerFields(txtField_gender)
        self.layerFields(txtField_mobileno)
        self.layerFields(txtField_relationship)
        self.layerFields(txtFieldNationality)
        self.layerFields(txtField_location)
        self.layerFields(txtField_DOB)
        
        btn_uploadId.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        CommonFunctions.makeRoundCornerOfButton(btn_uploadId)
       CommonFunctions.makeRoundCornerOfButton(btnChangePassword)
        
        
        self.rtIcon(txtFild_FirstName, imageName: "user-name")
        self.rtIcon(txtField_lastName, imageName: "user-name")
        self.rtIcon(txtField_language, imageName: "country-language")
        self.rtIcon(txtField_gender, imageName: "gender")
        self.rtIcon(txtField_mobileno, imageName: "phone")
        self.rtIcon(txtField_DOB, imageName: "calender")
        self.rtIcon(txtField_relationship, imageName: "relationship")
        self.rtIcon(txtFieldNationality, imageName: "country-language")
        self.rtIcon(txtField_location, imageName: "location-dark")
        
        btnChangePassword.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        
        self.txtFieldNationality.delegate = self
        self.txtFieldNationality.backgroundColor = UIColor.whiteColor()
        self.txtFieldNationality.textColor = UIColor.blackColor()
        
        
        if arrUserData[0].registerVia?.containsString("manual") == true {
            btnChangePassword.hidden = false
            btnChangePassword.userInteractionEnabled = true
        }
        else {
            btnChangePassword.hidden = true
            btnChangePassword.userInteractionEnabled = false
        }
        
        
        
        
        
        self.setEditibilityFalse()
    
    }
    
    
    func textField(textField: MPGTextField!, didEndEditingWithSelection result: [NSObject : AnyObject]!) {
        print(result)
        let dic : NSDictionary = result as NSDictionary
        
        if let country : MyCountry = dic.objectForKey("Object") as? MyCountry{
            self.txtFieldNationality.text = country.countryName
        }
        else{
            self.txtFieldNationality.text = ""
        }
    }
    
    func dataForPopoverInTextField(textField: MPGTextField!) -> [AnyObject]! {
        return coutriesList as [AnyObject];
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        
        switch tappedImageView.tag{
            
        case 1:
            txtField_language.becomeFirstResponder()
        case 2:
            txtField_gender.becomeFirstResponder()
        case 3:
            txtField_relationship.becomeFirstResponder()
        case 4:
            txtFieldNationality.becomeFirstResponder()
        case 5:
            txtField_DOB.becomeFirstResponder()
        default:
            print("invalid")
        }
    }
    
    
    func rtIcon(textField : UITextField , imageName : String) {
        if textField == txtField_mobileno{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 90, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            
            codeBtnOne.frame = CGRectMake(40, 0, 50, 40)
            codeBtnOne.setTitle( arrUserData[0].countryCode , forState: .Normal)
            codeBtnOne.setTitleColor(UIColor.blackColor(), forState: .Normal)
            codeBtnOne.titleLabel!.font =  UIFont.systemFontOfSize(15)
            codeBtnOne.addTarget(self, action: #selector(ProfileViewController.pressed(_:)), forControlEvents: .TouchUpInside)
            codeBtnOne.contentHorizontalAlignment = .Left;
            
            rtPad.addSubview(codeBtnOne)
            
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
        else{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
    }
    
    func pressed(sender: UIButton!) {
        let countriesViewController = CountriesViewController.standardController()
        countriesViewController.cancelBarButtonItemHidden = true
        countriesViewController.delegate = self
        countriesViewController.majorCountryLocaleIdentifiers = ["GB", "US", "IT", "DE", "RU", "BR", "IN"]
        
        navigationController?.pushViewController(countriesViewController, animated: true)
    }
    
    func fill_user_details(){
        txtFild_FirstName.text = arrUserData[0].firstName
        txtField_lastName.text = arrUserData[0].lastName
        txtField_language.text = arrUserData[0].language
        txtField_gender.text = arrUserData[0].gender
        txtField_mobileno.text = arrUserData[0].mobileNumber
        txtField_relationship.text = arrUserData[0].relationshipStatus
        txtFieldNationality.text = arrUserData[0].nationality
        txtField_location.text = arrUserData[0].locationName
        
        if arrUserData[0].dateOfBirth!.containsString(":00") == true {
            txtField_DOB.text = CommonFunctions.dateFromString(arrUserData[0].dateOfBirth!)
        }
        else {
            txtField_DOB.text = arrUserData[0].dateOfBirth
        }
        //txtField_DOB.text = dateFromString(arrUserData[0].dateOfBirth!)
    }
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    
    
    func addPadding(textField: UITextField){
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 15, 44)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    func btnClicked(sender: UIBarButtonItem) {
        
        if sender.title == "Edit" {
            isEditBtnClicked = true
            self.setEditibilityActive()
            sender.title = "Save"
        }
        else if sender.title == "Save" {
            isEditBtnClicked = false
            self.setEditibilityFalse()
            if validationToEditProfile() == true {
            self.callToEditProfile()
            }
            sender.title = "Edit"
        }
    }
    
    
    func validationToEditProfile() -> Bool {
        if txtFild_FirstName.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please enter first name")
            return false
        }
        if txtField_lastName.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please enter last name")
            return false
        }
        if txtField_language.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please select language")
            return false
        }
        if txtField_gender.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please select gender")
            return false
        }
        if txtField_mobileno.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please enter mobile no.")
            return false
        }
        if txtField_relationship.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please select marital status")
            return false
        }
        if txtFieldNationality.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please select nationality")
            return false
        }
        if txtField_location.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please select location")
            return false
        }
        if txtField_DOB.text!.characters.count < 0 {
            FTIndicator.showToastMessage("Please select DOB")
            return false
        }
       return true
    }
    
    func setEditibilityFalse(){
        imgVwProfile.userInteractionEnabled = false
        txtFild_FirstName.userInteractionEnabled = false
        txtField_lastName.userInteractionEnabled = false
        txtField_language.userInteractionEnabled = false
        txtField_gender.userInteractionEnabled = false
        txtField_mobileno.userInteractionEnabled = false
        codeBtnOne.userInteractionEnabled = false
        txtField_relationship.userInteractionEnabled = false
        txtFieldNationality.userInteractionEnabled = false
        txtField_location.userInteractionEnabled = false
        txtField_DOB.userInteractionEnabled = false
        collection_vw.userInteractionEnabled = false
        btnChangePassword.userInteractionEnabled = false
        btn_uploadId.userInteractionEnabled = false
        imgVw_IDProof.userInteractionEnabled = false
    }
    func setEditibilityActive(){
        imgVwProfile.userInteractionEnabled = true
        txtFild_FirstName.userInteractionEnabled = true
        txtField_lastName.userInteractionEnabled = true
        txtField_language.userInteractionEnabled = true
        txtField_gender.userInteractionEnabled = true
        txtField_mobileno.userInteractionEnabled = true
        codeBtnOne.userInteractionEnabled = true
        txtField_relationship.userInteractionEnabled = true
        txtFieldNationality.userInteractionEnabled = true
        txtField_location.userInteractionEnabled = true
        txtField_DOB.userInteractionEnabled = true
        collection_vw.userInteractionEnabled = true
        btnChangePassword.userInteractionEnabled = true
        imgVw_IDProof.userInteractionEnabled = true
        btn_uploadId.userInteractionEnabled = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        univTxtField.resignFirstResponder()
    }
    
    
    
    
    //MARK:- CUSTOM METHODS
    func fectCoutries(){
        if let art : NSArray = DatabaseBLFunctions.fetchCountries() {
            if art.count>0{
                let mutArt : NSMutableArray = NSMutableArray(array: art)
                mutArt.removeObjectAtIndex(0)
                coutriesList = mutArt
            }
        }
    }
    
    internal func countriesViewControllerDidCancel(countriesViewController: CountriesViewController) { }
    
    internal func countriesViewController(countriesViewController: CountriesViewController, didSelectCountry country: Country) {
        navigationController?.popViewControllerAnimated(true)
        
        print(country.phoneExtension)
        codeBtn.setTitle("+\(country.phoneExtension)-", forState: .Normal)
    }
    
    
    //MARK:- BUTTON ACTIONS ADD CONTACT
    @IBAction func btnAddContactSubmitClick(sender: UIButton) {
        if btnAddContact.currentTitle == "Submit"{
            if validationForAddContact() == true {
            self.callToAddContact()
            }
        }
        else if btnAddContact.currentTitle == "Update" {
            if validationForAddContact() == true {
            self.callToUpdateContact()
            }
        }
        
    }
    
    
    func validationForAddContact() -> Bool{
        if txtField_contactName.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter contact name")
            return false
        }
        if txtField_relationship_addContact.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter relationship")
            return false
        }
        if txtField_mobileNo.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter mobile no.")
            return false
        }
        
        return true
    }
    
    @IBAction func btn_ChangePassword_Click(sender: UIButton) {
        self.btnChangePasswordClick()
    }
    
    @IBAction func BtnCancelAddContactClick(sender: UIButton) {
        dynamicView.removeFromSuperview()
    }
    
    func toRefresh(){
        self.callToGetContactsByID()
    }
    
    @IBAction func btn_uploadID_Click(sender: UIButton) {
        let  value:String = NSLocalizedString("select your ID Proof", comment: "")
        let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(0)
        }
        let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(1)
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
            print("Cancel")
        }
        sheet.addAction(cameraAction)
        sheet.addAction(galleryAction)
        sheet.addAction(cancelAction)
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            if let popoverController = sheet.popoverPresentationController {
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = sender.bounds
            }
            self.presentViewController(sheet, animated: true, completion: nil)
        }
        else{
            presentViewController(sheet, animated: true, completion: nil)
        }
    }
    
    func tappedOnImageVw(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        var titleString:String = ""
        
        if tappedImageView.tag == 1 {
            titleString = "Select your profile photo"
            isProfilePicImgVwTapped = true
            
            let  value:String = NSLocalizedString( titleString , comment: "")
            let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
            let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
                self.openCamorGallary(0)
            }
            let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
                self.openCamorGallary(1)
            }
            
            let removeAction = UIAlertAction(title: NSLocalizedString("Remove image", comment: ""), style: .Destructive) { (alert) -> Void in
                print("remove Image")
                self.imgVwProfile.image = UIImage(named: "avatar")
                self.isPhotoRemove = true
            }
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
                print("Cancel")
            }
            
            
            sheet.addAction(cameraAction)
            sheet.addAction(galleryAction)
            sheet.addAction(cancelAction)
            
            
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
                if let popoverController = sheet.popoverPresentationController {
                    popoverController.sourceView = tappedImageView
                    popoverController.sourceRect = tappedImageView.bounds
                }
                self.presentViewController(sheet, animated: true, completion: nil)
            }
            else{
                presentViewController(sheet, animated: true, completion: nil)
            }
        }
            
        else if tappedImageView.tag == 2 {
            titleString = "Select your ID proof"
            isProfilePicImgVwTapped = false
            
            if let selectedImg:UIImage = imgVw_IDProof.image{
                let imageInfo = JTSImageInfo()
                imageInfo.image = selectedImg
                imageInfo.referenceRect = self.btn_uploadId.frame
                imageInfo.referenceView = self.btn_uploadId.superview
                imageInfo.title = "ID"
                let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
                imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
            }
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageView.contentMode = .ScaleAspectFit
            //imageView.image = pickedImage
            
            if isProfilePicImgVwTapped {
                imgVwProfile.image = pickedImage
                self.isPhotoRemove = true
                self.isUpdatePhoto = true
            }
            else {
                isUploadedIdChanged = true
                imgVw_IDProof.image = pickedImage
                btn_uploadId.setTitleColor(UIColor.blackColor(), forState: .Normal)
                btn_uploadId.setTitle("Photo Identity Attached", forState: .Normal)
            }
//                imgVwProfile.image = pickedImage
//                self.isPhotoRemove = false
//                self.isUpdatePhoto = true
            
            //selectedIdentityImg = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func openCamorGallary(type : Int){
        if type == 0 {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        presentViewController(imagePicker, animated: true, completion: imagePickerHandler)
    }
    
    func imagePickerHandler(){
        imagePicker.navigationBar.translucent = false
        imagePicker.navigationBar.barTintColor = Constants.Colors.BACKGROUND_COLOUR
    }
    
    //MARK:- WEB SERVICE
    
    func callToGetContactsByID(){
        var authenticateToken: String = ""
        var userId: String = ""
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userId = arrUserData[0].userId!
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId
        ]
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.GET_CONTACT_BY_USERID, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                
                print("contacts-----" , json)
                if let status = json.objectForKey("status"){
                    if status as! String == "success"{
                      
                        if let dic: NSArray = json.valueForKey("usercontact") as? NSArray {
                            if let arr:NSDictionary = dic[0] as? NSDictionary {
                                let art : NSArray = (arr.objectForKey("userContact") as? NSArray)!
                                self.parseContactJsonData(art)
                                
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    
    
    func parseContactJsonData(arr: NSArray){
        
        arrContacts = []
        
        for item in arr {
            let contact = Contact()
            
            if let name = item.objectForKey("contactName") as? String{
                contact.name = name
            }
            if let number = item.objectForKey("contactPhonenumber") as? String{
                contact.phoneNumber = number
            }
            if let relation = item.objectForKey("contactRelationShip") as? String{
                contact.relationShip = relation
            }
            else{
                contact.relationShip = ""
            }
            if let countryCode = item.objectForKey("countryCode") as? String{
                contact.countryCode = countryCode
            }
            else{
                contact.countryCode = ""
            }
            if let userContactID = item.objectForKey("id") as? String{
               contact.userContactId = userContactID
            }
            
           arrContacts.append(contact)
        }
        
        self.collection_vw.reloadData()
    }
    
    
    //to update contact
    func callToUpdateContact(){
        var authenticateToken: String = ""
        var userId: String = ""
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userId = arrUserData[0].userId!
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId,
            URLParams.CONTACT_NAME: txtField_contactName.text!,
            URLParams.CONTACT_RELATIONSHIP: txtField_relationship_addContact.text!,
            URLParams.COUNTRY_CODE: codeBtn.titleLabel!.text!,
            URLParams.CONTACT_PHONE_NO: txtField_mobileNo.text!,
            URLParams.USER_CONTACT_ID: selectedContactId
        ]
        
        print(params)
         FTIndicator.showInfoWithMessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.UPDATE_CONTACT, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status"){
                    if status as! String == "success"{
                        self.toRefresh()
                        FTIndicator.dismissToast()
                        FTIndicator.showSuccessWithMessage("Contact Updated Successfully")
                        self.dynamicView.removeFromSuperview()
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    //to add contact
    func callToAddContact(){
        var authenticateToken: String = ""
        var userId: String = ""
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userId = arrUserData[0].userId!
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId,
            URLParams.CONTACT_NAME: txtField_contactName.text!,
            URLParams.CONTACT_RELATIONSHIP: txtField_relationship_addContact.text!,
            URLParams.COUNTRY_CODE: codeBtn.titleLabel!.text!,
            URLParams.CONTACT_PHONE_NO: txtField_mobileNo.text!
        ]
        
        print(params)
       FTIndicator.showInfoWithMessage("Loading..", userInteractionEnable: false)
        
        WebserviceUtils.callPostRequest(URLConstants.ADD_CONTACT, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status"){
                    if status as! String == "success"{
                
                    FTIndicator.dismissToast()
                     FTIndicator.showSuccessWithMessage("Contact Added Successfully")
                        self.dynamicView.removeFromSuperview()
                        self.toRefresh()
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
                FTIndicator.dismissToast()
        })
        
        
    }
    
    
    //to change password
    func callToChangePassword(){
        var authenticateToken: String = ""
        var userId: String = ""
        var userType: String = ""
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userId = arrUserData[0].userId!
        }
        
        if let type = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) {
            userType = type
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId,
            URLParams.USER_TYPE: userType,
            URLParams.CURRENT_PASSWORD: currentPassTxtField.text! ,
            URLParams.NEW_PASSWORD: newPassTxtField.text!
        ]
        
        print(params)
        WebserviceUtils.callPostRequest(URLConstants.CHANGE_PASSWORD, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status: String = json.objectForKey("status") as? String{
                    if status.containsString("success") {
                    
                        self.newPassword = self.newPassTxtField.text!
                        
                        if let msg: String = json.objectForKey("status") as? String {
                            FTIndicator.showInfoWithMessage(msg)
                        }
                        
                        
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
//- WEB SERVICE EDIT USER / host profile
    func callToEditProfile(){
        var firstName: String = ""
        var lastName: String = ""
        var language: String = ""
        var gender: String = ""
        var code: String = ""
        var mobile: String = ""
        var relation: String = ""
        var country: String = ""
        var location: String = ""
        var dob: String = ""
        var longitude:String = ""
        var latitude:String = ""
        var applicationID: String = ""
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        
        let user = arrUserData[0]
        
        
        firstName = txtFild_FirstName.text!
        lastName  = txtField_lastName.text!
        language = txtField_language.text!
        gender = txtField_gender.text!
        mobile = txtField_mobileno.text!
        relation = txtField_relationship.text!
        dob = txtField_DOB.text!
        country = txtFieldNationality.text!
        location = txtField_location.text!
        code = codeBtnOne.titleLabel!.text!
        
        
        //to set user location coordinates 
        if userCoordinates.latitude == 0.0 {
            if let lat = user.locationLatitude {
                latitude = lat
            }
            
        }
        else {
            latitude = String(userCoordinates.latitude)
        }
        if userCoordinates.longitude == 0.0 {
            if let long = user.locationLongitude {
                 longitude = long
            }
           
        }
        else{
           longitude = String(userCoordinates.longitude)
        }
        
        
        if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.DEVICE_TOKEN){
            applicationID = token
        }
        else {
            applicationID = "123456"
        }
        
        //start loader
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        let params = [
            URLParams.FIRST_NAME: firstName,
            URLParams.LAST_NAME: lastName,
            URLParams.LANGUAGE: language,
            URLParams.GENDER: gender,
            URLParams.MOBILE_NUMBER: mobile,
            URLParams.RELATIONSHIP_STATUS: relation,
            URLParams.DOB: dob,
            URLParams.NATIONALITY: country,
            URLParams.LOCATION_NAME: location,
            URLParams.LOCATION_LATITUDE: latitude,
            URLParams.LOCATION_LONGITUDE: longitude,
            URLParams.COUNTRY_CODE: code,
            URLParams.USER_TYPE: user.userType!,
            URLParams.REGISTER_VIA: user.registerVia!,
            URLParams.SOCIAL_MEDIA_ID: user.socialMediaId!,
            URLParams.DEVICE_TYPE: "Ios",
            URLParams.APPLICATION_ID: applicationID,
            URLParams.PHOTO_REMOVE: String(isPhotoRemove),
            URLParams.USER_ID: user.userId!,
            URLParams.AUTHENTICATE_TOKEN: user.authenticateToken!,
            URLParams.CONTACT_NAME: ""
        ]
        
        
        print(params)
        let imgParam = imgVwProfile.image
        
        
        WebserviceUtils.callPostRequestForUserRegistrationToEditProfile(URLConstants.EDIT_PROFILE ,image: imgParam , params: params , isUpdatePhoto: String(isUpdatePhoto) , success: { (response) in
            
            FTIndicator.dismissProgress()
            if let json = response as? NSDictionary {
                
                print("edit profile json", json)
                if let message = json.objectForKey("message"){
                    FTIndicator.showToastMessage(message as! String)
                }
                
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                        if let dic = json.objectForKey("user"){
                            
                            if self.isUploadedIdChanged {
                                self.callToAddPhotoIdentity()
                                self.isUploadedIdChanged = false
                            }
                            else {
                          
                            //update user / host data into database
                        let userData = CommonDataParsingMethods.saveDicToUserRegistrationType(dic as! NSMutableDictionary)
                        DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(userData)
                                
                            }

                        }}
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
                FTIndicator.dismissProgress()
                FTIndicator.showToastMessage(error.localizedDescription)
        })
    }
    
    
    
    func callToAddPhotoIdentity(){
        
        let user = arrUserData[0]
        
        
        let params = [
            URLParams.USER_TYPE: user.userType!,
            URLParams.AUTHENTICATE_TOKEN: user.authenticateToken!,
            URLParams.USER_ID: user.userId!
        ]
        
        
        print(params)
        let image = imgVw_IDProof.image
        
        WebserviceUtils.callPostRequestForUserRegistration(URLConstants.SERVICE_PROVIDER_ADD_PHOTO_IDENTITY ,image: image, params: params , imageParam: "photoIdentity" , success: { (response) in
            
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                        if let dic = json.objectForKey("user"){
                            
                            //update user / host data into database
                            let userData = CommonDataParsingMethods.saveDicToUserRegistrationType(dic as! NSMutableDictionary)
                            DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(userData)
                            
                        }
                        
                    }
                    
                }}
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    
    
    
    
    func btnChangePasswordClick()
    {
        print("Button Clicked")
        let alert = UIAlertController(title: "Change Password", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler {
            (txtCurrentPass) -> Void in
            self.currentPassTxtField = txtCurrentPass
            self.currentPassTxtField.secureTextEntry = true
            self.currentPassTxtField.delegate = self
            self.currentPassTxtField.placeholder = "Enter current password"
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (txtNewPass) -> Void in
            self.newPassTxtField = txtNewPass
            self.newPassTxtField.secureTextEntry = true
            self.newPassTxtField.delegate = self
            self.newPassTxtField.placeholder = "Enter new password"
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (txtConfirmPass) -> Void in
            self.confirmPassTxtField = txtConfirmPass
            self.confirmPassTxtField.secureTextEntry = true
            self.confirmPassTxtField.delegate = self
            self.confirmPassTxtField.placeholder = "Enter confirm password"
        }
        
        alert.addAction(UIAlertAction(title: "Submit", style: .Default, handler:{ (UIAlertAction) in
            if self.changePassword() == true {
                self.callToChangePassword()
            }
            else {
            
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:handleCancel))
        
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func changePassword() -> Bool {
        if currentPassTxtField.text?.characters.count > 0 && newPassTxtField.text?.characters.count > 0 && confirmPassTxtField.text?.characters.count > 0 {
        
            if newPassTxtField.text! != confirmPassTxtField.text! {
             FTIndicator.showToastMessage("New and confirm password should be same")
             return false
            }
        }
        else {
           if currentPassTxtField.text?.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter current password")
            return false
            }
           else if newPassTxtField.text?.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter new password")
            return false
            }
           else {
            FTIndicator.showToastMessage("Please enter confirm password")
            return false
            }
        }
        return true
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    
    //MARK:- CUSTOM METHOD
    
    
    func openLocationPicker(textField: UITextField){
        
        let locationPicker = LocationPickerViewController()
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        locationPicker.currentLocationButtonBackground = Constants.Colors.BACKGROUND_COLOUR
        locationPicker.showCurrentLocationInitially = true // default: true
        locationPicker.mapType = .Standard // default: .Hybrid
        
        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false
        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
        
        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
        locationPicker.resultRegionDistance = 500 // default: 600
        
        locationPicker.completion = { location in
            
            let locationName : String = (location?.address)!
            self.userCoordinates  = (location?.coordinate)!
            
            let countryName : String = (location?.placemark.country)!
            
            textField.text = locationName
            self.txtFieldNationality.text = countryName
            
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    func addDatePicker(textField: UITextField){
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
        let todayDate = NSDate()
        let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
        let minimumYear = (components?.year)! - 1917
        let minimumMonth = (components?.month)! - 1
        let minimumDay = (components?.day)! - 1
        let comps = NSDateComponents();
        comps.year = -minimumYear
        comps.month = -minimumMonth
        comps.day = -minimumDay
        let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        let maxYear = (components?.year)! - 2017
        let maxMonth = (components?.month)! - 1
        let maxDay = (components?.day)! - 1
        let maxcomps = NSDateComponents();
        maxcomps.year = -maxYear
        maxcomps.month = -maxMonth
        maxcomps.day = -maxDay
        let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        
        textField.inputView = datePicker
    }
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        univTxtField.text! = formattor.stringFromDate(sender.date)
        
        print("Selected value \(univTxtField.text!)")
        
    }
    

    //MARK:- TEXTFIELD DELEGATES
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        univTxtField = textField
        
        
        if textField == txtField_language || textField == txtField_gender || textField == txtField_relationship || textField == txtField_DOB {
            textField.tintColor = UIColor.clearColor()
        }
        
        print("called")
        if textField == txtField_location {
            openLocationPicker(textField)
        }
        if textField == txtField_DOB{
            addDatePicker(txtField_DOB)
        }
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        if textField == txtFieldNationality{
            txtField_location.text = ""
            
        }
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        //limits
        if textField == txtFild_FirstName || textField == txtField_lastName{
            if (string == " ") {
                return true
            }
            if string.characters.count <= 50 {
                return true
            }
        }
        
        //avoid to enter white spaces while password field
        if textField == currentPassTxtField || textField == newPassTxtField || textField == confirmPassTxtField{
        if (string == "") {
            return false
        }
            if string.characters.count <= 8 {
                return true
            }
        }
        
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        switch univTxtField {
        case txtField_language:
            return arrLanguages.count
        case txtField_gender:
            return arrGender.count
        case txtField_relationship:
            return relationshipArray.count
        case txtFieldNationality:
            return coutriesList.count
        default:
            print("not valid")
        }
        return 1
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch univTxtField {
        case txtField_language:
            return arrLanguages[row]
        case txtField_gender:
            return arrGender[row]
        case txtField_relationship:
            return relationshipArray[row]
        case txtFieldNationality:
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            return countryDetail.countryName
        default:
            print("not valid")
        }
        return ""
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let indexPath = NSIndexPath(forRow: univTxtField.tag , inSection: 0)
        print(indexPath)
        
        switch univTxtField {
        case txtField_language:
            txtField_language.text! = arrLanguages[row]
        case txtField_gender:
            txtField_gender.text! = arrGender[row]
        case txtField_relationship:
            txtField_relationship.text! = relationshipArray[row]
        case txtFieldNationality:
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            txtFieldNationality.text! = countryDetail.countryName!
        default:
            print("not valid")
        }
        
    }
    
    
    
    
  
}






//MARK:- UICOLLECTION-VIEW DATASOURCE AND DELEGATES
extension ProfileViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrContacts.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ContactCollectionViewCell
        
        if indexPath.row == arrContacts.count {
            cell.imgVwContact.image = UIImage(named: "user.png")
            cell.imgVwContact.contentMode = .ScaleAspectFill
        }
        else {
        //let contact = arrContacts[indexPath.row]
        cell.imgVwContact.image = UIImage(named: "editContact.png")
        cell.imgVwContact.contentMode = .ScaleAspectFill
        //cell.lblContactName.text = contact.name!
        }
        
        
        
        
        
        
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //let cell = collectionView.cellForItemAtIndexPath(indexPath) as! HomeCollectionViewCell
        print("did select called")
        
        self.openAddContactView()
        if indexPath.row == arrContacts.count {
            btnAddContact.setTitle("Submit", forState: .Normal)
            txtField_contactName.text! = ""
            txtField_mobileNo.text! = ""
            codeBtn.setTitle("+91", forState: .Normal)
            txtField_relationship_addContact.text! = ""
        }
        else {
           btnAddContact.setTitle("Update", forState: .Normal)
            txtField_contactName.text! = arrContacts[indexPath.row].name
            txtField_mobileNo.text! = arrContacts[indexPath.row].phoneNumber
            codeBtn.setTitle(arrContacts[indexPath.row].countryCode, forState: .Normal)
            txtField_relationship_addContact.text! = arrContacts[indexPath.row].relationShip
            selectedContactId = arrContacts[indexPath.row].userContactId
        }
        
    }
    
    func openAddContactView(){
        print("Button Clicked")
        
        dynamicView = UIView(frame: CGRectMake(0, 0, app.window!.bounds.size.width , app.window!.bounds.size.height))
        self.view.addSubview(dynamicView)
        
        dynamicView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        vwAddContact.frame.size = CGSize(width: view.frame.size.width - 40 , height: 250)
        vwAddContact.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        vwAddContact.layer.cornerRadius = 8
        vwAddContact.clipsToBounds = true
        vwAddContact.center = dynamicView.center
        
       
        //add code btn with padding
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 60, 44)
        
        codeBtn.frame = CGRectMake(10, 0, 50, 40)
        codeBtn.setTitle("+\(presentCountry.phoneExtension)-", forState: .Normal)
        codeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        codeBtn.titleLabel!.font =  UIFont.systemFontOfSize(15)
        codeBtn.addTarget(self, action: #selector(ProfileViewController.pressed(_:)), forControlEvents: .TouchUpInside)
        codeBtn.contentHorizontalAlignment = .Left;
        
        rtPad.addSubview(codeBtn)
        
        txtField_mobileNo.leftView = rtPad
        txtField_mobileNo.leftViewMode = .Always
        
        //add padding
        self.addPadding(txtField_relationship_addContact)
        self.addPadding(txtField_contactName)
        
        dynamicView.addSubview(vwAddContact)
        
    }
    
    

    
    
}


 