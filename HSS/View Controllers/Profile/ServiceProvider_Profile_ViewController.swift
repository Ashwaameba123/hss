//
//  ServiceProvider_Profile_ViewController.swift
//  HSS
//
//  Created by Rajni on 09/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import LocationPicker
import CoreLocation
import FTIndicator
import JTSImageViewController

class ServiceProvider_Profile_ViewController: UIViewController , UIPickerViewDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate , UIPickerViewDataSource , CountriesViewControllerDelegate , MPGTextFieldDelegate , UITextFieldDelegate {
    
    //MARK:- OUTLETS
    @IBOutlet weak var imgVw_profile: UIImageView!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var txtField_firstName: UITextField!
    @IBOutlet weak var txtField_lastName: UITextField!
    @IBOutlet weak var txtField_gender: UITextField!
    @IBOutlet weak var txtField_DOB: UITextField!
    @IBOutlet weak var txtField_telephoneNo: UITextField!
    @IBOutlet weak var txtField_mobileNo: UITextField!
    
    @IBOutlet weak var btn_individual: UIButton!
    @IBOutlet weak var btn_company: UIButton!
    
    @IBOutlet weak var txtField_selectCompany: UITextField!
    @IBOutlet weak var txtField_companyName: UITextField!
    @IBOutlet weak var txtField_companyAddress: UITextField!
    @IBOutlet weak var txtField_companyRegNo: UITextField!
    
    @IBOutlet weak var txtField_nationality: MPGTextField!
   
    @IBOutlet weak var txtField_address: UITextField!
    @IBOutlet weak var txtField_postCode: UITextField!
    @IBOutlet weak var txtField_language: UITextField!
    @IBOutlet weak var txtField_website: UITextField!
    
    @IBOutlet weak var txtField_serviceType: UITextField!
    @IBOutlet weak var txtField_drivingLicenceExpiryDate: UITextField!
    @IBOutlet weak var txtField_conductNo: UITextField!
    @IBOutlet weak var txtField_issueNo: UITextField!
    @IBOutlet weak var txtField_birthCertificateORPassportNo: UITextField!
    
    @IBOutlet weak var btn_uploadID: UIButton!
   
    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var imgVw_uploadedId: UIImageView!
    //Constraints------------
    
    @IBOutlet weak var constraint_txt_selectCompany_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_companyName_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_CompanyName_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_selectCompany_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_companyAdd_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_companyAdd_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_companyRegNo_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_txt_companyRegNo_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_licenceExpiryDate_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_licenceExpiryDate_height: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_conductNo_height: NSLayoutConstraint!
//    @IBOutlet weak var constraint_conductNo_height: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_conductNo_top: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_issue_top: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_issue_height: NSLayoutConstraint!
    
    //MARK:- VARIABLES
    var arrUserData = [ServiceProviderRegistrationDB]()
    var arrContacts = [Contact]()
    let codeBtn : UIButton = UIButton()
    var userCoordinates : CLLocationCoordinate2D!
    public var presentCountry = Country.currentCountry
    var univTxtField = UITextField()
    var pickerView = UIPickerView()
    var arrGender = ["Male", "Female"]
    var arrLanguages = ["English", "French", "Italian"]
    var relationshipArray = ["Married","Single"]
    var coutriesList : NSArray = []
    var isEditBtnClicked:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var dynamicView:UIView = UIView()
    let checkedImage = UIImage(named: "radio-selected")! as UIImage
    let uncheckedImage = UIImage(named: "radio-unselected")! as UIImage
    var arrCompany = [Company]()
    var companyID: String = ""
    var companyName: String = ""
    var arrServiceTypes = [ServiceType]()
    var serviceTypeID: String = ""
    var isProfilePicImgVwTapped: Bool = false
    var registerUnder: String = ""

    
    //change password view textfield
    var currentPassTxtField = UITextField()
    var newPassTxtField = UITextField()
    var confirmPassTxtField = UITextField()
    var rightBarBtn = UIBarButtonItem()
    let imagePicker = UIImagePickerController()
    var newPassword: String = ""
    var isPhotoRemove: Bool = false
    var isUpdatePhoto: Bool = false
    var isUploadedIdChanged:Bool = false
    
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchServiceProviderUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
        }
        
        self.fectCoutries()
        self.callToRetrieveAllCompanies()
        self.callToRetrieveAllServiceTypes()
    
        
        //add right navigation button
        rightBarBtn = UIBarButtonItem(
            title: "Edit",
            style: .Plain,
            target: self,
            action: #selector(btnClicked(_:))
        )
        self.navigationItem.rightBarButtonItem = rightBarBtn
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        userCoordinates = CLLocationCoordinate2DMake(0.00000, 0.00000)
        
        //make round host image view
        imgVw_profile.layer.cornerRadius = imgVw_profile.frame.size.width / 2
        imgVw_profile.clipsToBounds = true
        imgVw_profile.layer.borderWidth = 2.0
        imgVw_profile.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(ServiceProvider_Profile_ViewController.tappedOnImageVw))
        imgVw_profile.tag = 1
        imgVw_profile.addGestureRecognizer(tap)
        imgVw_profile.userInteractionEnabled = true
        
        //to set profile image
        let imageName = arrUserData[0].userProfileImage!
        //print(imageName)
        
        let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
        // print(urlString)
        let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        imgVw_profile.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        
        
        
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(ServiceProvider_Profile_ViewController.tappedOnImageVw))
        imgVw_uploadedId.tag = 2
        imgVw_uploadedId.addGestureRecognizer(tap2)
        imgVw_uploadedId.userInteractionEnabled = true
        
        //to set id proof image
        let idImageName = arrUserData[0].photoIdentityImage!
        //print(imageName)
        
        let urlString2:String = "\(URLConstants.GET_ID_PROOF_IMAGE)\(idImageName)"
        // print(urlString)
        let escapedAddress2 = urlString2.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        imgVw_uploadedId.sd_setImageWithURL(NSURL(string: escapedAddress2!), placeholderImage:UIImage(named: "avatar.png"))
        
        
        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        
        txtField_language.inputView = pickerView
        txtField_gender.inputView = pickerView
        txtField_selectCompany.inputView = pickerView
        //txtField_nationality.inputView = pickerView
        txtField_serviceType.inputView = pickerView
        imagePicker.delegate = self
        initView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        
        btn_company.backgroundColor = UIColor.clearColor()
        btn_individual.backgroundColor = UIColor.clearColor()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        univTxtField.resignFirstResponder()
    }
    
    func tappedOnImageVw(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        var titleString:String = ""
        
        if tappedImageView.tag == 1 {
            titleString = "Select your profile photo"
            isProfilePicImgVwTapped = true
            let  value:String = NSLocalizedString( titleString , comment: "")
            let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
            let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
                self.openCamorGallary(0)
            }
            let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
                self.openCamorGallary(1)
            }
            
            let removeAction = UIAlertAction(title: NSLocalizedString("Remove image", comment: ""), style: .Destructive) { (alert) -> Void in
                print("remove Image")
                self.imgVw_profile.image = UIImage(named: "avatar")
                self.isPhotoRemove = true
            }
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
                print("Cancel")
            }
            
            
            sheet.addAction(cameraAction)
            sheet.addAction(galleryAction)
            sheet.addAction(cancelAction)
            
            
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
                if let popoverController = sheet.popoverPresentationController {
                    popoverController.sourceView = tappedImageView
                    popoverController.sourceRect = tappedImageView.bounds
                }
                self.presentViewController(sheet, animated: true, completion: nil)
            }
            else{
                presentViewController(sheet, animated: true, completion: nil)
            }
        }
        else if tappedImageView.tag == 2 {
            titleString = "Select your ID proof"
            isProfilePicImgVwTapped = false
            if let selectedImg:UIImage = imgVw_uploadedId.image{
                let imageInfo = JTSImageInfo()
                imageInfo.image = selectedImg
                imageInfo.referenceRect = self.btn_uploadID.frame
                imageInfo.referenceView = self.btn_uploadID.superview
                imageInfo.title = "ID"
                let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
                imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
            }
        }
        
        
        
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageView.contentMode = .ScaleAspectFit
            
            if isProfilePicImgVwTapped {
                imgVw_profile.image = pickedImage
                self.isPhotoRemove = true
                self.isUpdatePhoto = true
            }
            else {
                isUploadedIdChanged = true
                imgVw_uploadedId.image = pickedImage
                btn_uploadID.setTitleColor(UIColor.blackColor(), forState: .Normal)
                btn_uploadID.setTitle("Photo Identity Attached", forState: .Normal)
            }
            
            
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func openCamorGallary(type : Int){
        if type == 0 {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        presentViewController(imagePicker, animated: true, completion: imagePickerHandler)
    }
    
    func imagePickerHandler(){
        imagePicker.navigationBar.translucent = false
        imagePicker.navigationBar.barTintColor = Constants.Colors.BACKGROUND_COLOUR
    }
    
    func initView(){
        
        //Add Drop Icon
        self.dropIcon(txtField_language)
        self.dropIcon(txtField_gender)
        self.dropIcon(txtField_selectCompany)
        self.dropIcon(txtField_DOB)
        self.dropIcon(txtField_nationality)
        self.dropIcon(txtField_serviceType)
        self.dropIcon(txtField_drivingLicenceExpiryDate)
        
        self.layerFields(txtField_firstName)
        self.layerFields(txtField_lastName)
        self.layerFields(txtField_gender)
        self.layerFields(txtField_DOB)
        self.layerFields(txtField_telephoneNo)
        self.layerFields(txtField_mobileNo)
        self.layerFields(txtField_selectCompany)
        self.layerFields(txtField_companyName)
        self.layerFields(txtField_companyAddress)
        self.layerFields(txtField_companyRegNo)
        self.layerFields(txtField_nationality)
        self.layerFields(txtField_address)
        self.layerFields(txtField_postCode)
        self.layerFields(txtField_language)
        self.layerFields(txtField_website)
        self.layerFields(txtField_serviceType)
        self.layerFields(txtField_drivingLicenceExpiryDate)
        self.layerFields(txtField_conductNo)
        self.layerFields(txtField_issueNo)
        self.layerFields(txtField_birthCertificateORPassportNo)
        btn_uploadID.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        CommonFunctions.makeRoundCornerOfButton(btn_uploadID)
        CommonFunctions.makeRoundCornerOfButton(btnChangePassword)
        
        self.rtIcon(txtField_firstName, imageName: "user-name")
        self.rtIcon(txtField_lastName, imageName: "user-name")
        self.rtIcon(txtField_gender, imageName: "gender")
        self.rtIcon(txtField_DOB, imageName: "calender")
        self.rtIcon(txtField_telephoneNo, imageName: "telephone")
        self.rtIcon(txtField_mobileNo, imageName: "phone")
        
        self.rtIcon(txtField_selectCompany, imageName: "company")
        self.rtIcon(txtField_companyName, imageName: "company")
        self.rtIcon(txtField_companyAddress, imageName: "address")
        self.rtIcon(txtField_companyRegNo, imageName: "passport")
        self.rtIcon(txtField_nationality, imageName: "flag")
        self.rtIcon(txtField_address, imageName: "address")
        self.rtIcon(txtField_postCode, imageName: "post-code")
        self.rtIcon(txtField_language, imageName: "country-language")
        self.rtIcon(txtField_website, imageName: "website")
        
        self.rtIcon(txtField_serviceType, imageName: "service-type")
        self.rtIcon(txtField_drivingLicenceExpiryDate, imageName: "license")
        self.rtIcon(txtField_conductNo, imageName: "contact-number")
        self.rtIcon(txtField_issueNo, imageName: "issue")
        self.rtIcon(txtField_birthCertificateORPassportNo, imageName: "passport")
        
        btnChangePassword.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        
        self.txtField_nationality.delegate = self
        self.txtField_nationality.backgroundColor = UIColor.whiteColor()
        self.txtField_nationality.textColor = UIColor.blackColor()
        
        
        if arrUserData[0].registerVia?.containsString("manual") == true {
            btnChangePassword.hidden = false
            btnChangePassword.userInteractionEnabled = true
        }
        else {
            btnChangePassword.hidden = true
            btnChangePassword.userInteractionEnabled = false
        }
        
        self.setEditibilityFalse()
        
    }
    
    
    func textField(textField: MPGTextField!, didEndEditingWithSelection result: [NSObject : AnyObject]!) {
        print(result)
        let dic : NSDictionary = result as NSDictionary
        
        if let country : MyCountry = dic.objectForKey("Object") as? MyCountry{
            self.txtField_nationality.text = country.countryName
        }
        else{
            self.txtField_nationality.text = ""
        }
    }
    
    func dataForPopoverInTextField(textField: MPGTextField!) -> [AnyObject]! {
        return coutriesList as [AnyObject];
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        
        switch tappedImageView.tag{
            
        case 1:
            txtField_gender.becomeFirstResponder()
        case 2:
            txtField_DOB.becomeFirstResponder()
        case 3:
            txtField_selectCompany.becomeFirstResponder()
        case 4:
            txtField_nationality.becomeFirstResponder()
        case 5:
            txtField_language.becomeFirstResponder()
        case 6:
            txtField_serviceType.becomeFirstResponder()
        case 7:
            txtField_drivingLicenceExpiryDate.becomeFirstResponder()
        default:
            print("invalid")
        }
    }
    
    
    func rtIcon(textField : UITextField , imageName : String) {
        if textField == txtField_mobileNo{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 90, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            
            codeBtn.frame = CGRectMake(40, 0, 50, 40)
            codeBtn.setTitle( arrUserData[0].countryCode , forState: .Normal)
            codeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            codeBtn.titleLabel!.font =  UIFont.systemFontOfSize(15)
            codeBtn.addTarget(self, action: #selector(ProfileViewController.pressed(_:)), forControlEvents: .TouchUpInside)
            codeBtn.contentHorizontalAlignment = .Left;
            
            rtPad.addSubview(codeBtn)
            
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
        else{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
    }
    
    func pressed(sender: UIButton!) {
        let countriesViewController = CountriesViewController.standardController()
        countriesViewController.cancelBarButtonItemHidden = true
        countriesViewController.delegate = self
        countriesViewController.majorCountryLocaleIdentifiers = ["GB", "US", "IT", "DE", "RU", "BR", "IN"]
        
        navigationController?.pushViewController(countriesViewController, animated: true)
    }
    
    func fill_user_details(){
        txtField_firstName.text = arrUserData[0].firstName
        txtField_lastName.text = arrUserData[0].lastName
        txtField_gender.text = arrUserData[0].gender
        txtField_DOB.text = arrUserData[0].dateOfBirth
        txtField_telephoneNo.text = arrUserData[0].telephoneNo
        txtField_mobileNo.text = arrUserData[0].mobileNumber
        
        toGetCompanyName(arrUserData[0].companyId!)
        txtField_nationality.text = arrUserData[0].nationality
        txtField_address.text = arrUserData[0].address
        txtField_postCode.text = arrUserData[0].postCode
        txtField_language.text = arrUserData[0].language
        txtField_website.text = arrUserData[0].website
        txtField_serviceType.text = arrUserData[0].serviceTypeName
        txtField_conductNo.text = arrUserData[0].conductNo
        txtField_issueNo.text = arrUserData[0].issueNo
        txtField_birthCertificateORPassportNo.text = arrUserData[0].birthCertificateORPassportNo
        
        if arrUserData[0].dateOfBirth!.containsString(":00") == true {
            txtField_DOB.text = CommonFunctions.dateFromString(arrUserData[0].dateOfBirth!)
        }
        else {
            txtField_DOB.text = arrUserData[0].dateOfBirth
        }
        
        
        if arrUserData[0].driverLicenceExpiryDate!.containsString(":00") == true {
            txtField_drivingLicenceExpiryDate.text = CommonFunctions.dateFromString(arrUserData[0].driverLicenceExpiryDate!)
        }
        else {
            txtField_drivingLicenceExpiryDate.text = arrUserData[0].driverLicenceExpiryDate
        }
        
        
        
        //txtField_DOB.text = dateFromString(arrUserData[0].dateOfBirth!)
        
        if arrUserData[0].registerUnder?.containsString("underCompany") == true {
            btn_company.setImage(checkedImage, forState: .Normal)
            btn_individual.setImage(uncheckedImage, forState: .Normal)
            constraint_txt_companyName_top.constant = 0
            constraint_txt_CompanyName_height.constant = 0
            
            if arrUserData[0].serviceTypeName?.containsString("Driver") == false {
                constraint_licenceExpiryDate_top.constant = 0
                constraint_licenceExpiryDate_height.constant = 0
                constraint_issue_top.constant = 0
                constraint_issue_height.constant = 0
                constraint_conductNo_top.constant = 0
                constraint_conductNo_height.constant = 0
                
            }
            
        }
        else {
            btn_company.setImage(uncheckedImage, forState: .Normal)
            btn_individual.setImage(checkedImage, forState: .Normal)
            
            constraint_txt_selectCompany_top.constant = 0
            constraint_txt_selectCompany_height.constant = 0
            constraint_txt_companyName_top.constant = 0
            constraint_txt_CompanyName_height.constant = 0
            constraint_txt_companyAdd_top.constant = 0
            constraint_txt_companyAdd_height.constant = 0
            constraint_txt_companyRegNo_top.constant = 0
            constraint_txt_companyRegNo_height.constant = 0
            
            if arrUserData[0].serviceTypeName?.containsString("Driver") == false {
                constraint_licenceExpiryDate_top.constant = 0
                constraint_licenceExpiryDate_height.constant = 0
                constraint_issue_top.constant = 0
                constraint_issue_height.constant = 0
                constraint_conductNo_top.constant = 0
                constraint_conductNo_height.constant = 0
            }
        }
        
    }
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    
    
    func addPadding(textField: UITextField){
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 15, 44)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    func btnClicked(sender: UIBarButtonItem) {
        
        if sender.title == "Edit" {
            isEditBtnClicked = true
            self.setEditibilityActive()
            sender.title = "Save"
        }
        else if sender.title == "Save" {
            isEditBtnClicked = false
            self.setEditibilityFalse()
            if validationToEditProfile() == true {
                self.callToPostServiceProviderUserRegistrationDataEditProfile()
            }
            sender.title = "Edit"
        }
    }
    
    
    func validationToEditProfile() -> Bool {
        
        
        if txtField_firstName.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter first name")
            return false
        }
        if txtField_lastName.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter last name")
            return false
        }
        if txtField_language.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please select language")
            return false
        }
        if txtField_gender.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please select gender")
            return false
        }
        if txtField_mobileNo.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter mobile no.")
            return false
        }
        if txtField_nationality.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please select nationality")
            return false
        }
        if txtField_address.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please select location")
            return false
        }
        if txtField_DOB.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please select DOB")
            return false
        }
        
        if txtField_telephoneNo.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter telephone no.")
            return false
        }
        
        if registerUnder != "" || registerUnder.containsString("underCompany") == true {
            
            if txtField_selectCompany.text?.containsString("Other") == true {
            if txtField_companyName.text!.characters.count < 1 {
                FTIndicator.showToastMessage("Please enter company name")
                return false
            }}
                    if txtField_companyAddress.text!.characters.count < 1 {
                        FTIndicator.showToastMessage("Please enter company address")
                        return false
                    }
                    if txtField_companyRegNo.text!.characters.count < 1 {
                        FTIndicator.showToastMessage("Please enter registration no.")
                        return false
                    }
                    if txtField_postCode.text!.characters.count < 1 {
                        FTIndicator.showToastMessage("Please enter post code")
                        return false
                    }
        }
        
        
        if txtField_serviceType.text?.containsString("Driver") == true {
            if txtField_drivingLicenceExpiryDate.text!.characters.count < 1 {
                FTIndicator.showToastMessage("Please enter licence expiry date")
                return false
            }
            if txtField_conductNo.text!.characters.count < 1 {
                FTIndicator.showToastMessage("Please enter conduct no.")
                return false
            }
            if txtField_issueNo.text!.characters.count < 1 {
                FTIndicator.showToastMessage("Please enter issue no.")
                return false
            }
        }
        

        if txtField_website.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter website")
            return false
        }
        if txtField_serviceType.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please select service type")
            return false
        }
        
        if txtField_birthCertificateORPassportNo.text!.characters.count < 1 {
            FTIndicator.showToastMessage("Please enter birth certification or passport no.")
            return false
        }
        
        
        
        return true
    }
    
    func setEditibilityFalse(){
        imgVw_profile.userInteractionEnabled = false
        txtField_firstName.userInteractionEnabled = false
        txtField_lastName.userInteractionEnabled = false
        txtField_gender.userInteractionEnabled = false
        txtField_DOB.userInteractionEnabled = false
        txtField_telephoneNo.userInteractionEnabled = false
        txtField_mobileNo.userInteractionEnabled = false
        txtField_selectCompany.userInteractionEnabled = false
        txtField_companyName.userInteractionEnabled = false
        txtField_companyAddress.userInteractionEnabled = false
        txtField_companyRegNo.userInteractionEnabled = false
        txtField_nationality.userInteractionEnabled = false
        txtField_address.userInteractionEnabled = false
        txtField_postCode.userInteractionEnabled = false
        txtField_language.userInteractionEnabled = false
        txtField_website.userInteractionEnabled = false
        txtField_serviceType.userInteractionEnabled = false
        txtField_drivingLicenceExpiryDate.userInteractionEnabled = false
        txtField_conductNo.userInteractionEnabled = false
        txtField_issueNo.userInteractionEnabled = false
        txtField_birthCertificateORPassportNo.userInteractionEnabled = false
        btn_uploadID.userInteractionEnabled = false
        btnChangePassword.userInteractionEnabled = false
        btn_individual.userInteractionEnabled = false
        btn_company.userInteractionEnabled = false
        imgVw_uploadedId.userInteractionEnabled = false
    }
    func setEditibilityActive(){
        imgVw_profile.userInteractionEnabled = true
        txtField_firstName.userInteractionEnabled = true
        txtField_lastName.userInteractionEnabled = true
        txtField_gender.userInteractionEnabled = true
        txtField_DOB.userInteractionEnabled = true
        txtField_telephoneNo.userInteractionEnabled = true
        txtField_mobileNo.userInteractionEnabled = true
        txtField_selectCompany.userInteractionEnabled = true
        txtField_companyName.userInteractionEnabled = true
        txtField_companyAddress.userInteractionEnabled = true
        txtField_companyRegNo.userInteractionEnabled = true
        txtField_nationality.userInteractionEnabled = true
        txtField_address.userInteractionEnabled = true
        txtField_postCode.userInteractionEnabled = true
        txtField_language.userInteractionEnabled = true
        txtField_website.userInteractionEnabled = true
        txtField_serviceType.userInteractionEnabled = true
        txtField_drivingLicenceExpiryDate.userInteractionEnabled = true
        txtField_conductNo.userInteractionEnabled = true
        txtField_issueNo.userInteractionEnabled = true
        txtField_birthCertificateORPassportNo.userInteractionEnabled = true
        btnChangePassword.userInteractionEnabled = true
        btn_individual.userInteractionEnabled = true
        btn_company.userInteractionEnabled = true
        imgVw_uploadedId.userInteractionEnabled = true
        btn_uploadID.userInteractionEnabled = true
    }
    
    
    
    //MARK:- CUSTOM METHODS
    func fectCoutries(){
        if let art : NSArray = DatabaseBLFunctions.fetchCountries() {
            if art.count>0{
                let mutArt : NSMutableArray = NSMutableArray(array: art)
                mutArt.removeObjectAtIndex(0)
                coutriesList = mutArt
            }
        }
    }
    
    internal func countriesViewControllerDidCancel(countriesViewController: CountriesViewController) { }
    
    internal func countriesViewController(countriesViewController: CountriesViewController, didSelectCountry country: Country) {
        navigationController?.popViewControllerAnimated(true)
        
        print(country.phoneExtension)
        codeBtn.setTitle("+\(country.phoneExtension)-", forState: .Normal)
    }
    
    func openLocationPicker(textField: UITextField){
        
        let locationPicker = LocationPickerViewController()
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        locationPicker.currentLocationButtonBackground = Constants.Colors.BACKGROUND_COLOUR
        locationPicker.showCurrentLocationInitially = true // default: true
        locationPicker.mapType = .Standard // default: .Hybrid
        
        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false
        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
        
        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
        locationPicker.resultRegionDistance = 500 // default: 600
        
        locationPicker.completion = { location in
            
            let locationName : String = (location?.address)!
            self.userCoordinates  = (location?.coordinate)!
            
            var postalCode:String = ""
            
            let countryName : String = (location?.placemark.country)!
            
            if let postalCodeObj: String = (location?.placemark.postalCode){
                postalCode = postalCodeObj
            }
            
                self.txtField_nationality.text! = countryName
                self.txtField_postCode.text = postalCode
            
            
            textField.text = locationName
            
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    func addDatePicker(textField: UITextField){
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
        let todayDate = NSDate()
        let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
        let minimumYear = (components?.year)! - 1917
        let minimumMonth = (components?.month)! - 1
        let minimumDay = (components?.day)! - 1
        let comps = NSDateComponents();
        comps.year = -minimumYear
        comps.month = -minimumMonth
        comps.day = -minimumDay
        let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        let maxYear = (components?.year)! - 2017
        let maxMonth = (components?.month)! - 1
        let maxDay = (components?.day)! - 1
        let maxcomps = NSDateComponents();
        maxcomps.year = -maxYear
        maxcomps.month = -maxMonth
        maxcomps.day = -maxDay
        let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        
        textField.inputView = datePicker
    }
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        univTxtField.text! = formattor.stringFromDate(sender.date)
        
        print("Selected value \(univTxtField.text!)")
        
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        
        univTxtField = textField
        
        
        if textField == txtField_language || textField == txtField_gender || textField == txtField_selectCompany || textField == txtField_DOB || textField == txtField_serviceType || textField == txtField_drivingLicenceExpiryDate {
            textField.tintColor = UIColor.clearColor()
        }
        
        print("called")
        if textField == txtField_address {
            openLocationPicker(textField)
        }
        if textField == txtField_DOB{
            addDatePicker(txtField_DOB)
        }
        if textField == txtField_drivingLicenceExpiryDate {
            addDatePicker(txtField_drivingLicenceExpiryDate)
        }
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        if textField == txtField_nationality{
            txtField_address.text = ""
            txtField_postCode.text = ""
            
        }
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        //avoid to enter white spaces while password field
        if textField == currentPassTxtField || textField == newPassTxtField || textField == confirmPassTxtField{
            if (string == " ") {
                return false
            }
        }
        
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if univTxtField == txtField_serviceType {
            return arrServiceTypes.count
        }
        
        switch univTxtField {
        case txtField_language:
            return arrLanguages.count
        case txtField_gender:
            return arrGender.count
        case txtField_nationality:
            return coutriesList.count
        case txtField_selectCompany:
            return arrCompany.count + 1
            
        default:
            print("not valid")
        }
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if univTxtField == txtField_selectCompany {
            
            if row == arrCompany.count {
                return "Other"
            }
            else {
                return arrCompany[row].companyName
            }
        }
        
        if univTxtField == txtField_serviceType {
            return arrServiceTypes[row].serviceName
        }
        
        switch univTxtField {
        case txtField_language:
            return arrLanguages[row]
        case txtField_gender:
            return arrGender[row]
        case txtField_nationality:
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            return countryDetail.countryName
        default:
            print("not valid")
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if univTxtField == txtField_selectCompany {
            if row == arrCompany.count {
                constraint_txt_CompanyName_height.constant = 44
                constraint_txt_companyName_top.constant = 12
                univTxtField.text! = "Other"
                companyID = "changed"
                txtField_companyAddress.text = ""
                txtField_companyRegNo.text = ""
                txtField_companyName.text = ""
            }
            else {
                constraint_txt_CompanyName_height.constant = 0
                constraint_txt_companyName_top.constant = 0
                univTxtField.text! = arrCompany[row].companyName
                txtField_companyAddress.text! = arrCompany[row].companyAddress
                txtField_companyRegNo.text! = arrCompany[row].companyRegisterationNumber
                companyID = arrCompany[row].id
                companyName = arrCompany[row].companyName
                
            }
        }
        
        if univTxtField == txtField_serviceType {
            serviceTypeID = arrServiceTypes[row].id
            txtField_serviceType.text! = arrServiceTypes[row].serviceName
            if arrServiceTypes[row].serviceName.containsString("Driver") {
                constraint_licenceExpiryDate_top.constant = 8
                constraint_licenceExpiryDate_height.constant = 44
                constraint_issue_top.constant = 8
                constraint_issue_height.constant = 44
                constraint_conductNo_top.constant = 8
                constraint_conductNo_height.constant = 44
            }
            else {
                constraint_licenceExpiryDate_height.constant = 0
                constraint_licenceExpiryDate_top.constant = 0
                constraint_issue_top.constant = 0
                constraint_issue_height.constant = 0
                constraint_conductNo_top.constant = 0
                constraint_conductNo_height.constant = 0
            }
        }
       
        
        switch univTxtField {
        case txtField_language:
            txtField_language.text! = arrLanguages[row]
        case txtField_gender:
            txtField_gender.text! = arrGender[row]
        case txtField_nationality:
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            txtField_nationality.text! = countryDetail.countryName!
        default:
            print("not valid")
        }
        
    }
    
    //to change password
    func callToChangePassword(){
        var authenticateToken: String = ""
        var userId: String = ""
        var userType: String = ""
        
        if arrUserData.count > 0 {
            authenticateToken = arrUserData[0].authenticateToken!
            userId = arrUserData[0].userId!
        }
        
        if let type = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) {
            userType = type
        }
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId,
            URLParams.USER_TYPE: userType,
            URLParams.CURRENT_PASSWORD: currentPassTxtField.text! ,
            URLParams.NEW_PASSWORD: newPassTxtField.text!
        ]
        
        print(params)
        WebserviceUtils.callPostRequest(URLConstants.CHANGE_PASSWORD, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status: String = json.objectForKey("status") as? String{
                    if status.containsString("success") {
                        
                        self.newPassword = self.newPassTxtField.text!
                        
                        if let msg: String = json.objectForKey("status") as? String {
                            FTIndicator.showInfoWithMessage(msg)
                        }
                        
                        
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    //MARK:- Service provider edit profile
    func callToPostServiceProviderUserRegistrationDataEditProfile(){
        var params = [String: String]()
        var countryCode:String = ""
        var socialMediaID: String = ""
        var longitude:String = ""
        var latitude:String = ""
        var telephone: String = ""
        var applicationID: String = ""
        
        
        //fetch user data from db
        if DatabaseBLFunctions.fetchServiceProviderUserRegistrationData().count > 0 {
            arrUserData = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
        }
        
        let user = arrUserData[0]
        
        
        countryCode = codeBtn.titleLabel!.text!
        
    
        
        if let id = user.socialMediaId {
            socialMediaID = id
        }
        
        //to set company id
        if registerUnder == ""{
            
            //to set company id
            if companyID.containsString("changed") == true {
                companyID = ""
            }
            else {
                if companyID == "" {
                    companyID = user.companyId!
                }
            }
            registerUnder = user.registerUnder!
        }
        
        //to set user location coordinates
        if userCoordinates.latitude == 0.0 {
            latitude = user.serviceProviderLatitude!
        }
        else {
            latitude = String(userCoordinates.latitude)
        }
        if userCoordinates.longitude == 0.0 {
            longitude = user.serviceProviderLongitude!
        }
        else{
            longitude = String(userCoordinates.longitude)
        }
        
        if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.DEVICE_TOKEN){
            applicationID = token
        }
        else {
            applicationID = "123456"
        }
        
    
        //start loader
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        params = [
                URLParams.FIRST_NAME: txtField_firstName.text!,
                URLParams.LAST_NAME: txtField_lastName.text!,
                URLParams.EMAIL: lbl_email.text!,
                URLParams.ADDRESS: txtField_address.text!,
                URLParams.WEBSITE: txtField_website.text!,
                URLParams.POST_CODE: txtField_postCode.text!,
                URLParams.TELEPHONE_NUMBER: txtField_telephoneNo.text!,
                URLParams.REGISTER_UNDER: registerUnder,
                URLParams.LANGUAGE: txtField_language.text!,
                URLParams.GENDER: txtField_gender.text!,
                URLParams.MOBILE_NUMBER: txtField_mobileNo.text!,
                URLParams.COUNTRY_CODE: countryCode,
                URLParams.COMPANY_ID: companyID,
                URLParams.COMPANY_NAME: companyName,
                URLParams.COMPANY_ADDRESS: txtField_companyAddress.text!,
                URLParams.COMPANY_REGISTRATION_NO: txtField_companyRegNo.text!,
                URLParams.DOB: txtField_DOB.text!,
                URLParams.SERVICE_TYPE_ID: serviceTypeID,
                URLParams.SERVICE_TYPE_NAME: txtField_serviceType.text!,
                URLParams.NATIONALITY: txtField_nationality.text!,
                URLParams.DRIVING_LICENCE_EXPIRY_DATE: txtField_drivingLicenceExpiryDate.text!,
                URLParams.CONDUCT_NO: txtField_conductNo.text!,
                URLParams.ISSUE_NO: txtField_issueNo.text!,
                URLParams.BIRTH_CERTIFICATE_OR_PASSPORT_NO: txtField_birthCertificateORPassportNo.text!,
                URLParams.SERVICE_PROVIDER_LATITUDE: latitude,
                URLParams.SERVICE_PROVIDER_LONGITUDE: longitude,
                URLParams.REGISTER_VIA: user.registerVia!,
                URLParams.APPLICATION_ID: applicationID,
                URLParams.DEVICE_TYPE: "Ios",
                URLParams.SOCIAL_MEDIA_ID: socialMediaID,
                URLParams.USER_TYPE: user.userType!,
                URLParams.PHOTO_REMOVE: String(isPhotoRemove),
                URLParams.USER_ID: user.userId!,
                URLParams.AUTHENTICATE_TOKEN: user.authenticateToken!
            ]
        
        
        
        print(params)
        let image = imgVw_profile.image
        
        WebserviceUtils.callPostRequestForUserRegistrationToEditProfile(URLConstants.EDIT_PROFILE ,image: image , params: params , isUpdatePhoto: String(isUpdatePhoto) , success: { (response) in
            
            FTIndicator.dismissProgress()
            if let json = response as? NSDictionary {
                
                print(json)
                if let message = json.objectForKey("message"){
                    FTIndicator.showToastMessage(message as! String)
                }
                
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                        if let dic = json.objectForKey("serviceProvider"){
                          
                            if self.isUploadedIdChanged {
                                self.callToAddPhotoIdentityServiceProvider()
                                self.isUploadedIdChanged = false
                                self.registerUnder = ""
                                self.userCoordinates.latitude = 0.0
                                self.userCoordinates.longitude = 0.0
                                
                            }
                            else {
                                //save service provider data into DB
                                let userData = CommonDataParsingMethods.saveDicToServiceProviderRegistrationType(dic as! NSMutableDictionary)
                                DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(userData)
                            }
                            
                        
                            
                         
                        }
                        
                        
                    }
                    
                    
                    
                }}
            }, failure: { (error) in
                print(error.localizedDescription)
                FTIndicator.dismissProgress()
                FTIndicator.showToastMessage(error.localizedDescription)
        })
        
        
    }
    
    
    func callToAddPhotoIdentityServiceProvider(){
        
        let user = arrUserData[0]
        
        
        let params = [
            URLParams.USER_TYPE: user.userType!,
            URLParams.AUTHENTICATE_TOKEN: user.authenticateToken!,
            URLParams.USER_ID: user.userId!
        ]
        
        
        print(params)
        let image = imgVw_uploadedId.image
        
        WebserviceUtils.callPostRequestForUserRegistration(URLConstants.SERVICE_PROVIDER_ADD_PHOTO_IDENTITY ,image: image, params: params , imageParam: "photoIdentity" , success: { (response) in
            
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                        if let dic = json.objectForKey("serviceProvider"){
                            
                            //save service provider data into DB
                            let userData = CommonDataParsingMethods.saveDicToServiceProviderRegistrationType(dic as! NSMutableDictionary)
                            DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(userData)
                            
                        }
                        
                    }
                    
                }}
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btn_ChangePassword_Click(sender: UIButton) {
        print("Button Clicked")
        let alert = UIAlertController(title: "Change Password", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler {
            (txtCurrentPass) -> Void in
            self.currentPassTxtField = txtCurrentPass
            self.currentPassTxtField.secureTextEntry = true
            self.currentPassTxtField.delegate = self
            self.currentPassTxtField.placeholder = "Enter current password"
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (txtNewPass) -> Void in
            self.newPassTxtField = txtNewPass
            self.newPassTxtField.secureTextEntry = true
            self.newPassTxtField.delegate = self
            self.newPassTxtField.placeholder = "Enter new password"
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (txtConfirmPass) -> Void in
            self.confirmPassTxtField = txtConfirmPass
            self.confirmPassTxtField.secureTextEntry = true
            self.confirmPassTxtField.delegate = self
            self.confirmPassTxtField.placeholder = "Enter confirm password"
        }
        
        alert.addAction(UIAlertAction(title: "Submit", style: .Default, handler:{ (UIAlertAction) in
            if self.changePassword() == true {
                self.callToChangePassword()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:handleCancel))
        
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    func changePassword() -> Bool {
        if currentPassTxtField.text?.characters.count > 0 && newPassTxtField.text?.characters.count > 0 && confirmPassTxtField.text?.characters.count > 0 {
            
            if newPassTxtField.text! != confirmPassTxtField.text! {
                FTIndicator.showToastMessage("New and confirm password should be same")
                return false
            }
        }
        else {
            if currentPassTxtField.text?.characters.count < 1 {
                FTIndicator.showToastMessage("Please enter current password")
                return false
            }
            else if newPassTxtField.text?.characters.count < 1 {
                FTIndicator.showToastMessage("Please enter new password")
                return false
            }
            else {
                FTIndicator.showToastMessage("Please enter confirm password")
                return false
            }
        }
        return true
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    @IBAction func btn_individual_click(sender: UIButton) {
        btn_individual.setImage(checkedImage, forState: .Normal)
        btn_company.setImage(uncheckedImage, forState: .Normal)
        
        registerUnder = "asIndividual"
        constraint_txt_selectCompany_top.constant = 0
        constraint_txt_selectCompany_height.constant = 0
        constraint_txt_companyName_top.constant = 0
        constraint_txt_CompanyName_height.constant = 0
        constraint_txt_companyAdd_top.constant = 0
        constraint_txt_companyAdd_height.constant = 0
        constraint_txt_companyRegNo_top.constant = 0
        constraint_txt_companyRegNo_height.constant = 0
        
    }
    @IBAction func btn_company_click(sender: UIButton) {
        btn_company.setImage(checkedImage, forState: .Normal)
        btn_individual.setImage(uncheckedImage, forState: .Normal)
        
        registerUnder = "underCompany"
        constraint_txt_selectCompany_top.constant = 15
        constraint_txt_selectCompany_height.constant = 44
        constraint_txt_companyName_top.constant = 8
        constraint_txt_CompanyName_height.constant = 44
        constraint_txt_companyAdd_top.constant = 8
        constraint_txt_companyAdd_height.constant = 44
        constraint_txt_companyRegNo_top.constant = 8
        constraint_txt_companyRegNo_height.constant = 44
    }
    
    
    func callToRetrieveAllCompanies(){
        WebserviceUtils.callPostRequestWithEmptyParams(URLConstants.GET_ALL_COMPANIES, params: "", success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String{
                    if status.containsString("success"){
                        if let arr = json.objectForKey("company") as? NSArray {
                            print("ghsdkajlshas.kj",arr)
                            if arr.count > 0 {
                                self.toParseCompanies(arr)
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    func toParseCompanies(arrProperty: NSArray) {
        
        print(arrProperty.count)
        arrCompany = []
        
        for item in arrProperty {
            let companyObj = Company()
            
            if let companyAddress = item.objectForKey("companyAddress") as? String {
                companyObj.setValue(companyAddress, forKey: "companyAddress")
            }
            if let companyName = item.objectForKey("companyName") as? String {
                companyObj.setValue(companyName, forKey: "companyName")
            }
            if let companyRegisterationNumber = item.objectForKey("companyRegisterationNumber") as? String {
                companyObj.setValue(companyRegisterationNumber, forKey: "companyRegisterationNumber")
            }
            if let id = item.objectForKey("id") as? String {
                companyObj.setValue(id, forKey: "id")
            }
            arrCompany.append(companyObj)
        }
        
        
        if arrUserData.count > 0 {
            print("email \(arrUserData[0].email!)")
            
            //show username
            lbl_email.text! = arrUserData[0].email!
            fill_user_details()
        }
        else {
            lbl_email.text! = ""
        }
        
    }
    
    
    func toGetCompanyName(id: String){
        
        for item in arrCompany{
            if item.id == id {
             txtField_companyName.text = item.companyName
             txtField_companyRegNo.text = item.companyRegisterationNumber
             txtField_selectCompany.text = item.companyName
            txtField_companyAddress.text = item.companyAddress
                return
            }
        }
        
    }
    
    func callToRetrieveAllServiceTypes(){
        WebserviceUtils.callPostRequestWithEmptyParams(URLConstants.GET_ALL_SERVICE_TYPES, params: "", success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String{
                    if status.containsString("success"){
                        if let arr = json.objectForKey("serviceTypes") as? NSArray {
                            print("ghsdkajlshas.kj",arr)
                            if arr.count > 0 {
                                self.toParseServiceTypes(arr)
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    func toParseServiceTypes(arrProperty: NSArray) {
        
        print(arrProperty.count)
        arrServiceTypes = []
        
        for item in arrProperty {
            let serviceObj = ServiceType()
            
            if let id = item.objectForKey("id") as? String {
                serviceObj.setValue(id, forKey: "id")
            }
            if let serviceName = item.objectForKey("serviceName") as? String {
                serviceObj.setValue(serviceName, forKey: "serviceName")
            }
            
            arrServiceTypes.append(serviceObj)
        }
        
    }
    
    @IBAction func btn_uploadID_Click(sender: UIButton) {
        let  value:String = NSLocalizedString("select your ID Proof", comment: "")
        let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(0)
        }
        let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(1)
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
            print("Cancel")
        }
        sheet.addAction(cameraAction)
        sheet.addAction(galleryAction)
        sheet.addAction(cancelAction)
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            if let popoverController = sheet.popoverPresentationController {
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = sender.bounds
            }
            self.presentViewController(sheet, animated: true, completion: nil)
        }
        else{
            presentViewController(sheet, animated: true, completion: nil)
        }
    }
    
    
}
