//
//  Property_Map_Controller.swift
//  HSS
//
//  Created by Rajni on 01/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SDWebImage

class Property_Map_Controller: UIViewController {
    //IBoutlet------
    var mapProperties = [PropertyDetailsModel]()
    @IBOutlet weak var map_Property: MKMapView!
    //Constant------
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    var calloutView  : CustomCallout!
    var selectedView : MKAnnotationView!
    var myScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer()
    var checkToShowDetails:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(mapProperties)
        map_Property.delegate = self
        if let currentLocation =  NSUserDefaults.standardUserDefaults().valueForKey("CurrentLocation") as? NSDictionary{
            print("current Location is",currentLocation)
            let lat = currentLocation.objectForKey("latitude") as! String
            let long = currentLocation.objectForKey("longitude") as! String
            let latFloat = Double(lat)
            let longFloat = Double(long)
            let location = CLLocation(latitude:latFloat!,longitude:longFloat!)
            self.centerMapOnLocation(location)
        }
        map_Property.showsUserLocation = true
        
        if mapProperties.count > 0{
            addAnnotations()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(Property_Map_Controller.handleTap(_:)))
        tap.delegate = self
        map_Property.addGestureRecognizer(tap)
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        // handling code
        print("tap on map")
        if calloutView != nil {
        if ((sender.view?.isKindOfClass(CustomCallout)) != nil){
            print("inside")
            }}
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_DID_SELECT_MAP, value: false)
    }
    
    
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 800
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        map_Property.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotations(){
        
        for  dic in mapProperties{
            
            let dict : PropertyDetailsModel = dic
            let pname : String = dict.profileName
            let lat : String = dic.propertyLatitude
            let lng : String = dic.propertyLongitude
            
            var image: String = ""
            
            if let imageName : String = dic.images as String {
                if imageName != "" {
                    let imgNameArr = imageName.characters.split{$0 == ","}.map(String.init)
                    image  = imgNameArr[0]
                }
            }
            let locationName: String = dic.streetAddress
            let latFloat = Double(lat)
            let longFloat = Double(lng)
            let propertyID: String = dic.id
            
            
            print(lat)
            
            let isInteger = latFloat! % 1
            
            if isInteger != 0{
                self.pointAnnotation = CustomPointAnnotation(coordinate: CLLocationCoordinate2D(latitude: latFloat! , longitude: longFloat! ))
                self.pointAnnotation.propertyName = pname
                self.pointAnnotation.propertyLocation = locationName
                self.pointAnnotation.propertyImageName = image
                self.pointAnnotation.propertyID = propertyID
                self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: "pin")
                
                self.map_Property.addAnnotation(self.pointAnnotation)
            }
            
            

            
        }
    }
}

extension Property_Map_Controller : MKMapViewDelegate , UIGestureRecognizerDelegate , UINavigationControllerDelegate{

    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        let annotationReuseId = "Place"
        var anView = map_Property.dequeueReusableAnnotationViewWithIdentifier(annotationReuseId)
        if anView == nil {
            anView = AnnotationView(annotation: annotation, reuseIdentifier: annotationReuseId)
            //anView!.canShowCallout = false
            
//            var rightButton: AnyObject! = UIButton.withType
//            rightButton.titleForState(UIControlState.Normal)
//            
//            pinView!.rightCalloutAccessoryView = rightButton as! UIView
        } else {
            anView!.annotation = annotation
            //anView!.canShowCallout = true
        }
        anView?.image = UIImage(named: "map_icon1")
        return anView
    }
    
    
    
    
    
    
    
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {

        
        if view.annotation is MKUserLocation
        {
            return
        }

        selectedView = view
        let annot = view.annotation as! CustomPointAnnotation
        
        print(annot.coordinate)
        let views = NSBundle.mainBundle().loadNibNamed("CustomCallout", owner: nil, options: nil)
        calloutView = views?[0] as! CustomCallout
        PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_DID_SELECT_MAP, value: true)
        
        calloutView.lbl_Name?.text = annot.propertyName
        calloutView.txtVw_locationName?.text = annot.propertyLocation
        checkToShowDetails = true
        
        // to get selected property id
        calloutView.propertyID = annot.propertyID!
        print("id----", calloutView.propertyID)
        
        var selectedProperty = PropertyDetailsModel()
        for item in mapProperties {
            if item.id == annot.propertyID! {
                print(item.id)
                selectedProperty = item
            }
        }
        Constants.map_property.property = selectedProperty
        
        self.calloutView.img_UserImage?.contentMode = .ScaleAspectFit
        let property_img_name = annot.propertyImageName! as String
        
        if property_img_name != "" {
            
            let urlString:String = "\(URLConstants.GET_PROPERTY_IMAGE)\(property_img_name)"
            let escapedAddress: String = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            
            self.calloutView.img_UserImage?.sd_setImageWithURL(NSURL(string: escapedAddress)!, completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!)  -> Void in
                if error != nil{
                    CommonFunctions.getDataFromUrl(NSURL(string: escapedAddress)!) { (data, response, error) in
                        dispatch_async(dispatch_get_main_queue()) {
                            
                            if let img = UIImage(data:data!){
                                self.calloutView.img_UserImage!.image = img
                            }
                            else{
                                self.calloutView.img_UserImage!.image = UIImage(named: "logoIcon")
                            }
                        }
                    }
                    print(error.description)
                }
                self.calloutView.img_UserImage!.image = image
            })
        }
        else {
            self.calloutView.img_UserImage!.image = UIImage(named: "logoIcon")
        }
        
        calloutView.btn.addTarget(self, action: #selector(Property_Map_Controller.buttonTapAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)

        
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        self.map_Property.setCenterCoordinate((view.annotation?.coordinate)!, animated: true)
    }
    
    func buttonTapAction(sender:UIButton!)
    {
        print("Button is working")
    }
    
    
    
    
    
    
    

    
    func moveToDetails() {
        
            //let selectedPropertyID: String = calloutView.propertyID
//        let selectedPropertyID: String = callOut.propertyID
//        var selectedProperty = PropertyDetailsModel()
//        //to get selected property model
//        for item in mapProperties {
//            if item.id == selectedPropertyID {
//                print(item.id)
//                selectedProperty = item
//            }
//        }
//        calloutView.removeFromSuperview()
        
        //calloutView.removeFromSuperview()
        
//        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
//        show_Detail_obj.property = Constants.map_property.property 
//        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
        
    }

   
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        
        
        if view.isKindOfClass(AnnotationView)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
        
        if view.annotation is MKUserLocation{
            return
        }
        
        if calloutView != nil {
            print("****")
             PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_DID_SELECT_MAP, value: false)
            calloutView.removeFromSuperview()
        }
        //view.removeGestureRecognizer(view.gestureRecognizers!.first!)
       
    }
    
    
    func mapView(MapView: MKMapView, annotationView: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if control == annotationView.rightCalloutAccessoryView {
           // performSegueWithIdentifier("NameOfYourSegue", sender: self)
            print("Going to the next VC!")
        }
    }
    
    func btnDetails(sender: UIButton){
        //let buttonTag = sender.tag
        print("rajni *************")
    }
    
}


