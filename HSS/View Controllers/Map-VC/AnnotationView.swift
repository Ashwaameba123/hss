//
//  AnnotationView.swift
//  Demo
//
//  Created by Rajni on 22/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import MapKit

class AnnotationView: MKAnnotationView
{
    
//    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
//        let hitView = super.hitTest(point, withEvent: event)
//        if (hitView != nil)
//        {
//            self.superview?.bringSubviewToFront(self)
//            //self.superview?.bringSubview(toFront: self)
//        }
//        return hitView
//    }
//    func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        let rect = self.bounds;
//        var isInside: Bool = rect.contains(point);
//        if(!isInside)
//        {
//            for view in self.subviews
//            {
//                isInside = view.frame.contains(point);
//                if isInside
//                {
//                    break;
//                }
//            }
//        }
//        return isInside;
//    }
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, withEvent: event)
        if (hitView != nil)
        {
            self.superview?.bringSubviewToFront(self)
           // self.superview?.bringSubview(toFront: self)
        }
        return hitView
    }
    func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds;
        var isInside: Bool = rect.contains(point);
        if(!isInside)
        {
            for view in self.subviews
            {
                isInside = view.frame.contains(point);
                if isInside
                {
                    
                    break;
                }
            }
        }
        return isInside;
    }

}