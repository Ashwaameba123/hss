//
//  Map_View_Controller.swift
//  HSS
//
//  Created by Rajni on 22/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import SDWebImage

private let reuseIdentifier = "CustomPin"

class Map_View_Controller: UIViewController {
    
    //IBoutlet------
    @IBOutlet weak var map_view: MKMapView!

    var mapProperty: PropertyDetailsModel!
    
    //Constant------
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    var calloutView  : CustomCallout!
    var selectedView : MKAnnotationView!
    var myScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer()
    var checkToShowDetails:Bool = false
    var isNearBYAtt: Bool = false
    var near_by_att_obj: NearBy_Attrraction!
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map_view.delegate = self
        
        if let currentLocation =  NSUserDefaults.standardUserDefaults().valueForKey("CurrentLocation") as? NSDictionary{
            print("current Location is",currentLocation)
            let lat = currentLocation.objectForKey("latitude") as! String
            let long = currentLocation.objectForKey("longitude") as! String
            let latFloat = Double(lat)
            let longFloat = Double(long)
            let location = CLLocation(latitude:latFloat!,longitude:longFloat!)
            //self.centerMapOnLocation(location)
        }
        map_view.showsUserLocation = true
        
        if mapProperty != nil {
            addAnnotations()
        }
        
        if near_by_att_obj != nil {
            addAnnotations()
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
    }
    
    //MARK:- Zoom to region
    
    func zoomToRegion(lat: Double , long: Double) {
        
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        let region = MKCoordinateRegionMakeWithDistance(location, 5000.0, 7000.0)
        
        map_view.setRegion(region, animated: true)
    }
    
    
    
    
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 800
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        map_view.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotations(){
        
        if !isNearBYAtt {
        let dic = mapProperty
            
            let dict : PropertyDetailsModel = dic
            let pname : String = dict.profileName
            let lat : String = dic.propertyLatitude
            let lng : String = dic.propertyLongitude
            var image: String = ""
        
        if let imageName : String = dic.images as String {
            if imageName != "" {
            let imgNameArr = imageName.characters.split{$0 == ","}.map(String.init)
               image  = imgNameArr[0]
            }
        }
        
            let locationName: String = dic.streetAddress
            let latFloat = Double(lat)
            let longFloat = Double(lng)
            let propertyID: String = dic.id
            
            
            print(lat)
            
            let isInteger = latFloat! % 1
            
            if isInteger != 0{
                self.pointAnnotation = CustomPointAnnotation(coordinate: CLLocationCoordinate2D(latitude: latFloat! , longitude: longFloat! ))
                self.pointAnnotation.propertyName = pname
                self.pointAnnotation.propertyLocation = locationName
                self.pointAnnotation.propertyImageName = image
                self.pointAnnotation.propertyID = propertyID
                self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: "pin")
                
                self.map_view.addAnnotation(self.pointAnnotation)
                self.zoomToRegion(latFloat! , long: longFloat!)
            }}
        else {
            let dic = near_by_att_obj
            
            let dict : NearBy_Attrraction = dic
            let pname : String = dict.locationName
            let lat : String = dic.loc_lat
            let lng : String = dic.loc_long
            let image: String = ""
            
            let latFloat = Double(lat)
            let longFloat = Double(lng)
            
            print(lat)
            
            let isInteger = latFloat! % 1
            
            if isInteger != 0{
                self.pointAnnotation = CustomPointAnnotation(coordinate: CLLocationCoordinate2D(latitude: latFloat! , longitude: longFloat! ))
                self.pointAnnotation.propertyName = pname
                self.pointAnnotation.propertyImageName = image
                self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: "pin")
                
                self.map_view.addAnnotation(self.pointAnnotation)
                self.zoomToRegion(latFloat! , long: longFloat!)
            }
        }
            
            
            
            
        
    }
}

extension Map_View_Controller : MKMapViewDelegate , UIGestureRecognizerDelegate , UINavigationControllerDelegate{
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        
        let annotationReuseId = "Place"
        var anView = map_view.dequeueReusableAnnotationViewWithIdentifier(annotationReuseId)
        
        if anView == nil {
            anView = AnnotationView(annotation: annotation, reuseIdentifier: annotationReuseId)
            anView!.canShowCallout = false
        } else {
            anView!.annotation = annotation
            //anView!.canShowCallout = true
        }
        
        anView?.image = UIImage(named: "map_icon1")
        
        
        return anView
    }
    
    
    func disclosureTapped() {
        var alert = UIAlertView(title: "Tap!", message: "You tapped the disclosure button.", delegate: nil, cancelButtonTitle: nil)
        alert.show()
    }
    
    
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        
        
        if view.annotation is MKUserLocation
        {
            return
        }
        
        selectedView = view
        let annot = view.annotation as! CustomPointAnnotation
        print(annot.coordinate)
        let views = NSBundle.mainBundle().loadNibNamed("CustomCallout", owner: nil, options: nil)
        
        calloutView = views?[0] as! CustomCallout
        calloutView.userInteractionEnabled = true
        calloutView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.disclosureTapped)))
        
        if !isNearBYAtt {
        calloutView.lbl_Name?.text = annot.propertyName
        calloutView.txtVw_locationName?.text = annot.propertyLocation
        calloutView.btn.addTarget(self, action: #selector(Property_Map_Controller.btnDetails(_:)), forControlEvents: .TouchUpInside)
        checkToShowDetails = true
        
        // to get selected property id
        calloutView.propertyID = annot.propertyID!
        print("id----", calloutView.propertyID)
        
        self.calloutView.img_UserImage?.contentMode = .ScaleAspectFit
        self.calloutView.img_UserImage?.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        let property_img_name = annot.propertyImageName! as String
        
        if property_img_name != "" {
            
            let urlString:String = "\(URLConstants.GET_PROPERTY_IMAGE)\(property_img_name)"
            let escapedAddress: String = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            
            self.calloutView.img_UserImage?.sd_setImageWithURL(NSURL(string: escapedAddress)!, completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!)  -> Void in
                if error != nil{
                    CommonFunctions.getDataFromUrl(NSURL(string: escapedAddress)!) { (data, response, error) in
                        dispatch_async(dispatch_get_main_queue()) {
                            
                            if let img = UIImage(data:data!){
                                self.calloutView.img_UserImage!.image = img
                            }
                            else{
                                self.calloutView.img_UserImage!.image = UIImage(named: "logoIcon")
                            }
                        }
                    }
                    print(error.description)
                }
                self.calloutView.img_UserImage!.image = image
            })
        }
        else {
            self.calloutView.img_UserImage!.image = UIImage(named: "logoIcon")
        }
        }
        else {
            calloutView.lbl_Name?.text = annot.propertyName
            self.calloutView.img_UserImage!.image = UIImage(named: "logoIcon")
        }
        
        
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        self.map_view.setCenterCoordinate((view.annotation?.coordinate)!, animated: true)
        
        let latitude = annot.coordinate.latitude
        let longitude = annot.coordinate.longitude
        let latDelta: CLLocationDegrees = 0.05
        let lonDelta: CLLocationDegrees = 0.05
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        self.map_view.setRegion(region, animated: true)
        self.map_view.showsUserLocation = true
        
        
        
        
    }
    
    
    
    
    
    
    
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        
        if view.annotation is MKUserLocation{
            return
        }
        
        if calloutView != nil {
            calloutView.removeFromSuperview()
        }
    }
    
    
    func btnDetails(sender: UIButton){
        //let buttonTag = sender.tag
        print("rajni *************")
    }

}
