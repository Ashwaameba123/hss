//
//  Custom_Call_Out_View.swift
//  HSS
//
//  Created by Rajni on 22/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class Custom_Call_Out_View: UIView {
    
    @IBOutlet weak var img_vw_property: UIImageView!
    @IBOutlet weak var btn_details: UIButton!
    @IBOutlet weak var txtVw_location: UITextView!
    
    
    override func drawRect(rect: CGRect) {
        img_vw_property?.layer.cornerRadius = (img_vw_property?.frame.size.height)! / 2
        img_vw_property?.clipsToBounds = true
        btn_details.addTarget(self, action: #selector(Property_Map_Controller.btnDetails(_:)), forControlEvents: .TouchUpInside)
    }
}