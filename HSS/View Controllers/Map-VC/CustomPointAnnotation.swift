//
//  CustomPointAnnotation.swift
//  HSS
//
//  Created by Rajni on 01/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import UIKit
import MapKit

class CustomPointAnnotation: NSObject,MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var propertyImageName:String?
    var propertyName:String?
    var propertyLocation:String?
    var propertyID: String?
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
 
    }
    
    
}
