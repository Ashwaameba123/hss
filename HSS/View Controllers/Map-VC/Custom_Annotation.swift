//
//  Custom_Annotation.swift
//  HSS
//
//  Created by Rajni on 22/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import MapKit

class Custom_Annotation: NSObject, MKAnnotation {
    let placeName: String?
    let placeLocationName: String?
    let coordinate: CLLocationCoordinate2D
    init(placeName: String, placeLocationName: String , coordinate: CLLocationCoordinate2D) {
        self.placeName = placeName
        self.placeLocationName = placeLocationName
        self.coordinate = coordinate
        super.init()
    }
}
