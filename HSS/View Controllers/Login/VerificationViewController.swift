//
//  VerificationViewController.swift
//  HSS
//
//  Created by Rajni on 02/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import FTIndicator
import UIKit

class VerificationViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnSignUP: UIButton!
    
    //MARK:- VARIABLES
    var arrUserData = [UserRegistration]()
    var arrServiceProviderData = [ServiceProviderRegistrationDB]()
    var userID: String = ""
    var userType: String = ""
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)!
        
        if userType == Enumerations.USER_TYPE.serviceProvider.rawValue {
            //fetch service provider registration data
            arrServiceProviderData = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
            print(arrServiceProviderData.count)
            if arrServiceProviderData.count > 0 {
                lblUserName.text! = ("Hi, \(arrServiceProviderData[0].firstName!) \(arrServiceProviderData[0].lastName!)")
            }
            
            userID = arrServiceProviderData[0].userId!
        }
        else {
        //fetch user registration data
        arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        print(arrUserData.count)
        
            if arrUserData.count > 0 {
                lblUserName.text! = ("Hi, \(arrUserData[0].firstName!) \(arrUserData[0].lastName!)")
            }
            
            userID = arrUserData[0].userId!
        }
        
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        CommonFunctions.makeRoundCornerOfButton(btnSignUP)
        CommonFunctions.makeRoundCornerOfButton(btnContinue)

    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func btnContinueClick(sender: AnyObject) {
        
        btnContinue.userInteractionEnabled = false
        let params = [
            URLParams.USER_ID: userID,
            URLParams.USER_TYPE: userType
        ]
        WebserviceUtils.callToPostUrl(URLConstants.USER_VERIFICATION, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                self.btnContinue.userInteractionEnabled = true
                
                if let status = json.objectForKey("status"){
                        if status as! String == "error"
                        {
                            if let msg = json.objectForKey("message"){
                                FTIndicator.showToastMessage(msg as! String)
                            }
                        }
                        else{
                            if let dic = json.objectForKey("serviceProvider"){
                                if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                                    
                                    //save service provider data into DB
                                    let userData = CommonDataParsingMethods.saveDicToServiceProviderRegistrationType(dic as! NSMutableDictionary)
                                    DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(userData)
                                }}
                            else {
                            if let dic = json.objectForKey("user"){
                                    //save user data into DB
                                    let userData = CommonDataParsingMethods.saveDicToUserRegistrationType(dic as! NSMutableDictionary)
                                    DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(userData)
                                }}
                            FTIndicator.showSuccessWithMessage("User Verified")
                            self.moveToHomeVC()
                            }
                    }
                
                
            }
            }, failure: { (error) in
                self.btnContinue.userInteractionEnabled = true
                FTIndicator.showInfoWithMessage(error.localizedDescription)
                print(error.localizedDescription)
        })
    }
    
    
    func moveToHomeVC(){
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isLogin")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        appDelegate.window?.rootViewController = yourVC
        appDelegate.window?.makeKeyAndVisible()
        
    }
    

    @IBAction func btnSignUPAsOtherUserClick(sender: AnyObject) {
        
        let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let controller : UINavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("nav") as! UINavigationController
        app.window?.rootViewController = controller
        
    }
    
    
    
}