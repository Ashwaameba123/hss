//
//  InitialViewController.swift
//  HSS
//
//  Created by Rajni on 26/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator

class InitialViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnHost: UIButton!
    @IBOutlet weak var btnServiceProvider: UIButton!
    @IBOutlet weak var btnExploreAsGuest: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
   
    
    //MARK:- VARIABLES
    var scrollInSet: CGFloat!
    
    //MARK:- CONSTANTS
    let loginVCIdentifier = "SHOW-LOGIN-VC"
    
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //scrollView.setContentOffset(CGPointMake(0.0, 64), animated: true)
    }
    
    //MARK:- CUSTOM METHODS
    func initView(){
        
        //to get countries list
        if DatabaseBLFunctions.fetchCountries().count < 1 {
        callToRetrieveCountries()
        }
        
        //to get guest authenticate token
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN) == nil || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN) == "" {
        callToRetrieveGuestToken()
        }
        
        
    btnUser.backgroundColor = Constants.Colors.GREEN_COLOUR
    btnHost.backgroundColor = Constants.Colors.GREEN_COLOUR
    btnServiceProvider.backgroundColor = Constants.Colors.GREEN_COLOUR
    self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
    CommonFunctions.makeRoundCornerOfButton(btnHost)
    CommonFunctions.makeRoundCornerOfButton(btnUser)
    CommonFunctions.makeRoundCornerOfButton(btnServiceProvider)
    
        
        
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnUserClick(sender: AnyObject) {
        
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.USER_TYPE, value: Enumerations.USER_TYPE.user.rawValue)
        self.performSegueWithIdentifier(loginVCIdentifier, sender: nil)
    }
    
    @IBAction func btnHostClick(sender: AnyObject) {
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.USER_TYPE, value: Enumerations.USER_TYPE.host.rawValue)
        self.performSegueWithIdentifier(loginVCIdentifier, sender: nil)
    }
    @IBAction func btnServiceProviderClick(sender: AnyObject) {
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.USER_TYPE, value: Enumerations.USER_TYPE.serviceProvider.rawValue)
        self.performSegueWithIdentifier(loginVCIdentifier, sender: nil)
    }
    
    @IBAction func btnExploreAsGuestClick(sender: AnyObject) {
     PrefrencesUtils.saveStringToPrefs(PreferenceConstant.USER_TYPE, value: Enumerations.USER_TYPE.guest.rawValue)
      toMakeRevealVwInitial()
    }
    
    func toMakeRevealVwInitial(){
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isLogin")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        appDelegate.window?.rootViewController = yourVC
        appDelegate.window?.makeKeyAndVisible()
        
    }
    //MARK:- NAVIGATION METHODS
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == loginVCIdentifier{
            let vc = segue.destinationViewController as! LoginViewController
     }
}
    
    //MARK:- WEB SERVICE
    func callToRetrieveCountries(){
        if  DatabaseBLFunctions.fetchCountries().count == 0{
            WebserviceUtils.callPostRequestWithEmptyParams(URLConstants.GET_COUNTRIES, params: "", success: { (response) in
                if let json = response as? NSDictionary {
                    DatabaseBLFunctions.saveCountries(json["countries"] as! NSArray)
                }
                }, failure: { (error) in
                    print(error.localizedDescription)
            })
        }

    }
    
    
    //MARK:- WEB SERVICE
    func callToRetrieveGuestToken(){
            WebserviceUtils.callPostRequestWithEmptyParams(URLConstants.GET_GUEST_AUTHENTICATE_TOKEN, params: "", success: { (response) in
                if let json = response as? NSDictionary {
                    print(json)
                    if let status = json.objectForKey("status"){
                        if status as! String == "success"{
                            
                            //to save guest authenticate token into prefrences
                            if let token = json.objectForKey("authenticateToken") {
                           PrefrencesUtils.saveStringToPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN , value: token as? String)
                            }
                        }
                    }
                }
                }, failure: { (error) in
                    print(error.localizedDescription)
            })
        
        
    }
    
 
    

}
