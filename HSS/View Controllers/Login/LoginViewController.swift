//
//  LoginViewController.swift
//  HSS
//
//  Created by Rajni on 05/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import UIKit
import FTIndicator
import Fabric
import TwitterKit
//G+
import SwiftyJSON
import AddressBook
import MediaPlayer
import AssetsLibrary
import CoreLocation
import CoreMotion

class LoginViewController:UIViewController , UITextFieldDelegate , GPPSignInDelegate {
    
    
    var forgotField: UITextField!
    
    //MARK:- OUTLETS
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var vwPassword: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var txtFieldUserEmail: UITextField!
    @IBOutlet weak var txtFieldUserPassword: UITextField!
    @IBOutlet weak var btnOrLogin: UILabel!
    
    @IBOutlet weak var scrollVIew: UIScrollView!
    //MARK:- VARIABLES
    var loginType:Int!
    var signIn: GPPSignIn?
    var isLoginViaSocialMedia:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        FTIndicator.dismissProgress()
    }
    
    
    //MARK:- CUSTOM METHODS
    func initView(){
        
        txtFieldUserEmail.autocorrectionType = .No
        
        
        forgotField = UITextField()
        
        
        //deleagtes
        txtFieldUserEmail.delegate = self
        txtFieldUserPassword.delegate = self
        
        btnOrLogin.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
     btnLogin.backgroundColor = Constants.Colors.GREEN_COLOUR
     self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
//     CommonFunctions.makeRoundCornerOfView(vwEmail)
//     CommonFunctions.makeRoundCornerOfView(vwPassword)
//     CommonFunctions.makeRoundCornerOfButton(btnLogin)
        
        
        btnLogin.layer.cornerRadius = 8
        btnLogin.clipsToBounds = true
        
        
           self.leftIcon(txtFieldUserEmail, imageName: "user-name")
        
           self.leftIcon(txtFieldUserPassword, imageName: "password")
        
        self.layerFields(txtFieldUserEmail)
        self.layerFields(txtFieldUserPassword)
    }
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    func leftIcon(textField : UITextField , imageName : String) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 40, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(10, 10, 20, 20)
        imgView.image = UIImage(named: imageName)
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        rtPad.addSubview(imgView)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    func configurationTextField(textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = "Email"
        forgotField = textField
        forgotField.delegate = self
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    func callForgot(forgotEmail : String){
        let params = [
            URLParams.EMAIL: forgotEmail ,
        ]
        print(params)
        
        FTIndicator.showProgressWithmessage("Loading...", userInteractionEnable: false)
        WebserviceUtils.callToPostUrl(URLConstants.USER_FORGOT, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                FTIndicator.dismissProgress()
                print(json)
                if let msg = json.objectForKey("message"){
                    FTIndicator.showToastMessage(msg as! String)
                }
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
    }
    
    
    @IBAction func btnForgotPasswordClick(sender: AnyObject) {
        let alert = UIAlertController(title: "Enter Email", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Submit", style: .Default, handler:{ (UIAlertAction) in
            print("Item : \(self.forgotField.text)")
            if self.forgotField.text?.characters.count < 1 {
               FTIndicator.showToastMessage("Please enter your email")
            }
            else {
            self.callForgot(self.forgotField.text!)
            }
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })

    }
    
    
    func loginResponseManual(){
        
        let params = [
            URLParams.EMAIL: txtFieldUserEmail.text! ,
            URLParams.SOCIAL_MEDIA_ID: "",
            URLParams.PASSWORD: txtFieldUserPassword.text!,
            URLParams.REGISTER_VIA: "manual",
            URLParams.USER_TYPE: PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)!
        ]
        print(params)
        FTIndicator.showProgressWithmessage("Loading...", userInteractionEnable: false)
        WebserviceUtils.callToPostUrl(URLConstants.USER_LOGIN, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                FTIndicator.dismissProgress()
                print(json)
                self.btnLogin.userInteractionEnabled = true
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        
                        if let dic = json.objectForKey("serviceProvider"){
                            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                                
                                //save service provider data into DB
                                let userData = CommonDataParsingMethods.saveDicToServiceProviderRegistrationType(dic as! NSMutableDictionary)
                                DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(userData)
                                WebserviceUtils.callToUpdateDeviceToken()
                            }}
                        else {
                        if let dic = json.objectForKey("user"){
                            //save user data into DB
                            let userData = CommonDataParsingMethods.saveDicToUserRegistrationType(dic as! NSMutableDictionary)
                            DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(userData)
                            WebserviceUtils.callToUpdateDeviceToken()
                            }}
                        
                        if let msg = json.objectForKey("message") {
                        FTIndicator.showToastMessage(msg as! String)

                            if msg as! String == "Email verification pending."{
                              self.toMoveToVerificationVC()
                            }
                            else {
                                self.txtFieldUserPassword.text! = ""
                            }
                        }
                        
                        
                    }
                    else {
                        if let msg = json.objectForKey("message") {
                        FTIndicator.showSuccessWithMessage(msg as! String)
                            
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: "manual")
                            
                            if let dic = json.objectForKey("serviceProvider"){
                            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                                
                                //save service provider data into DB
                                let userData = CommonDataParsingMethods.saveDicToServiceProviderRegistrationType(dic as! NSMutableDictionary)
                                DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(userData)
                                WebserviceUtils.callToUpdateDeviceToken()
                                }}
                            else {
                            
                            if let dic = json.objectForKey("user"){
                                //save user data into DB
                                let userData = CommonDataParsingMethods.saveDicToUserRegistrationType(dic as! NSMutableDictionary)
                                DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(userData)
                                WebserviceUtils.callToUpdateDeviceToken()
                                
                                }}
                        self.toMakeRevealVwInitial()
                        }
                    }
                }
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                self.btnLogin.userInteractionEnabled = true
                print(error.localizedDescription)
        })

    }
    
    
    func toMoveToVerificationVC(){
        performSegueWithIdentifier("SHOW_VERIFICATION_VC", sender: self)
    }
    
    
    @IBAction func btnLoginClick(sender: AnyObject) {
        self.view.endEditing(true)
        if self.Validate() == true {
            
            btnLogin.userInteractionEnabled = false
            self.loginResponseManual()
           // toMakeRevealVwInitial()
           //performSegueWithIdentifier("SHOW_HOME_VIEW_CONTROLLER", sender: nil)
        }
    }
    
    func toMakeRevealVwInitial(){
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isLogin")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        appDelegate.window?.rootViewController = yourVC
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func btnFacebookClick(sender: AnyObject) {
        self.view.userInteractionEnabled = false
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: Enumerations.REGISTER_VIA.facebook.rawValue)

    
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        
//        let isInstalled : Bool = UIApplication.sharedApplication().canOpenURL(NSURL(string: "fb://")!)
//        if isInstalled {
            fbLoginManager.loginBehavior = .Native
//        }
//        else {
//            fbLoginManager.loginBehavior = .Web
//        }
    
        
//        if((FBSDKAccessToken.currentAccessToken()) != nil){
//            print("Token Id Exists")
//            fbLoginManager.loginBehavior = .Native
//        }
//        else{
//             print("Token Id Empty")
//          fbLoginManager.loginBehavior = .Native
//        }
        FBSDKAccessToken.setCurrentAccessToken(nil)
        
        fbLoginManager.logInWithReadPermissions(["email"], fromViewController: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result
                if result.isCancelled {
                    self.view.userInteractionEnabled = true
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                   // LoadingOverlay.shared.showOverlay(self.app.window!)
                    self.getFBUserData()
                }
            }
        }

    }
    func getFBUserData(){
        if((FBSDKAccessToken.currentAccessToken()) != nil){
      

            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    
                    print(result)
                    if let arrResult = result as? NSDictionary {
                        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: Enumerations.REGISTER_VIA.facebook.rawValue)
                        //save facebook id
                        if let facebookID = arrResult.objectForKey("id"){
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_ID, value: facebookID as? String)
                        }
                        else {
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_ID, value: "")
                        }
                        //save facebook email
                        if let fbEmail = arrResult.objectForKey("email"){
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL, value: fbEmail as? String)
                        }
                        else {
                             PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL, value: "")
                        }
                        
                        //save facebook first name
                        if let fbUserFirstName = arrResult.objectForKey("first_name"){
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME, value: fbUserFirstName as? String)
                        }
                        else {
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME, value: "")
                        }
                        
                        //save facebook last name
                        if let fbUserLastName = arrResult.objectForKey("last_name"){
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_LAST_NAME, value: fbUserLastName as? String)
                        }
                        else{
                            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_LAST_NAME, value: "")
                        }
                        
                        self.isLoginViaSocialMedia = true
                        self.toCheckUserRegisteredOrNot()
                    }
                    
                }
            })
        }
       
    }
    
    
    func toOpenSignUPVCViaSocialMedia(){
        let userType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)
        
        if userType! == Enumerations.USER_TYPE.user.rawValue  || userType! == Enumerations.USER_TYPE.host.rawValue   {
            performSegueWithIdentifier("SHOW_SIGN_UP", sender: self)
        }
        else if userType! == Enumerations.USER_TYPE.serviceProvider.rawValue{
            performSegueWithIdentifier("SHOW_SIGNUP_SERVICE_PROVIDER", sender: self)
        }
    }

    
    @IBAction func btnTwitterClick(sender: AnyObject) {
         self.view.userInteractionEnabled = false
        //let btn : UIButton = sender as! UIButton
        Twitter.sharedInstance().logInWithCompletion {
            (session, error) -> Void in
            if (session != nil) {
     
                print(session!.userID)
                print(session!.userName)
                print(session!.authToken)
                print(session!.authTokenSecret)
                
                
                
                
                PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: Enumerations.REGISTER_VIA.twitter.rawValue)
                
                //save twitter id
                if let twitterId: String = session!.userID {
                    PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_ID, value: twitterId)
                }
                else {
                    PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_ID, value: "")
                }
                //save twitter name
                if let twitterUserName:String = session!.userName {
                    PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME, value: twitterUserName)
                }
                else{
                     PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME, value: "")
                }
                
                self.isLoginViaSocialMedia = true
                self.toCheckUserRegisteredOrNot()
                
            }else {
                self.view.userInteractionEnabled = true
                print("Not Login")
            }
        }
    }
    
    @IBAction func btnGooglePlusClick(sender: AnyObject) {
        self.view.userInteractionEnabled = false
        
        print(Enumerations.REGISTER_VIA.googlePlus.rawValue)
        
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: Enumerations.REGISTER_VIA.googlePlus.rawValue)
        //configure google
        signIn = GPPSignIn.sharedInstance()
        signIn?.disconnect()
        signIn?.shouldFetchGooglePlusUser = true
        signIn?.clientID = "368174064620-f987ivo92jdvq2kvp42g93burq412utn.apps.googleusercontent.com"
        signIn?.shouldFetchGoogleUserEmail = true
        signIn?.scopes = ["profile"]
        signIn?.scopes = [kGTLAuthScopePlusLogin , kGTLAuthScopePlusUserinfoEmail]
        signIn?.delegate = self
        signIn?.authenticate()
    }
    
    @IBAction func btnSignUpClick(sender: AnyObject) {
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: Enumerations.REGISTER_VIA.manual.rawValue)
        let userType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)
        isLoginViaSocialMedia = false
        if userType! == Enumerations.USER_TYPE.user.rawValue  || userType! == Enumerations.USER_TYPE.host.rawValue   {
           performSegueWithIdentifier("SHOW_SIGN_UP", sender: self)
        }
        else if userType! == Enumerations.USER_TYPE.serviceProvider.rawValue{
            performSegueWithIdentifier("SHOW_SIGNUP_SERVICE_PROVIDER", sender: self)
        }
    }
    
    //MARK: G+ Delegate
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        FTIndicator.showProgressWithmessage("Loading...", userInteractionEnable: false)
        let plusService :GTLServicePlus = GTLServicePlus.init()
        plusService.retryEnabled = true
        plusService.authorizer = GPPSignIn.sharedInstance().authentication
        plusService.apiVersion = "v1"
        
        let query : GTLQueryPlus = GTLQueryPlus.queryForPeopleGetWithUserId("me") as! GTLQueryPlus
        
        
        plusService.executeQuery(query) { (ticket,person, error) -> Void in
            if ((error) != nil) {
                //Handle Error
                FTIndicator.dismissProgress()
            } else {
                FTIndicator.dismissProgress()
                NSLog("Email= %@", GPPSignIn.sharedInstance().authentication.userEmail)
                NSLog("GoogleID=%@", person.identifier)
                print(person)
                
                if let arrResult = person as? GTLPlusPerson {
                    
                    PrefrencesUtils.saveStringToPrefs(PreferenceConstant.REGISTER_VIA, value: Enumerations.REGISTER_VIA.googlePlus.rawValue)
                    
                    //save google id
                    if let googleId = arrResult.identifier{
                        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_ID, value: googleId)
                    }
                    else {
                       PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_ID, value: "")
                    }
                    
                    //save google email
                    if let googleEmail:String = GPPSignIn.sharedInstance().authentication.userEmail {
                        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL, value: googleEmail)
                    }
                    else{
                       PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL, value: "")
                    }
                    
                    //save google user-name
                    if let googleUserName = arrResult.displayName{
                        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME, value: googleUserName)
                    }
                    else{
                        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME, value: "")
                    }
                   
                    self.isLoginViaSocialMedia = true
                    self.toCheckUserRegisteredOrNot()
                }
                
                //print(person.displayName)
            }
        }

        
    }
    func didDisconnectWithError(error: NSError!) {
        FTIndicator.dismissProgress()
        self.view.userInteractionEnabled = true
     }
    
    func toCheckUserRegisteredOrNot(){
        var paramEmail:String = ""
        var socialMediaId:String = ""
        var registerVia:String = ""
        
//        if let email = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL) {
//            paramEmail = email
//        }
        if let socialID = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_ID) {
            socialMediaId = socialID
        }
        if let registerVIA = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) {
            registerVia = registerVIA
        }

        
        let params = [
            URLParams.EMAIL: paramEmail ,
            URLParams.SOCIAL_MEDIA_ID: socialMediaId,
            URLParams.PASSWORD: "",
            URLParams.REGISTER_VIA: registerVia,
            URLParams.USER_TYPE: PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)!
        ]
        print(params)
        self.view.userInteractionEnabled = true
        if registerVia != Enumerations.REGISTER_VIA.googlePlus.rawValue {
        FTIndicator.showProgressWithmessage("Loading...", userInteractionEnable: false)
        }
        WebserviceUtils.callToPostUrl(URLConstants.USER_LOGIN, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                 print(json)
                
                if registerVia != Enumerations.REGISTER_VIA.googlePlus.rawValue {
                 FTIndicator.dismissProgress()
                }
                
               
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        if let msg : String = json.objectForKey("message") as? String{
                            
                            if msg.containsString("not exists"){
                                FTIndicator.showToastMessage("Please fill the details")
                                self.toOpenSignUPVCViaSocialMedia()
                            }
                            else{
                                FTIndicator.showToastMessage(msg)
                            }
                            
                        }
                     
                    }
                    else {
                        
                        if let dic = json.objectForKey("serviceProvider"){
                            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                                
                                //save service provider data into DB
                                let userData = CommonDataParsingMethods.saveDicToServiceProviderRegistrationType(dic as! NSMutableDictionary)
                                DatabaseBLFunctions.saveUserRegistrationDataSIGNUPServiceProvider(userData)
                                WebserviceUtils.callToUpdateDeviceToken()
                            }}
                        else {
                        if let dic = json.objectForKey("user"){
                            //save user data into DB
                            let userData = CommonDataParsingMethods.saveDicToUserRegistrationType(dic as! NSMutableDictionary)
                            DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(userData)
                            WebserviceUtils.callToUpdateDeviceToken()
                            }}
                        if let msg = json.objectForKey("message"){
                            FTIndicator.showSuccessWithMessage(msg as! String)
                        }
                        self.toMakeRevealVwInitial()
                    }
                }
            }
            }, failure: { (error) in
                if registerVia != Enumerations.REGISTER_VIA.googlePlus.rawValue {
                FTIndicator.dismissProgress()
                }
                print(error.localizedDescription)
        })
    }
   
    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        //to check validation of Email
        if txtFieldUserEmail.text!.utf8.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldUserEmail.attributedPlaceholder = NSAttributedString(string: "Please enter Email ID", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        else{
            valid = CommonFunctions.isEmailValid(txtFieldUserEmail.text!)
            if !valid{
                txtFieldUserEmail.text=""
                self.AnimationShakeTextField(txtFieldUserEmail)
                txtFieldUserEmail.attributedPlaceholder = NSAttributedString(string: "Please enter valid Email ID", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            
        }
        
        //to check validation of password
        if txtFieldUserPassword.text!.utf8.count==0{
            // Change the placeholder color to red for textfield passWord
            txtFieldUserPassword.attributedPlaceholder = NSAttributedString(string: "Please enter Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
//        else{
//            let newLength = txtFieldUserPassword.text!.utf16.count
//            if newLength < 6{
//                valid = false
//                txtFieldUserPassword.text=""
//                self.AnimationShakeTextField(self.txtFieldUserPassword)
//                txtFieldUserPassword.attributedPlaceholder = NSAttributedString(string: "Please enter Password minimum of 6 characters", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
//                
//            }
//        }
        return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
         textField.minimumFontSize = 10
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder()
        return true;
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
         if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    //MARK:- NAVIGATION METHODS
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SHOW_SIGN_UP"{
            let vc = segue.destinationViewController as! SignUpViewController
            vc.loginViaSocialMedia = isLoginViaSocialMedia
        }
        if segue.identifier == "SHOW_SIGNUP_SERVICE_PROVIDER"{
            let vc = segue.destinationViewController as! SignUpServiceProviderViewController
            vc.isLoginViaSocialMedia = isLoginViaSocialMedia
        }
    }
    
    
    
    
    

    
    
}
