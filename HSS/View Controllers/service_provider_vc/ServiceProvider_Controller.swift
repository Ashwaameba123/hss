//
//  ServiceProvider_Controller.swift
//  HSS
//
//  Created by Rajni on 31/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import UIKit
import Foundation
import FTIndicator

class ServiceProvider_Controller: UIViewController , UICollectionViewDataSource{
    
//MARK:- OUTLETS
    @IBOutlet weak var service_Collection_View: UICollectionView!
    @IBOutlet weak var btn_AddSign: UIButton!
    
 //MARK:- VARIABLES
    var property: PropertyDetailsModel!
    var userData: UserRegistration!
    var serviceProviderUserData: ServiceProviderRegistrationDB!
    var arrServiceProviders = [ServiceProviderRegistration]()
    var selectedPropertyId: String = ""
    var isAddedByHost: Bool = false
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
//MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        //set back button
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ServiceProvider_Controller.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton
   
   }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        
        
        let userType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)
        
        if userType == Enumerations.USER_TYPE.host.rawValue {
            btn_AddSign.hidden = false
            btn_AddSign.userInteractionEnabled = true
        }
        else {
            btn_AddSign.hidden = true
            btn_AddSign.userInteractionEnabled = false
        }
        
        pageNumber = 0
        arrServiceProviders.removeAll()
        self.callGetServiceProviderOfParticularProperty()
    }
    
    
    //MARK:- BUTTON ACTIONS
    func back(sender: UIBarButtonItem) {
        
        if isAddedByHost{
            self.navigationController?.popToRootViewControllerAnimated(true)
        }else {
           self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    
    //MARK:- WEB SERVICE
    func callGetServiceProviderOfParticularProperty(){
        var userId: String = ""
        var authenticateToken: String = ""
        var propertyID: String = ""
        let page_no:String = String(pageNumber)
        
        //when guest
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
         
            
        }
            
       else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            let arr = DatabaseBLFunctions.fetchUserRegistrationData()
            if arr.count > 0 {
                userData = arr[0]
            }
            
            //get user id
            if let user_id = userData.userId {
                userId = user_id
            }
            //get authenticate token
            if let token = userData.authenticateToken {
                authenticateToken = token
            }
            
            
            if selectedPropertyId == "" {
            //get property id
            if let id = property.id {
                propertyID = id
                }}
            else {
                propertyID = selectedPropertyId
            }
        }
            
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            //get user id
            if let user_id = serviceProviderUserData.userId {
                userId = user_id
            }
            //get authenticate token
            if let token = serviceProviderUserData.authenticateToken {
                authenticateToken = token
            }
            //get property id
            if let id = property.id {
                propertyID = id
            }
        }
        else {
            //get user id
        if let user_id = userData.userId {
            userId = user_id
        }
            //get authenticate token
            if let token = userData.authenticateToken {
                authenticateToken = token
            }
            //get property id
            if let id = property.id {
                propertyID = id
            }
        }
        
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId,
            URLParams.PROPERTY_ID: propertyID,
            URLParams.PAGE_NUMBER: page_no
        ]
        
        print(params)
        
        WebserviceUtils.callToPostUrl(URLConstants.GET_SERVICE_PROVIDER_FOR_PARTICULAR_PROPERTY, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String{
                    if status.containsString("success"){
                        if let arr = json.objectForKey("propertyServiceProvider") as? NSArray {
                            print("ghsdkajlshas.kj",arr)
                            if arr.count > 0 {
                                self.toParseServiceProviders(arr)
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                            
                        }
                    }
                    else {
                        if status.containsString("error") {
                            if let msg = json.objectForKey("message") as? String {
                                if msg.containsString("not exists") {
                               FTIndicator.showInfoWithMessage("No Service Provider Found")
                                }
                            }
                        }
                    }
                }
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    
    func toParseServiceProviders(arrSP: NSArray) {
        
        print(arrSP.count)
        
        for item in arrSP {
            let spObject = ServiceProviderRegistration()
            
            if let userProfileImage = item.objectForKey("userProfileImage") as? String {
                spObject.userProfileImage = userProfileImage
            }
            if let firstName = item.objectForKey("firstName") as? String {
                spObject.firstName = firstName
            }
            if let lastName = item.objectForKey("lastName") as? String {
                spObject.lastName = lastName
            }
            if let serviceTypeName = item.objectForKey("serviceTypeName") as? String {
                spObject.serviceTypeName = serviceTypeName
            }
            if let id = item.objectForKey("id") as? String {
                spObject.userId = id
            }
            if let address = item.objectForKey("address") as? String {
                spObject.address = address
            }
            if let countryCode = item.objectForKey("countryCode") as? String {
                spObject.countryCode = countryCode
            }
            if let emailId = item.objectForKey("emailId") as? String {
                spObject.email = emailId
            }
            if let mobileNumber = item.objectForKey("mobileNumber") as? String {
                spObject.mobileNo = mobileNumber
            }
            if let registerVia = item.objectForKey("registerVia") as? String {
                spObject.registerVia = registerVia
            }
            if let userType = item.objectForKey("userType") as? String {
                spObject.userType = userType
            }
            
            
            arrServiceProviders.append(spObject)
            if pageNumber == 0{
                self.service_Collection_View.reloadData()
            }
            else{
                let insertIndexPath = NSIndexPath(forItem: arrServiceProviders.count - 1, inSection: 0)
                service_Collection_View.insertItemsAtIndexPaths([insertIndexPath])
            }
        }
        //self.service_Collection_View.reloadData()
    }
    
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            print("reach bottom")
            
            if isMoreDataLeft{
                self.loadMoreServiceProviders()
                isMoreDataLeft = false
            }
        }
    }
    
    func loadMoreServiceProviders(){
        pageNumber += 1
        self.callGetServiceProviderOfParticularProperty()
    }
    
    
    
//MARK:- CollectionView_DataSource & Delegates-----
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrServiceProviders.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell1", forIndexPath: indexPath) as! ServiceProvider_CollectionCell
        Cell.lbl_Provider_Name.text = "\(arrServiceProviders[indexPath.row].firstName) \(arrServiceProviders[indexPath.row].lastName)"
        Cell.lbl_Message_Provider.text = arrServiceProviders[indexPath.row].serviceTypeName
        
        
        //to set profile image
        let imageName = arrServiceProviders[indexPath.row].userProfileImage
        
        let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
        let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        Cell.imgVW_Service.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        
        
        return Cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((collectionView.frame.size.width / 2) - 13, 200)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
    let detailProvider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailProvider_Controller") as! DetailProvider_Controller
        detailProvider_obj.serviceProviderDetails = self.arrServiceProviders[indexPath.row]
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
        detailProvider_obj.userData = self.userData
        }
        self.navigationController?.pushViewController(detailProvider_obj, animated: true)
    }
    
//ADD_Button_Action---------
    @IBAction func btn_Add(sender: AnyObject) {
        
        performSegueWithIdentifier("SHOW_ADD_SERVICE_PROVIDER_VC", sender: self)
//        let addProvider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("Add_serviceProvider_controller") as! Add_serviceProvider_controller
//        
//        self.navigationController?.pushViewController(addProvider_obj, animated: true)
        

    
    }
       }
