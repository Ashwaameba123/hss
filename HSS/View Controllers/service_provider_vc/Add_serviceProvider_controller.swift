//
//  Add_serviceProvider_controller.swift
//  
//
//  Created by Rajni on 31/01/17.
//
//

import UIKit
import FTIndicator
class Add_serviceProvider_controller: UIViewController,UITableViewDelegate,UITableViewDataSource {
//IBoutlet(s)------
    
 
    @IBOutlet weak var btn_AddNewProvider: UIBarButtonItem!
    @IBOutlet weak var tableVw_Provider: UITableView!
        
    
//Constant(s)------
 var pressed = false
    
    //MARK:- VARIABLES
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
     var arrUserData = [UserRegistration]()
     var arrServiceProviders = [ServiceProviderRegistration]()
     var selectedServiceProviderID: String = ""
     var serviceProviderWhileNewRegistration: String = ""
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    
//MARk:--View_Life_Cycle-------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        
         //to fetch user data
        arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        pageNumber = 0
        arrServiceProviders = []
        callGetAllServiceProvider()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
    }
    
    
    
    //MARK:- WEB SERVICE
    func callGetAllServiceProvider(){
        var userId: String = ""
        var authenticateToken: String = ""
        var propertyId: String = ""
        
        let page_no:String = String(pageNumber)
        
        //when guest
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            
            
        }
        else {
            if arrUserData.count > 0 {
            //get user id
            if let user_id = arrUserData[0].userId {
                userId = user_id
            }
            //get authenticate token
            if let token = arrUserData[0].authenticateToken {
                authenticateToken = token
            }
            }
        }
        
        //get propertyId
        if let property_id = Constants.selectedProperty.selectedPropertyDetails.id {
            propertyId = property_id
        }
        
        
        let params = [
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.USER_ID: userId,
            URLParams.PAGE_NUMBER: page_no,
            URLParams.PROPERTY_ID: propertyId
        ]
        
        print(params)
        
        WebserviceUtils.callToPostUrl(URLConstants.GET_ALL_SERVICE_PROVIDERS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                    if let status = json.objectForKey("status") as? String{
                        if status.containsString("success"){
                            if let arr = json.objectForKey("serviceProvider") as? NSArray {
                                print("ghsdkajlshas.kj",arr)
                                if arr.count > 0 {
                                    self.toParseServiceProviders(arr)
                                }
                                
                                if arr.count < 10 {
                                    self.isMoreDataLeft = false
                                }
                                else {
                                    self.isMoreDataLeft = true
                                }
                            }
                        }
                    }
                    
                }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    func callToAddServiceProvider(){
        var userId: String = ""
        var authenticateToken: String = ""
        var propertyId: String = ""
        
        if arrUserData.count > 0 {
            //get user id
            if let user_id = arrUserData[0].userId {
                userId = user_id
            }
            //get authenticate token
            if let token = arrUserData[0].authenticateToken {
                authenticateToken = token
            }
        }
        
        //get propertyId
        if let property_id = Constants.selectedProperty.selectedPropertyDetails.id {
            propertyId = property_id
        }
        
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken,
            URLParams.PROPERTY_ID: propertyId,
            URLParams.SERVICE_PROVIDER_ID: selectedServiceProviderID
        ]
        print(params)
        
        FTIndicator.showProgressWithmessage("Loading...", userInteractionEnable: false)
        WebserviceUtils.callToPostUrl(URLConstants.ADD_SERVICE_PROVIDER_TO_PROPERTY, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                FTIndicator.dismissProgress()
                print(json)
                if let msg = json.objectForKey("message"){
                    FTIndicator.showToastMessage(msg as! String)
                    self.navigationController?.popViewControllerAnimated(true)
                }
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
    }
    
    
    func toParseServiceProviders(arrSP: NSArray) {
        
        print(arrSP.count)
        
        for item in arrSP {
            let spObject = ServiceProviderRegistration()
            
            if let userProfileImage = item.objectForKey("userProfileImage") as? String {
                spObject.userProfileImage = userProfileImage
            }
            if let firstName = item.objectForKey("firstName") as? String {
                spObject.firstName = firstName
            }
            if let lastName = item.objectForKey("lastName") as? String {
                spObject.lastName = lastName
            }
            if let serviceTypeName = item.objectForKey("serviceTypeName") as? String {
                spObject.serviceTypeName = serviceTypeName
            }
            if let id = item.objectForKey("id") as? String {
                spObject.userId = id
            }
            arrServiceProviders.append(spObject)
            self.tableVw_Provider.delegate = self
            self.tableVw_Provider.dataSource = self
            
            if pageNumber == 0{
                self.tableVw_Provider.reloadData()
            }
            else {

            self.tableVw_Provider.beginUpdates()
            self.tableVw_Provider.insertRowsAtIndexPaths([
                NSIndexPath(forRow: arrServiceProviders.count-1, inSection: 0)
                ], withRowAnimation: .Automatic)
            self.tableVw_Provider.endUpdates()
            }
        }
        
        
    }
    
    
    
    
//MARK:-TableView_DataSource & Delegates---------
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65.0
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    
    return arrServiceProviders.count
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let Cell = tableView.dequeueReusableCellWithIdentifier("cell1",forIndexPath: indexPath ) as! AddProvider_TableCell
        Cell.btn_Check.hidden = true
        Cell.btn_Check.userInteractionEnabled = false
        
        Cell.lbl_ProviderName.text = "\(arrServiceProviders[indexPath.row].firstName) \(arrServiceProviders[indexPath.row].lastName)"
        
        Cell.lbl_provider_serviceTypeName.text = arrServiceProviders[indexPath.row].serviceTypeName
        
        //to set profile image
        let imageName = arrServiceProviders[indexPath.row].userProfileImage
        
        let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
        let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        Cell.imgVw_ProviderImage.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))

        
        //Cell.btn_Check.addTarget(self, action: #selector(Add_serviceProvider_controller.action_CheckBtn), forControlEvents: .TouchUpInside)
      // Cell.btn_Check.tag = indexPath.row
    return Cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
           FTIndicator.showErrorWithMessage("First sign up")
            
        }
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            FTIndicator.showErrorWithMessage("")
        }
        else {
        let alert = UIAlertController(title: "Do you want to add this service provider ?", message: "", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler:{ (UIAlertAction) in
            self.callToAddServiceProvider()
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
            selectedServiceProviderID = arrServiceProviders[indexPath.row].userId
        }
    }
    
    
    
    //MARK:- BUTTON ACTIONS
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    @IBAction func action_AddNewProvider(sender: UIBarButtonItem) {
        
       PrefrencesUtils.saveBoolToPrefs(PreferenceConstant.IS_SERVICE_PROVIDER_ADDED_BY_HOST, value: true)
       Constants.ServiceProviderRegistrationData.ServiceProviderUserData.hostUserId = arrUserData[0].userId
        
    let service_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpServiceProviderViewController") as! SignUpServiceProviderViewController
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.AUTHENTICATE_TOKEN_HOST, value: arrUserData[0].authenticateToken)
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.HOST_ID, value: arrUserData[0].userId)
        PrefrencesUtils.saveStringToPrefs(PreferenceConstant.HOST_PROPERTY_ID, value: Constants.selectedProperty.selectedPropertyDetails.id)
        
     self.navigationController?.pushViewController(service_provider_obj, animated: true)
        
    }
    
    
    func action_CheckBtn(sender : UIButton) {
         let check_btn = sender.tag
        if !pressed {
            let image = UIImage(named: "filld_check") as UIImage!
            
            sender.setImage(image, forState: .Normal)
            pressed = true
            
            //FTIndicator.showNotificationWithTitle("Home Stay Safari", message: "Do you want to add this Serviceprovider")
            //else if {}
        } else {
            
            let image = UIImage(named: "hollow_check") as UIImage!
            sender.setImage(image, forState: .Normal)
            pressed = false
        }
    }
    
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            print("reach bottom")
            
            if isMoreDataLeft{
                self.loadMoreServiceProviders()
                isMoreDataLeft = false
            }
        }
    }
    
    func loadMoreServiceProviders(){
        pageNumber += 1
        self.callGetAllServiceProvider()
    }
}
