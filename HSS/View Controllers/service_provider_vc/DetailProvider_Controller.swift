//
//  DetailProvider_Controller.swift
//  HSS
//
//  Created by Rajni on 31/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import UIKit
import FTIndicator
import JTSImageViewController

class DetailProvider_Controller: UIViewController,UITableViewDataSource , LGChatControllerDelegate {

    
    //MARK:- OUTLETS
    @IBOutlet weak var imgVw_DetailImg: UIImageView!
    @IBOutlet weak var tblVW_ProviderDetail: UITableView!
    @IBOutlet weak var btn_chat: UIButton!
    
    
    //MARK:- VARIABLES
    var userData: UserRegistration!
    var userDataServiceProvider: ServiceProviderRegistrationDB!
    var userID: String!
    var arrBasicInfo = [String]()
    var arrContactDetails = [String]()
    var serviceProviderDetails: ServiceProviderRegistration!
    var isShowHostDetails:Bool = false
    var hostId: String = ""
    var hostData = Registration()
    var isShowUserDetails: Bool = false
    var notification: Notification!
    let codeBtn : UIButton = UIButton()
    var noToCall:String = ""
    var showDetailsViaHistory:Bool = false
    var booking: Property_Booking!
    var isHostViaHistory: Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    //MARK:- VIEW LIFE - CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblVW_ProviderDetail.sectionHeaderHeight = 50
        self.imgVw_DetailImg.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailProvider_Controller.imageTapped(_:)))
        self.imgVw_DetailImg.addGestureRecognizer(tapRecognizer)
        
        tblVW_ProviderDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        
        //for new request notification
        if CommonFunctions.globals.isNewRequestFrmUser {
        CommonFunctions.globals.isNeedToRefreshNotifications = true
        CommonFunctions.globals.isNewRequestFrmUser = false
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        arrContactDetails = []
        arrBasicInfo = []
        
        if !isShowHostDetails && !isShowUserDetails && !showDetailsViaHistory {
            setServiceProviderDetailsData()
        }
        else if isShowHostDetails || isShowUserDetails || showDetailsViaHistory {
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue && isShowHostDetails {
                btn_chat.hidden = true
                btn_chat.userInteractionEnabled = false
            }
            else if isShowUserDetails {
                btn_chat.hidden = true
                btn_chat.userInteractionEnabled = false
            }
            else {
                btn_chat.hidden = false
                btn_chat.userInteractionEnabled = true
            }
            self.callGetHostDetails()
        }
        
        
        //hide chat button for service provider
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            btn_chat.hidden = true
            btn_chat.userInteractionEnabled = false
        }
        
        //show chat button for User
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            btn_chat.hidden = false
            btn_chat.userInteractionEnabled = true
        }
        
    }
    
    
    
    
    //MARK- CUSTOM METHODS
    func imageTapped(sender: UITapGestureRecognizer)
    {
        if let selectedImg:UIImage = self.imgVw_DetailImg.image {
            let imageInfo = JTSImageInfo()
            imageInfo.image = selectedImg
            imageInfo.referenceRect = self.imgVw_DetailImg.frame
            imageInfo.referenceView = self.imgVw_DetailImg.superview
            imageInfo.title = "ID"
            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
        }}
    
    func setHostDetailsData(){
        arrBasicInfo = []
        arrBasicInfo.append(hostData.firstName + " " + hostData.lastName)
        if hostData.email != "" {
            arrBasicInfo.append(hostData.email)
        }
        arrBasicInfo.append(hostData.locationName)
        
        arrContactDetails = []
        arrContactDetails.append(hostData.countryCode + " " + hostData.mobileNumber)
        
        
        //to set host profile image
        var imageName:String = ""
        
        if let img = hostData.userProfileImage {
            imageName = img
        }
        
        if imageName != "" {
            let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
            let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            
            imgVw_DetailImg.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        }
        else {
            imgVw_DetailImg.image = UIImage(named: "avatar.png")
        }
    }
    
    func setServiceProviderDetailsData(){
        
        
        if serviceProviderDetails.registerVia.containsString("manual") {
            arrBasicInfo.append(serviceProviderDetails.email!)
        }
        //        else {
        //            arrBasicInfo.append("")
        //        }
        
        arrBasicInfo.append(serviceProviderDetails.serviceTypeName!)
        arrBasicInfo.append(serviceProviderDetails.address!)
        let country_code = serviceProviderDetails.countryCode!
        arrContactDetails.append("\(country_code) \(serviceProviderDetails.mobileNo!)")
        
        //to set profile image
        let imageName = serviceProviderDetails.userProfileImage!
        
        let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
        let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        imgVw_DetailImg.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        
    }
    
    
    //MARK:- WEB SERVICE
    func callGetHostDetails(){
        var params = [String: String]()
        
        if isShowHostDetails{
        params = [
            URLParams.USER_ID: hostId
            ]
        }
        
        if isShowUserDetails{
            
            
            if isHostViaHistory{
            params = [
                    URLParams.USER_ID: userID
                ]
            }
            else {
            if let id = notification.userId {
                userID = id
            }
            params = [
                URLParams.USER_ID: userID
            ]
            }
        }
        
        if showDetailsViaHistory{
            
            params = [
                URLParams.USER_ID: userID
            ]
        }
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.GET_USER_INFO, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                      
                        if let dic = json.objectForKey("user") as? NSMutableDictionary {
                            self.hostData = self.saveDicToUserRegistrationType(dic)
                            self.setHostDetailsData()
                            self.tblVW_ProviderDetail.reloadData()
                        }
                    }
                    else {
                        FTIndicator.showInfoWithMessage("No Information Found")
                    }
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
        
        
    }
    
    func callToConfirmBookingByHost(){
        var bookedID: String = ""
        var startDate: String = ""
        var endDate: String = ""
        var auth_token: String = ""
        
        
        if !isHostViaHistory {
            if let id = notification.id {
                bookedID = id
            }
            if let start = notification.bookingStartDate {
                startDate = start
            }
            if let end = notification.bookingEndDate {
                endDate = end
            }
            if let token = userData.authenticateToken {
                auth_token = token
            }
        }
        else {
            if let id = booking.propertyId {
                bookedID = id
            }
            if let start = booking.bookingStartDate {
                startDate = start
            }
            if let end = booking.bookingEndDate {
                endDate = end
            }
            if let token = userData.authenticateToken {
                auth_token = token
            }
        }
        
        
        let params = [URLParams.BOOKED_PROPERTY_ID: bookedID,
                      URLParams.IS_APPROVED: "true",
                      URLParams.BOOKING_START_DATE: startDate,
                      URLParams.BOOKING_END_DATE: endDate,
                      URLParams.AUTHENTICATE_TOKEN: auth_token
        ]
        
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.TO_CONFIRM_BOOKING_BY_HOST, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        FTIndicator.showInfoWithMessage("Request Accepted Successfully")
                        CommonFunctions.globals.isNeedToRefreshNotifications = true
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
    }
    
    func callToDeclineBookingByHost(){
        var bookedPropertyID: String = ""
        var requestID: String = ""
        var bookingStartDate: String = ""
        var bookingEndDate: String = ""
        var auth_token: String = ""
        
        
        if !isHostViaHistory {
            if let id = notification.id {
                requestID = id
            }
            if let p_id = notification.propertyId {
                bookedPropertyID = p_id
            }
            if let start = notification.bookingStartDate {
                bookingStartDate = start
            }
            if let end = notification.bookingEndDate {
                bookingEndDate = end
            }
            if let token = userData.authenticateToken {
                auth_token = token
            }
        }
        else {
            
            if let id = booking.id {
                requestID = id
            }
            if let p_id = booking.propertyId {
                bookedPropertyID = p_id
            }
            if let start = booking.bookingStartDate {
                bookingStartDate = start
            }
            if let end = booking.bookingEndDate {
                bookingEndDate = end
            }
            if let token = userData.authenticateToken {
                auth_token = token
            }
        }
        
        
        let params = [URLParams.DECLINE_REQUEST_ID: requestID,
                      URLParams.PROPERTY_ID: bookedPropertyID,
                      URLParams.BOOKING_START_DATE: bookingStartDate,
                      URLParams.BOOKING_END_DATE: bookingEndDate,
                      URLParams.AUTHENTICATE_TOKEN: auth_token
        ]
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        WebserviceUtils.callPostRequest(URLConstants.TO_DECLINE_BOOKING_BY_HOST, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        
                        if let msg = json.objectForKey("message") as? String {
                            FTIndicator.showInfoWithMessage(msg)
                        }
                        CommonFunctions.globals.isNeedToRefreshNotifications = true
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
    }
    
    
    
    func saveDicToUserRegistrationType(dic: NSMutableDictionary) -> Registration{
        let user = Registration()
        
        if let id = dic.objectForKey("id") {
            user.userId = id as! String
        }
        if let firstName = dic.objectForKey("firstName") {
            user.firstName = firstName as! String
        }
        if let lastName = dic.objectForKey("lastName") {
            user.lastName = lastName as! String
        }
        if let LocationName = dic.objectForKey("LocationName") {
            user.locationName = LocationName as! String
        }
        if let countryCode = dic.objectForKey("countryCode") {
            user.countryCode = countryCode as! String
        }
        
        if let registerVia = dic.objectForKey("registerVia") {
            user.registerVia = registerVia as! String
            
            if user.registerVia.containsString("manual") {
                if let emailId = dic.objectForKey("emailId") {
                    user.email = emailId as! String
                }
            }
            else {
                user.email = ""
            }
            
        }
        if let mobileNumber = dic.objectForKey("mobileNumber") {
            user.mobileNumber = mobileNumber as! String
        }
        if let userProfileImage = dic.objectForKey("userProfileImage") {
            user.userProfileImage = userProfileImage as! String
        }
        if let userType = dic.objectForKey("userType") {
            user.userType = userType as! String
        }
        return user
    }
    
    
    
    //MARK:-TableView_DataSource & Delegates---------
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    
    return 2
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            
        if arrBasicInfo.count > 0 {
//            // when show host details
//            if isShowHostDetails {
//                if arrBasicInfo[1] == "" {
//                    let farray = arrBasicInfo.filter() {$0 != ""}
//                    arrBasicInfo = farray
//                   return 2
//                }
//            }
//            else {
//                if arrBasicInfo[0] == "" {
//                  let farray = arrBasicInfo.filter() {$0 != ""}
//                  arrBasicInfo = farray
//                    return 2
//                }
//                }}
            
           return arrBasicInfo.count
            }}
        if section == 1 {
        return 1
        }
        else{
        return 1
        }
        }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    
        if  indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)as! ProviderDetail_TableCell
            
            if indexPath.row == 2 {
                cell.textLabel?.font = UIFont(name: "Montserrat-Regular", size: 11.0)
                cell.textLabel?.numberOfLines = 4
                cell.textLabel?.text = arrBasicInfo[indexPath.row]
            }
            else {
            
            if arrBasicInfo.count > 0 {
            cell.txtField_title.text = arrBasicInfo[indexPath.row]
            }}
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath) as! ProviderDetail_TableCell
            
            if arrContactDetails.count > 0 {
            self.rtIcon(cell.txtField_title)
            cell.txtField_title.text = arrContactDetails[indexPath.row]
            noToCall = arrContactDetails[indexPath.row]
            }
            
            return cell
        }
        
        }
    
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let phone = noToCall.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if indexPath.section == 1 {
            if let url = NSURL(string: "tel://\(phone)") where UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
            else{
                let alert = UIAlertController(title: "Error", message: "Invalid Number", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 18))
        view.backgroundColor = UIColor.clearColor() // Set your background color
        
        let label = UILabel(frame: CGRectMake(14, 5, 300, 30))
        label.backgroundColor = UIColor.clearColor()
        label.font = UIFont.boldSystemFontOfSize(15)
        
        let labelLine = UILabel(frame: CGRectMake(9,35, 393, 1))
        labelLine.backgroundColor = UIColor.grayColor()
        
        if section == 0 {
            label.text = "Basic Information"
        }
        if section == 1{
            label.text = "Contact Details"
        }
        view.addSubview(label)
        view.addSubview(labelLine)
        return view
    }
    
    //-- FOR HOST TO ACCEPT PROPERTY BOOKING
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 150))
        footerView.backgroundColor = UIColor.clearColor()
        
        if section == 1 {
            if isShowUserDetails {
                let requestBtn: UIButton = UIButton()
                requestBtn.setTitle("ACCEPT REQUEST", forState: .Normal)
                requestBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                requestBtn.backgroundColor = Constants.Colors.GREEN_COLOUR
                requestBtn.frame = CGRect(x:10,y:45,width:tableView.frame.size.width - 20,height:40)
                requestBtn.layer.cornerRadius = 10
                requestBtn.clipsToBounds = true
                requestBtn.titleLabel!.font = UIFont.boldSystemFontOfSize(16)
                // requestBtn.center = footerView.center
                requestBtn.addTarget(self, action: #selector(DetailProvider_Controller.btnToAcceptRequest(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                
                let property_btn: UIButton = UIButton()
                property_btn.setTitle("PROPERTY DETAILS", forState: .Normal)
                property_btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                property_btn.backgroundColor = Constants.Colors.GREEN_COLOUR
                property_btn.frame = CGRect(x:10,y:0,width:tableView.frame.size.width - 20,height:40)
                property_btn.layer.cornerRadius = 10
                property_btn.clipsToBounds = true
                property_btn.titleLabel!.font = UIFont.boldSystemFontOfSize(16)
                // property_btn.center = footerView.center
                property_btn.addTarget(self, action: #selector(DetailProvider_Controller.btnToSeePropertyDetails(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                
                
                let DeclineBtn: UIButton = UIButton()
                DeclineBtn.setTitle("DECLINE REQUEST", forState: .Normal)
                DeclineBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                DeclineBtn.backgroundColor = Constants.Colors.GREEN_COLOUR
                DeclineBtn.frame = CGRect(x:10,y:90,width:tableView.frame.size.width - 20,height:40)
                DeclineBtn.layer.cornerRadius = 10
                DeclineBtn.clipsToBounds = true
                DeclineBtn.titleLabel!.font = UIFont.boldSystemFontOfSize(16)
                // requestBtn.center = footerView.center
                DeclineBtn.addTarget(self, action: #selector(DetailProvider_Controller.btnToDeclineRequest(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                footerView.addSubview(property_btn)
                footerView.addSubview(requestBtn)
                footerView.addSubview(DeclineBtn)
            }
        }
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 120.0
        }
        return 0.0
    }

    
    
    
    //MARK:- DESIGN METHODS
    func rtIcon(textField : UITextField) {
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
        
            codeBtn.frame = CGRectMake(10, 0, 30, 40)
            let img:UIImage = UIImage(named: "call_iconGreen")!
            codeBtn.setImage(img, forState: .Normal)
            codeBtn.addTarget(self, action: #selector(DetailProvider_Controller.callBtnClicked(_:)), forControlEvents: .TouchUpInside)
            codeBtn.contentHorizontalAlignment = .Left;
            
            rtPad.addSubview(codeBtn)
        
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
    
    
    
    
    //MARK:- BUTTON ACTIONS
    
    func callBtnClicked(sender: UIButton!) {
        if let url = NSURL(string: "tel://\(noToCall)") where UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    @IBAction func btn_chat_click(sender: UIButton) {
        print("btn chat click")
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            FTIndicator.showErrorWithMessage("First Sign Up")
        }
        else {
        let chatController = LGChatController()
            let chat = Chat_Model()
    
            
            //*********** when chat with host***************
            if isShowHostDetails {
            //when user
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
                chat.senderId = userData.userId!
                chat.senderAuthToken = userData.authenticateToken!
                chat.senderType = userData.userType!
               }
            //service provider
            else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
                chat.senderId = userDataServiceProvider.userId!
                chat.senderAuthToken = userDataServiceProvider.authenticateToken!
                chat.senderType = userDataServiceProvider.userType!
            }
            
            chat.reciverId = hostData.userId!
            chat.receiverName = hostData.firstName!
            chat.receiverImage = hostData.userProfileImage!
            chat.receiverType = hostData.userType!
            }
            else {
            //*********** when chat with sp***************
                //when user OR host
                if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
                    chat.senderId = userData.userId!
                    chat.senderAuthToken = userData.authenticateToken!
                    chat.senderType = userData.userType!
                }
                
                chat.reciverId = serviceProviderDetails.userId!
                chat.receiverName = serviceProviderDetails.firstName!
                chat.receiverImage = serviceProviderDetails.userProfileImage!
                chat.receiverType = serviceProviderDetails.userType!
            }
            
            
            chatController.chat = chat
            chatController.opponentImage = imgVw_DetailImg.image
            print(chatController.chat)
            self.navigationController?.pushViewController(chatController, animated: true)
        }
       
    }
    
    func btnToAcceptRequest(sender:UIButton!)
    {
        self.callToConfirmBookingByHost()
    }
    
    func btnToDeclineRequest(sender:UIButton!)
    {
        self.callToDeclineBookingByHost()
    }
    
    func btnToSeePropertyDetails(sender:UIButton!)
    {
    if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
        if isHostViaHistory{
         show_Detail_obj.noti_property_id = booking.propertyId
        }
        else {
        show_Detail_obj.noti_property_id = notification.propertyId
        }
        show_Detail_obj.userData = self.userData
        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
        }
    }
    
    }
