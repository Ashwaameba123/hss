//
//  MenuViewController.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator
import FBSDKLoginKit
import Fabric
import TwitterKit
import SDWebImage
import JTSImageViewController

class MenuViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var imgVwPerson: UIImageView!
    @IBOutlet weak var lblPersonName: UILabel!
    @IBOutlet weak var lblPersonEmail: UILabel!
    @IBOutlet weak var tblSideMenu: UITableView!
    
    //MARK:- IMAGES
    let imgHome = UIImage(named: "home")
    let imgMessage = UIImage(named: "message")
    let imgNotification = UIImage(named: "notification")
    let imgSetting = UIImage(named: "setting")
    let imgLogout = UIImage(named: "logout")
    
    //MARK:- VARIABLES
    var arrMenuListItems = ["Home" , "Messages" , "Notifications" , "Settings" , "History" , "Logout"]
    var arrImages = [UIImage]()
    var arrUserData = [UserRegistration]()
    var arrServiceProviderData = [ServiceProviderRegistrationDB]()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        initView()
    }
    
    //MARK:- CUSTOM METHODS
    func initView(){
        self.tblSideMenu.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        arrImages.removeAll()
        
        //*********** WHEN GUEST *************
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            arrMenuListItems = ["Home" , "Logout"]
            arrImages.append(imgHome!)
            arrImages.append(imgLogout!)
            lblPersonName.text = "As Guest"
            lblPersonEmail.text = ""
            imgVwPerson.image = UIImage(named: "avatar.png")
        }
        
        //*********** WHEN SERVICE PROVIDER *************
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            //fetch user data from database
            arrServiceProviderData = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
            arrMenuListItems = ["Home" , "Messages" , "Settings" , "Logout"]
            arrImages.append(imgHome!)
            arrImages.append(imgMessage!)
            arrImages.append(imgSetting!)
            arrImages.append(imgLogout!)
            
            if arrServiceProviderData.count > 0 {
                let firstName :String = arrServiceProviderData[0].firstName!
                lblPersonName.text = firstName
                
                if arrServiceProviderData[0].registerVia == Enumerations.REGISTER_VIA.manual.rawValue {
                    lblPersonEmail.text = arrServiceProviderData[0].email
                }
                else {
                    lblPersonEmail.text = ""
                }
                
                //to set profile image
                let imageName = arrServiceProviderData[0].userProfileImage!
                
                let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
                let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
                
                imgVwPerson.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
            }
        }
        
        //*********** WHEN USER OR HOST *************
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            //fetch user data from database
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
            arrMenuListItems = ["Home" , "Messages" , "History" , "Notifications" , "Settings" , "Logout"]
            arrImages.append(imgHome!)
            arrImages.append(imgMessage!)
            arrImages.append(imgNotification!)
            arrImages.append(imgSetting!)
            arrImages.append(imgNotification!)
            arrImages.append(imgLogout!)
            
            if arrUserData.count > 0 {
                let firstName :String = arrUserData[0].firstName!
                lblPersonName.text = firstName
                
                if arrUserData[0].registerVia == Enumerations.REGISTER_VIA.manual.rawValue {
                    lblPersonEmail.text = arrUserData[0].email
                }
                else {
                    lblPersonEmail.text = ""
                }
                
                //to set profile image
                let imageName = arrUserData[0].userProfileImage!
                
                let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
                let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
                
                imgVwPerson.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
            }
        }
        
        self.imgVwPerson.layer.cornerRadius = self.imgVwPerson.frame.size.width / 2
        self.imgVwPerson.clipsToBounds = true
        self.imgVwPerson.layer.borderWidth = 1.0
        self.imgVwPerson.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        self.imgVwPerson.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.imageTapped(_:)))
        self.imgVwPerson.addGestureRecognizer(tapRecognizer)
        //self.imgVwPerson.contentMode = .ScaleAspectFit
    }
        
        
        
    
    func imageTapped(img: AnyObject)
    {
        if let selectedImg:UIImage = imgVwPerson.image{
            let imageInfo = JTSImageInfo()
            imageInfo.image = selectedImg
            imageInfo.referenceRect = self.imgVwPerson.frame
            imageInfo.referenceView = self.imgVwPerson.superview
            imageInfo.title = "ID"
            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
        }}
    
        
    
    
    //MARK:- TABLE VIEW DATA-SOURCE AND DELEGATES
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuListItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tblSideMenu.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.textLabel?.text = arrMenuListItems[indexPath.row]
        cell.imageView?.image = arrImages[indexPath.row]
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        
        //*********** WHEN GUEST *************
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.guest.rawValue {
            switch indexPath.row {
            case 0:
                self.performSegueWithIdentifier("home", sender: self)
            case 1:
                 self.performSegueWithIdentifier("messages", sender: self)
            default:
                print("invalid selection")
            }
        }
        
        //*********** WHEN SERVICE PROVIDER *************
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            switch indexPath.row {
            case 0:
                self.performSegueWithIdentifier("home", sender: self)
            case 1:
                self.performSegueWithIdentifier("messages", sender: self)
            case 2:
                self.performSegueWithIdentifier("settings", sender: self)
            case 3:
                logoutUser()
            default:
                print("invalid selection")
            }
        }
        
        //*********** WHEN USER OR HOST *************
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            switch indexPath.row {
            case 0:
                self.performSegueWithIdentifier("home", sender: self)
            case 1:
                self.performSegueWithIdentifier("messages", sender: self)
            case 2:
                self.performSegueWithIdentifier("history", sender: self)
            case 3:
                self.performSegueWithIdentifier("notifications", sender: self)
            case 4:
                self.performSegueWithIdentifier("settings", sender: self)
            case 5:
                logoutUser()
            default:
                print("invalid selection")
            }
        }
   }
    
    
    
    func logoutUser(){
        let alert = UIAlertController(title: "HSS", message: "Are you sure you want to logout?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler:nil))
        alert.addAction(UIAlertAction(title: "Logout", style: .Default, handler:{ (UIAlertAction) in
            
            NSUserDefaults.standardUserDefaults().removeObjectForKey("isLogin")
            NSUserDefaults.standardUserDefaults().synchronize()
            

            
            
            let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let viewController:UINavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("nav") as! UINavigationController
            app.window?.rootViewController = viewController
            
            //to clear DB 
            DatabaseBLFunctions.deleteAllData("UserRegistration")
            PrefrencesUtils.saveStringToPrefs(PreferenceConstant.GUEST_AUTHENTICATE_TOKEN, value: "")
            
           FTIndicator.showToastMessage("Logout Successfully")
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    
    
}
