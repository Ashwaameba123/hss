//
//  MessagesViewController.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator
import JTSImageViewController

class MessagesViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var barBtnMenu: UIBarButtonItem!
    @IBOutlet weak var table_View: UITableView!
    
    //MARK:- VARIABLES
    var arrUserData = [UserRegistration]()
    var arrUserDataServiceProvider = [ServiceProviderRegistrationDB]()
    var arrChats = [Message]()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            self.revealViewController().rearViewRevealWidth = 200
            barBtnMenu.target = self.revealViewController()
            barBtnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        if CommonFunctions.chat.noti_chat != nil {
            let chatController = LGChatController()
            chatController.chat = CommonFunctions.chat.noti_chat
            self.navigationController?.pushViewController(chatController, animated: true)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        if CommonFunctions.chat.noti_chat == nil {
        arrChats = []
        self.initView()
        }
        
    }
    
    //MARK:- CUSTOM METHODS
    func initView(){
        
        self.title = "Messages"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) != Enumerations.USER_TYPE.guest.rawValue {
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue ||  PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            arrUserData = DatabaseBLFunctions.fetchUserRegistrationData()
        }
        else if  PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            arrUserDataServiceProvider = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
       }
        
            self.callToGetRecentChatList()
        }
        
    }
    
    func callToGetRecentChatList(){
        
        var userID: String = ""
        var authenticateToken: String = ""
        var params = [String : String]()
        
        
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            if let id = arrUserData[0].userId {
                userID = id
            }
            if let token = arrUserData[0].authenticateToken {
                authenticateToken = token
            }
            params = [
                URLParams.USER_SENDER_ID: userID ,
                URLParams.AUTHENTICATE_TOKEN: authenticateToken
            ]
        }
        else {
            if let id = arrUserDataServiceProvider[0].userId {
                userID = id
            }
            if let token = arrUserDataServiceProvider[0].authenticateToken {
                authenticateToken = token
            }
            params = [
                URLParams.SERVICE_PROVIDER_SENDER_ID: userID ,
                URLParams.AUTHENTICATE_TOKEN: authenticateToken
            ]
        }
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        WebserviceUtils.callPostRequest(URLConstants.GET_ALL_CHATS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                FTIndicator.dismissProgress()
                
                if let arr = json.objectForKey("chat") as? NSArray {
                    self.toParseChats(arr)
                }
                
            }
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    
    func toParseChats(arr: NSArray) {
        
        print(arr.count)
        
        for item in arr {
            
            let msg_obj = Message()
            
            if let applicationId = item.objectForKey("applicationId") as? String {
                msg_obj.setValue(applicationId, forKey: "applicationId")
            }
            if let authenticateToken = item.objectForKey("authenticateToken") as? String {
                msg_obj.setValue(authenticateToken, forKey: "authenticateToken")
            }
            if let firstName = item.objectForKey("firstName") as? String {
                msg_obj.setValue(firstName, forKey: "firstName")
            }
            if let id = item.objectForKey("id") as? String {
                msg_obj.setValue(id, forKey: "id")
            }
            if let lastName = item.objectForKey("lastName") as? String {
                msg_obj.setValue(lastName, forKey: "lastName")
            }
            if let recentMessage = item.objectForKey("recentMessage") as? String {
                msg_obj.setValue(recentMessage, forKey: "recentMessage")
            }
            if let userProfileImage = item.objectForKey("userProfileImage") as? String {
                msg_obj.setValue(userProfileImage, forKey: "userProfileImage")
            }
            if let userType = item.objectForKey("userType") as? String {
                msg_obj.setValue(userType, forKey: "userType")
            }
            
            arrChats.append(msg_obj)
    }
        self.table_View.reloadData()
    }
    
    
    //MARK:- TABLE VIEW DATA SOURCE AND DELEGATES
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrChats.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! Messages_List_Table_Cell
        
        //make round person image view
        cell.img_vw_person.layer.cornerRadius = cell.img_vw_person.frame.size.width / 2
        cell.img_vw_person.clipsToBounds = true
        cell.img_vw_person.layer.borderWidth = 1.0
        cell.img_vw_person.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        cell.img_vw_person.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        cell.img_vw_person.contentMode = .ScaleAspectFit
        cell.img_vw_person.tag = indexPath.row
        cell.img_vw_person.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MessagesViewController.imageTapped(_:)))
        cell.img_vw_person.addGestureRecognizer(tapRecognizer)

        if arrChats.count > 0 {
        let chat = arrChats[indexPath.row]
          cell.lbl_person_name.text = chat.firstName
          cell.lbl_last_msg.text = chat.recentMessage
            
            //to set profile image
            let hostImgName = chat.userProfileImage!
            print(hostImgName)
            
            if hostImgName != "" {
            let urlHostString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(hostImgName)"
            print(urlHostString)
            let escapedAddressHost = urlHostString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            print(escapedAddressHost)
            
            cell.img_vw_person.sd_setImageWithURL(NSURL(string: escapedAddressHost!), placeholderImage:UIImage(named: "avatar.png"))
            }
            else {
                cell.img_vw_person.image = UIImage(named: "avatar.png")
            }
        }
        return cell
    }
    
    
    func imageTapped(sender: UITapGestureRecognizer)
    {
        let point: UIView = sender.view!
        let tag = point.tag
        //let tag = img.tag
        let indexPath = NSIndexPath(forRow: tag, inSection: 0)
        let cell = table_View.cellForRowAtIndexPath(indexPath) as! Messages_List_Table_Cell!
        if let selectedImg:UIImage = cell.img_vw_person.image {
            let imageInfo = JTSImageInfo()
            imageInfo.image = selectedImg
            imageInfo.referenceRect = cell.img_vw_person.frame
            imageInfo.referenceView = cell.img_vw_person.superview
            imageInfo.title = "ID"
            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
    }}
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! Messages_List_Table_Cell
        
        let chatController = LGChatController()
        let chat = Chat_Model()
        
        //to pass chat model
        //********when user OR host
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue{
        chat.senderId = arrUserData[0].userId
        chat.senderAuthToken = arrUserData[0].authenticateToken
        chat.senderType = arrUserData[0].userType
        }
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
            chat.senderId = arrUserDataServiceProvider[0].userId
            chat.senderAuthToken = arrUserDataServiceProvider[0].authenticateToken
            chat.senderType = arrUserDataServiceProvider[0].userType
        }
        
        chat.reciverId = arrChats[indexPath.row].id
        chat.receiverName = arrChats[indexPath.row].firstName
        chat.receiverImage = arrChats[indexPath.row].userProfileImage
        chat.receiverType = arrChats[indexPath.row].userType
        
        
        chatController.chat = chat
        chatController.opponentImage = cell.img_vw_person.image
        print(chatController.chat)
        self.navigationController?.pushViewController(chatController, animated: true)
    }
    

}
