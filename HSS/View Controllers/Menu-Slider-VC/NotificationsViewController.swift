//
//  NotificationsViewController.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator
import SDWebImage

class NotificationsViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var barbtnMenu: UIBarButtonItem!
    @IBOutlet weak var table_view: UITableView!
    
    //MARK:- VARIABLES
    //var arr = ["aaaaaaaaaavfgfgfgrfgfgfggfgfgfgfggbfghjfvjngbvjhgbhjgjhgjfgjhfjhdfgfhjdfhjgfjhgfhjjhjkjjjffg" ,"aaaaaaaaaavfgfgfgrfgfgfggfgfgfgfggbfghjfvjngbvjhgbhjgjhgjfgjhfjhdfgfhjdfhjgfjhgfhjjhjkjjjffgaaaaaaaaaavfgfgfgrfgfgfggfgfgfgfggbfghjfvjngbvjhgbhjgjhgjfgjhfjhdfgfhjdfhjgfjhgfhjjhjkjjjffgaaaaaaaaaavfgfgfgrfgfgfggfgfgfgfggbfghjfvjngbvjhgbhjgjhgjfgjhfjhdfgfhjdfhjgfjhgfhjjhjkjjjffg" , "aaaaaaaaaavfgfgfgrfgfgfggfgfgfgfggbfghjfvjn"]
    
    var height:CGFloat = 0.0
    var arr_Notification = [Notification]()
    var userData: UserRegistration!
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    let nc=NSNotificationCenter.defaultCenter()
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.estimatedRowHeight = 44.0
        table_view.rowHeight = UITableViewAutomaticDimension
        
        if CommonFunctions.globals.isNewRequestFrmUser{
          self.toMoveTodetailScreen(0)
        }
        else {
        self.toRefreshNotificationViewWhilePop()
        }
        
        //notification for new request from user
        nc.addObserver(self, selector: #selector(refreshView(_:)), name: "newRequestFromUser", object: nil)
        nc.addObserver(self, selector: #selector(refreshView(_:)), name: "declineRequestFromHost", object: nil)
        nc.addObserver(self, selector: #selector(refreshView(_:)), name: "hostApprovalForProperty", object: nil)
        nc.addObserver(self, selector: #selector(refreshView(_:)), name: "instantBookingFromUser", object: nil)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
        if CommonFunctions.globals.isNeedToRefreshNotifications {
           self.toRefreshNotificationViewWhilePop()
           CommonFunctions.globals.isNeedToRefreshNotifications = false
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        nc.removeObserver("newRequestFromUser")
        nc.removeObserver("declineRequestFromHost")
        nc.removeObserver("hostApprovalForProperty")
        nc.removeObserver("instantBookingFromUser")
    }
    
    
    func toRefreshNotificationViewWhilePop(){
        //to fetch user data
        let arr = DatabaseBLFunctions.fetchUserRegistrationData()
        if arr.count > 0 {
            self.userData = arr[0]
        }
        initView()
    }
    
    //MARK:- CUSTOM METHODS
    
    func refreshView(notification:NSNotification){
        print("refresh")
        let info = notification.userInfo
        print(info)
        
        var dictonary:NSDictionary?
        var noti_msg:String = ""
        var title_show: String = ""
        
        
        
        
        if let aps = info!["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    print(body)
                    noti_msg = body as String
                }
                if let title = alert["title"] as? NSString {
                    print(title)
                    title_show = title as String
                }
            }}
        
        //to get booked property dic
        if let property = info!["bookedPropertyDetail"]  {
            
            if let data = property.dataUsingEncoding(NSUTF8StringEncoding) {
                
                do {
                    dictonary =  try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
            print(dictonary)
            FTIndicator.showNotificationWithTitle(title_show, message: noti_msg)
           let obj = CommonDataParsingMethods.toParseNotificationDic(dictonary!)
            arr_Notification.insert(obj, atIndex: 0)
            self.table_view.beginUpdates()
            self.table_view.insertRowsAtIndexPaths([
                NSIndexPath(forRow: 0, inSection: 0)
                ], withRowAnimation: .Automatic)
            self.table_view.endUpdates()
            
        }}
    
    func initView(){
        
        
        self.title = "Notifications"
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        
        if self.revealViewController() != nil {
            self.revealViewController().rearViewRevealWidth = 200
            barbtnMenu.target = self.revealViewController()
            barbtnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        pageNumber = 0
        arr_Notification = []
        self.callToGetNotifications()
    }
    
    func callToGetNotifications(){
        var id: String = ""
        var auth_token: String = ""
        var params = [String: String]()
        
        //to fetch user data 
            if let user_id = userData.userId {
                id = user_id
            }
            if let token = userData.authenticateToken {
                auth_token = token
            }
        
        
        
        //when user 
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            params = [URLParams.USER_ID: id,
                      URLParams.AUTHENTICATE_TOKEN: auth_token
                      ]
        }
        //when host
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            params = [URLParams.HOST_ID: id,
                      URLParams.AUTHENTICATE_TOKEN: auth_token
                    ]
        }
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.GET_NOTIFICATIONS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
                if let status = json.objectForKey("status") as? String {
                    if status.containsString("success") {
                        
                        if let arr = json.objectForKey("notifications") as? NSArray {
                            
                            if arr.count > 0 {
                            self.toParseNotification(arr)
                            }
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                        }
                    }
                    else if status.containsString("error") {
                        
                        if let msg = json.objectForKey("message") as? String {
                            FTIndicator.showInfoWithMessage(msg)
                        }
                        self.table_view.reloadData()
                    }
                }
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    
    
    
    
    func toParseNotification(arr: NSArray) {
        
        print(arr.count)
        self.arr_Notification = []
        
        for item in arr {
            
            let obj = Notification()
            
            if let id = item.objectForKey("id") as? String {
                obj.setValue(id, forKey: "id")
            }
            if let propertyId = item.objectForKey("propertyId") as? String {
                obj.setValue(propertyId, forKey: "propertyId")
            }
            if let userId = item.objectForKey("userId") as? String {
                obj.setValue(userId, forKey: "userId")
            }
            if let userName = item.objectForKey("userName") as? String {
                obj.setValue(userName, forKey: "userName")
            }
            if let userProfileImage = item.objectForKey("userProfileImage") as? String {
                obj.setValue(userProfileImage, forKey: "userProfileImage")
            }
            else {
               obj.setValue("", forKey: "userProfileImage")
            }
            if let updatedAt = item.objectForKey("updatedAt") as? String {
                obj.setValue(updatedAt, forKey: "updatedAt")
            }
            
            if let bookingStartDate = item.objectForKey("bookingStartDate") as? String {
                let date = CommonFunctions.dateFromString(bookingStartDate)
                obj.setValue(date, forKey: "bookingStartDate")
            }
            
            if let bookingEndDate = item.objectForKey("bookingEndDate") as? String {
                let date = CommonFunctions.dateFromString(bookingEndDate)
                obj.setValue(date, forKey: "bookingEndDate")
            }
            if let isCancel = item.objectForKey("isCancel") {
                let str = String(isCancel)
                if str == "0" {
                  obj.setValue("false", forKey: "isCancel")
                }
                else {
                  obj.setValue("true", forKey: "isCancel")
                }
            }
            if let isApproved = item.objectForKey("isApproved") {
                let str = String(isApproved)
                if str == "0" {
                    obj.setValue("false", forKey: "isApproved")
                }
                else {
                    obj.setValue("true", forKey: "isApproved")
                }
            }
            if let availableRoom = item.objectForKey("availableRoom") {
                let str = String(availableRoom)
                obj.setValue(str, forKey: "availableRoom")
            }
            if let hostId = item.objectForKey("hostId") {
                let str = String(hostId)
                obj.setValue(str, forKey: "hostId")
            }
            if let totalRoom = item.objectForKey("totalRoom") {
                let str = String(totalRoom)
                obj.setValue(str, forKey: "totalRoom")
            }
            if let bookedRoom = item.objectForKey("bookedRoom") {
                let str = String(bookedRoom)
                obj.setValue(str, forKey: "bookedRoom")
            }
            if let setPrice = item.objectForKey("setPrice") {
                let str = String(setPrice)
                obj.setValue(str, forKey: "setPrice")
            }
            if let bookingType = item.objectForKey("bookingType") {
                let str = String(bookingType)
                obj.setValue(str, forKey: "bookingType")
            }
            if let currency = item.objectForKey("currency") {
                let str = String(currency)
                obj.setValue(str, forKey: "currency")
            }
            if let paymentWith = item.objectForKey("paymentWith") {
                let str = String(paymentWith)
                obj.setValue(str, forKey: "paymentWith")
            }
            
            
            
            
            arr_Notification.append(obj)
            
            if pageNumber == 0{
                self.table_view.reloadData()
            }
            else {
                
                self.table_view.beginUpdates()
                self.table_view.insertRowsAtIndexPaths([
                    NSIndexPath(forRow: arr_Notification.count-1, inSection: 0)
                    ], withRowAnimation: .Automatic)
                self.table_view.endUpdates()
            }
        }
        //self.table_view.reloadData()
    }
    
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            print("reach bottom")
            
            if isMoreDataLeft{
                self.loadMoreServiceProviders()
                isMoreDataLeft = false
            }
        }
    }
    
    func loadMoreServiceProviders(){
        pageNumber += 1
        self.callToGetNotifications()
    }
    
    
    //MARK:- TABLE VIEW DATA SOURCE AND DELEGATES
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr_Notification.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return height + 74.0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! Notification_tbl_cell
        CommonFunctions.makeRoundCornerOfView(cell.vw_background)
        //make round person image view
        cell.img_vw_notification.layer.cornerRadius = cell.img_vw_notification.frame.size.width / 2
        cell.img_vw_notification.clipsToBounds = true
        cell.img_vw_notification.layer.borderWidth = 1.0
        cell.img_vw_notification.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        cell.img_vw_notification.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        cell.img_vw_notification.contentMode = .ScaleAspectFit
        
        let notification = arr_Notification[indexPath.row]
        
        //to set profile image
        let imageName = notification.userProfileImage
        
        if imageName != "" {
            
            let imgNameArr = imageName.characters.split{$0 == ","}.map(String.init)
            
            let str:String = imgNameArr[0]
            
            let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(str)"
            let escapedAddress: String = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            
            cell.img_vw_notification.sd_setImageWithURL(NSURL(string: escapedAddress)!, completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!)  -> Void in
                if error != nil{
                    CommonFunctions.getDataFromUrl(NSURL(string: escapedAddress)!) { (data, response, error) in
                        dispatch_async(dispatch_get_main_queue()) {
                            
                            if let img = UIImage(data:data!){
                                cell.img_vw_notification.image = img
                            }
                            else{
                                cell.img_vw_notification.image = UIImage(named: "avatar.png")
                            }
                        }
                    }
                    print(error.description)
                }
                cell.img_vw_notification.image = image
            })
        }
        else {
            cell.img_vw_notification.image = UIImage(named: "avatar.png")
        }
        
        
        
        
//        if imageName != "" {
//        let urlString:String = "\(URLConstants.GET_PROFILE_PHOTO)\(imageName)"
//        let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
//        
//        cell.img_vw_notification.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
//        }
//        else {
//            cell.img_vw_notification.image = UIImage(named: "avatar.png")
//        }
        
        cell.lbl_notification_title.text = notification.userName
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            cell.txtVw_Notification_descriptn.text = "New request from user to book the property."
        }
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            
            if notification.isApproved.containsString("true") == true {
                cell.txtVw_Notification_descriptn.text = "Host accepts your request to book the property."
            }
            else {
               cell.txtVw_Notification_descriptn.text = "Host decline your request to book the property."
            }
        }
        
        cell.lbl_time.text = CommonFunctions.dateAndTimeFromString(notification.updatedAt)
        
        height = self.adjustUITextViewHeight(cell.txtVw_Notification_descriptn)
        print(height)
        return cell
    }
    
    func adjustUITextViewHeight(arg : UITextView) -> CGFloat
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.scrollEnabled = false
        let height = arg.frame.size.height
        
        return height
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       self.toMoveTodetailScreen(indexPath.row)
    }
    
    
    func toMoveTodetailScreen(index: Int){
        
        
        //to fetch user data
        let arr = DatabaseBLFunctions.fetchUserRegistrationData()
        if arr.count > 0 {
            self.userData = arr[0]
        }
      
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            let show_Detail_provider_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailProvider_Controller") as! DetailProvider_Controller
            show_Detail_provider_obj.isShowUserDetails = true
            show_Detail_provider_obj.userData = self.userData
            
            if CommonFunctions.globals.isNewRequestFrmUser {
                show_Detail_provider_obj.userID = CommonFunctions.NewRequest.Noti_obj.userId
                show_Detail_provider_obj.notification = CommonFunctions.NewRequest.Noti_obj
            }
            else {
            show_Detail_provider_obj.userID = arr_Notification[index].userId
            show_Detail_provider_obj.notification = arr_Notification[index]
            }
            self.navigationController?.pushViewController(show_Detail_provider_obj, animated: true)
        }
        else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
            if CommonFunctions.globals.isNewRequestFrmUser {
                show_Detail_obj.noti_property_id = CommonFunctions.NewRequest.Noti_obj.propertyId
            }
            else{
              show_Detail_obj.noti_property_id = arr_Notification[index].propertyId
            }
            show_Detail_obj.userData = self.userData
            show_Detail_obj.isToShowViaNoti = true
            self.navigationController?.pushViewController(show_Detail_obj, animated: true)
        }
    }
    
    
}