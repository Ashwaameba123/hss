//
//  SettingsViewController.swift
//  HSS
//
//  Created by Rajni on 30/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import FTIndicator

class SettingsViewController: UIViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var barBtnMenu: UIBarButtonItem!
    
    //MARK:- VARIABLES
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
    }
    
    //MARK:- CUSTOM METHODS
    func initView(){
        
        //FTIndicator.showInfoWithMessage("Under Development")
        
        self.title = "Settings"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = false
        if self.revealViewController() != nil {
            barBtnMenu.target = self.revealViewController()
            barBtnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnEditProfileClick(sender: UIButton) {
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) != Enumerations.USER_TYPE.guest.rawValue {
            
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
             performSegueWithIdentifier("SHOW-EDIT-PROFILE-VC", sender: self)
            }
            else if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.serviceProvider.rawValue {
              performSegueWithIdentifier("SHOW_SERVICE_PROVIDER_PROFILE_VC", sender: self)
            }
        }
        else{
            FTIndicator.showErrorWithMessage("First Sign Up")
        }
        
    }
    
    @IBAction func switch_Notification_click(sender: UISwitch) {
        FTIndicator.showInfoWithMessage("Under Development")
    }
    
    @IBAction func btn_TermsAndConditions_Click(sender: UIButton) {
        FTIndicator.showInfoWithMessage("Under Development")
    }
    @IBAction func btn_PrivacyPolicy_Click(sender: UIButton) {
        FTIndicator.showInfoWithMessage("Under Development")
    }
}
