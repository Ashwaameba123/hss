//
//  SignUpViewController.swift
//  HSS
//
//  Created by Rajni on 05/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import FTIndicator

class SignUpViewController: UIViewController ,CountriesViewControllerDelegate {
    let codeBtn : UIButton = UIButton()
    var univTextField : UITextField!
    public var presentCountry = Country.currentCountry
    //MARK:- OUTLETS
    @IBOutlet weak var btnContinue: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtFieldUserFirstName: UITextField!
    @IBOutlet weak var txtFieldUserLastName: UITextField!
    @IBOutlet weak var txtFieldUserEmail: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var txtFieldUserPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldUserLanguage: UITextField!
    @IBOutlet weak var txtFieldUserGender: UITextField!
    
    @IBOutlet weak var constraintPasswordHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintEmailHeight: NSLayoutConstraint!
   // @IBOutlet weak var constraintEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintEmailTopSpace: NSLayoutConstraint!
    //@IBOutlet weak var constraintPasswordHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintPasswordTopSpace: NSLayoutConstraint!
    @IBOutlet weak var constraintVwBackgroundHeignt: NSLayoutConstraint!
    
    
    
    
    
     //constraints
//    @IBOutlet weak var constraintVwBackgroundHeignt: NSLayoutConstraint!
//    @IBOutlet weak var constraintPasswordTopSpace: NSLayoutConstraint!
//    @IBOutlet weak var constraintEmailTopSpace: UIView!
//    @IBOutlet weak var constraintPasswordHeight: NSLayoutConstraint!
//    @IBOutlet weak var constraintEmailHeight: NSLayoutConstraint!

    //MARK:- VARIABLES
    var pickerView = UIPickerView()
    var arrGender = ["Male", "Female"]
    var arrLanguages = ["English", "French", "Italian"]
    var arrCountryCode = ["11", "22", "33"]
    var pickerType:String = ""
    var loginViaSocialMedia:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE-CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if loginViaSocialMedia == true {
            constraintEmailHeight.constant = 0
            constraintPasswordHeight.constant = 0
            constraintEmailTopSpace.constant = 0
            constraintPasswordTopSpace.constant = 0
            constraintVwBackgroundHeignt.constant = (355 - 100)
            self.fillEnteriesViaSocialMedia()
        }
        else {
            constraintEmailTopSpace.constant = 6
            constraintPasswordTopSpace.constant = 6
            constraintEmailHeight.constant = 44
            constraintPasswordHeight.constant = 44
            constraintVwBackgroundHeignt.constant = 355
        }

        
        UIApplication.sharedApplication().statusBarStyle = .LightContent

        
        //Add PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.clearColor()
        
        //Add PickerView Input
        univTextField = txtFieldUserFirstName;
        txtFieldUserLanguage.inputView = pickerView
        txtFieldUserGender.inputView = pickerView
        
        txtFieldUserEmail.autocorrectionType = .No
        //Add Drop Icon
        self.dropIcon(txtFieldUserGender)
        self.dropIcon(txtFieldUserLanguage)
        
        self.layerFields(txtFieldUserPhoneNumber)
        self.layerFields(txtFieldUserEmail)
        self.layerFields(txtFieldUserGender)
        self.layerFields(txtFieldUserLanguage)
        self.layerFields(txtFieldUserLastName)
        self.layerFields(txtFieldUserFirstName)
        self.layerFields(txtFieldPassword)
        CommonFunctions.makeRoundCornerOfButton(btnContinue)

        
        self.rtIcon(txtFieldUserFirstName, imageName: "user-name")
        self.rtIcon(txtFieldUserLastName, imageName: "user-name")
        self.rtIcon(txtFieldUserLanguage, imageName: "country-language")
        self.rtIcon(txtFieldUserGender, imageName: "gender")
        self.rtIcon(txtFieldUserEmail, imageName: "mail")
        self.rtIcon(txtFieldUserPhoneNumber, imageName: "phone")
        self.rtIcon(txtFieldPassword, imageName: "password")
        
        btnContinue.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        CommonFunctions.makeRoundCornerOfButton(self.btnContinue)
        
        
    }
    
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    
    func fillEnteriesViaSocialMedia(){
        
        txtFieldUserEmail.hidden = true
        txtFieldPassword.hidden = true
        
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME) != nil {
            txtFieldUserFirstName.text! = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_FIRST_NAME)!
        }
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) == Enumerations.REGISTER_VIA.facebook.rawValue {
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_LAST_NAME) != nil {
            txtFieldUserLastName.text! = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_LAST_NAME)!
        }}
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        app.uni_vc = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = true
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func rtIcon(textField : UITextField , imageName : String) {
        if textField == txtFieldUserPhoneNumber{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 90, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            
            codeBtn.frame = CGRectMake(40, 0, 50, 40)
            codeBtn.setTitle("+\(presentCountry.phoneExtension)-", forState: .Normal)
            codeBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            codeBtn.titleLabel!.font =  UIFont.systemFontOfSize(15)
            codeBtn.addTarget(self, action: #selector(SignUpViewController.pressed(_:)), forControlEvents: .TouchUpInside)
            codeBtn.contentHorizontalAlignment = .Left;

            rtPad.addSubview(codeBtn)
            
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
        else{
            let rtPad : UIView = UIView()
            rtPad.frame = CGRectMake(0, 0, 40, 40)
            let imgView : UIImageView = UIImageView()
            imgView.frame = CGRectMake(10, 10, 20, 20)
            imgView.image = UIImage(named: imageName)
            imgView.contentMode = UIViewContentMode.ScaleAspectFit
            rtPad.addSubview(imgView)
            textField.leftView = rtPad
            textField.leftViewMode = .Always
        }
    }
    
    func pressed(sender: UIButton!) {
        let countriesViewController = CountriesViewController.standardController()
        countriesViewController.cancelBarButtonItemHidden = true
        countriesViewController.delegate = self
        countriesViewController.majorCountryLocaleIdentifiers = ["GB", "US", "IT", "DE", "RU", "BR", "IN"]
        
        navigationController?.pushViewController(countriesViewController, animated: true)
    }
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        if tappedImageView.tag == 1 {
            txtFieldUserLanguage.becomeFirstResponder()
        }
        else if tappedImageView.tag == 2 {
            txtFieldUserGender.becomeFirstResponder()
        }
    }

    
    //MARK:- BUTTON ACTIONS
    @IBAction func btnContinueClick(sender: AnyObject) {
        if Validate() == true {
            //to save user data in global array
            let user = Registration()
            Constants.userData.arrUserData = []
            user.firstName = txtFieldUserFirstName.text!
            user.lastName = txtFieldUserLastName.text!
        if !loginViaSocialMedia {
            user.email = txtFieldUserEmail.text!
        }
        else{
            user.email = ""
        }
            user.password = txtFieldPassword.text!
            user.language = txtFieldUserLanguage.text!
            user.gender = txtFieldUserGender.text!
            user.countryCode = codeBtn.titleLabel!.text
            user.mobileNumber = txtFieldUserPhoneNumber.text!
            Constants.userData.arrUserData.append(user)
           performSegueWithIdentifier("SHOW_SIGN_UP_SUBMIT_VIEW_CONTROLLER", sender: nil)
        }
    }
    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        // to check validation of first name
        if txtFieldUserFirstName.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldUserFirstName.attributedPlaceholder = NSAttributedString(string: "Please enter first name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of last name
        if txtFieldUserLastName.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldUserLastName.attributedPlaceholder = NSAttributedString(string: "Please enter last name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        
        
        if !loginViaSocialMedia {
        //to check validation of Email
        if txtFieldUserEmail.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldUserEmail.attributedPlaceholder = NSAttributedString(string: "Please enter email id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }else{
            valid = CommonFunctions.isEmailValid(txtFieldUserEmail.text!)
            if !valid{
                txtFieldUserEmail.text=""
                self.AnimationShakeTextField(txtFieldUserEmail)
                txtFieldUserEmail.attributedPlaceholder = NSAttributedString(string: "Please enter valid email id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            
            }
        
            //to check validation of password
            if txtFieldPassword.text!.utf8.count==0{
                // Change the placeholder color to red for textfield passWord
                txtFieldPassword.attributedPlaceholder = NSAttributedString(string: "Please enter password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                valid = false
            }else{
                let newLength = txtFieldPassword.text!.utf16.count
                if newLength < 6{
                    valid = false
                    txtFieldPassword.text=""
                    self.AnimationShakeTextField(self.txtFieldPassword)
                    txtFieldPassword.attributedPlaceholder = NSAttributedString(string: "Please enter password minimum of 6 characters", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                    
                }
            }
        }
        
        
        // to check validation of language
        if txtFieldUserLanguage.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldUserLanguage.attributedPlaceholder = NSAttributedString(string: "Please select language", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        // to check validation of Gender
        if txtFieldUserGender.text == ""{
            //Change the placeholder color to red for textfield email if
            txtFieldUserGender.attributedPlaceholder = NSAttributedString(string: "Please select gender", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        //to check validation of phone number
        if txtFieldUserPhoneNumber.text!.utf16.count==0{
            // Change the placeholder color to red for textfield passWord
            txtFieldUserPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Please enter mobile no.", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }else{
            let newLength = txtFieldUserPhoneNumber.text!.utf16.count
            if newLength > 15{
                valid = false
                txtFieldUserPhoneNumber.text=""
                self.AnimationShakeTextField(self.txtFieldUserPhoneNumber)
                txtFieldUserPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Please enter valid mobile no. ", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                
            }
        }
        return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }
    
    

    
    internal func countriesViewControllerDidCancel(countriesViewController: CountriesViewController) { }
    
    internal func countriesViewController(countriesViewController: CountriesViewController, didSelectCountry country: Country) {
        navigationController?.popViewControllerAnimated(true)
    
        print(country.phoneExtension)
        codeBtn.setTitle("+\(country.phoneExtension)-", forState: .Normal)
    }
    
    //MARK:- NAVIGATION METHODS
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SHOW_SIGN_UP_SUBMIT_VIEW_CONTROLLER"{
            let vc = segue.destinationViewController as! SignUpSubmitViewController
            vc.isLoginViaSocialMedia = loginViaSocialMedia
        }
    }
    

    
}

//MARK:- Textfield Delegates/Datasource
extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        univTextField = textField
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
        if textField == txtFieldUserPhoneNumber{
            textField.keyboardType = UIKeyboardType.NumberPad
            
        }
        else if (textField == txtFieldUserGender) || (textField == txtFieldUserLanguage) {
            
            if textField == txtFieldUserGender{
                textField.text = arrGender[0]
            }
            else{
                textField.text = arrLanguages[0]
            }
        }
        
        
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if textField.text?.characters.count > 100 {
            return false
        }
        
        if textField == txtFieldUserPhoneNumber{
            
            if string.rangeOfCharacterFromSet(characterset.invertedSet) != nil {
                print("string contains special characters")
                return false
            }
            else {
                if string == ""{
                    return true
                }
                if textField.text?.characters.count >= 15 {
                    return false
                }
            }
            
        }
 
        return true
    }
    
}

//MARK:- PickerView Delegates/Datasource
extension SignUpViewController: UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if univTextField == txtFieldUserGender{
            return arrGender.count
        }
        else{
            return arrLanguages.count
        }
    }
}

extension SignUpViewController: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if univTextField == txtFieldUserGender {
            return arrGender[row]
        }
        else{
            return arrLanguages[row]
        }
        
        
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if univTextField == txtFieldUserGender {
            txtFieldUserGender.text! = arrGender[row]
        }
        else{
            txtFieldUserLanguage.text! = arrLanguages[row]
        }
        //self.view.endEditing(true)
    }
}

