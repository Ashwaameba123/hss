//
//  SignUpSubmitViewController.swift
//  HSS
//
//  Created by Rajni on 05/12/16.
//  Copyright © 2016 Rajni. All rights reserved.
//

import Foundation
import LocationPicker
import CoreLocation
import FTIndicator
import JTSImageViewController

class SignUpSubmitViewController:UIViewController , UIPickerViewDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate ,UITextFieldDelegate, MPGTextFieldDelegate {
    var coutriesList : NSArray = []
    //MARK:- OUTLETS
    var userCoordinates : CLLocationCoordinate2D!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtRelationship: UITextField!
    @IBOutlet weak var txtFieldNationality: MPGTextField!
    @IBOutlet weak var txtFieldLocation: UITextField!
    @IBOutlet weak var btnAddIDProof: UIButton!
    @IBOutlet weak var previewImage: UIImageView!
    //UIVIEW
        @IBOutlet weak var dummyNationalityView: UIView!

    @IBOutlet weak var btnSubmit: UIButton!

    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- VARIABLES
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var isLoginViaSocialMedia:Bool!
    var selectedIdentityImg = UIImage()
    var pickerView = UIPickerView()
    let imagePicker = UIImagePickerController()
    var pickerType: String = ""
    var arrLocation = ["AA", "BB", "CC", "DD"]
    var relationshipArray = ["Married","Single"]
      public var presentCountry = Country.currentCountry
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        
         userCoordinates = CLLocationCoordinate2DMake(0.00000, 0.00000)
        
        
        super.viewDidLoad()
        initView()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        CommonFunctions.makeRoundCornerOfView(dummyNationalityView)
        self.view.endEditing(true)
    }
    
    
    func dropIcon(textField : UITextField) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 30, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(5, 12, 15, 15)
        imgView.image = UIImage(named: "drop-down-green")
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.tag = textField.tag
        rtPad.addSubview(imgView)
        textField.rightView = rtPad
        textField.rightViewMode = .Always
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tappedMe))
        imgView.addGestureRecognizer(tap)
        imgView.userInteractionEnabled = true
    }
    
    func dataForPopoverInTextField(textField: MPGTextField!) -> [AnyObject]! {
        return coutriesList as [AnyObject];
    }
    func tappedMe(gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("Tapped on Image")
        
        if tappedImageView.tag == 3 {
            txtRelationship.becomeFirstResponder()
        }
        else if tappedImageView.tag == 4 {
            txtFieldNationality.becomeFirstResponder()
        }
    }
    
    //MARK:- CUSTOM METHODS
    func initView(){
        self.fectCoutries()
        //Add Drop Icon
        self.dropIcon(txtRelationship)
        self.dropIcon(txtFieldNationality)
        
        btnAddIDProof.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        btnAddIDProof.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        btnSubmit.backgroundColor = Constants.Colors.GREEN_COLOUR
        self.view.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        btnAddIDProof.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left

        CommonFunctions.makeRoundCornerOfButton(btnSubmit)
        CommonFunctions.makeRoundCornerOfButton(btnAddIDProof)
        
        //picker view
        pickerView.delegate = self
        
        self.txtFieldNationality.delegate = self
        
        self.txtFieldNationality.backgroundColor = UIColor.whiteColor()
        self.txtFieldNationality.textColor = UIColor.blackColor()
        let Locale = NSLocale(localeIdentifier: "en_US")
        let country = Locale.displayNameForKey(NSLocaleCountryCode, value: presentCountry.countryCode)!
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") != nil{
            let locationDictionary : NSDictionary = NSUserDefaults.standardUserDefaults().objectForKey("CurrentLocation") as! NSDictionary
            
            let locationName : String = locationDictionary["locationName"] as! String
            let latitude: String = locationDictionary["latitude"] as! String
            let longitude: String = locationDictionary["longitude"] as! String
            userCoordinates = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            txtFieldLocation.text = locationName
            
        }
        
        
        
        print("MY Country : \(country)")
        
        self.txtFieldNationality.text = country
        
        
        txtRelationship.inputView = pickerView
        

        
        
        self.layerFields(txtDOB)
        self.layerFields(txtRelationship)
        self.layerFields(txtFieldLocation)
        self.layerFields(txtFieldNationality)
        
        self.rtIcon(txtDOB, imageName: "calender")
        self.rtIcon(txtRelationship, imageName: "relationship")
        self.rtIcon(txtFieldNationality, imageName: "country-language")
        self.rtIcon(txtFieldLocation, imageName: "location-dark")
        
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(SignUpSubmitViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian);
        let todayDate = NSDate()
        let components = calendar?.components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day], fromDate: todayDate)
        let minimumYear = (components?.year)! - 1917
        let minimumMonth = (components?.month)! - 1
        let minimumDay = (components?.day)! - 1
        let comps = NSDateComponents();
        comps.year = -minimumYear
        comps.month = -minimumMonth
        comps.day = -minimumDay
        let minDate = calendar?.dateByAddingComponents(comps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        let maxYear = (components?.year)! - 2017
        let maxMonth = (components?.month)! - 1
        let maxDay = (components?.day)! - 1
        let maxcomps = NSDateComponents();
        maxcomps.year = -maxYear
        maxcomps.month = -maxMonth
        maxcomps.day = -maxDay
        let maxDate = calendar?.dateByAddingComponents(maxcomps, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
       
        self.txtDOB.inputView = datePicker
        
        
        
        previewImage.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpSubmitViewController.imageTapped(_:)))
        previewImage.addGestureRecognizer(tapRecognizer)
        previewImage.contentMode = .ScaleAspectFit
        
    }
    
    
    func imageTapped(img: AnyObject)
    {
        if let selectedImg:UIImage = selectedIdentityImg{
            let imageInfo = JTSImageInfo()
            imageInfo.image = selectedImg
            imageInfo.referenceRect = self.btnAddIDProof.frame
            imageInfo.referenceView = self.btnAddIDProof.superview
            imageInfo.title = "ID"
            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
        }
    }
    
    func layerFields(textField : UITextField){
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
    }
    func rtIcon(textField : UITextField , imageName : String) {
        let rtPad : UIView = UIView()
        rtPad.frame = CGRectMake(0, 0, 40, 40)
        let imgView : UIImageView = UIImageView()
        imgView.frame = CGRectMake(10, 10, 20, 20)
        imgView.image = UIImage(named: imageName)
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        rtPad.addSubview(imgView)
        textField.leftView = rtPad
        textField.leftViewMode = .Always
    }
    
    override func viewWillAppear(animated: Bool) {
        txtFieldLocation.resignFirstResponder()
        app.uni_vc = self
        self.view.endEditing(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        self.navigationController?.navigationBar.translucent = true
    }
    
//    func showRelationshipSelectionList() {
//        let sheet = UIAlertController(title: "", message: "Please Select your Relation", preferredStyle: .ActionSheet)
//        
//        let married = UIAlertAction(title: "Married", style: .Default) { (action) in
//            self.btnRelationship.titleLabel!.text = "Married"
//        }
//        let unmarried = UIAlertAction(title: "Unmarried", style: .Default) { (action) in
//            self.btnRelationship.titleLabel!.text = "Unmarried"
//        }
//        let cancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
//            
//        }
//        
//        sheet.addAction(married)
//        sheet.addAction(unmarried)
//        sheet.addAction(cancel)
//        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone {
//            presentViewController(sheet, animated: true, completion: nil)
//        }
//        else {
//            sheet.popoverPresentationController!.sourceView = self.view
//            sheet.popoverPresentationController!.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0)
//            self.presentViewController(sheet, animated: true, completion: nil)
//        }
//        
//    }
    func textField(textField: MPGTextField!, didEndEditingWithSelection result: [NSObject : AnyObject]!) {
        print(result)
        let dic : NSDictionary = result as NSDictionary
        
        if let country : MyCountry = dic.objectForKey("Object") as? MyCountry{
            self.txtFieldNationality.text = country.countryName
        }
        else{
            self.txtFieldNationality.text = ""
        }
    }
    
    //MARK:- BUTTON ACTIONS

    @IBAction func btnSubmitClick(sender: AnyObject) {
        if Validate() == true {
            if Constants.userData.arrUserData.count == 1{
                
                let lat :String = String(format:"%f", userCoordinates.latitude)
                let lng :String = String(format:"%f", userCoordinates.longitude)
                
                
                Constants.userData.arrUserData[0].contactName = ""
                
               
                Constants.userData.arrUserData[0].relationshipStatus = txtRelationship.text!
                Constants.userData.arrUserData[0].DOB = txtDOB.text!
                Constants.userData.arrUserData[0].nationality = txtFieldNationality.text!
                Constants.userData.arrUserData[0].locationName = txtFieldLocation.text
                Constants.userData.arrUserData[0].locationLatitude = lat
                Constants.userData.arrUserData[0].locationLongitude = lng
                Constants.userData.arrUserData[0].userType = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE)
                if let token = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.DEVICE_TOKEN) {
                    Constants.userData.arrUserData[0].applicationId = token
                }
                else {
                    Constants.userData.arrUserData[0].applicationId = "123456"
                }
                Constants.userData.arrUserData[0].deviceType = "Ios"
                Constants.userData.arrUserData[0].registerVia = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA)
                let imageData = UIImagePNGRepresentation(selectedIdentityImg) as NSData?
                Constants.userData.arrUserData[0].userProfileImage = "\(imageData)"
                
            }
            
            callToPostUserRegistrationData()
            
        }
    }
    
    

    
    
    //MARK:- VALIDATION METHODS
    func Validate() -> Bool{
        var valid:Bool = true
        
        // to check validation of Relationship
        if txtRelationship.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtRelationship.attributedPlaceholder = NSAttributedString(string: "Please select relationship", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of Date of Birth
        if txtDOB.text!.utf16.count==0 {
            //Change the placeholder color to red for textfield email if
            txtDOB.attributedPlaceholder = NSAttributedString(string: "Please select DOB", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        // to check validation of Nationality
        if txtFieldNationality.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldNationality.attributedPlaceholder = NSAttributedString(string: "Please select nationality", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
         //to check validation of Location
        if txtFieldLocation.text!.utf16.count==0{
            //Change the placeholder color to red for textfield email if
            txtFieldLocation.attributedPlaceholder = NSAttributedString(string: "Please select your location", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            valid = false
        }
        
        
        
        //to check validation of id proof
        if previewImage.image == nil {
          btnAddIDProof.setTitle("Please add profile photo", forState: .Normal)
          btnAddIDProof.setTitleColor(UIColor.redColor(), forState: .Normal)
            valid = false
        }
        
        
       
        return valid
    }
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(textField.center.x - 5, textField.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(textField.center.x + 5, textField.center.y))
        textField.layer.addAnimation(animation, forKey: "position")
    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(textField: UITextField) {
        print("called")
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
        if textField == txtFieldNationality{
            pickerType = "nationality"
        }
        else if textField == txtRelationship{
            textField.text = relationshipArray[0]
            pickerType = "relation"
        }
        else if textField == txtFieldLocation{
            
            openLocationPicker()
        }
        
    }
    
    func openLocationPicker(){

        let locationPicker = LocationPickerViewController()
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        locationPicker.currentLocationButtonBackground = Constants.Colors.BACKGROUND_COLOUR
        locationPicker.showCurrentLocationInitially = true // default: true
        locationPicker.mapType = .Standard // default: .Hybrid
            
            // for searching, see `MKLocalSearchRequest`'s `region` property
            locationPicker.useCurrentLocationAsHint = true // default: false
            locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
            
            locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
            locationPicker.resultRegionDistance = 500 // default: 600
            
            locationPicker.completion = { location in
                
                let locationName : String = (location?.address)!
                self.userCoordinates  = (location?.coordinate)!
                
                let countryName : String = (location?.placemark.country)!
                self.txtFieldNationality.text! = countryName
                
                
                self.txtFieldLocation.text = locationName
                
            }
        
            navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if textField == txtFieldNationality{
            txtFieldLocation.text = ""
        }
        textField.resignFirstResponder();
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.characters.count > 100 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    //MARK:- PICKER VIEW DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerType == "nationality"{
            return coutriesList.count
        }
        else if pickerType == "relation"{
            return relationshipArray.count
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerType == "nationality"{
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            return countryDetail.countryName
        }
        else if pickerType == "relation"{
            return relationshipArray[row]
        }
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerType == "nationality"{
            if coutriesList.count > 0 {
            let countryDetail : MyCountry = coutriesList[row] as! MyCountry
            txtFieldNationality.text = countryDetail.countryName
        }
        }
        else if pickerType == "relation"{
            txtRelationship.text = relationshipArray[row]
        }
    }
    
    //MARK:- HELPERS
    @IBAction func AddIDProofButtonClick(sender: AnyObject) {
        let  value:String = NSLocalizedString("select your ID Proof", comment: "")
        let sheet = UIAlertController(title: "", message:value , preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: NSLocalizedString("Capture from Camera", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(0)
        }
        let galleryAction = UIAlertAction(title: NSLocalizedString("Select from Gallery", comment: ""), style: .Default) { (alert) -> Void in
            self.openCamorGallary(1)
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (alert) -> Void in
            print("Cancel")
        }
        sheet.addAction(cameraAction)
        sheet.addAction(galleryAction)
        sheet.addAction(cancelAction)
        

        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            if let popoverController = sheet.popoverPresentationController {
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = sender.bounds
            }
            self.presentViewController(sheet, animated: true, completion: nil)
        }
        else{
            presentViewController(sheet, animated: true, completion: nil)
        }
        
        
    }
    
    //MARK:- WEB SERVICE USER REGISTRATION
    func callToPostUserRegistrationData(){
        var email:String = ""
        var socialMediaId:String = ""
        var registerVia:String = ""
        var imgData = NSData()
        
        let user = Constants.userData.arrUserData[0]
        //start loader    
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
       
        if isLoginViaSocialMedia == true {
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_ID) != nil{
                socialMediaId = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_ID)!
            }
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL) != nil {
               // email = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.SOCIAL_MEDIA_EMAIL)!
            }
            if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA) != nil {
                registerVia = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.REGISTER_VIA)!
            }
        }
        else {
            socialMediaId = ""
            email = user.email!
            registerVia = Enumerations.REGISTER_VIA.manual.rawValue
        }
        
        
        let params = [
            URLParams.FIRST_NAME: user.firstName!,
            URLParams.LAST_NAME: user.lastName!,
            URLParams.PASSWORD: user.password!,
            URLParams.EMAIL: email,
            URLParams.LANGUAGE: user.language!,
            URLParams.GENDER: user.gender!,
            URLParams.MOBILE_NUMBER: user.mobileNumber!,
            URLParams.CONTACT_NAME: user.contactName!,
            URLParams.RELATIONSHIP_STATUS: user.relationshipStatus!,
            URLParams.DOB: user.DOB!,
            URLParams.NATIONALITY: user.nationality!,
            URLParams.LOCATION_NAME: user.locationName!,
            URLParams.LOCATION_LATITUDE: user.locationLatitude!,
            URLParams.LOCATION_LONGITUDE: user.locationLongitude!,
            URLParams.COUNTRY_CODE: user.countryCode!,
            URLParams.USER_TYPE: user.userType!,
            URLParams.REGISTER_VIA: registerVia,
            URLParams.SOCIAL_MEDIA_ID: socialMediaId,
            URLParams.DEVICE_TYPE: user.deviceType!,
            URLParams.APPLICATION_ID: user.applicationId!,
            //URLParams.PHOTO_IDENTITY: imgData
            //URLParams.SOCIAL_MEDIA_ID: "11"
        ]
        
        
        print(params)
        
        WebserviceUtils.callPostRequestForUserRegistration(URLConstants.USER_REGISTRATION ,image: selectedIdentityImg , params: params , imageParam: "userProfileImage" , success: { (response) in
            
            FTIndicator.dismissProgress()
            if let json = response as? NSDictionary {
                
                
                if let message = json.objectForKey("message"){
                     FTIndicator.showToastMessage(message as! String)
                }
        
                if let status = json.objectForKey("status"){
                    if status as! String == "error"{
                        return
                    }
                    else {
                        
                        if let dic = json.objectForKey("user"){
                            
                        if let userId = dic.objectForKey("id"){
                            Constants.userData.arrUserData[0].userId = userId as! String
                        }
                        if let authenticateToken = dic.objectForKey("authenticateToken"){
                            Constants.userData.arrUserData[0].authenticateToken = authenticateToken as! String
                        }
                        
                        //save user data in DB
                        DatabaseBLFunctions.saveUserRegistrationDataSIGNUP(Constants.userData.arrUserData[0])
                        
                        //move to verification screen
                        self.toMoveToNextScreenAccordingToUserRegistrationType()
                        }}
                }
                
                
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
                FTIndicator.dismissProgress()
                FTIndicator.showToastMessage(error.localizedDescription)
        })
        
        
    }
    
    func toMoveToNextScreenAccordingToUserRegistrationType(){
        if !isLoginViaSocialMedia{
        performSegueWithIdentifier("SHOW_VERIFICATION_VC", sender: self)
        }
        else {
            FTIndicator.showSuccessWithMessage("User Created Successfully")
            toMakeRevealVwInitial()
       // let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HomeViewContoller") as! HomeViewContoller
       // self.presentViewController(viewController, animated: true, completion: nil)
        }
    }
    
    func toMakeRevealVwInitial(){
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isLogin")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        appDelegate.window?.rootViewController = yourVC
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageView.contentMode = .ScaleAspectFit
            //imageView.image = pickedImage
            selectedIdentityImg = pickedImage
            previewImage.image = pickedImage
            btnAddIDProof.setTitleColor(UIColor.blackColor(), forState: .Normal)
            btnAddIDProof.setTitle("Profile Photo Attached", forState: .Normal)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    

    
    func openCamorGallary(type : Int){

        
        if type == 0 {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        presentViewController(imagePicker, animated: true, completion: imagePickerHandler)
    }
    
    func imagePickerHandler(){
        imagePicker.navigationBar.translucent = false
        imagePicker.navigationBar.barTintColor = Constants.Colors.BACKGROUND_COLOUR
    }

    
//    func showDateAndTimePicker() {
//            let pickerMode: UIDatePickerMode = .Date
//            title = NSLocalizedString("Select Date", comment: "")
//        
//        
//        ActionSheetDatePicker.showPickerWithTitle(title,
//                                                  datePickerMode: pickerMode,
//                                                  selectedDate: NSDate(),
//                                                  doneBlock: { (sheet, selectedDate, object) -> Void in
//                                                    if let date = selectedDate as? NSDate {
//                                                        self.formattedDOBShowInTextField(date)
//                                                        //self.showDateInField(date, textField: textfield)
//                                                    }
//            },
//                                                  cancelBlock: { (sheet) -> Void in
//                                                    
//            },
//                                                  origin: self.view)
//    }
    
    
    
    func datePickerValueChanged(sender: UIDatePicker){
        
        let formattor = NSDateFormatter()
        formattor.dateFormat = "yyyy-MM-dd"
        txtDOB.text! = formattor.stringFromDate(sender.date)

            print("Selected value \(txtDOB.text!)")
        
    }
    
    
    func fectCoutries(){
        if let art : NSArray = DatabaseBLFunctions.fetchCountries() {
            if art.count>0{
                let mutArt : NSMutableArray = NSMutableArray(array: art)
                mutArt.removeObjectAtIndex(0)
                coutriesList = mutArt
            }
        }
    }
    
    
    
}





//
//    func formattedDOBShowInTextField(nsDate: NSDate){
//        
//        let formattor = NSDateFormatter()
//        formattor.dateFormat = "MM/dd/YYYY"
//        btnDOB.titleLabel!.text = formattor.stringFromDate(nsDate)
//    }







