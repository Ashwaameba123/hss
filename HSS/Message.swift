//
//  Message.swift
//  HSS
//
//  Created by Rajni on 22/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
class Message : NSObject {
    
    var applicationId: String!
    var authenticateToken: String!
    var firstName: String!
    var id: String!
    var lastName: String!
    var recentMessage: String!
    var userProfileImage: String!
    var userType: String!
    
}