//
//  History_view_controller.swift
//  HSS
//
//  Created by Rajni on 14/03/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import JTSImageViewController
import FTIndicator


class History_view_controller: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    //MARK:- Outlets
    @IBOutlet weak var barBtn_side_menu: UIBarButtonItem!
    @IBOutlet weak var table_view: UITableView!
    
    //MARK:- Variables
    var arr_property_booking = [Property_Booking]()
    var pageNumber: Int = 0
    var isMoreDataLeft:Bool = false
    let app :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            barBtn_side_menu.target = self.revealViewController()
            barBtn_side_menu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.title = "History"
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        pageNumber = 0
        arr_property_booking = []
        self.callToGetMyBookings()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         app.uni_vc = self
    }
    
    
    //MARK:- WEB SERVICES
    func callToGetMyBookings(){
        var id_param: String = "userId"
        var authenticateToken: String = ""
        var id: String = ""
        let page_no:String = String(pageNumber)
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            id_param = "hostId"
        }
        
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            let arr = DatabaseBLFunctions.fetchUserRegistrationData()
            if let ID = arr[0].userId {
                id = ID
            }
            if let token = arr[0].authenticateToken {
                authenticateToken = token
            }
            
        }
        
        let params = [
            id_param: id ,
            URLParams.AUTHENTICATE_TOKEN: authenticateToken ,
            URLParams.PAGE_NUMBER: page_no
        ]
        
        print(params)
        FTIndicator.showProgressWithmessage("Loading..", userInteractionEnable: false)
        
        WebserviceUtils.callPostRequest(URLConstants.TO_GET_ALL_BOOKINGS, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                FTIndicator.dismissProgress()
                
                if let status = json.objectForKey("status") as? String {
                    
                    if status.containsString("success") == true {
                        
                        if let arr = json.objectForKey("bookings") as? NSArray {
                            
                            if arr.count > 0 {
                                self.toParsePropertyBookings(arr)
                            }
                            else {
                                self.table_view.reloadData()
                            }
                            
                            if arr.count < 10 {
                                self.isMoreDataLeft = false
                            }
                            else {
                                self.isMoreDataLeft = true
                            }
                        }}
                    else if status.containsString("error") {
                        
                        if let msg = json.objectForKey("message") as? String {
                            FTIndicator.showInfoWithMessage(msg)
                        }
                        self.table_view.reloadData()
                    }
                    
                    
                }}
            }, failure: { (error) in
                FTIndicator.dismissProgress()
                print(error.localizedDescription)
        })
        
        
    }
    
    
    func toParsePropertyBookings(arr: NSArray) {
        
        print(arr.count)
        
        for item in arr {
            
            let obj = Property_Booking()
            
            if let availableRoom = item.objectForKey("availableRoom") {
                let str = String(availableRoom)
                obj.setValue(str, forKey: "availableRoom")
            }
            if let bookedRoom = item.objectForKey("bookedRoom") {
                let str = String(bookedRoom)
                obj.setValue(str, forKey: "bookedRoom")
            }
            if let bookingEndDate = item.objectForKey("bookingEndDate") {
                let str = String(bookingEndDate)
                obj.setValue(str, forKey: "bookingEndDate")
            }
            if let bookingStartDate = item.objectForKey("bookingStartDate") {
                let str = String(bookingStartDate)
                obj.setValue(str, forKey: "bookingStartDate")
            }
            if let bookingType = item.objectForKey("bookingType") {
                let str = String(bookingType)
                obj.setValue(str, forKey: "bookingType")
            }
            if let createdAt = item.objectForKey("createdAt") {
                let str = String(createdAt)
                obj.setValue(str, forKey: "createdAt")
            }
            if let hostId = item.objectForKey("hostId") {
                let str = String(hostId)
                obj.setValue(str, forKey: "hostId")
            }
            if let id = item.objectForKey("id") {
                let str = String(id)
                obj.setValue(str, forKey: "id")
            }
            if let isApproved = item.objectForKey("isApproved") {
                let str = String(isApproved)
                if str == "0" {
                    obj.setValue("false", forKey: "isApproved")
                }
                else {
                    obj.setValue("true", forKey: "isApproved")
                }
            }
            if let isCancel = item.objectForKey("isCancel") {
                let str = String(isCancel)
                if str == "0" {
                    obj.setValue("false", forKey: "isCancel")
                }
                else {
                    obj.setValue("true", forKey: "isCancel")
                }
            }
            if let paymentWith = item.objectForKey("paymentWith") {
                let str = String(paymentWith)
                obj.setValue(str, forKey: "paymentWith")
            }
            if let profileName = item.objectForKey("profileName") {
                let str = String(profileName)
                obj.setValue(str, forKey: "profileName")
            }
            if let propertyId = item.objectForKey("propertyId") {
                let str = String(propertyId)
                obj.setValue(str, forKey: "propertyId")
            }
            if let propertyImage = item.objectForKey("propertyImage") {
                let str = String(propertyImage)
                obj.setValue(str, forKey: "propertyImage")
            }
            if let streetAddress = item.objectForKey("streetAddress") {
                let str = String(streetAddress)
                obj.setValue(str, forKey: "streetAddress")
            }
            if let totalRoom = item.objectForKey("totalRoom") {
                let str = String(totalRoom)
                obj.setValue(str, forKey: "totalRoom")
            }
            if let updatedAt = item.objectForKey("updatedAt") {
                let str = String(updatedAt)
                obj.setValue(str, forKey: "updatedAt")
            }
            if let userId = item.objectForKey("userId") {
                let str = String(userId)
                obj.setValue(str, forKey: "userId")
            }
            if let userName = item.objectForKey("userName") {
                let str = String(userName)
                obj.setValue(str, forKey: "userName")
            }
            if let userProfileImage = item.objectForKey("userProfileImage") {
                let str = String(userProfileImage)
                obj.setValue(str, forKey: "userProfileImage")
            }
            if let userType = item.objectForKey("userType") {
                let str = String(userType)
                obj.setValue(str, forKey: "userType")
            }
            if let currency = item.objectForKey("currency") {
                let str = String(currency)
                obj.setValue(str, forKey: "currency")
            }
            if let setPrice = item.objectForKey("setPrice") {
                let str = String(setPrice)
                obj.setValue(str, forKey: "setPrice")
            }
            
            arr_property_booking.append(obj)
            if pageNumber == 0{
                self.table_view.reloadData()
            }
            else {
                
                self.table_view.beginUpdates()
                self.table_view.insertRowsAtIndexPaths([
                    NSIndexPath(forRow: arr_property_booking.count-1, inSection: 0)
                    ], withRowAnimation: .Automatic)
                self.table_view.endUpdates()
            }
        }
    }
    
    //MARK:- TABLE VIEW DATA SOURCE AND DELEGATES
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_property_booking.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! History_table_cell
        let booking = arr_property_booking[indexPath.row]
        
        //make round corner of view
        CommonFunctions.makeRoundCornerOfView(cell.vw_background)
        
        //make round person image view
        cell.imgVw_Property.layer.cornerRadius = cell.imgVw_Property.frame.size.width / 2
        cell.imgVw_Property.clipsToBounds = true
        cell.imgVw_Property.layer.borderWidth = 1.0
        cell.imgVw_Property.layer.borderColor = Constants.Colors.GREEN_COLOUR.CGColor
        cell.imgVw_Property.backgroundColor = Constants.Colors.BACKGROUND_COLOUR
        cell.imgVw_Property.contentMode = .ScaleAspectFit
        cell.imgVw_Property.tag = indexPath.row
        cell.imgVw_Property.userInteractionEnabled = true
        //let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MessagesViewController.imageTapped(_:)))
        //cell.imgVw_Property.addGestureRecognizer(tapRecognizer)
        
        
        //to set property image
        //to set profile image
        let imageName = booking.propertyImage
        if imageName != "" {
            let urlString:String = "\(URLConstants.GET_PROPERTY_IMAGE)\(imageName)"
            let escapedAddress = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            
            cell.imgVw_Property.sd_setImageWithURL(NSURL(string: escapedAddress!), placeholderImage:UIImage(named: "avatar.png"))
        }
        else {
            cell.imgVw_Property.image = UIImage(named: "avatar.png")
        }
        
        //to set property name
        cell.lbl_property_name.text = booking.profileName
        
        
        //to set user name
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue {
            cell.lbl_userName.text = "Hosted By :" + " " + booking.userName
        }
        else {
            cell.lbl_userName.text = "Booked By :" + " " + booking.userName
        }
        
        //to set date and time
        cell.lbl_date_time.text = CommonFunctions.dateAndTimeFromString(booking.updatedAt)
        
        
        return cell
    }
    
    
    func imageTapped(sender: UITapGestureRecognizer)
    {
        //        let point: UIView = sender.view!
        //        let tag = point.tag
        //        //let tag = img.tag
        //        let indexPath = NSIndexPath(forRow: tag, inSection: 0)
        //        let cell = table_view.cellForRowAtIndexPath(indexPath) as! Messages_List_Table_Cell!
        //        if let selectedImg:UIImage = cell.img_vw_person.image {
        //            let imageInfo = JTSImageInfo()
        //            imageInfo.image = selectedImg
        //            imageInfo.referenceRect = cell.img_vw_person.frame
        //            imageInfo.referenceView = cell.img_vw_person.superview
        //            imageInfo.title = "ID"
        //            let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .Image , backgroundStyle: .Blurred)
        //            imageViewer.showFromViewController(self, transition: .FromOriginalPosition)
        //        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let show_Detail_obj = self.storyboard?.instantiateViewControllerWithIdentifier("History_Detail_VC") as! History_Detail_VC
        show_Detail_obj.booking = arr_property_booking[indexPath.row]
        self.navigationController?.pushViewController(show_Detail_obj, animated: true)
    }
    
    
    //MARK:- SCROLL VIEW DELEGATES
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            print("reach bottom")
            
            if isMoreDataLeft{
                self.loadMoreHistory()
                isMoreDataLeft = false
            }
        }
    }
    
    func loadMoreHistory(){
        pageNumber += 1
        self.callToGetMyBookings()
    }
    
    
    
}