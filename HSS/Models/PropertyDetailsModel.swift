//
//  PropertyDetailsModel.swift
//  HSS
//
//  Created by Rajni on 25/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class PropertyDetailsModel: NSObject {
     var accommodation: String!
     var additionalMenties: String!
     var apartment: String!
     var bathRoom: String!
     var bedRoom: String!
     var beds: String!
     var city: String!
     var commonAmenties: String!
     var country: String!
     var currency: String!
     var emergencyExit: String!
     var fireAlarm: String!
     var fireExtinguisher: String!
     var gasShutoffValve: String!
     var homeSafety: String!
     var id: String!
     var images: String!
     var profileName: String!
     var propertyLatitude: String!
     var propertyLongitude: String!
     var propertyType: String!
     var roomType: String!
     var setPrice: String!
     var specialFeature: String!
     var state: String!
     var streetAddress: String!
     var summary: String!
     var typeOfBooking: String!
     var userId: String!
     var zipCode: String!
    
    var hostName: String!
    var hostProfile: String!
    var cancellationPolicy: String!
    var furnishing: String!
    var totalPropertyReview: String!
    var totalReviewCount: String!
    
    
    var arrImages = [PropertyImage]()
    
}