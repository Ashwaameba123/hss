//
//  Contact.swift
//  HSS
//
//  Created by Rajni on 24/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class Contact: NSObject {
    
    var name: String!
    var phoneNumber: String!
    var relationShip: String!
    var countryCode: String!
    var userContactId: String!
}
