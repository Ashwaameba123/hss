//
//  PropertyReview.swift
//  HSS
//
//  Created by Ameba on 07/11/1938 Saka.
//  Copyright © 1938 Saka Rajni. All rights reserved.
//

import Foundation
import UIKit

class PropertyReview : NSObject {
    
    var id: String!
    var propertyId: String!
    var ratings: String!
    var reviewMessage: String!
    var userId: String!
    var userName: String!
    var userProfileImage: String!
    var time: String!
    
}