//
//  Registration.swift
//  HSS
//
//  Created by Rajni on 03/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class Registration: NSObject {
    var userId: String!
    
    var firstName: String!
    var lastName: String!
    var email: String!
    var password: String!
    var language: String!
    var gender: String!
    var mobileNumber: String!
    var contactName: String!
    var relationshipStatus: String!
    var DOB: String!
    var nationality: String!
    var locationName: String!
    var locationLatitude: String!
    var locationLongitude: String!
    var photoIdentity: String!
    var userProfileImage: String!
    var countryCode: String!
    var userType: String!
    var registerVia: String!
    var socialMediaId: String!
    
    var deviceType: String!
    var applicationId: String!
    var authenticateToken: String!
}