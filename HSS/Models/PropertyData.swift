//
//  PropertyData.swift
//  HSS
//
//  Created by Rajni on 18/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class PropertyData {
    
    var authenticateToken: String!
    var userId: String!
    
    var propertyType: String!
    var roomType: String!
    var accommodationType: String!
    var bedRoomCount: String!
    var bedsCount: String!
    var bathRoomCount: String!
    var profileName: String!
    var summary: String!
    
    var countryName: String!
    var latitude: String!
    var longitude: String!
    var streetAdd: String!
    var AptOrSuite: String!
    var city: String!
    var state: String!
    var zipCode: String!
    var price: String!
    var bookingType: String!
    var currencyType: String!
    
    var commonAmenties: String!
    var addAmenties: String!
    var spclFeature: String!
    var homeSafety: String!
    var fireExtinguisher: String!
    var fireAlarm: String!
    var gasShutOffValve: String!
    var emergencyExit: String!
    var furnishing: String!
    var cancellation_policy: String!
    
    //array of images
    var arrImges: [UIImage]!
    
}
