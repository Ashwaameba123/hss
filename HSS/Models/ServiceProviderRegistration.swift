//
//  ServiceProviderRegistration.swift
//  HSS
//
//  Created by Rajni on 04/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class ServiceProviderRegistration: NSObject {
    
    var firstName: String!
    var lastName: String!
    var email: String!
    var password: String!
    var gender: String!
    var DOB: String!
    var telephoneNo: String!
    var countryCode: String!
    var mobileNo: String!
    
    //secong view
    var nationality: String!
    var address: String!
    var serviceProviderLatitude: String!
    var serviceProviderLongitude: String!
    var postCode: String!
    var language: String!
    var website: String!
    var registerUnder: String!
    var companyId: String!
    var companyName: String!
    var companyAddress: String!
    var companyRegisterationNumber: String!
    
    //third view
    var serviceTypeId: String!
    var serviceTypeName: String!
    var driverLicenseExpiryDate: String!
    var conductNo: String!
    var issueNo: String!
    var birthCeriticateOrPassportNo: String!
    
    var registerVia: String!
    var applicationId: String!
    var deviceType: String!
    var socialMediaId: String!
    var userType: String!
    var userProfileImage: String!
    var photoIdentityImage: String!
    
    //after success
    var authenticateToken: String!
    var userId: String!
    var hostUserId: String!
    
    
}
