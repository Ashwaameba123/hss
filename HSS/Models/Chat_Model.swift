//
//  Chat_Model.swift
//  HSS
//
//  Created by Rajni on 21/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation

class Chat_Model: NSObject {
    
    var senderId: String!
    var reciverId: String!
    var receiverType: String!
    var senderType: String!
    var receiverName: String!
    var receiverImage: String!
    var senderAuthToken: String!
}