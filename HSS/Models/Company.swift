//
//  Company.swift
//  HSS
//
//  Created by Rajni on 06/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class Company: NSObject {
    
    var companyAddress: String!
    var companyName: String!
    var companyRegisterationNumber: String!
    var id: String!
    
}
