//
//  PropertyImage.swift
//  HSS
//
//  Created by Rajni on 08/02/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class PropertyImage: NSObject {
    var id: String!
    var propertyId: String!
    var propertyImage: String!
    var userId: String!
    
    var selectedImage: UIImage!
}
