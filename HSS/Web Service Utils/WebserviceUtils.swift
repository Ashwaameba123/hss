//
//  WebserviceUtils.swift
//  Tazligen
//
//  Created by Abdul Wahib on 3/4/16.
//  Copyright © 2016 Arc Coders. All rights reserved.
//

import Foundation
import AFNetworking
import SwiftyJSON
import Alamofire
class WebserviceUtils {
    
    
    
    
    
    class func callPostRequestWithEmptyParams(url: String, params: String, success: ((AnyObject?)) -> Void ,failure: ((NSError)->Void)) {
        let manager = AFHTTPSessionManager()
        
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.GET(url, parameters: "", success: { (session, response) -> Void in
            if let json = response as? NSMutableDictionary {
                print(json)
            }
            
            success(response)
            }, failure: { (session, error) -> Void in
                print("error")
                failure(error)
        })
        
    }
    
    class func callToPostUrl(url: String, params: [String: String], success: ((AnyObject?)) -> Void ,failure: ((NSError)->Void)) {
        let manager = AFHTTPSessionManager()
        
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        print(url)
        manager.POST(url, parameters: params, success: { (session, response) -> Void in
            if let json = response as? NSMutableDictionary {
               print(json)
                
            }
            success(response)
            }, failure: { (session, error) -> Void in
                print("error")
                failure(error)
        })
        
    }
    
    class func callPostRequestForUserRegistration(url: String,image: UIImage? , params: [String: String], imageParam: String, success: ((AnyObject?)) -> Void , failure: ((NSError)->Void))  {
        
        let compression = 0.5
    
        
        let imageData = UIImageJPEGRepresentation(image!, CGFloat(compression))
        
        
            let manager = AFHTTPRequestOperationManager()
            manager.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html", "application/json"]) as Set<NSObject>
            
            
            manager.POST(url, parameters: params, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
                //code
                if imageData != nil{
                formData.appendPartWithFileData(imageData!, name: imageParam , fileName: "photoIdentity.png", mimeType: "image/jpg")
                }
                },
          success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
                    print(responseObject)
                    success(responseObject)
                },
                         
                         
                failure: { (operation, error) in
                            print("Error: " + error.localizedDescription)
                    failure(error)
            })
        }
    
    
    class func callPostRequestForUserRegistrationToEditProfile(url: String,image: UIImage? , params: [String: String], isUpdatePhoto: String , success: ((AnyObject?)) -> Void , failure: ((NSError)->Void))  {
        
        let compression = 0.5
        
        
        let imageData = UIImageJPEGRepresentation(image!, CGFloat(compression))
        
        
        let manager = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html", "application/json"]) as Set<NSObject>
        
        
        manager.POST(url, parameters: params, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
            //code
            
            if isUpdatePhoto == "true" {
            if imageData != nil{
                formData.appendPartWithFileData(imageData!, name: "userProfileImage" , fileName: "userProfileImage.png", mimeType: "image/jpg")
            }
            }
            },
                     success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
                        print(responseObject)
                        success(responseObject)
            },
                     
                     
                     failure: { (operation, error) in
                        print("Error: " + error.localizedDescription)
                        failure(error)
        })
    }
    
    
    
    
    
    
    class func callToAddProperty(url: String,arrImages: [UIImage] , params: [String: String], success: ((AnyObject?)) -> Void , failure: ((NSError)->Void))  {
        
        let compression = 0.5
        let manager = AFHTTPRequestOperationManager()
        
        manager.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html", "application/json"]) as Set<NSObject>
        
        
        manager.POST(url, parameters: params, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
            //code
            for image in arrImages {
            let imageData = UIImageJPEGRepresentation(image, CGFloat(compression))
            if imageData != nil{
                formData.appendPartWithFileData(imageData!, name: "propertyImages", fileName: "propertyImages.png", mimeType: "image/jpg")
                }}
            },
                     success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
                        print(responseObject)
                        success(responseObject)
            },
                     
                     
                     failure: { (operation, error) in
                        print("Error: " + error.localizedDescription)
                        failure(error)
        })
    }
    
    

    
        
        
   class func getJSON(params:NSDictionary, urlToRequest: String, resultJSON:(JSON) -> Void) -> Void {
        var returnResult: JSON = JSON.null
        var paramString : String = ""
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(params, options:[])
            let dataString = NSString(data: data, encoding: NSUTF8StringEncoding)!
            print(dataString)
            paramString = dataString as String
            print(dataString)
            
        } catch {
            print("JSON serialization failed:  \(error)")
        }
        
        
        
        
        Alamofire.request(.POST, urlToRequest, parameters: paramString as AnyObject as? [String : AnyObject] , encoding: .JSON)
            .response { request, response, data, error in
                //                let dataString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                //                print(dataString)
                print(response)
                if error != nil {
                    let swiftyJsonVar = JSON(data!)
                    
                    resultJSON(swiftyJsonVar)
                }
                else{
                    resultJSON(nil)
                }
                
                
                
        }
    }
    
    
//    class func callGetRequest(url: String, params: [String: String], success: ((AnyObject?)) -> Void ,failure: ((NSError)->Void)) {
//        let manager = AFHTTPSessionManager()
//        
//        manager.requestSerializer = AFHTTPRequestSerializer()
//        manager.responseSerializer = AFJSONResponseSerializer()
//        
//        manager.GET(url, parameters: "", success: { (session, response) -> Void in
//            if let json = response as? NSMutableDictionary {
//                print(json)
//            }
//            
//            success(response)
//            }, failure: { (session, error) -> Void in
//                print("error")
//                failure(error)
//        })
//        
//    }
    
    class func callPostRequest(url: String, params: [String: String], success: ((AnyObject?)) -> Void ,failure: ((NSError)->Void)) {
        let manager = AFHTTPSessionManager()
        
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.POST(url, parameters: params, success: { (session, response) -> Void in
            if let json = response as? NSMutableDictionary {
                print(json)
            }
            
            success(response)
            }, failure: { (session, error) -> Void in
                print("error")
                failure(error)
        })
        
    }
    
    
   class func callToUpdateDeviceToken(){
        var token:String = ""
        var userId: String = ""
        var userType: String = ""
        
        
        //when user or host
        if PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.user.rawValue || PrefrencesUtils.getStringFromPrefs(PreferenceConstant.USER_TYPE) == Enumerations.USER_TYPE.host.rawValue {
            let arr = DatabaseBLFunctions.fetchUserRegistrationData()
            if arr.count > 0 {
                if let token_param = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.DEVICE_TOKEN) {
                    token = token_param
                }
                else {
                    token = "123456"
                }
                
                userType = arr[0].userType!
                userId = arr[0].userId!
            }
        }
        else {
            let arr = DatabaseBLFunctions.fetchServiceProviderUserRegistrationData()
            if arr.count > 0 {
                if let token_param = PrefrencesUtils.getStringFromPrefs(PreferenceConstant.DEVICE_TOKEN) {
                    token = token_param
                }
                else {
                    token = "123456"
                }
                
                userType = arr[0].userType!
                userId = arr[0].userId!
            }
        }
        
        
        let params = [
            URLParams.USER_ID: userId,
            URLParams.USER_TYPE: userType,
            URLParams.APPLICATION_ID: token
        ]
        
        print(params)
        
        WebserviceUtils.callPostRequest(URLConstants.UPDATE_DEVICE_TOKEN, params: params, success: { (response) in
            if let json = response as? NSDictionary {
                print(json)
                
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
    
    
    
    
    
//        //let imageData = UIImageJPEGRepresentation(self.myImage.image, 0.2)
//        let manager = AFHTTPRequestOperationManager()
//        //manager.requestSerializer.setValue(self.accessToken, forHTTPHeaderField: "Authorization")
//        manager.requestSerializer.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
//        manager.responseSerializer = AFJSONResponseSerializer()
//      let operation = manager.POST(url, parameters: params, constructingBodyWithBlock: { formData in
//            formData.appendPartWithFileData(imageData!,name: "image",fileName: "image.jpg", mimeType: "image/jpeg")
//        }, success: { (operation, response) in
//                    print(response)
//        }, failure: { (operation, error) in
//            // TODO: - Handle error case
//                print("********************************\(error)**********************")
//            })
////        operation?.start()
        
        
    }
    


//    Alamofire.request(.POST, urlToRequest, parameters: paramString as AnyObject as? [String : AnyObject] , encoding: .JSON)
//    .responseJSON { response in
//    switch response.result {
//    case .Success:
//    resultJSON(response)
//    case .Failure(let error):
//    print(error)
//    resultJSON(returnResult)
//    }
//    }
    

   

