//
//  AccommodationType+CoreDataProperties.swift
//  
//
//  Created by Rajni on 13/01/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension AccommodationType {

    @NSManaged var id: String?
    @NSManaged var name: String?

}
