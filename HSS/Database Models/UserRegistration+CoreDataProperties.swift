//
//  UserRegistration+CoreDataProperties.swift
//  
//
//  Created by Rajni on 10/02/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserRegistration {

    @NSManaged var applicationId: String?
    @NSManaged var authenticateToken: String?
    @NSManaged var contactName: String?
    @NSManaged var countryCode: String?
    @NSManaged var dateOfBirth: String?
    @NSManaged var deviceType: String?
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var gender: String?
    @NSManaged var language: String?
    @NSManaged var lastName: String?
    @NSManaged var locationLatitude: String?
    @NSManaged var locationLongitude: String?
    @NSManaged var locationName: String?
    @NSManaged var mobileNumber: String?
    @NSManaged var nationality: String?
    @NSManaged var password: String?
    @NSManaged var photoIdentity: String?
    @NSManaged var registerVia: String?
    @NSManaged var relationshipStatus: String?
    @NSManaged var socialMediaId: String?
    @NSManaged var userId: String?
    @NSManaged var userType: String?
    @NSManaged var userProfileImage: String?

}
