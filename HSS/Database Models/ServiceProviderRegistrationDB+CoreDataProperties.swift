//
//  ServiceProviderRegistrationDB+CoreDataProperties.swift
//  
//
//  Created by Rajni on 07/02/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ServiceProviderRegistrationDB {

    @NSManaged var userType: String?
    @NSManaged var userId: String?
    @NSManaged var registerVia: String?
    @NSManaged var password: String?
    @NSManaged var nationality: String?
    @NSManaged var mobileNumber: String?
    @NSManaged var serviceProviderLongitude: String?
    @NSManaged var serviceProviderLatitude: String?
    @NSManaged var lastName: String?
    @NSManaged var language: String?
    @NSManaged var gender: String?
    @NSManaged var firstName: String?
    @NSManaged var email: String?
    @NSManaged var deviceType: String?
    @NSManaged var dateOfBirth: String?
    @NSManaged var countryCode: String?
    @NSManaged var contactName: String?
    @NSManaged var authenticateToken: String?
    @NSManaged var applicationId: String?
    @NSManaged var telephoneNo: String?
    @NSManaged var address: String?
    @NSManaged var postCode: String?
    @NSManaged var website: String?
    @NSManaged var registerUnder: String?
    @NSManaged var companyId: String?
    @NSManaged var companyName: String?
    @NSManaged var companyAddress: String?
    @NSManaged var companyRegistrationNo: String?
    @NSManaged var serviceTypeID: String?
    @NSManaged var serviceTypeName: String?
    @NSManaged var driverLicenceExpiryDate: String?
    @NSManaged var conductNo: String?
    @NSManaged var photoIdentityImage: String?
    @NSManaged var socialMediaId: String?
    @NSManaged var issueNo: String?
    @NSManaged var birthCertificateORPassportNo: String?
    @NSManaged var userProfileImage: String?
    @NSManaged var hostUserId: String?

}
