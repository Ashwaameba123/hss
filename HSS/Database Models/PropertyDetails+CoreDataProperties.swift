//
//  PropertyDetails+CoreDataProperties.swift
//  
//
//  Created by Rajni on 18/01/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PropertyDetails {

    @NSManaged var accommodation: String?
    @NSManaged var additionalMenties: String?
    @NSManaged var apartment: String?
    @NSManaged var bathRoom: String?
    @NSManaged var bedRoom: String?
    @NSManaged var beds: String?
    @NSManaged var city: String?
    @NSManaged var commonAmenties: String?
    @NSManaged var country: String?
    @NSManaged var currency: String?
    @NSManaged var emergencyExit: String?
    @NSManaged var fireAlarm: String?
    @NSManaged var fireExtinguisher: String?
    @NSManaged var gasShutoffValve: String?
    @NSManaged var homeSafety: String?
    @NSManaged var id: String?
    @NSManaged var images: String?
    @NSManaged var profileName: String?
    @NSManaged var propertyLatitude: String?
    @NSManaged var propertyLongitude: String?
    @NSManaged var propertyType: String?
    @NSManaged var roomType: String?
    @NSManaged var setPrice: String?
    @NSManaged var specialFeature: String?
    @NSManaged var state: String?
    @NSManaged var streetAddress: String?
    @NSManaged var summary: String?
    @NSManaged var typeOfBooking: String?
    @NSManaged var userId: String?
    @NSManaged var zipCode: String?

}
