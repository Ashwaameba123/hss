//
//  ContactCollectionViewCell.swift
//  HSS
//
//  Created by Rajni on 25/01/17.
//  Copyright © 2017 Rajni. All rights reserved.
//

import Foundation
import UIKit

class ContactCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgVwContact: UIImageView!
    
    @IBOutlet weak var lblContactName: UILabel!
}
